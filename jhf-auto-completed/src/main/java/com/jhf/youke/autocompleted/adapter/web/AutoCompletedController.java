package com.jhf.youke.autocompleted.adapter.web;

import com.jhf.youke.autocompleted.app.executor.AutoCompletedAppService;
import com.jhf.youke.autocompleted.domain.model.dto.AutoCompletedDto;
import com.jhf.youke.autocompleted.domain.model.vo.AutoCompletedVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/autoCompleted")
public class AutoCompletedController {

    @Resource
    private AutoCompletedAppService autoCompletedAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody AutoCompletedDto dto) {
        return Response.ok(autoCompletedAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody AutoCompletedDto dto) {
        return Response.ok(autoCompletedAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody AutoCompletedDto dto) {
        return Response.ok(autoCompletedAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<AutoCompletedVo>> findById(Long id) {
        return Response.ok(autoCompletedAppService.findById(id));
    }

    @PostMapping("/autoCompleted")
    @ApiOperation("自动补全")
    public Response<List<Map<String, String>>> autoCompleted(@RequestBody AutoCompletedDto dto) throws Exception{

        return  Response.ok(autoCompletedAppService.autoCompleted(dto));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(autoCompletedAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<AutoCompletedVo>> list(@RequestBody AutoCompletedDto dto, @RequestHeader("token") String token) {

        return Response.ok(autoCompletedAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<AutoCompletedVo>> selectPage(@RequestBody  AutoCompletedDto dto, @RequestHeader("token") String token) {
        return Response.ok(autoCompletedAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(autoCompletedAppService.getPreData());
    }
    

}

