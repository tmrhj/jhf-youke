package com.jhf.youke.autocompleted.domain.converter;

import com.jhf.youke.autocompleted.domain.model.Do.AutoCompletedDo;
import com.jhf.youke.autocompleted.domain.model.dto.AutoCompletedDto;
import com.jhf.youke.autocompleted.domain.model.po.AutoCompletedPo;
import com.jhf.youke.autocompleted.domain.model.vo.AutoCompletedVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 自动完成转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface AutoCompletedConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param autoCompletedDto 自动完成dto
     * @return {@link AutoCompletedDo}
     */
    AutoCompletedDo dto2Do(AutoCompletedDto autoCompletedDto);

    /**
     * 洗阿宝
     *
     * @param autoCompletedDo 自动完成做
     * @return {@link AutoCompletedPo}
     */
    AutoCompletedPo do2Po(AutoCompletedDo autoCompletedDo);

    /**
     * 洗签证官
     *
     * @param autoCompletedDo 自动完成做
     * @return {@link AutoCompletedVo}
     */
    AutoCompletedVo do2Vo(AutoCompletedDo autoCompletedDo);


    /**
     * 洗订单列表
     *
     * @param autoCompletedDoList 自动完成任务清单
     * @return {@link List}<{@link AutoCompletedPo}>
     */
    List<AutoCompletedPo> do2PoList(List<AutoCompletedDo> autoCompletedDoList);

    /**
     * 警察乙做
     *
     * @param autoCompletedPo 自动完成订单
     * @return {@link AutoCompletedDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    AutoCompletedDo po2Do(AutoCompletedPo autoCompletedPo);

    /**
     * 警察乙签证官
     *
     * @param autoCompletedPo 自动完成订单
     * @return {@link AutoCompletedVo}
     */
    AutoCompletedVo po2Vo(AutoCompletedPo autoCompletedPo);

    /**
     * 警察乙做列表
     *
     * @param autoCompletedPoList 自动完成订单列表
     * @return {@link List}<{@link AutoCompletedDo}>
     */
    List<AutoCompletedDo> po2DoList(List<AutoCompletedPo> autoCompletedPoList);

    /**
     * 警察乙vo列表
     *
     * @param autoCompletedPoList 自动完成订单列表
     * @return {@link List}<{@link AutoCompletedVo}>
     */
    List<AutoCompletedVo> po2VoList(List<AutoCompletedPo> autoCompletedPoList);


}

