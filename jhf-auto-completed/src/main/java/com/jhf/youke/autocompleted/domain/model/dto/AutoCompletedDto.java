package com.jhf.youke.autocompleted.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AutoCompletedDto extends BaseDtoEntity {

    private static final long serialVersionUID = -10410699859670516L;


    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "sql")
    private String sql;

    @ApiModelProperty(value = "微服务名")
    private String serverName;

    private String key;



}


