package com.jhf.youke.autocompleted.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_auto_completed")
public class AutoCompletedPo extends BasePoEntity {

    private static final long serialVersionUID = -92600610236299713L;

    /**名称 **/
    private String name;

    /**sql **/
    private String sql;

    /**微服务名 **/
    private String serverName;



}


