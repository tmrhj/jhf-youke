package com.jhf.youke.autocompleted.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AutoCompletedVo implements Serializable{

    private static final long serialVersionUID = -32380379987676779L;


    @ApiModelProperty(value = "id")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "sql")
    private String sql;

    @ApiModelProperty(value = "微服务名")
    private String serverName;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private String remark;

    private String delFlag;

}


