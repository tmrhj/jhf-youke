package com.jhf.youke.autocompleted.domain.service;

import com.jhf.youke.autocompleted.domain.converter.AutoCompletedConverter;
import com.jhf.youke.autocompleted.domain.model.Do.AutoCompletedDo;
import com.jhf.youke.autocompleted.domain.gateway.AutoCompletedRepository;
import com.jhf.youke.autocompleted.domain.model.po.AutoCompletedPo;
import com.jhf.youke.autocompleted.domain.model.vo.AutoCompletedVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service
public class AutoCompletedService extends AbstractDomainService<AutoCompletedRepository, AutoCompletedDo, AutoCompletedVo> {


    @Resource
    AutoCompletedConverter autoCompletedConverter;

    @Override
    public boolean update(AutoCompletedDo entity) {
        AutoCompletedPo autoCompletedPo = autoCompletedConverter.do2Po(entity);
        autoCompletedPo.preUpdate();
        return repository.update(autoCompletedPo);
    }

    @Override
    public boolean updateBatch(List<AutoCompletedDo> doList) {
        List<AutoCompletedPo> poList = autoCompletedConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(AutoCompletedDo entity) {
        AutoCompletedPo autoCompletedPo = autoCompletedConverter.do2Po(entity);
        return repository.delete(autoCompletedPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(AutoCompletedDo entity) {
        AutoCompletedPo autoCompletedPo = autoCompletedConverter.do2Po(entity);
        autoCompletedPo.preInsert();
        return repository.insert(autoCompletedPo);
    }

    @Override
    public boolean insertBatch(List<AutoCompletedDo> doList) {
        List<AutoCompletedPo> poList = autoCompletedConverter.do2PoList(doList);
        poList = AutoCompletedPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<AutoCompletedVo> findById(Long id) {
        Optional<AutoCompletedPo> autoCompletedPo =  repository.findById(id);
        AutoCompletedVo autoCompletedVo = autoCompletedConverter.po2Vo(autoCompletedPo.orElse(new AutoCompletedPo()));
        return Optional.ofNullable(autoCompletedVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<AutoCompletedVo> findAllMatching(AutoCompletedDo entity) {
        AutoCompletedPo autoCompletedPo = autoCompletedConverter.do2Po(entity);
        List<AutoCompletedPo>autoCompletedPoList =  repository.findAllMatching(autoCompletedPo);
        return autoCompletedConverter.po2VoList(autoCompletedPoList);
    }


    @Override
    public Pagination<AutoCompletedVo> selectPage(AutoCompletedDo entity){
        AutoCompletedPo autoCompletedPo = autoCompletedConverter.do2Po(entity);
        PageQuery<AutoCompletedPo> pageQuery = new PageQuery<>(autoCompletedPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<AutoCompletedPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                autoCompletedConverter.po2VoList(pagination.getList()));
    }


    public List<Map<String,String>> autoCompleted(Map<String, Object> map) {
        return repository.autoCompleted(map);
    }
}

