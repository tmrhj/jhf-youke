package com.jhf.youke;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * @author RHJ
 */
@SpringBootApplication
@EnableFeignClients
@ServletComponentScan
public class BaseApplication {

    private final Logger logger = LoggerFactory.getLogger(BaseApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BaseApplication.class, args);
    }


}
