package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.AreaAppService;
import com.jhf.youke.base.domain.model.dto.AreaDto;
import com.jhf.youke.base.domain.model.vo.AreaVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "地区表")
@RestController
@RequestMapping("/area")
public class AreaController {

    @Resource
    private AreaAppService areaAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody AreaDto dto) {
        return Response.ok(areaAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody AreaDto dto) {
        return Response.ok(areaAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody AreaDto dto) {
        return Response.ok(areaAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<AreaVo>> findById(Long id) {
        return Response.ok(areaAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(areaAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<AreaVo>> list(@RequestBody AreaDto dto) {

        return Response.ok(areaAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<AreaVo>> selectPage(@RequestBody  AreaDto dto) {
        return Response.ok(areaAppService.selectPage(dto));
    }

}

