package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.AuditAppService;
import com.jhf.youke.base.domain.model.dto.AuditDto;
import com.jhf.youke.base.domain.model.vo.AuditVo;
import com.jhf.youke.core.entity.DeleteDto;
import com.jhf.youke.core.entity.DetailsDto;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  makejava
 **/

@Api(tags = "")
@RestController
@RequestMapping("/audit")
public class AuditController {

    @Resource
    private AuditAppService auditAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody AuditDto dto) {
        return Response.ok(auditAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody AuditDto dto) {
        return Response.ok(auditAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody AuditDto dto) {
        return Response.ok(auditAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<AuditVo>> findById(Long id) {
        return Response.ok(auditAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(auditAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<AuditVo>> list(@RequestBody AuditDto dto, @RequestHeader("token") String token) {

        return Response.ok(auditAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<AuditVo>> selectPage(@RequestBody  AuditDto dto, @RequestHeader("token") String token) {
        return Response.ok(auditAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(auditAppService.getPreData());
    }

    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody DeleteDto dto) {
        return  Response.ok(auditAppService.removeBatch(dto.getIdList()));
    }

    @PostMapping("/auditUser")
    @ApiOperation("团长注册审核")
    public Response<Boolean> auditUserAgree(@RequestBody AuditDto dto) {
        return Response.ok(auditAppService.auditUser(dto.getId(), dto.getStatus()));
    }

}

