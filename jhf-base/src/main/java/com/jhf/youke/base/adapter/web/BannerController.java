package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.BannerAppService;
import com.jhf.youke.base.domain.model.dto.BannerDto;
import com.jhf.youke.base.domain.model.vo.BannerVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Api(tags = "轮播图")
@RestController
@RequestMapping("/banner")
public class BannerController {

    @Resource
    private BannerAppService bannerAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody BannerDto dto) {
        return Response.ok(bannerAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody BannerDto dto) {
        return Response.ok(bannerAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestHeader("token") String token, @RequestBody BannerDto dto) {
        return Response.ok(bannerAppService.insert(dto, token));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<BannerVo>> findById(Long id) {
        return Response.ok(bannerAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(bannerAppService.remove(id));
    }


    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody List<Long> idList) {
        return  Response.ok(bannerAppService.removeBatch(idList));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<BannerVo>> list(@RequestBody BannerDto dto) {
        return Response.ok(bannerAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<BannerVo>> selectPage(@RequestHeader("token") String token, @RequestBody  BannerDto dto) {
        return Response.ok(bannerAppService.selectPage(dto, token));
    }

}

