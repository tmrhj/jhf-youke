package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.ButtonAppService;
import com.jhf.youke.base.domain.model.dto.ButtonDto;
import com.jhf.youke.base.domain.model.vo.ButtonVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/button")
public class ButtonController {

    @Resource
    private ButtonAppService buttonAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ButtonDto dto) {
        return Response.ok(buttonAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ButtonDto dto) {
        return Response.ok(buttonAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ButtonDto dto) {
        return Response.ok(buttonAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ButtonVo>> findById(Long id) {
        return Response.ok(buttonAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(buttonAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ButtonVo>> list(@RequestBody ButtonDto dto, @RequestHeader("token") String token) {

        return Response.ok(buttonAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ButtonVo>> selectPage(@RequestBody  ButtonDto dto, @RequestHeader("token") String token) {
        return Response.ok(buttonAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(buttonAppService.getPreData());
    }


    @GetMapping("/getListByStatus")
    @ApiOperation("根据当前状态获取按钮")
    public Response<List<ButtonVo>> getListByStatus(Integer status){
        List<ButtonVo> list = null;
        try {
         list = buttonAppService.getListByStatus(status);

        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
        return Response.ok(list);
    }

}

