package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.CompanyAppService;
import com.jhf.youke.base.domain.model.dto.CompanyDto;
import com.jhf.youke.base.domain.model.vo.CompanyVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "单位表")
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Resource
    private CompanyAppService companyAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody CompanyDto dto) {
        return Response.ok(companyAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody CompanyDto dto) {
        return Response.ok(companyAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody CompanyDto dto) {
        return Response.ok(companyAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<CompanyVo>> findById(Long id) {
        return Response.ok(companyAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(companyAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<CompanyVo>> list(@RequestBody CompanyDto dto) {
        return Response.ok(companyAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<CompanyVo>> selectPage(@RequestBody  CompanyDto dto) {
        return Response.ok(companyAppService.selectPage(dto));
    }

    @GetMapping("/companyTree")
    @ApiOperation("树形列表")
    public Response<List<CompanyVo>> companyTree(@RequestHeader("token") String token) {
        return Response.ok(companyAppService.companyTree());
    }

}

