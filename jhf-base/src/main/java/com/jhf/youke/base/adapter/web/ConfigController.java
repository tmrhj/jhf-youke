package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.ConfigAppService;
import com.jhf.youke.base.domain.model.dto.ConfigDto;
import com.jhf.youke.base.domain.model.vo.ConfigVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Api(tags = "系统参数表")
@RestController
@RequestMapping("/config")
public class ConfigController {

    @Resource
    private ConfigAppService configAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ConfigDto dto) {
        return Response.ok(configAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ConfigDto dto) {
        return Response.ok(configAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ConfigDto dto) {
        return Response.ok(configAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ConfigVo>> findById(Long id) {
        return Response.ok(configAppService.findById(id));
    }

    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(configAppService.remove(id));
    }

    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody List<Long> idList) {
        return  Response.ok(configAppService.removeBatch(idList));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ConfigVo>> list(@RequestBody ConfigDto dto) {
        return Response.ok(configAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ConfigVo>> selectPage(@RequestBody  ConfigDto dto) {
        return Response.ok(configAppService.selectPage(dto));
    }

}

