package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.DeptAppService;
import com.jhf.youke.base.domain.model.dto.DeptDto;
import com.jhf.youke.base.domain.model.vo.DeptVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "班级表")
@RestController
@RequestMapping("/dept")
public class DeptController {

    @Resource
    private DeptAppService deptAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody DeptDto dto) {
        return Response.ok(deptAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody DeptDto dto) {
        return Response.ok(deptAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody DeptDto dto) {
        return Response.ok(deptAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<DeptVo>> findById(Long id) {
        return Response.ok(deptAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(deptAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<DeptVo>> list(@RequestBody DeptDto dto) {

        return Response.ok(deptAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<DeptVo>> selectPage(@RequestBody  DeptDto dto) {
        return Response.ok(deptAppService.selectPage(dto));
    }

}

