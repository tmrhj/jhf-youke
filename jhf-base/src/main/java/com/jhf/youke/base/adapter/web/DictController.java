package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.DictAppService;
import com.jhf.youke.base.domain.model.dto.DictDto;
import com.jhf.youke.base.domain.model.vo.DictVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "数据字典表")
@RestController
@RequestMapping("/dict")
public class DictController {

    @Resource
    private DictAppService dictAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody DictDto dto) {
        return Response.ok(dictAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody DictDto dto) {
        return Response.ok(dictAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody DictDto dto) {
        return Response.ok(dictAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<DictVo>> findById(Long id) {
        return Response.ok(dictAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(dictAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<DictVo>> list(@RequestBody DictDto dto, @RequestHeader("token") String token) {

        return Response.ok(dictAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<DictVo>> selectPage(@RequestBody  DictDto dto, @RequestHeader("token") String token) {
        return Response.ok(dictAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(dictAppService.getPreData());
    }
    

}

