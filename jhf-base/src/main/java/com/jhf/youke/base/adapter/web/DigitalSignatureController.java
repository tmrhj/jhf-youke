package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.DigitalSignatureAppService;
import com.jhf.youke.base.domain.model.dto.DigitalSignatureDto;
import com.jhf.youke.base.domain.model.vo.DigitalSignatureVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "公私匙接口")
@RestController
@RequestMapping("/digitalSignature")
public class DigitalSignatureController {

    @Resource
    private DigitalSignatureAppService digitalSignatureAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody DigitalSignatureDto dto) {
        return Response.ok(digitalSignatureAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody DigitalSignatureDto dto) {
        return Response.ok(digitalSignatureAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody DigitalSignatureDto dto) {
        return Response.ok(digitalSignatureAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<DigitalSignatureVo>> findById(Long id) {
        return Response.ok(digitalSignatureAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(digitalSignatureAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<DigitalSignatureVo>> list(@RequestBody DigitalSignatureDto dto) {

        return Response.ok(digitalSignatureAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<DigitalSignatureVo>> selectPage(@RequestBody  DigitalSignatureDto dto) {
        return Response.ok(digitalSignatureAppService.selectPage(dto));
    }

    @GetMapping("/getByCompany")
    @ApiOperation("根据单位查询公私匙")
    public Response<DigitalSignatureVo> getByCompany(Long companyId) throws Exception {
        Optional<DigitalSignatureVo> optional = digitalSignatureAppService.getByCompany(companyId);
        return Response.ok(optional.orElse(new DigitalSignatureVo()));
    }


}

