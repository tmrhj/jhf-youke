package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.LogAppService;
import com.jhf.youke.base.domain.model.dto.LogDto;
import com.jhf.youke.base.domain.model.vo.LogVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "登录日志")
@RestController
@RequestMapping("/log")
public class LogController {

    @Resource
    private LogAppService logAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody LogDto dto) {
        return Response.ok(logAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody LogDto dto) {
        return Response.ok(logAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody LogDto dto) {
        return Response.ok(logAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<LogVo>> findById(Long id) {
        return Response.ok(logAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(logAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<LogVo>> list(@RequestBody LogDto dto) {

        return Response.ok(logAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<LogVo>> selectPage(@RequestBody  LogDto dto) {
        return Response.ok(logAppService.selectPage(dto));
    }

}

