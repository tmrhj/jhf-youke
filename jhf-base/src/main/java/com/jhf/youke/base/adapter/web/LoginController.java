package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.LoginAppService;
import com.jhf.youke.base.domain.exception.UserException;
import com.jhf.youke.base.domain.model.dto.LoginDto;
import com.jhf.youke.base.domain.model.dto.VideoLoginDto;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 系统用户(User)登录控制层
 * @author RHJ
 */
@Api(tags = "用户登录相关接口")
@RestController
@RequestMapping("/login")
public class LoginController {

    @Resource
    private LoginAppService loginAppService;


    @PostMapping("/loginByPwd")
    @ApiOperation("根据用户名与密码登录")
    public Response<Map<String,Object>> loginByPwd(@RequestBody LoginDto dto) throws Exception {
        try {
            return Response.ok(loginAppService.loginByPwd(dto));
        }catch (UserException e){
            return Response.fail(e.getMessage());
        }
    }

    @PostMapping("/logout")
    @ApiOperation("退出登录")
    public Response<Boolean> logout(@RequestHeader("token") String token) {
        return Response.ok(loginAppService.logout(token));
    }

    @PostMapping("/loginByMobile")
    @ApiOperation("移动端登录【团长，用户】")
    public Response<Map<String,Object>> loginByMobile(@RequestBody LoginDto dto) {
        try {
            return Response.ok(loginAppService.loginByMobile(dto));
        }catch (UserException e){
            return Response.fail(e.getMessage());
        }
    }


    @ApiOperation("视频小程序登录")
    @PostMapping("/videoLogin")
    public Response<Map<String,Object>> videoLogin(@RequestBody VideoLoginDto dto) {
        return Response.ok(loginAppService.videoLogin(dto));
    }

}

