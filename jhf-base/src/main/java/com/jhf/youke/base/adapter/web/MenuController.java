package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.MenuAppService;
import com.jhf.youke.base.domain.model.dto.MenuDto;
import com.jhf.youke.base.domain.model.vo.MenuVo;
import com.jhf.youke.base.domain.model.vo.RouterVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "菜单管理")
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Resource
    private MenuAppService menuAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody MenuDto dto) {
        return Response.ok(menuAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody MenuDto dto) {
        return Response.ok(menuAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody MenuDto dto) {
        return Response.ok(menuAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<MenuVo>> findById(Long id) {
        return Response.ok(menuAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(menuAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<MenuVo>> list(@RequestBody MenuDto dto) {
        return Response.ok(menuAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<MenuVo>> selectPage(@RequestBody  MenuDto dto) {
        return Response.ok(menuAppService.selectPage(dto));
    }

    @GetMapping("/getRouters")
    @ApiOperation("管理员菜单权限")
    public Response<List<RouterVo>> getRouters(@RequestHeader("token") String token) throws Exception{
        User user = CacheUtils.getUser(token);
        return Response.ok(menuAppService.getRouters(user));
    }

}

