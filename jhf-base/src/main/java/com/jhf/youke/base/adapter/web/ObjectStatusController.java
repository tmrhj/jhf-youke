package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.ObjectStatusAppService;
import com.jhf.youke.base.domain.model.dto.ObjectStatusDto;
import com.jhf.youke.base.domain.model.vo.ObjectStatusVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "业务状态表")
@RestController
@RequestMapping("/objectStatus")
public class ObjectStatusController {

    @Resource
    private ObjectStatusAppService objectStatusAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ObjectStatusDto dto) {
        return Response.ok(objectStatusAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ObjectStatusDto dto) {
        return Response.ok(objectStatusAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ObjectStatusDto dto) {
        return Response.ok(objectStatusAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ObjectStatusVo>> findById(Long id) {
        return Response.ok(objectStatusAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(objectStatusAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ObjectStatusVo>> list(@RequestBody ObjectStatusDto dto, @RequestHeader("token") String token) {

        return Response.ok(objectStatusAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ObjectStatusVo>> selectPage(@RequestBody  ObjectStatusDto dto, @RequestHeader("token") String token) {
        return Response.ok(objectStatusAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(objectStatusAppService.getPreData());
    }
    

}

