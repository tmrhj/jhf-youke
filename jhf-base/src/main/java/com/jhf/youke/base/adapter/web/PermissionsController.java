package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.PermissionsAppService;
import com.jhf.youke.base.domain.model.dto.PermissionsDto;
import com.jhf.youke.base.domain.model.vo.PermissionsVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "权限标识")
@RestController
@RequestMapping("/permissions")
public class PermissionsController {

    @Resource
    private PermissionsAppService permissionsAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody PermissionsDto dto) {
        return Response.ok(permissionsAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody PermissionsDto dto) {
        return Response.ok(permissionsAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody PermissionsDto dto) {
        return Response.ok(permissionsAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<PermissionsVo>> findById(Long id) {
        return Response.ok(permissionsAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(permissionsAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<PermissionsVo>> list(@RequestBody PermissionsDto dto) {
        return Response.ok(permissionsAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<PermissionsVo>> selectPage(@RequestBody  PermissionsDto dto) {
        return Response.ok(permissionsAppService.selectPage(dto));
    }

    @PostMapping("/listByMenuId")
    @ApiOperation("根据menuId查询")
    public Response<List<PermissionsVo>> listByMenuId(@RequestBody PermissionsDto dto) {
        return Response.ok(permissionsAppService.findAllMatching(dto));
    }
}

