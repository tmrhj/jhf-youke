package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.PermissionsSetAppService;
import com.jhf.youke.base.domain.model.dto.PermissionsSetDto;
import com.jhf.youke.base.domain.model.dto.PermissionsSetMenuDto;
import com.jhf.youke.base.domain.model.vo.PermissionsSetVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import com.jhf.youke.core.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "菜单角色关联表")
@RestController
@RequestMapping("/permissionsSet")
public class PermissionsSetController {

    @Resource
    private PermissionsSetAppService permissionsSetAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<PermissionsSetVo>> findById(Long id) {
        return Response.ok(permissionsSetAppService.findById(id));
    }

    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(permissionsSetAppService.remove(id));
    }

    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody List<Long> idList) {
        return  Response.ok(permissionsSetAppService.removeBatch(idList));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<PermissionsSetVo>> list(@RequestBody PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<PermissionsSetVo>> selectPage(@RequestBody  PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.selectPage(dto));
    }

    @PostMapping("/getWhiteList")
    @ApiOperation("权限白名单")
    public Response<Pagination<PermissionsSetVo>> getWhiteList(@RequestBody  PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.getWhiteList(dto));
    }

    @PostMapping("/getAllListToMap")
    @ApiOperation("网关操作查询方法")
    public Response<Map<String, Object>> getAllListToMap(@RequestBody PermissionsSetDto dto) {
        return Response.ok(permissionsSetAppService.getAllListToMap(dto));
    }

    @PostMapping("/getListByMenuId")
    @ApiOperation("根据菜单ID查询权限")
    public Response<List<PermissionsSetVo>> getListByMenuId(@RequestBody Map<String,Object> map) {
        Long menuId = StringUtils.toLong(map.get("menuId"));
        return Response.ok(permissionsSetAppService.getListByMenuId(menuId));
    }

    @PostMapping("/updateMenuSet")
    @ApiOperation("更新菜单权限")
    public Response<Boolean> updateMenuSet(@RequestBody PermissionsSetMenuDto dto) {
        permissionsSetAppService.updateMenuSet(dto);
        return Response.ok(true);
    }

}

