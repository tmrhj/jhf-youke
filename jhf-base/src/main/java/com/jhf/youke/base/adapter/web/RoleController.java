package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.RoleAppService;
import com.jhf.youke.base.domain.model.dto.RoleDto;
import com.jhf.youke.base.domain.model.vo.RoleVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Api(tags = "角色表")
@RestController
@RequestMapping("/role")
public class RoleController {

    @Resource
    private RoleAppService roleAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody RoleDto dto) {
        return Response.ok(roleAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody RoleDto dto) {
        return Response.ok(roleAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody RoleDto dto) {
        return Response.ok(roleAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<RoleVo>> findById(Long id) {
        return Response.ok(roleAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(roleAppService.remove(id));
    }

    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody List<Long> idList) {
        return  Response.ok(roleAppService.removeBatch(idList));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<RoleVo>> list(@RequestBody RoleDto dto) {
        return Response.ok(roleAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<RoleVo>> selectPage(@RequestBody  RoleDto dto) {
        return Response.ok(roleAppService.selectPage(dto));
    }

}

