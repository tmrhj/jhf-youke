package com.jhf.youke.base.adapter.web;

import com.jhf.youke.base.app.executor.UnitsAppService;
import com.jhf.youke.base.domain.model.dto.UnitsDto;
import com.jhf.youke.base.domain.model.vo.UnitsVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 * **/
@Api(tags = "")
@RestController
@RequestMapping("/units")
public class UnitsController {

    @Resource
    private UnitsAppService unitsAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody UnitsDto dto) {
        return Response.ok(unitsAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody UnitsDto dto) {
        return Response.ok(unitsAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody UnitsDto dto) {
        return Response.ok(unitsAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<UnitsVo>> findById(Long id) {
        return Response.ok(unitsAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(unitsAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<UnitsVo>> list(@RequestBody UnitsDto dto) {

        return Response.ok(unitsAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<UnitsVo>> selectPage(@RequestBody  UnitsDto dto) {
        return Response.ok(unitsAppService.selectPage(dto));
    }

}

