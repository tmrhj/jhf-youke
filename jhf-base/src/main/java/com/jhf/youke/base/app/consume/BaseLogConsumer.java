package com.jhf.youke.base.app.consume;

import cn.hutool.json.JSONUtil;
import com.jhf.youke.base.app.executor.LogAppService;
import com.jhf.youke.base.domain.model.dto.LogDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Map;
import java.util.function.Consumer;

/**
 * description:
 * date: 2022/9/23 14:12
 * @author: cyx
 */
@Slf4j
@Service
public class BaseLogConsumer {

    @Resource
    private LogAppService logAppService;

    @Bean
    public Consumer<Message<String>> baseLog() {
        return message -> {
            LogDto dto = JSONUtil.toBean(message.getPayload(), LogDto.class);
            logAppService.addUpdate(dto);
        };
    }

    @Bean
    public Consumer<Message<Map<String, Object>>> demo() {
        System.out.println("初始化消费者");
        return message -> {
            MessageHeaders headers = message.getHeaders();
            // 可以获取tag等信息
            System.out.println(message.getPayload());
        };
    }

}
