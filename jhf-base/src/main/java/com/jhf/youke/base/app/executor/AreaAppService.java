package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.AreaConverter;
import com.jhf.youke.base.domain.model.Do.AreaDo;
import com.jhf.youke.base.domain.model.dto.AreaDto;
import com.jhf.youke.base.domain.model.vo.AreaVo;
import com.jhf.youke.base.domain.service.AreaService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class AreaAppService {

    @Resource
    private AreaService areaService;

    @Resource
    private AreaConverter areaConverter;


    public boolean update(AreaDto dto) {
        AreaDo areaDo =  areaConverter.dto2Do(dto);
        return areaService.update(areaDo);
    }

    public boolean delete(AreaDto dto) {
        AreaDo areaDo =  areaConverter.dto2Do(dto);
        return areaService.delete(areaDo);
    }


    public boolean insert(AreaDto dto) {
        AreaDo areaDo =  areaConverter.dto2Do(dto);
        return areaService.insert(areaDo);
    }

    public Optional<AreaVo> findById(Long id) {
        return areaService.findById(id);
    }


    public boolean remove(Long id) {
        return areaService.remove(id);
    }


    public List<AreaVo> findAllMatching(AreaDto dto) {
        AreaDo areaDo =  areaConverter.dto2Do(dto);
        return areaService.findAllMatching(areaDo);
    }


    public Pagination<AreaVo> selectPage(AreaDto dto) {
        AreaDo areaDo =  areaConverter.dto2Do(dto);
        return areaService.selectPage(areaDo);
    }

}

