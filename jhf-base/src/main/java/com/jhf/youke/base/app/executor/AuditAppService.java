package com.jhf.youke.base.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.base.domain.converter.AuditConverter;
import com.jhf.youke.base.domain.model.Do.AuditDo;
import com.jhf.youke.base.domain.model.dto.AuditDto;
import com.jhf.youke.base.domain.model.vo.AuditVo;
import com.jhf.youke.base.domain.service.AuditService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  makejava
 **/

@Slf4j
@Service
public class AuditAppService extends BaseAppService {

    @Resource
    private AuditService auditService;

    @Resource
    private AuditConverter auditConverter;


    public boolean update(AuditDto dto) {
        AuditDo auditDo =  auditConverter.dto2Do(dto);
        return auditService.update(auditDo);
    }

    public boolean delete(AuditDto dto) {
        AuditDo auditDo =  auditConverter.dto2Do(dto);
        return auditService.delete(auditDo);
    }


    public boolean insert(AuditDto dto) {
        AuditDo auditDo =  auditConverter.dto2Do(dto);
        return auditService.insert(auditDo);
    }

    public Optional<AuditVo> findById(Long id) {
        return auditService.findById(id);
    }


    public boolean remove(Long id) {
        return auditService.remove(id);
    }


    public List<AuditVo> findAllMatching(AuditDto dto) {
        AuditDo auditDo =  auditConverter.dto2Do(dto);
        return auditService.findAllMatching(auditDo);
    }


    public Pagination<AuditVo> selectPage(AuditDto dto) {
        AuditDo auditDo =  auditConverter.dto2Do(dto);
        return auditService.selectPage(auditDo);
    }

    public Boolean removeBatch(List<Long> idList) {
        return auditService.removeBatch(idList);
    }
    
    public boolean auditUser(Long id, Integer status) {
        return auditService.auditUser(id, status);
    }

}

