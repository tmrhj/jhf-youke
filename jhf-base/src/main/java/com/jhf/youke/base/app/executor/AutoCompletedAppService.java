package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.AutoCompletedConverter;
import com.jhf.youke.base.domain.model.Do.AutoCompletedDo;
import com.jhf.youke.base.domain.model.dto.AutoCompletedDto;
import com.jhf.youke.base.domain.model.vo.AutoCompletedVo;
import com.jhf.youke.base.domain.service.AutoCompletedService;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class AutoCompletedAppService extends BaseAppService {

    @Resource
    private AutoCompletedService autoCompletedService;

    @Resource
    private AutoCompletedConverter autoCompletedConverter;


    public boolean update(AutoCompletedDto dto) {
        AutoCompletedDo autoCompletedDo =  autoCompletedConverter.dto2Do(dto);
        return autoCompletedService.update(autoCompletedDo);
    }

    public boolean delete(AutoCompletedDto dto) {
        AutoCompletedDo autoCompletedDo =  autoCompletedConverter.dto2Do(dto);
        return autoCompletedService.delete(autoCompletedDo);
    }


    public boolean insert(AutoCompletedDto dto) {
        AutoCompletedDo autoCompletedDo =  autoCompletedConverter.dto2Do(dto);
        return autoCompletedService.insert(autoCompletedDo);
    }

    public Optional<AutoCompletedVo> findById(Long id) {
        return autoCompletedService.findById(id);
    }


    public boolean remove(Long id) {
        return autoCompletedService.remove(id);
    }


    public List<AutoCompletedVo> findAllMatching(AutoCompletedDto dto) {
        AutoCompletedDo autoCompletedDo =  autoCompletedConverter.dto2Do(dto);
        return autoCompletedService.findAllMatching(autoCompletedDo);
    }


    public Pagination<AutoCompletedVo> selectPage(AutoCompletedDto dto) {
        AutoCompletedDo autoCompletedDo =  autoCompletedConverter.dto2Do(dto);
        return autoCompletedService.selectPage(autoCompletedDo);
    }
    

    /**
    * @Description:   自动补全
    *  1。
    *
    *
    * @Param: [dto]
    * @return: java.util.Map<java.lang.String,java.lang.Object>
    * @Author: RHJ
    * @Date: 2022/10/24
    */

    public Map<String,Object> autoCompleted(AutoCompletedDto dto) {
        Map<String,Object> map = new HashMap<>(8);
        AutoCompletedDo autoCompletedDo =  autoCompletedConverter.dto2Do(dto);
        Map<String,Object> res = autoCompletedService.autoCompleted(autoCompletedDo);
        map.putAll(res);
        return map;
    }
}

