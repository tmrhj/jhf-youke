package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.BannerConverter;
import com.jhf.youke.base.domain.model.Do.BannerDo;
import com.jhf.youke.base.domain.model.dto.BannerDto;
import com.jhf.youke.base.domain.model.vo.BannerVo;
import com.jhf.youke.base.domain.service.BannerService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author rhj
 * **/
@Slf4j
@Service
public class BannerAppService {

    @Resource
    private BannerService bannerService;

    @Resource
    private BannerConverter bannerConverter;


    public boolean update(BannerDto dto) {
        BannerDo bannerDo =  bannerConverter.dto2Do(dto);
        return bannerService.update(bannerDo);
    }

    public boolean delete(BannerDto dto) {
        BannerDo bannerDo =  bannerConverter.dto2Do(dto);
        return bannerService.delete(bannerDo);
    }


    public boolean insert(BannerDto dto, String token) {
        User user = CacheUtils.getUser(token);
        BannerDo bannerDo =  bannerConverter.dto2Do(dto);
        bannerDo.setCompanyId(user.getRootId());
        return bannerService.insert(bannerDo);
    }

    public Optional<BannerVo> findById(Long id) {
        return bannerService.findById(id);
    }


    public boolean remove(Long id) {
        return bannerService.remove(id);
    }

    public boolean removeBatch(List<Long> idList) {
        return bannerService.removeBatch(idList);
    }

    public List<BannerVo> findAllMatching(BannerDto dto) {
        BannerDo bannerDo =  bannerConverter.dto2Do(dto);
        return bannerService.findAllMatching(bannerDo);
    }

    public Pagination<BannerVo> selectPage(BannerDto dto, String token) {
        User user = CacheUtils.getUser(token);
        dto.setCompanyId(user.getRootId());
        BannerDo bannerDo =  bannerConverter.dto2Do(dto);
        return bannerService.selectPage(bannerDo);
    }

}

