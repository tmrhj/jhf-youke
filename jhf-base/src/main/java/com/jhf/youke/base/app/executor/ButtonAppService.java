package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.ButtonConverter;
import com.jhf.youke.base.domain.model.Do.ButtonDo;
import com.jhf.youke.base.domain.model.dto.ButtonDto;
import com.jhf.youke.base.domain.model.vo.ButtonVo;
import com.jhf.youke.base.domain.service.ButtonService;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Slf4j
@Service
public class ButtonAppService extends BaseAppService {

    @Resource
    private ButtonService buttonService;

    @Resource
    private ButtonConverter buttonConverter;


    public boolean update(ButtonDto dto) {
        ButtonDo buttonDo =  buttonConverter.dto2Do(dto);
        return buttonService.update(buttonDo);
    }

    public boolean delete(ButtonDto dto) {
        ButtonDo buttonDo =  buttonConverter.dto2Do(dto);
        return buttonService.delete(buttonDo);
    }


    public boolean insert(ButtonDto dto) {
        ButtonDo buttonDo =  buttonConverter.dto2Do(dto);
        return buttonService.insert(buttonDo);
    }

    public Optional<ButtonVo> findById(Long id) {
        return buttonService.findById(id);
    }


    public boolean remove(Long id) {
        return buttonService.remove(id);
    }


    public List<ButtonVo> findAllMatching(ButtonDto dto) {
        ButtonDo buttonDo =  buttonConverter.dto2Do(dto);
        return buttonService.findAllMatching(buttonDo);
    }


    public Pagination<ButtonVo> selectPage(ButtonDto dto) {
        ButtonDo buttonDo =  buttonConverter.dto2Do(dto);
        return buttonService.selectPage(buttonDo);
    }
    

    public List<ButtonVo> getListByStatus(Integer status)throws Exception {
        return buttonService.getListByStatus(status);
    }
}

