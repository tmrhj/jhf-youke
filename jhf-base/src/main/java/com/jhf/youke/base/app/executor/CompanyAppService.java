package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.CompanyConverter;
import com.jhf.youke.base.domain.model.Do.CompanyDo;
import com.jhf.youke.base.domain.model.dto.CompanyDto;
import com.jhf.youke.base.domain.model.vo.CompanyVo;
import com.jhf.youke.base.domain.service.CompanyService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Slf4j
@Service
public class CompanyAppService {

    @Resource
    private CompanyService companyService;

    @Resource
    private CompanyConverter companyConverter;


    public boolean update(CompanyDto dto) {
        CompanyDo companyDo =  companyConverter.dto2Do(dto);
        return companyService.update(companyDo);
    }

    public boolean delete(CompanyDto dto) {
        CompanyDo companyDo =  companyConverter.dto2Do(dto);
        return companyService.delete(companyDo);
    }


    public boolean insert(CompanyDto dto) {
        CompanyDo companyDo =  companyConverter.dto2Do(dto);
        return companyService.insert(companyDo);
    }

    public Optional<CompanyVo> findById(Long id) {
        return companyService.findById(id);
    }


    public boolean remove(Long id) {
        return companyService.remove(id);
    }


    public List<CompanyVo> findAllMatching(CompanyDto dto) {
        CompanyDo companyDo =  companyConverter.dto2Do(dto);
        return companyService.findAllMatching(companyDo);
    }


    public Pagination<CompanyVo> selectPage(CompanyDto dto) {
        CompanyDo companyDo =  companyConverter.dto2Do(dto);
        return companyService.selectPage(companyDo);
    }

    public List<CompanyVo> companyTree(){
        return companyService.companyTree();
    }

}

