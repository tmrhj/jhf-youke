package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.ConfigConverter;
import com.jhf.youke.base.domain.model.Do.ConfigDo;
import com.jhf.youke.base.domain.model.dto.ConfigDto;
import com.jhf.youke.base.domain.model.vo.ConfigVo;
import com.jhf.youke.base.domain.service.ConfigService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class ConfigAppService {

    @Resource
    private ConfigService configService;

    @Resource
    private ConfigConverter configConverter;


    public boolean update(ConfigDto dto) {
        ConfigDo configDo =  configConverter.dto2Do(dto);
        return configService.update(configDo);
    }

    public boolean delete(ConfigDto dto) {
        ConfigDo configDo =  configConverter.dto2Do(dto);
        return configService.delete(configDo);
    }


    public boolean insert(ConfigDto dto) {
        ConfigDo configDo =  configConverter.dto2Do(dto);
        return configService.insert(configDo);
    }

    public Optional<ConfigVo> findById(Long id) {
        return configService.findById(id);
    }


    public boolean remove(Long id) {
        return configService.remove(id);
    }

    public boolean removeBatch(List<Long> idList) {
        return configService.removeBatch(idList);
    }

    public List<ConfigVo> findAllMatching(ConfigDto dto) {
        ConfigDo configDo =  configConverter.dto2Do(dto);
        return configService.findAllMatching(configDo);
    }

    public Pagination<ConfigVo> selectPage(ConfigDto dto) {
        ConfigDo configDo =  configConverter.dto2Do(dto);
        return configService.selectPage(configDo);
    }

}

