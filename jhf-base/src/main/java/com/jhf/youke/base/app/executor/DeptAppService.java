package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.DeptConverter;
import com.jhf.youke.base.domain.model.Do.DeptDo;
import com.jhf.youke.base.domain.model.dto.DeptDto;
import com.jhf.youke.base.domain.model.vo.DeptVo;
import com.jhf.youke.base.domain.service.DeptService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class DeptAppService {

    @Resource
    private DeptService deptService;

    @Resource
    private DeptConverter deptConverter;


    public boolean update(DeptDto dto) {
        DeptDo deptDo =  deptConverter.dto2Do(dto);
        return deptService.update(deptDo);
    }

    public boolean delete(DeptDto dto) {
        DeptDo deptDo =  deptConverter.dto2Do(dto);
        return deptService.delete(deptDo);
    }


    public boolean insert(DeptDto dto) {
        DeptDo deptDo =  deptConverter.dto2Do(dto);
        return deptService.insert(deptDo);
    }

    public Optional<DeptVo> findById(Long id) {
        return deptService.findById(id);
    }


    public boolean remove(Long id) {
        return deptService.remove(id);
    }


    public List<DeptVo> findAllMatching(DeptDto dto) {
        DeptDo deptDo =  deptConverter.dto2Do(dto);
        return deptService.findAllMatching(deptDo);
    }


    public Pagination<DeptVo> selectPage(DeptDto dto) {
        DeptDo deptDo =  deptConverter.dto2Do(dto);
        return deptService.selectPage(deptDo);
    }

}

