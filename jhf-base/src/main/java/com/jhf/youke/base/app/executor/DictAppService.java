package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.DictConverter;
import com.jhf.youke.base.domain.model.Do.DictDo;
import com.jhf.youke.base.domain.model.dto.DictDto;
import com.jhf.youke.base.domain.model.vo.DictVo;
import com.jhf.youke.base.domain.service.DictService;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class DictAppService extends BaseAppService {

    @Resource
    private DictService dictService;

    @Resource
    private DictConverter dictConverter;


    public boolean update(DictDto dto) {
        DictDo dictDo =  dictConverter.dto2Do(dto);
        return dictService.update(dictDo);
    }

    public boolean delete(DictDto dto) {
        DictDo dictDo =  dictConverter.dto2Do(dto);
        return dictService.delete(dictDo);
    }


    public boolean insert(DictDto dto) {
        DictDo dictDo =  dictConverter.dto2Do(dto);
        return dictService.insert(dictDo);
    }

    public Optional<DictVo> findById(Long id) {
        return dictService.findById(id);
    }


    public boolean remove(Long id) {
        return dictService.remove(id);
    }


    public boolean removeBatch(List<Long> idList) {
        return dictService.removeBatch(idList);
    }

    public List<DictVo> findAllMatching(DictDto dto) {
        DictDo dictDo =  dictConverter.dto2Do(dto);
        return dictService.findAllMatching(dictDo);
    }


    public Pagination<DictVo> selectPage(DictDto dto) {
        DictDo dictDo =  dictConverter.dto2Do(dto);
        return dictService.selectPage(dictDo);
    }

}

