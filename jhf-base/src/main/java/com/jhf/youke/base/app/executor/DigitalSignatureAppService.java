package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.DigitalSignatureConverter;
import com.jhf.youke.base.domain.model.Do.DigitalSignatureDo;
import com.jhf.youke.base.domain.model.dto.DigitalSignatureDto;
import com.jhf.youke.base.domain.model.vo.DigitalSignatureVo;
import com.jhf.youke.base.domain.service.DigitalSignatureService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class DigitalSignatureAppService {

    @Resource
    private DigitalSignatureService digitalSignatureService;

    @Resource
    private DigitalSignatureConverter digitalSignatureConverter;


    public boolean update(DigitalSignatureDto dto) {
        DigitalSignatureDo digitalSignatureDo =  digitalSignatureConverter.dto2Do(dto);
        return digitalSignatureService.update(digitalSignatureDo);
    }

    public boolean delete(DigitalSignatureDto dto) {
        DigitalSignatureDo digitalSignatureDo =  digitalSignatureConverter.dto2Do(dto);
        return digitalSignatureService.delete(digitalSignatureDo);
    }


    public boolean insert(DigitalSignatureDto dto) {
        DigitalSignatureDo digitalSignatureDo =  digitalSignatureConverter.dto2Do(dto);

        return digitalSignatureService.insert(digitalSignatureDo);
    }

    public Optional<DigitalSignatureVo> findById(Long id) {
        return digitalSignatureService.findById(id);
    }


    public boolean remove(Long id) {
        return digitalSignatureService.remove(id);
    }


    public List<DigitalSignatureVo> findAllMatching(DigitalSignatureDto dto) {
        DigitalSignatureDo digitalSignatureDo =  digitalSignatureConverter.dto2Do(dto);
        return digitalSignatureService.findAllMatching(digitalSignatureDo);
    }


    public Pagination<DigitalSignatureVo> selectPage(DigitalSignatureDto dto) {
        DigitalSignatureDo digitalSignatureDo =  digitalSignatureConverter.dto2Do(dto);
        return digitalSignatureService.selectPage(digitalSignatureDo);
    }

    public Optional<DigitalSignatureVo> getByCompany(Long companyId)throws Exception{
        return digitalSignatureService.getByCompany(companyId);
    }

}

