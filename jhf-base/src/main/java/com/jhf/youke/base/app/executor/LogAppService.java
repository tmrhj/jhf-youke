package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.LogConverter;
import com.jhf.youke.base.domain.model.Do.LogDo;
import com.jhf.youke.base.domain.model.dto.LogDto;
import com.jhf.youke.base.domain.model.vo.LogVo;
import com.jhf.youke.base.domain.service.LogService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class LogAppService {

    private static final Integer INSERT = 1;
    private static final Integer UPDATE = 2;

    @Resource
    private LogService logService;

    @Resource
    private LogConverter logConverter;


    public boolean update(LogDto dto) {
        LogDo logDo =  logConverter.dto2Do(dto);
        return logService.update(logDo);
    }

    public boolean delete(LogDto dto) {
        LogDo logDo =  logConverter.dto2Do(dto);
        return logService.delete(logDo);
    }


    public boolean insert(LogDto dto) {
        LogDo logDo =  logConverter.dto2Do(dto);
        return logService.insert(logDo);
    }

    public Optional<LogVo> findById(Long id) {
        return logService.findById(id);
    }


    public boolean remove(Long id) {
        return logService.remove(id);
    }


    public List<LogVo> findAllMatching(LogDto dto) {
        LogDo logDo =  logConverter.dto2Do(dto);
        return logService.findAllMatching(logDo);
    }


    public Pagination<LogVo> selectPage(LogDto dto) {
        LogDo logDo =  logConverter.dto2Do(dto);
        return logService.selectPage(logDo);
    }

    public void addUpdate(LogDto dto) {
        if (INSERT.equals(dto.getUpdateType())) {
            insert(dto);
        } else if (UPDATE.equals(dto.getUpdateType())) {
            update(dto);
        }
    }


}

