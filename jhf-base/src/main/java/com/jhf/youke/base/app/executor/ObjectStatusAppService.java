package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.ObjectStatusConverter;
import com.jhf.youke.base.domain.model.Do.ObjectStatusDo;
import com.jhf.youke.base.domain.model.dto.ObjectStatusDto;
import com.jhf.youke.base.domain.model.vo.ObjectStatusVo;
import com.jhf.youke.base.domain.service.ObjectStatusService;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 * **/
@Slf4j
@Service
public class ObjectStatusAppService extends BaseAppService {

    @Resource
    private ObjectStatusService objectStatusService;

    @Resource
    private ObjectStatusConverter objectStatusConverter;


    public boolean update(ObjectStatusDto dto) {
        ObjectStatusDo objectStatusDo =  objectStatusConverter.dto2Do(dto);
        return objectStatusService.update(objectStatusDo);
    }

    public boolean delete(ObjectStatusDto dto) {
        ObjectStatusDo objectStatusDo =  objectStatusConverter.dto2Do(dto);
        return objectStatusService.delete(objectStatusDo);
    }


    public boolean insert(ObjectStatusDto dto) {
        ObjectStatusDo objectStatusDo =  objectStatusConverter.dto2Do(dto);
        return objectStatusService.insert(objectStatusDo);
    }

    public Optional<ObjectStatusVo> findById(Long id) {
        return objectStatusService.findById(id);
    }


    public boolean remove(Long id) {
        return objectStatusService.remove(id);
    }


    public List<ObjectStatusVo> findAllMatching(ObjectStatusDto dto) {
        ObjectStatusDo objectStatusDo =  objectStatusConverter.dto2Do(dto);
        return objectStatusService.findAllMatching(objectStatusDo);
    }


    public Pagination<ObjectStatusVo> selectPage(ObjectStatusDto dto) {
        ObjectStatusDo objectStatusDo =  objectStatusConverter.dto2Do(dto);
        return objectStatusService.selectPage(objectStatusDo);
    }
    


}

