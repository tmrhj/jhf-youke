package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.PermissionsConverter;
import com.jhf.youke.base.domain.model.Do.PermissionsDo;
import com.jhf.youke.base.domain.model.dto.PermissionsDto;
import com.jhf.youke.base.domain.model.vo.PermissionsVo;
import com.jhf.youke.base.domain.service.PermissionsService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class PermissionsAppService {

    @Resource
    private PermissionsService permissionsService;

    @Resource
    private PermissionsConverter permissionsConverter;


    public boolean update(PermissionsDto dto) {
        PermissionsDo permissionsDo =  permissionsConverter.dto2Do(dto);
        return permissionsService.update(permissionsDo);
    }

    public boolean delete(PermissionsDto dto) {
        PermissionsDo permissionsDo =  permissionsConverter.dto2Do(dto);
        return permissionsService.delete(permissionsDo);
    }


    public boolean insert(PermissionsDto dto) {
        PermissionsDo permissionsDo =  permissionsConverter.dto2Do(dto);
        return permissionsService.insert(permissionsDo);
    }

    public Optional<PermissionsVo> findById(Long id) {
        return permissionsService.findById(id);
    }


    public boolean remove(Long id) {
        return permissionsService.remove(id);
    }


    public List<PermissionsVo> findAllMatching(PermissionsDto dto) {
        PermissionsDo permissionsDo =  permissionsConverter.dto2Do(dto);
        return permissionsService.findAllMatching(permissionsDo);
    }


    public Pagination<PermissionsVo> selectPage(PermissionsDto dto) {
        PermissionsDo permissionsDo =  permissionsConverter.dto2Do(dto);
        return permissionsService.selectPage(permissionsDo);
    }

}

