package com.jhf.youke.base.app.executor;

import cn.hutool.core.thread.ThreadUtil;
import com.jhf.youke.base.app.feign.RocketMqFeign;
import com.jhf.youke.base.domain.converter.PermissionsSetConverter;
import com.jhf.youke.base.domain.model.Do.PermissionsSetDo;
import com.jhf.youke.base.domain.model.dto.PermissionsSetDto;
import com.jhf.youke.base.domain.model.dto.PermissionsSetMenuDto;
import com.jhf.youke.base.domain.model.vo.PermissionsSetVo;
import com.jhf.youke.base.domain.service.PermissionsSetService;
import com.jhf.youke.core.entity.Message;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.CacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author RHJ
 * **/
@Slf4j
@Service
public class PermissionsSetAppService {

    @Resource
    private PermissionsSetService permissionsSetService;

    @Resource
    private PermissionsSetConverter permissionsSetConverter;

    @Resource
    private RocketMqFeign rocketMqFeign;

    /**
     * 清除网关权限缓存
     */
    public void updateGatewayCache() {
        ThreadUtil.execAsync(()-> {
            CacheUtils.del("all_permissions_map");

            Message msg = new Message();
            msg.setGroupName("gatewayGroup");
            msg.setTopic("updatePermissionsCacheConsume");
            msg.setTag("all");
            msg.setMessages(new HashMap<>(1));
            rocketMqFeign.send(msg);
        });
    }

    public boolean update(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        updateGatewayCache();
        return permissionsSetService.update(permissionsSetDo);
    }

    public boolean delete(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        updateGatewayCache();
        return permissionsSetService.delete(permissionsSetDo);
    }


    public boolean insert(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        updateGatewayCache();
        return permissionsSetService.insert(permissionsSetDo);
    }

    public Optional<PermissionsSetVo> findById(Long id) {
        return permissionsSetService.findById(id);
    }


    public boolean remove(Long id) {
        updateGatewayCache();
        return permissionsSetService.remove(id);
    }

    public boolean removeBatch(List<Long> idList) {
        updateGatewayCache();
        return permissionsSetService.removeBatch(idList);
    }

    public List<PermissionsSetVo> findAllMatching(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        return permissionsSetService.findAllMatching(permissionsSetDo);
    }


    public Pagination<PermissionsSetVo> selectPage(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        return permissionsSetService.selectPage(permissionsSetDo);
    }

    public Pagination<PermissionsSetVo> getWhiteList(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        return permissionsSetService.getWhiteList(permissionsSetDo);
    }

    public Map<String, Object> getAllListToMap(PermissionsSetDto dto) {
        PermissionsSetDo permissionsSetDo =  permissionsSetConverter.dto2Do(dto);
        return permissionsSetService.getAllListToMap(permissionsSetDo);
    }

    public List<PermissionsSetVo> getListByMenuId(Long menuId) {
        return permissionsSetService.getListByMenuId(menuId);
    }
    /**
     * 更新菜单关联的按钮权限
     * @param dto
     */
    public void updateMenuSet(PermissionsSetMenuDto dto) {
        updateGatewayCache();
        permissionsSetService.updateMenuSet(dto);
    }

    public Map<String, Object> getPermissionCodeList(Long userId) {
        Map<String, Object> data = new HashMap<>(8);
        List<String> permissions = permissionsSetService.getPermissionCodeListByUser(userId);
        data.put("permissions", permissions);
        List<String> roles = new ArrayList<>();
        roles.add("admin");
        data.put("roles", roles);
        return data;
    }

}

