package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.RoleConverter;
import com.jhf.youke.base.domain.model.Do.RoleDo;
import com.jhf.youke.base.domain.model.dto.RoleDto;
import com.jhf.youke.base.domain.model.po.RolePo;
import com.jhf.youke.base.domain.model.vo.RoleVo;
import com.jhf.youke.base.domain.service.PermissionsSetService;
import com.jhf.youke.base.domain.service.RoleService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class RoleAppService {

    @Resource
    private RoleService roleService;

    @Resource
    private RoleConverter roleConverter;

    @Resource
    private PermissionsSetService permissionsSetService;

    public boolean update(RoleDto dto) {
        RoleDo roleDo =  roleConverter.dto2Do(dto);
        permissionsSetService.updateRoleSet(dto.getId(), dto.getPermissionsList());
        return roleService.update(roleDo);
    }

    public boolean delete(RoleDto dto) {
        RoleDo roleDo =  roleConverter.dto2Do(dto);
        return roleService.delete(roleDo);
    }


    public boolean insert(RoleDto dto) {
        RoleDo roleDo =  roleConverter.dto2Do(dto);
        RolePo po = roleService.save(roleDo);
        permissionsSetService.updateRoleSet(po.getId(), dto.getPermissionsList());
        return true;
    }

    public Optional<RoleVo> findById(Long id) {
        Optional<RoleVo> vo = roleService.findById(id);
        vo.get().setPermissionsIds(permissionsSetService.getListByRole(id));
        return vo;
    }

    public boolean remove(Long id) {
        return roleService.remove(id);
    }
    public boolean removeBatch(List<Long> idList) {
        return roleService.removeBatch(idList);
    }


    public List<RoleVo> findAllMatching(RoleDto dto) {
        RoleDo roleDo =  roleConverter.dto2Do(dto);
        return roleService.findAllMatching(roleDo);
    }


    public Pagination<RoleVo> selectPage(RoleDto dto) {
        RoleDo roleDo =  roleConverter.dto2Do(dto);
        return roleService.selectPage(roleDo);
    }

}

