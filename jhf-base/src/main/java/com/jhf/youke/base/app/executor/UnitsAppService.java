package com.jhf.youke.base.app.executor;

import com.jhf.youke.base.domain.converter.UnitsConverter;
import com.jhf.youke.base.domain.model.Do.UnitsDo;
import com.jhf.youke.base.domain.model.dto.UnitsDto;
import com.jhf.youke.base.domain.model.vo.UnitsVo;
import com.jhf.youke.base.domain.service.UnitsService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class UnitsAppService {

    @Resource
    private UnitsService unitsService;

    @Resource
    private UnitsConverter unitsConverter;


    public boolean update(UnitsDto dto) {
        UnitsDo unitsDo =  unitsConverter.dto2Do(dto);
        return unitsService.update(unitsDo);
    }

    public boolean delete(UnitsDto dto) {
        UnitsDo unitsDo =  unitsConverter.dto2Do(dto);
        return unitsService.delete(unitsDo);
    }


    public boolean insert(UnitsDto dto) {
        UnitsDo unitsDo =  unitsConverter.dto2Do(dto);
        return unitsService.insert(unitsDo);
    }

    public Optional<UnitsVo> findById(Long id) {
        return unitsService.findById(id);
    }


    public boolean remove(Long id) {
        return unitsService.remove(id);
    }


    public List<UnitsVo> findAllMatching(UnitsDto dto) {
        UnitsDo unitsDo =  unitsConverter.dto2Do(dto);
        return unitsService.findAllMatching(unitsDo);
    }


    public Pagination<UnitsVo> selectPage(UnitsDto dto) {
        UnitsDo unitsDo =  unitsConverter.dto2Do(dto);
        return unitsService.selectPage(unitsDo);
    }

}

