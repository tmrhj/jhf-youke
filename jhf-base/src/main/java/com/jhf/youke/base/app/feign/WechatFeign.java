package com.jhf.youke.base.app.feign;


import com.jhf.youke.base.app.feign.hystrix.WechatHystrix;
import com.jhf.youke.core.entity.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * 微信假装
 *
 * @author Administrator
 * @date 2022/11/17
 */
@Component
@FeignClient(name = "jhf-wechat-service", url = "${feign.wechatUrl}", fallback = WechatHystrix.class)
public interface WechatFeign {

    /**
     * 被代码开放id
     *
     * @param map 地图
     * @return {@link Response}<{@link String}>
     */
    @PostMapping("/wechat/wx/getOpenIdByCode")
    Response<String> getOpenIdByCode(@RequestBody Map<String, Object> map);

    /**
     * 得到用户信息开放id
     *
     * @param map 地图
     * @return {@link Response}<{@link Map}<{@link String}, {@link Object}>>
     */
    @PostMapping("/wechat/wx/getUserInfoByOpenId")
    Response<Map<String, Object>> getUserInfoByOpenId(@RequestBody Map<String, Object> map);

}
