package com.jhf.youke.base.app.feign.hystrix;

import com.jhf.youke.base.app.feign.RocketMqFeign;
import com.jhf.youke.core.entity.Message;
import org.springframework.stereotype.Component;


/**
 * @author RHJ
 */
@Component
public class RocketMqHystrix implements RocketMqFeign {

    @Override
    public Object send(Message msg) {
        return null;
    }

}
