package com.jhf.youke.base.app.feign.hystrix;


import com.jhf.youke.base.app.feign.WechatFeign;
import com.jhf.youke.core.entity.Response;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Administrator
 */
@Component
public class WechatHystrix implements WechatFeign {

    @Override
    public Response<String> getOpenIdByCode(Map<String,Object> map) {
        return null;
    }

    @Override
    public Response<Map<String, Object>> getUserInfoByOpenId(Map<String,Object> map) {
        return null;
    }

}
