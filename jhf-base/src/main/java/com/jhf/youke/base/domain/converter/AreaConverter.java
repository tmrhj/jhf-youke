package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.AreaDo;
import com.jhf.youke.base.domain.model.dto.AreaDto;
import com.jhf.youke.base.domain.model.po.AreaPo;
import com.jhf.youke.base.domain.model.vo.AreaVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


/**
 * 区域转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface AreaConverter {


    /**
     * dto2做
     *
     * @param areaDto 区域dto
     * @return {@link AreaDo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "pageSize",target = "pageSize"),
        @Mapping(source = "currentPage",target = "currentPage"),
        @Mapping(source = "querySort",target = "querySort"),
    })
    AreaDo dto2Do(AreaDto areaDto);

    /**
     * 洗阿宝
     *
     * @param areaDo 区做
     * @return {@link AreaPo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    AreaPo do2Po(AreaDo areaDo);

    /**
     * 洗订单列表
     *
     * @param areaDoList 区做列表
     * @return {@link List}<{@link AreaPo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<AreaPo> do2PoList(List<AreaDo> areaDoList);


    /**
     * 警察乙做
     *
     * @param areaPo 区域阿宝
     * @return {@link AreaDo}
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    @InheritInverseConfiguration(name = "do2Po")
    AreaDo po2Do(AreaPo areaPo);

    /**
     * 警察乙签证官
     *
     * @param areaPo 区域阿宝
     * @return {@link AreaVo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    AreaVo po2Vo(AreaPo areaPo);


    /**
     * 警察乙做列表
     *
     * @param areaPoList 区域订单列表
     * @return {@link List}<{@link AreaDo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<AreaDo> po2DoList(List<AreaPo> areaPoList);


    /**
     * 警察乙vo列表
     *
     * @param areaPoList 区域订单列表
     * @return {@link List}<{@link AreaVo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<AreaVo> po2VoList(List<AreaPo> areaPoList);


}

