package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.AuditDo;
import com.jhf.youke.base.domain.model.dto.AuditDto;
import com.jhf.youke.base.domain.model.po.AuditPo;
import com.jhf.youke.base.domain.model.vo.AuditVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  makejava
 **/

@Mapper(componentModel = "spring")
public interface AuditConverter extends BaseConverter<AuditDo,AuditPo,AuditDto,AuditVo> {


}

