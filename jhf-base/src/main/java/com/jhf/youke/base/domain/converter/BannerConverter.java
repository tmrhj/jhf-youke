package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.BannerDo;
import com.jhf.youke.base.domain.model.dto.BannerDto;
import com.jhf.youke.base.domain.model.po.BannerPo;
import com.jhf.youke.base.domain.model.vo.BannerVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 横幅转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface BannerConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param bannerDto 横幅dto
     * @return {@link BannerDo}
     */
    BannerDo dto2Do(BannerDto bannerDto);

    /**
     * 洗阿宝
     *
     * @param bannerDo 横幅做
     * @return {@link BannerPo}
     */
    BannerPo do2Po(BannerDo bannerDo);

    /**
     * 洗签证官
     *
     * @param bannerDo 横幅做
     * @return {@link BannerVo}
     */
    BannerVo do2Vo(BannerDo bannerDo);


    /**
     * 洗订单列表
     *
     * @param bannerDoList 横幅做列表
     * @return {@link List}<{@link BannerPo}>
     */
    List<BannerPo> do2PoList(List<BannerDo> bannerDoList);

    /**
     * 警察乙做
     *
     * @param bannerPo 横幅阿宝
     * @return {@link BannerDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    BannerDo po2Do(BannerPo bannerPo);

    /**
     * 警察乙签证官
     *
     * @param bannerPo 横幅阿宝
     * @return {@link BannerVo}
     */
    BannerVo po2Vo(BannerPo bannerPo);

    /**
     * 警察乙做列表
     *
     * @param bannerPoList 横幅订单列表
     * @return {@link List}<{@link BannerDo}>
     */
    List<BannerDo> po2DoList(List<BannerPo> bannerPoList);

    /**
     * 警察乙vo列表
     *
     * @param bannerPoList 横幅订单列表
     * @return {@link List}<{@link BannerVo}>
     */
    List<BannerVo> po2VoList(List<BannerPo> bannerPoList);


}

