package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.ButtonDo;
import com.jhf.youke.base.domain.model.dto.ButtonDto;
import com.jhf.youke.base.domain.model.po.ButtonPo;
import com.jhf.youke.base.domain.model.vo.ButtonVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * 按钮转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ButtonConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param buttonDto 按钮dto
     * @return {@link ButtonDo}
     */
    ButtonDo dto2Do(ButtonDto buttonDto);

    /**
     * 洗阿宝
     *
     * @param buttonDo 按钮做
     * @return {@link ButtonPo}
     */
    ButtonPo do2Po(ButtonDo buttonDo);

    /**
     * 洗签证官
     *
     * @param buttonDo 按钮做
     * @return {@link ButtonVo}
     */
    ButtonVo do2Vo(ButtonDo buttonDo);


    /**
     * 洗订单列表
     *
     * @param buttonDoList 按钮做列表
     * @return {@link List}<{@link ButtonPo}>
     */
    List<ButtonPo> do2PoList(List<ButtonDo> buttonDoList);

    /**
     * 警察乙做
     *
     * @param buttonPo 按钮阿宝
     * @return {@link ButtonDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ButtonDo po2Do(ButtonPo buttonPo);

    /**
     * 警察乙签证官
     *
     * @param buttonPo 按钮阿宝
     * @return {@link ButtonVo}
     */
    ButtonVo po2Vo(ButtonPo buttonPo);

    /**
     * 警察乙做列表
     *
     * @param buttonPoList 按钮订单列表
     * @return {@link List}<{@link ButtonDo}>
     */
    List<ButtonDo> po2DoList(List<ButtonPo> buttonPoList);

    /**
     * 警察乙vo列表
     *
     * @param buttonPoList 按钮订单列表
     * @return {@link List}<{@link ButtonVo}>
     */
    List<ButtonVo> po2VoList(List<ButtonPo> buttonPoList);


}

