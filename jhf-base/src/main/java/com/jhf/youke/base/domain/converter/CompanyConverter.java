package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.CompanyDo;
import com.jhf.youke.base.domain.model.dto.CompanyDto;
import com.jhf.youke.base.domain.model.po.CompanyPo;
import com.jhf.youke.base.domain.model.vo.CompanyVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 公司转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface CompanyConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param companyDto 公司dto
     * @return {@link CompanyDo}
     */
    CompanyDo dto2Do(CompanyDto companyDto);

    /**
     * 洗阿宝
     *
     * @param companyDo 公司是做
     * @return {@link CompanyPo}
     */
    CompanyPo do2Po(CompanyDo companyDo);

    /**
     * 洗订单列表
     *
     * @param companyDoList 公司是做列表
     * @return {@link List}<{@link CompanyPo}>
     */
    List<CompanyPo> do2PoList(List<CompanyDo> companyDoList);

    /**
     * 警察乙做
     *
     * @param companyPo 公司订单
     * @return {@link CompanyDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    CompanyDo po2Do(CompanyPo companyPo);

    /**
     * 警察乙签证官
     *
     * @param companyPo 公司订单
     * @return {@link CompanyVo}
     */
    CompanyVo po2Vo(CompanyPo companyPo);

    /**
     * 警察乙做列表
     *
     * @param companyPoList 公司订单列表
     * @return {@link List}<{@link CompanyDo}>
     */
    List<CompanyDo> po2DoList(List<CompanyPo> companyPoList);

    /**
     * 警察乙vo列表
     *
     * @param companyPoList 公司订单列表
     * @return {@link List}<{@link CompanyVo}>
     */
    List<CompanyVo> po2VoList(List<CompanyPo> companyPoList);


}

