package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.ConfigDo;
import com.jhf.youke.base.domain.model.dto.ConfigDto;
import com.jhf.youke.base.domain.model.po.ConfigPo;
import com.jhf.youke.base.domain.model.vo.ConfigVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 配置转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ConfigConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param configDto 配置dto
     * @return {@link ConfigDo}
     */
    ConfigDo dto2Do(ConfigDto configDto);

    /**
     * 洗阿宝
     *
     * @param configDo 配置做
     * @return {@link ConfigPo}
     */
    ConfigPo do2Po(ConfigDo configDo);

    /**
     * 洗订单列表
     *
     * @param configDoList 配置做列表
     * @return {@link List}<{@link ConfigPo}>
     */
    List<ConfigPo> do2PoList(List<ConfigDo> configDoList);

    /**
     * 警察乙做
     *
     * @param configPo 配置阿宝
     * @return {@link ConfigDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ConfigDo po2Do(ConfigPo configPo);

    /**
     * 警察乙签证官
     *
     * @param configPo 配置阿宝
     * @return {@link ConfigVo}
     */
    ConfigVo po2Vo(ConfigPo configPo);

    /**
     * 警察乙做列表
     *
     * @param configPoList 配置订单列表
     * @return {@link List}<{@link ConfigDo}>
     */
    List<ConfigDo> po2DoList(List<ConfigPo> configPoList);

    /**
     * 警察乙vo列表
     *
     * @param configPoList 配置订单列表
     * @return {@link List}<{@link ConfigVo}>
     */
    List<ConfigVo> po2VoList(List<ConfigPo> configPoList);


}

