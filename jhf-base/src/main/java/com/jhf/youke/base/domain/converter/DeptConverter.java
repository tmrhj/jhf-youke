package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.DeptDo;
import com.jhf.youke.base.domain.model.dto.DeptDto;
import com.jhf.youke.base.domain.model.po.DeptPo;
import com.jhf.youke.base.domain.model.vo.DeptVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


/**
 * 部门转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface DeptConverter {


    /**
     * dto2做
     *
     * @param deptDto 部门dto
     * @return {@link DeptDo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "pageSize",target = "pageSize"),
        @Mapping(source = "currentPage",target = "currentPage"),
        @Mapping(source = "querySort",target = "querySort"),
    })
    DeptDo dto2Do(DeptDto deptDto);

    /**
     * 洗阿宝
     *
     * @param deptDo 部门做
     * @return {@link DeptPo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    DeptPo do2Po(DeptDo deptDo);

    /**
     * 洗订单列表
     *
     * @param deptDoList 部门做列表
     * @return {@link List}<{@link DeptPo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<DeptPo> do2PoList(List<DeptDo> deptDoList);


    /**
     * 警察乙做
     *
     * @param deptPo 部门订单
     * @return {@link DeptDo}
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    @InheritInverseConfiguration(name = "do2Po")
    DeptDo po2Do(DeptPo deptPo);

    /**
     * 警察乙签证官
     *
     * @param deptPo 部门订单
     * @return {@link DeptVo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    DeptVo po2Vo(DeptPo deptPo);


    /**
     * 警察乙做列表
     *
     * @param deptPoList 部门订单列表
     * @return {@link List}<{@link DeptDo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<DeptDo> po2DoList(List<DeptPo> deptPoList);


    /**
     * 警察乙vo列表
     *
     * @param deptPoList 部门订单列表
     * @return {@link List}<{@link DeptVo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<DeptVo> po2VoList(List<DeptPo> deptPoList);


}

