package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.DictDo;
import com.jhf.youke.base.domain.model.dto.DictDto;
import com.jhf.youke.base.domain.model.po.DictPo;
import com.jhf.youke.base.domain.model.vo.DictVo;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface DictConverter extends BaseConverter<DictDo,DictPo,DictDto,DictVo> {


}

