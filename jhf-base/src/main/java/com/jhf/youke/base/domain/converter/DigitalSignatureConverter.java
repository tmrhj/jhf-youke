package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.DigitalSignatureDo;
import com.jhf.youke.base.domain.model.dto.DigitalSignatureDto;
import com.jhf.youke.base.domain.model.po.DigitalSignaturePo;
import com.jhf.youke.base.domain.model.vo.DigitalSignatureVo;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface DigitalSignatureConverter extends BaseConverter<DigitalSignatureDo,DigitalSignaturePo,DigitalSignatureDto,DigitalSignatureVo> {


}

