package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.LogDo;
import com.jhf.youke.base.domain.model.dto.LogDto;
import com.jhf.youke.base.domain.model.po.LogPo;
import com.jhf.youke.base.domain.model.vo.LogVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface LogConverter extends BaseConverter<LogDo,LogPo,LogDto,LogVo> {


}

