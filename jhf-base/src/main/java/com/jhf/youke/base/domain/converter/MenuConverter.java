package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.MenuDo;
import com.jhf.youke.base.domain.model.dto.MenuDto;
import com.jhf.youke.base.domain.model.po.MenuPo;
import com.jhf.youke.base.domain.model.vo.MenuVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 菜单转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface MenuConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param menuDto 菜单dto
     * @return {@link MenuDo}
     */
    MenuDo dto2Do(MenuDto menuDto);

    /**
     * 洗阿宝
     *
     * @param menuDo 菜单做
     * @return {@link MenuPo}
     */
    MenuPo do2Po(MenuDo menuDo);

    /**
     * 洗订单列表
     *
     * @param menuDoList 菜单做列表
     * @return {@link List}<{@link MenuPo}>
     */
    List<MenuPo> do2PoList(List<MenuDo> menuDoList);

    /**
     * 警察乙做
     *
     * @param menuPo 菜单阿宝
     * @return {@link MenuDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    MenuDo po2Do(MenuPo menuPo);

    /**
     * 警察乙签证官
     *
     * @param menuPo 菜单阿宝
     * @return {@link MenuVo}
     */
    MenuVo po2Vo(MenuPo menuPo);

    /**
     * 警察乙做列表
     *
     * @param menuPoList 阿宝菜单列表
     * @return {@link List}<{@link MenuDo}>
     */
    List<MenuDo> po2DoList(List<MenuPo> menuPoList);

    /**
     * 警察乙vo列表
     *
     * @param menuPoList 阿宝菜单列表
     * @return {@link List}<{@link MenuVo}>
     */
    List<MenuVo> po2VoList(List<MenuPo> menuPoList);


}

