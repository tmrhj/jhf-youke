package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.ObjectStatusDo;
import com.jhf.youke.base.domain.model.dto.ObjectStatusDto;
import com.jhf.youke.base.domain.model.po.ObjectStatusPo;
import com.jhf.youke.base.domain.model.vo.ObjectStatusVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 对象状态转换
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ObjectStatusConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param objectStatusDto 对象状态dto
     * @return {@link ObjectStatusDo}
     */
    ObjectStatusDo dto2Do(ObjectStatusDto objectStatusDto);

    /**
     * 洗阿宝
     *
     * @param objectStatusDo 对象状态
     * @return {@link ObjectStatusPo}
     */
    ObjectStatusPo do2Po(ObjectStatusDo objectStatusDo);

    /**
     * 洗签证官
     *
     * @param objectStatusDo 对象状态
     * @return {@link ObjectStatusVo}
     */
    ObjectStatusVo do2Vo(ObjectStatusDo objectStatusDo);


    /**
     * 洗订单列表
     *
     * @param objectStatusDoList 对象状态列表
     * @return {@link List}<{@link ObjectStatusPo}>
     */
    List<ObjectStatusPo> do2PoList(List<ObjectStatusDo> objectStatusDoList);

    /**
     * 警察乙做
     *
     * @param objectStatusPo 对象状态订单
     * @return {@link ObjectStatusDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ObjectStatusDo po2Do(ObjectStatusPo objectStatusPo);

    /**
     * 警察乙签证官
     *
     * @param objectStatusPo 对象状态订单
     * @return {@link ObjectStatusVo}
     */
    ObjectStatusVo po2Vo(ObjectStatusPo objectStatusPo);

    /**
     * 警察乙做列表
     *
     * @param objectStatusPoList 对象状态订单列表
     * @return {@link List}<{@link ObjectStatusDo}>
     */
    List<ObjectStatusDo> po2DoList(List<ObjectStatusPo> objectStatusPoList);

    /**
     * 警察乙vo列表
     *
     * @param objectStatusPoList 对象状态订单列表
     * @return {@link List}<{@link ObjectStatusVo}>
     */
    List<ObjectStatusVo> po2VoList(List<ObjectStatusPo> objectStatusPoList);


}

