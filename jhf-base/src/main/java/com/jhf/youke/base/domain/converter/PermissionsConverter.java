package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.PermissionsDo;
import com.jhf.youke.base.domain.model.dto.PermissionsDto;
import com.jhf.youke.base.domain.model.po.PermissionsPo;
import com.jhf.youke.base.domain.model.vo.PermissionsVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


/**
 * 权限转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface PermissionsConverter {


    /**
     * dto2做
     *
     * @param permissionsDto 权限dto
     * @return {@link PermissionsDo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "pageSize",target = "pageSize"),
        @Mapping(source = "currentPage",target = "currentPage"),
        @Mapping(source = "querySort",target = "querySort"),
    })
    PermissionsDo dto2Do(PermissionsDto permissionsDto);

    /**
     * 洗阿宝
     *
     * @param permissionsDo 权限做
     * @return {@link PermissionsPo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    PermissionsPo do2Po(PermissionsDo permissionsDo);

    /**
     * 洗订单列表
     *
     * @param permissionsDoList 权限做列表
     * @return {@link List}<{@link PermissionsPo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<PermissionsPo> do2PoList(List<PermissionsDo> permissionsDoList);


    /**
     * 警察乙做
     *
     * @param permissionsPo 权限阿宝
     * @return {@link PermissionsDo}
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    @InheritInverseConfiguration(name = "do2Po")
    PermissionsDo po2Do(PermissionsPo permissionsPo);

    /**
     * 警察乙签证官
     *
     * @param permissionsPo 权限阿宝
     * @return {@link PermissionsVo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    PermissionsVo po2Vo(PermissionsPo permissionsPo);


    /**
     * 警察乙做列表
     *
     * @param permissionsPoList 阿宝权限列表
     * @return {@link List}<{@link PermissionsDo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<PermissionsDo> po2DoList(List<PermissionsPo> permissionsPoList);


    /**
     * 警察乙vo列表
     *
     * @param permissionsPoList 阿宝权限列表
     * @return {@link List}<{@link PermissionsVo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<PermissionsVo> po2VoList(List<PermissionsPo> permissionsPoList);


}

