package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.PermissionsSetDo;
import com.jhf.youke.base.domain.model.dto.PermissionsSetDto;
import com.jhf.youke.base.domain.model.po.PermissionsSetPo;
import com.jhf.youke.base.domain.model.vo.PermissionsSetVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 权限设置转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface PermissionsSetConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param permissionsSetDto 权限设置dto
     * @return {@link PermissionsSetDo}
     */
    PermissionsSetDo dto2Do(PermissionsSetDto permissionsSetDto);

    /**
     * 洗阿宝
     *
     * @param permissionsSetDo 权限组做
     * @return {@link PermissionsSetPo}
     */
    PermissionsSetPo do2Po(PermissionsSetDo permissionsSetDo);

    /**
     * 洗订单列表
     *
     * @param permissionsSetDoList 权限设置做列表
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> do2PoList(List<PermissionsSetDo> permissionsSetDoList);

    /**
     * 警察乙做
     *
     * @param permissionsSetPo 权限设置阿宝
     * @return {@link PermissionsSetDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    PermissionsSetDo po2Do(PermissionsSetPo permissionsSetPo);

    /**
     * 警察乙签证官
     *
     * @param permissionsSetPo 权限设置阿宝
     * @return {@link PermissionsSetVo}
     */
    PermissionsSetVo po2Vo(PermissionsSetPo permissionsSetPo);

    /**
     * 警察乙做列表
     *
     * @param permissionsSetPoList 权限设置订单列表
     * @return {@link List}<{@link PermissionsSetDo}>
     */
    List<PermissionsSetDo> po2DoList(List<PermissionsSetPo> permissionsSetPoList);

    /**
     * 警察乙vo列表
     *
     * @param permissionsSetPoList 权限设置订单列表
     * @return {@link List}<{@link PermissionsSetVo}>
     */
    List<PermissionsSetVo> po2VoList(List<PermissionsSetPo> permissionsSetPoList);


}

