package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.RoleDo;
import com.jhf.youke.base.domain.model.dto.RoleDto;
import com.jhf.youke.base.domain.model.po.RolePo;
import com.jhf.youke.base.domain.model.vo.RoleVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


/**
 * 角色转换
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface RoleConverter {


    /**
     * dto2做
     *
     * @param roleDto 角色dto
     * @return {@link RoleDo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "pageSize",target = "pageSize"),
        @Mapping(source = "currentPage",target = "currentPage"),
        @Mapping(source = "querySort",target = "querySort"),
    })
    RoleDo dto2Do(RoleDto roleDto);

    /**
     * 洗阿宝
     *
     * @param roleDo 角色做
     * @return {@link RolePo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    RolePo do2Po(RoleDo roleDo);

    /**
     * 洗订单列表
     *
     * @param roleDoList
     * @return {@link List}<{@link RolePo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<RolePo> do2PoList(List<RoleDo> roleDoList);


    /**
     * 警察乙做
     *
     * @param rolePo
     * @return {@link RoleDo}
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    @InheritInverseConfiguration(name = "do2Po")
    RoleDo po2Do(RolePo rolePo);

    /**
     * 警察乙签证官
     *
     * @param rolePo 角色阿宝
     * @return {@link RoleVo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    RoleVo po2Vo(RolePo rolePo);


    /**
     * 警察乙做列表
     *
     * @param rolePoList 阿宝角色列表
     * @return {@link List}<{@link RoleDo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<RoleDo> po2DoList(List<RolePo> rolePoList);


    /**
     * 警察乙vo列表
     *
     * @param rolePoList 阿宝角色列表
     * @return {@link List}<{@link RoleVo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<RoleVo> po2VoList(List<RolePo> rolePoList);


}

