package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.UnitsDo;
import com.jhf.youke.base.domain.model.dto.UnitsDto;
import com.jhf.youke.base.domain.model.po.UnitsPo;
import com.jhf.youke.base.domain.model.vo.UnitsVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


/**
 * 单位转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface UnitsConverter {


    /**
     * dto2做
     *
     * @param unitsDto 单位dto
     * @return {@link UnitsDo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "pageSize",target = "pageSize"),
        @Mapping(source = "currentPage",target = "currentPage"),
        @Mapping(source = "querySort",target = "querySort"),
    })
    UnitsDo dto2Do(UnitsDto unitsDto);

    /**
     * 洗阿宝
     *
     * @param unitsDo 单位做
     * @return {@link UnitsPo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    UnitsPo do2Po(UnitsDo unitsDo);

    /**
     * 洗订单列表
     *
     * @param unitsDoList 单位做列表
     * @return {@link List}<{@link UnitsPo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<UnitsPo> do2PoList(List<UnitsDo> unitsDoList);


    /**
     * 警察乙做
     *
     * @param unitsPo 单位阿宝
     * @return {@link UnitsDo}
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    @InheritInverseConfiguration(name = "do2Po")
    UnitsDo po2Do(UnitsPo unitsPo);

    /**
     * 警察乙签证官
     *
     * @param unitsPo 单位阿宝
     * @return {@link UnitsVo}
     */
    @Mappings({
        @Mapping(source = "id",target = "id"),
        @Mapping(source = "remark",target = "remark"),
        @Mapping(source = "createTime",target = "createTime"),
        @Mapping(source = "updateTime",target = "updateTime"),
        @Mapping(source = "delFlag",target = "delFlag"),
    })
    UnitsVo po2Vo(UnitsPo unitsPo);


    /**
     * 警察乙做列表
     *
     * @param unitsPoList 单位订单列表
     * @return {@link List}<{@link UnitsDo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<UnitsDo> po2DoList(List<UnitsPo> unitsPoList);


    /**
     * 警察乙vo列表
     *
     * @param unitsPoList 单位订单列表
     * @return {@link List}<{@link UnitsVo}>
     */
    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "remark",target = "remark"),
            @Mapping(source = "createTime",target = "createTime"),
            @Mapping(source = "updateTime",target = "updateTime"),
            @Mapping(source = "delFlag",target = "delFlag"),
    })
    List<UnitsVo> po2VoList(List<UnitsPo> unitsPoList);


}

