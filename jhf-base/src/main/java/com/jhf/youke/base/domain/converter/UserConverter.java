package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.UserDo;
import com.jhf.youke.base.domain.model.dto.UserDto;
import com.jhf.youke.base.domain.model.po.UserPo;
import com.jhf.youke.base.domain.model.vo.UserVo;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;


/**
 * @author RHJ
 */
@Mapper(componentModel = "spring")
public interface UserConverter extends BaseConverter<UserDo,UserPo,UserDto,UserVo> {


}

