package com.jhf.youke.base.domain.converter;

import com.jhf.youke.base.domain.model.Do.UserRoleDo;
import com.jhf.youke.base.domain.model.dto.UserRoleDto;
import com.jhf.youke.base.domain.model.po.UserRolePo;
import com.jhf.youke.base.domain.model.vo.UserRoleVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 用户角色转换
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface UserRoleConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param userRoleDto
     * @return {@link UserRoleDo}
     */
    UserRoleDo dto2Do(UserRoleDto userRoleDto);

    /**
     * 洗阿宝
     *
     * @param userRoleDo 用户角色所做
     * @return {@link UserRolePo}
     */
    UserRolePo do2Po(UserRoleDo userRoleDo);

    /**
     * 洗订单列表
     *
     * @param userRoleDoList 用户角色做列表
     * @return {@link List}<{@link UserRolePo}>
     */
    List<UserRolePo> do2PoList(List<UserRoleDo> userRoleDoList);

    /**
     * 警察乙做
     *
     * @param userRolePo 用户角色阿宝
     * @return {@link UserRoleDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    UserRoleDo po2Do(UserRolePo userRolePo);

    /**
     * 警察乙签证官
     *
     * @param userRolePo 用户角色阿宝
     * @return {@link UserRoleVo}
     */
    UserRoleVo po2Vo(UserRolePo userRolePo);

    /**
     * 警察乙做列表
     *
     * @param userRolePoList 用户角色阿宝列表
     * @return {@link List}<{@link UserRoleDo}>
     */
    List<UserRoleDo> po2DoList(List<UserRolePo> userRolePoList);

    /**
     * 警察乙vo列表
     *
     * @param userRolePoList 用户角色阿宝列表
     * @return {@link List}<{@link UserRoleVo}>
     */
    List<UserRoleVo> po2VoList(List<UserRolePo> userRolePoList);


}

