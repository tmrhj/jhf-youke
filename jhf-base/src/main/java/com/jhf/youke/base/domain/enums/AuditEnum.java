package com.jhf.youke.base.domain.enums;

/**
 * 审计枚举
 * description:
 * date: 2022/11/22 16:09
 * author: cyx
 *
 * @author Administrator
 * @date 2022/11/22
 */
public enum AuditEnum {
    /**
     * 同意
     */
    STATUS_AGREE(1, "审核通过"),
    /**
     * 不同意
     */
    STATUS_DISAGREE(2, "审核不通过");

    /**
     * 编码
     */
    private int code;
    /**
     * 价值
     */
    private String value;

    /**
     * 审计枚举
     *
     * @param code  代码
     * @param value 价值
     */
    AuditEnum(int code, String value){
        this.code = code;
        this.value = value;
    }

    /**
     * 代码
     *
     * @return {@link Integer}
     */
    public int code() {
        return code;
    }

}
