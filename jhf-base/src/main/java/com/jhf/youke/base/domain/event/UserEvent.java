package com.jhf.youke.base.domain.event;

import com.jhf.youke.core.ddd.BaseEvent;
import lombok.Getter;


/**
 * @author RHJ
 */
@Getter
public class UserEvent  extends BaseEvent {

    public UserEvent(Object source, String type, String topic, String message) {
        super(source);
        this.topic = topic;
        this.type = type;
        this.message = message;
    }


}



