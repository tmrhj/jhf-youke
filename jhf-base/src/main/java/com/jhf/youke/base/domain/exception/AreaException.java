package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class AreaException extends DomainException {

    public AreaException(String message) { super(message); }
}

