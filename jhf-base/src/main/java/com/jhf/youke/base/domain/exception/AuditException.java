package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  makejava
 **/

public class AuditException extends DomainException {

    public AuditException(String message) { super(message); }
}

