package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class AutoCompletedException extends DomainException {

    public AutoCompletedException(String message) { super(message); }
}

