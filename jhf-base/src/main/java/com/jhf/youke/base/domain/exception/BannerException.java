package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class BannerException extends DomainException {

    public BannerException(String message) { super(message); }
}

