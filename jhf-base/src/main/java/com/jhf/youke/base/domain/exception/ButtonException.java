package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ButtonException extends DomainException {

    public ButtonException(String message) { super(message); }
}

