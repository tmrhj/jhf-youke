package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class CompanyException extends DomainException {

    public CompanyException(String message) { super(message); }
}

