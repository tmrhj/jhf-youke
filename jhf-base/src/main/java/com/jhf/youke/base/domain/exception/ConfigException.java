package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ConfigException extends DomainException {

    public ConfigException(String message) { super(message); }
}

