package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class DeptException extends DomainException {

    public DeptException(String message) { super(message); }
}

