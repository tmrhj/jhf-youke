package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class DictException extends DomainException {

    public DictException(String message) { super(message); }
}

