package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class DigitalSignatureException extends DomainException {

    public DigitalSignatureException(String message) { super(message); }
}

