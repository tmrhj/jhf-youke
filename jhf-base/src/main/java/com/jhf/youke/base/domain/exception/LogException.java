package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class LogException extends DomainException {

    public LogException(String message) { super(message); }
}

