package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class MenuException extends DomainException {

    public MenuException(String message) { super(message); }
}

