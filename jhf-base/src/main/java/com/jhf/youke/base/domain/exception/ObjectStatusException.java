package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ObjectStatusException extends DomainException {

    public ObjectStatusException(String message) { super(message); }
}

