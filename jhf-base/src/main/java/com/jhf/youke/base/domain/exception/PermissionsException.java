package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class PermissionsException extends DomainException {

    public PermissionsException(String message) { super(message); }
}

