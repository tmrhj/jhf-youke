package com.jhf.youke.base.domain.exception;

import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class PermissionsSetException extends DomainException {

    public PermissionsSetException(String message) { super(message); }
}

