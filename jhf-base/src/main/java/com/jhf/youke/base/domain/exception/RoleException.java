package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class RoleException extends DomainException {

    public RoleException(String message) { super(message); }
}

