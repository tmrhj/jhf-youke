package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class UnitsException extends DomainException {

    public UnitsException(String message) { super(message); }
}

