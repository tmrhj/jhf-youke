package com.jhf.youke.base.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class UserRoleException extends DomainException {

    public UserRoleException(String message) { super(message); }

}

