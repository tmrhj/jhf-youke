package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.AuditPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  makejava
 **/
public interface AuditRepository extends Repository<AuditPo> {


}

