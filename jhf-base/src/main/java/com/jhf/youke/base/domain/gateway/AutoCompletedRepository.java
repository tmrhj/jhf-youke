package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.AutoCompletedPo;
import com.jhf.youke.core.ddd.Repository;

import java.util.List;
import java.util.Map;

/**
 * 汽车库完成
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface AutoCompletedRepository extends Repository<AutoCompletedPo> {

      /**
       * 通过sql自动完成
       *
       * @param map 地图
       * @return {@link List}<{@link Map}<{@link String},{@link String}>>
       */
      List<Map<String,String>> autoCompleteBySql(Map<String,Object> map);

}

