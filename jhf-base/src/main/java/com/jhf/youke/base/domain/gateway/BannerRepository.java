package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.BannerPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface BannerRepository extends Repository<BannerPo> {


}

