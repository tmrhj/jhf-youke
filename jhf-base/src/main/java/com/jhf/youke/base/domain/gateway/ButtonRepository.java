package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.ButtonPo;
import com.jhf.youke.core.ddd.Repository;

import java.util.List;

/**
 * 按钮库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ButtonRepository extends Repository<ButtonPo> {

      /**
       * 被状态列表
       *
       * @param status 状态
       * @return {@link List}<{@link ButtonPo}>
       */
      List<ButtonPo> getListByStatus(Integer status);
}

