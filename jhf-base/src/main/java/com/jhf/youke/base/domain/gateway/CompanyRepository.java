package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.CompanyPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * 公司库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface CompanyRepository extends Repository<CompanyPo> {

    /**
     * 获得公司身份证
     *
     * @param companyId 公司标识
     * @return {@link String}
     */
    String getCompanyIds(Long companyId);

}

