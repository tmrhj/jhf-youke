package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.ConfigPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface ConfigRepository extends Repository<ConfigPo> {


}

