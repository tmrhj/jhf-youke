package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.DigitalSignaturePo;
import com.jhf.youke.core.ddd.Repository;

/**
 * 数字签名库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface DigitalSignatureRepository extends Repository<DigitalSignaturePo> {

     /**
      * 通过公司id
      *
      * @param id id
      * @return {@link DigitalSignaturePo}
      */
     DigitalSignaturePo getByCompanyId(Long id);

     /**
      * 被公司所有id
      *
      * @param companyId 公司标识
      * @return {@link DigitalSignaturePo}
      */
     DigitalSignaturePo getAllByCompanyId(Long companyId);

}

