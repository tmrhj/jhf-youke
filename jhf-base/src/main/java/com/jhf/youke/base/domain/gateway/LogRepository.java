package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.LogPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface LogRepository extends Repository<LogPo> {


}

