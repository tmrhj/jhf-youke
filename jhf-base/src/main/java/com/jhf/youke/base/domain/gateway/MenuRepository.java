package com.jhf.youke.base.domain.gateway;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jhf.youke.base.domain.model.po.MenuPo;
import com.jhf.youke.core.ddd.Repository;

import java.util.List;

/**
 * 菜单库
 *
 * @author RHJ
 * @date 2022/11/17
 **/
public interface MenuRepository extends Repository<MenuPo> {

    /**
     * 列表
     *
     * @param qw qw
     * @return {@link List}<{@link MenuPo}>
     */
    List<MenuPo> list(QueryWrapper<MenuPo> qw);

    /**
     * 被用户列表
     *
     * @param userId 用户id
     * @return {@link List}<{@link MenuPo}>
     */
    List<MenuPo> getListByUser(Long userId);

}

