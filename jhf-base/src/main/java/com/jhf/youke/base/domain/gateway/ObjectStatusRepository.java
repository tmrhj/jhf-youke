package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.ObjectStatusPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface ObjectStatusRepository extends Repository<ObjectStatusPo> {


}

