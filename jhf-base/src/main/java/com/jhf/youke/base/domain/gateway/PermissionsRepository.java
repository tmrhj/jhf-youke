package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.PermissionsPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface PermissionsRepository extends Repository<PermissionsPo> {


}

