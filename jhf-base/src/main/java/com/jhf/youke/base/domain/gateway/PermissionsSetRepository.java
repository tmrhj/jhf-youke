package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.PermissionsPo;
import com.jhf.youke.base.domain.model.po.PermissionsSetPo;
import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;

import java.util.List;


/**
 * 权限设置存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface PermissionsSetRepository extends Repository<PermissionsSetPo> {

    /**
     * 获取角色权限
     * @param roleId 角色id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getListByRole(Long roleId);

    /**
     * 被菜单列表
     *
     * @param menuId 菜单id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getListByMenu(Long menuId);

    /**
     * 被用户列表
     *
     * @param userId 用户id
     * @return {@link List}<{@link PermissionsPo}>
     */
    List<PermissionsPo> getListByUser(Long userId);

    /**
     * 得到用户url列表
     *
     * @param userId 用户id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getUrlListByUser(Long userId);

    /**
     * 被菜单id列表
     *
     * @param menuId 菜单id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getListByMenuId(Long menuId);

    /**
     * 获取所有菜单设置列表
     *
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    public List<PermissionsSetPo> getAllMenuSetList();

    /**
     * 得到白名单
     *
     * @param pageQuery 页面查询
     * @return {@link Pagination}<{@link PermissionsSetPo}>
     */
    public Pagination<PermissionsSetPo> getWhiteList(PageQuery<PermissionsSetPo> pageQuery);


    /**
     * 角色权限列表
     * @param roleId 角色id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsPo> getListByRoleId(Long roleId);

}

