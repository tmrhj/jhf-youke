package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.RolePo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface RoleRepository<T> extends Repository<RolePo> {


}

