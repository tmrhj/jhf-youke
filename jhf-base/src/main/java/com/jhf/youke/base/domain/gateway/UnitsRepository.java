package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.UnitsPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface UnitsRepository extends Repository<UnitsPo> {


}

