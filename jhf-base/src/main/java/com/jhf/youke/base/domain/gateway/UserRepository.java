package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.UserPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * 用户存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface UserRepository extends Repository<UserPo> {


    /**
     * 通过pwd
     *
     * @param loginName 登录名
     * @param password  密码
     * @return {@link UserPo}
     */
    UserPo getByPwd(String loginName, String password);

    /**
     * 登录名
     *
     * @param loginName 登录名
     * @return {@link UserPo}
     */
    UserPo getByLoginName(String loginName);

    /**
     * 通过移动
     *
     * @param openId 开放id
     * @return {@link UserPo}
     */
    UserPo getByMobile(String openId);
}
