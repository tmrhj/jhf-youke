package com.jhf.youke.base.domain.gateway;


import com.jhf.youke.base.domain.model.po.UserRolePo;
import com.jhf.youke.core.ddd.Repository;
import java.util.List;

/**
 * 用户角色库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface UserRoleRepository<T> extends Repository<UserRolePo> {

     /**
      * 被用户列表
      *
      * @param userId 用户id
      * @return {@link List}<{@link UserRolePo}>
      */
     List<UserRolePo> getListByUser(Long userId);

     /**
      * 删除角色用户id
      *
      * @param userId 用户id
      * @return {@link Boolean}
      */
     Boolean deleteRoleByUserId(Long userId);

}

