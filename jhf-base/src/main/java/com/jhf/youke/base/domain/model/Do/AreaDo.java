package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;


/**
 * @author RHJ
 * **/
@Data
public class AreaDo extends BaseDoEntity {

  private static final long serialVersionUID = 554488282694356189L;

    /** 名字 **/
    private String name;

    /** 编码 **/
    private String code;

    /** 上级 **/
    private Long parentId;

    /** 上级树 **/
    private String parentIds;

    /** 区域类型 **/
    private Integer type;



}


