package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.AuditException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @author  makejava
 **/

@Data
@NoArgsConstructor
public class AuditDo extends BaseDoEntity {

  private static final long serialVersionUID = 850774281675117354L;

    /**
     * 类型 1 团长注册审核
     */
    private Integer type;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 团长名
     */
    private String recommendMan;

    /**
     * 0待审核 1审核通过
     */
    private Integer status;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new AuditException(errorMessage);
        }
        return obj;
    }

    private AuditDo validateNull(AuditDo auditDo){
          //可使用链式法则进行为空检查
          auditDo.
          requireNonNull(auditDo, auditDo.getRemark(),"不能为NULL");
                
        return auditDo;
    }

    public AuditDo (UserDo userDo) {
        this.type = 1;
        this.userId = userDo.getId();
        this.userName = userDo.getName();
        this.status = 0;
    }


}


