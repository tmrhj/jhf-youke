package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.AutoCompletedException;
import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class AutoCompletedDo extends BaseDoEntity {

  private static final long serialVersionUID = -93855082309538232L;

    /** 名称 **/
    private String name;

    /** sql **/
    private String sqlstr;

    /** 微服务名 **/
    private String serverName;

    private String query;

    private Long companyId;

    private Long rootId;

    private Integer pageLimit;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new AutoCompletedException(errorMessage);
        }
        return obj;
    }

    private AutoCompletedDo validateNull(AutoCompletedDo autoCompletedDo){
          // 可使用链式法则进行为空检查
          autoCompletedDo.
          requireNonNull(autoCompletedDo, autoCompletedDo.getRemark(),"不能为NULL");
                
        return autoCompletedDo;
    }


}


