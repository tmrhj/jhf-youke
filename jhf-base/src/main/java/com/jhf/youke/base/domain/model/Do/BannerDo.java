package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.BannerException;
import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class BannerDo extends BaseDoEntity {

  private static final long serialVersionUID = 894110446720408814L;

    /** 单位ID **/
    private Long companyId;

    /** 标题 **/
    private String title;

    /** 链接地址 **/
    private String link;

    /**图片地址 **/
    private String imgUrl;

    /**1正常 2禁用 **/
    private Integer status;

    /** 排序 **/
    private Integer sort;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new BannerException(errorMessage);
        }
        return obj;
    }

    private BannerDo validateNull(BannerDo bannerDo){
          //可使用链式法则进行为空检查
          bannerDo.
          requireNonNull(bannerDo, bannerDo.getRemark(),"不能为NULL");

        return bannerDo;
    }


}


