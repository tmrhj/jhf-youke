package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.ButtonException;
import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;

import java.util.Objects;

/**
 * @author RHJ
 */
@Data
public class ButtonDo extends BaseDoEntity {

  private static final long serialVersionUID = -33910248555523078L;

    /** 名字 **/
    private String name;

    /** 当前状态 **/
    private Integer status;

    /**下一状态 **/
    private Integer nextStatus;

    /** 按钮类型 **/
    private String type;

    /** 样式 **/
    private String css;

    /** 执行方法 **/
    private String url;

    /** 函数名 **/
    private String function;

    /** 执行权限 **/
    private String permissions;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ButtonException(errorMessage);
        }
        return obj;
    }

    private ButtonDo validateNull(ButtonDo buttonDo){
          //可使用链式法则进行为空检查
          buttonDo.
          requireNonNull(buttonDo, buttonDo.getRemark(),"不能为NULL");
                
        return buttonDo;
    }


}


