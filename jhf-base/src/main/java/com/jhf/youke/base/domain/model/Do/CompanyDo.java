package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.CompanyException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class CompanyDo extends BaseDoEntity {

  private static final long serialVersionUID = 673595401873260191L;

    /** 名称 **/
    private String name;

    /**根上级ID **/
    private Long rootId;

    /**上级ID **/
    private Long parentId;

    /**上级单位树 **/
    private String parentIds;

    /**负责人 **/
    private String leader;

    /**手机号码 **/
    private String phone;

    /**类型 1公司 2部门 **/
    private Integer type;

    /**排序 **/
    private Integer sort;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new CompanyException(errorMessage);
        }
        return obj;
    }

    private CompanyDo validateNull(CompanyDo companyDo){
          // 可使用链式法则进行为空检查
          companyDo.
          requireNonNull(companyDo, companyDo.getRemark(),"不能为NULL");

        return companyDo;
    }


}


