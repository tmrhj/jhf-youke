package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.ConfigException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ConfigDo extends BaseDoEntity {

  private static final long serialVersionUID = -42513635314232723L;

    /**名字 **/
    private String name;

    /**编码 **/
    private String code;

    /**值 **/
    private String value;

    /**描述 **/
    private String description;

    /**排序 **/
    private Integer sort;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ConfigException(errorMessage);
        }
        return obj;
    }

    private ConfigDo validateNull(ConfigDo configDo){
          //可使用链式法则进行为空检查
          configDo.
          requireNonNull(configDo, configDo.getRemark(),"不能为NULL");

        return configDo;
    }


}


