package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;


/**
 * @author RHJ
 */
@Data
public class DeptDo extends BaseDoEntity {

  private static final long serialVersionUID = -40200245370271761L;

    /**学校ID **/
    private Long schoolId;

    /**班级名称 **/
    private String name;

    /**排序 **/
    private Integer sort;

    /**企业微信中的部门ID **/
    private String wxDeptId;



}


