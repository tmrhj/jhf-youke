package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.DictException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class DictDo extends BaseDoEntity {

  private static final long serialVersionUID = -37703860285695111L;

    /**表名 **/
    private String tableName;

    /**字段名 **/
    private String fieldName;

    /**值 **/
    private Integer value;

    /**标签 **/
    private String label;

    /**类型，可表+字段，全局唯一 **/
    private String type;

    /**前端列表回显样式 **/
    private String listClass;

    /**排序 **/
    private Integer sort;

    /**描述 **/
    private String description;

    /**字典名称 **/
    private String name;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new DictException(errorMessage);
        }
        return obj;
    }

    private DictDo validateNull(DictDo dictDo){
          //可使用链式法则进行为空检查
          dictDo.
          requireNonNull(dictDo, dictDo.getRemark(),"不能为NULL");

        return dictDo;
    }


}


