package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.DigitalSignatureException;
import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class DigitalSignatureDo extends BaseDoEntity {

  private static final long serialVersionUID = 838230827379075572L;

    /**单位 **/
    private Long companyId;

    /**盐值 **/
    private String salt;

    /**私钥 **/
    private String privateKey;

    /**公钥 **/
    private String publicKey;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new DigitalSignatureException(errorMessage);
        }
        return obj;
    }

    private DigitalSignatureDo validateNull(DigitalSignatureDo digitalSignatureDo){
          //可使用链式法则进行为空检查
          digitalSignatureDo.
          requireNonNull(digitalSignatureDo, digitalSignatureDo.getRemark(),"不能为NULL");
                
        return digitalSignatureDo;
    }


}


