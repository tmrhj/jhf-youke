package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;


/**
 * @author RHJ
 */
@Data
public class LogDo extends BaseDoEntity {

  private static final long serialVersionUID = 463897857802644476L;

    /** 用户ID **/
    private Long userId;

    /** 用户昵称  **/
    private String userName;

    /** 用户类型 **/
    private Integer userType;

    /** IP **/
    private String loginIp;

    /** 登录地点 **/
    private String address;

    /** 浏览器 **/
    private String browser;

    /** 操作系统 **/
    private String systemName;

    /** 状态 0正常 1失败 **/
    private Integer status;



}


