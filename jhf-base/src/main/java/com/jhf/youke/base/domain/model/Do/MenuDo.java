package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.MenuException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;

/**
 * @author RHJ
 * **/

@Data
public class MenuDo extends BaseDoEntity {

    private static final long serialVersionUID = 586065262455874347L;

    /** 父菜单ID，一级菜单为0 **/
    private Long parentId;

    /** 菜单名称 **/
    private String name;

    /** 授权(多个用逗号分隔，如：user:list,user:create) **/
    private String perms;

    /** 类型   0：目录   1：菜单   2：按钮 **/
    private Integer type;

    /** 菜单图标 **/
    private String icon;

    /** 排序 **/
    private Integer orderNum;

    /** 路由地址 **/
    private String path;

    /** 组件路径 **/
    private String component;

    /** 是否为外链 0否 1是  **/
    private Integer isFrame;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new MenuException(errorMessage);
        }
        return obj;
    }

    private MenuDo validateNull(MenuDo menuDo){
          // 可使用链式法则进行为空检查
          menuDo.
          requireNonNull(menuDo, menuDo.getRemark(),"不能为NULL");

        return menuDo;
    }

}


