package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.ObjectStatusException;
import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;

import java.util.Objects;

/**
 * @author RHJ
 * **/
@Data
public class ObjectStatusDo extends BaseDoEntity {

  private static final long serialVersionUID = 266673009273224687L;

    /** 业务对象ID **/
    private Integer objectId;

    /** 业务状态名称 **/
    private String name;

    /** 是否新建 **/
    private Integer isNew;

    /** 状态 0无效 1正常 **/
    private Integer status;

    /** 值 **/
    private Integer value;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ObjectStatusException(errorMessage);
        }
        return obj;
    }

    private ObjectStatusDo validateNull(ObjectStatusDo objectStatusDo){
          //可使用链式法则进行为空检查
          objectStatusDo.
          requireNonNull(objectStatusDo, objectStatusDo.getRemark(),"不能为NULL");
                
        return objectStatusDo;
    }


}


