package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.PermissionsException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class PermissionsDo extends BaseDoEntity {

  private static final long serialVersionUID = 264742113623888902L;

    /** 名称 **/
    private String name;

    /** 父类ID **/
    private Long parentId;

    /** 父类ID集合 **/
    private String parentIds;

    /** 1 目录 2 普通权限 3 数据权限 **/
    private Integer type;

    /** 编码 **/
    private String code;

    /** 排序 **/
    private Integer sort;

    /** 所在层级 **/
    private Integer layer;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new PermissionsException(errorMessage);
        }
        return obj;
    }

    private PermissionsDo validateNull(PermissionsDo permissionsDo){
          // 可使用链式法则进行为空检查
          permissionsDo.
          requireNonNull(permissionsDo, permissionsDo.getRemark(),"不能为NULL");

        return permissionsDo;
    }


}


