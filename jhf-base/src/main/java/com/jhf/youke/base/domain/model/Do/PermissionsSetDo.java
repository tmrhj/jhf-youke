package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.PermissionsSetException;
import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class PermissionsSetDo extends BaseDoEntity {

  private static final long serialVersionUID = -34283318368681612L;

    private String name;

    private Long permissionId;

    /** 权限编码 **/
    private String permissionCode;

    /** 1 角色 2 菜单 3 方法 **/
    private Integer type;

    /** 菜单ID/角色ID **/
    private Long bizId;

    /** 方法路径 **/
    private String url;

    private Integer insideStatus;

    private Integer whiteStatus;

    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new PermissionsSetException(errorMessage);
        }
        return obj;
    }

    private PermissionsSetDo validateNull(PermissionsSetDo permissionsSetDo){
          // 可使用链式法则进行为空检查
          permissionsSetDo.
          requireNonNull(permissionsSetDo, permissionsSetDo.getRemark(),"不能为NULL");
                
        return permissionsSetDo;
    }




}


