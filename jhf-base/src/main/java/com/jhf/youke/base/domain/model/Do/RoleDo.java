package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.base.domain.exception.RoleException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;



/**
 * @author RHJ
 */
@Data
public class RoleDo extends BaseDoEntity {

  private static final long serialVersionUID = -34143529397974783L;

    /** 角色名称 **/
    private String name;

    /** 排序  **/
    private Integer sort;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new RoleException(errorMessage);
        }
        return obj;
    }

    private RoleDo validateNull(RoleDo roleDo){
          //可使用链式法则进行为空检查
          roleDo.
          requireNonNull(roleDo, roleDo.getRemark(),"不能为NULL");

        return roleDo;
    }


}


