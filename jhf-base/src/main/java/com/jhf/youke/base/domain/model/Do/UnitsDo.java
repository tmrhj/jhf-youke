package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;


/**
 * @author RHJ
 */
@Data
public class UnitsDo extends BaseDoEntity {

    /**
     * 串行版本uid
     */
    private static final long serialVersionUID = 431049870607606230L;

    /**
     * 名字
     */
    private String name;

    /**
     * 状态 0无效 1有效
     */
    private Integer status;


}


