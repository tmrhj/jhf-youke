package com.jhf.youke.base.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;


/**
 * @author RHJ
 */
@Data
public class UserRoleDo extends BaseDoEntity {

  private static final long serialVersionUID = 182329801186393449L;

    /** 用户ID  **/
    private Long userId;

    /** 角色ID  **/
    private Long roleId;



}


