package com.jhf.youke.base.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AreaDto extends BaseDtoEntity {

    private static final long serialVersionUID = 310788465893337471L;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "上级")
    private Long parentId;

    @ApiModelProperty(value = "上级树")
    private String parentIds;

    @ApiModelProperty(value = "区域类型")
    private Integer type;




}


