package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author  makejava
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AuditDto extends BaseDtoEntity {

    private static final long serialVersionUID = -37510088828467992L;

    @ApiModelProperty(value = "类型 1 团长注册审核")
    private Integer type;
    @ApiModelProperty(value = "用户ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;
    @ApiModelProperty(value = "用户名")
    private String userName;
    @ApiModelProperty(value = "团长名")
    private String recommendMan;
    @ApiModelProperty(value = "0待审核 1审核通过 2审核不通过")
    private Integer status;

}


