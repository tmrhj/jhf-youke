package com.jhf.youke.base.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AutoCompletedDto extends BaseDtoEntity {

    private static final long serialVersionUID = -92410021798933109L;


    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "sql")
    private String sqlstr;

    @ApiModelProperty(value = "微服务名")
    private String serverName;

    private String query;

    private Long companyId;

    private Long rootId;

    private Integer pageLimit;


}


