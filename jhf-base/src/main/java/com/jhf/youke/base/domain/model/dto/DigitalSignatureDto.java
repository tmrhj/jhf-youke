package com.jhf.youke.base.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class DigitalSignatureDto extends BaseDtoEntity {

    private static final long serialVersionUID = -45547599390367255L;

    @ApiModelProperty(value = "单位")
    private Long companyId;

    @ApiModelProperty(value = "盐值")
    private String salt;

    @ApiModelProperty(value = "私钥")
    private String privateKey;

    @ApiModelProperty(value = "公钥")
    private String publicKey;



}


