package com.jhf.youke.base.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class LogDto extends BaseDtoEntity {

    private static final long serialVersionUID = 697478058434054850L;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "用户昵称")
    private String userName;

    @ApiModelProperty(value = "用户类型")
    private Integer userType;

    @ApiModelProperty(value = "IP")
    private String loginIp;

    @ApiModelProperty(value = "登录地点")
    private String address;

    @ApiModelProperty(value = "浏览器")
    private String browser;

    @ApiModelProperty(value = "操作系统")
    private String systemName;

    @ApiModelProperty(value = "状态 0正常 1失败")
    private Integer status;

    @ApiModelProperty(value = "更新类型 1新增 2修改")
    private Integer updateType;

}


