package com.jhf.youke.base.domain.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;

/**
 * @author RHJ
 */
@Getter
public class LoginDto implements Serializable {

    @ApiModelProperty(value = "随机字符串", required=true)
    @NonNull
    private String nonceStr;

    @ApiModelProperty(value = "签名", required=true)
    @NonNull
    private String sign;

    @ApiModelProperty(value = "加密数据", required=true)
    @NonNull
    private String data;

    @ApiModelProperty(value = "单位标识", required=true)
    @NonNull
    private Long companyId;


}
