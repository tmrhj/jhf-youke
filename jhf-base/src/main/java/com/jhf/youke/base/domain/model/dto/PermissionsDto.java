package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class PermissionsDto extends BaseDtoEntity {

    private static final long serialVersionUID = 411201084224913134L;


    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "父类ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long parentId;

    @ApiModelProperty(value = "父类ID集合")
    private String parentIds;

    @ApiModelProperty(value = "1 目录 2 普通权限 3 数据权限")
    private Integer type;

    @ApiModelProperty(value = "编码")
    private String code;


    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "所在层级")
    private Integer layer;

}


