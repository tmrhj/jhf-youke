package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class PermissionsSetDto extends BaseDtoEntity {

    private static final long serialVersionUID = -40494711020479149L;

    private String name;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long permissionId;

    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    @ApiModelProperty(value = "1 角色 2 菜单 3 方法")
    private Integer type;

    @ApiModelProperty(value = "菜单ID/角色ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "方法路径")
    private String url;


    @ApiModelProperty(value = "内部方法状态 标识为1")
    private Integer insideStatus;

    @ApiModelProperty(value = "白名单方法状态 标识为1")
    private Integer whiteStatus;


}


