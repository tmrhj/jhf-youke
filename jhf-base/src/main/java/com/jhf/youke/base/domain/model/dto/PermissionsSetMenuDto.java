package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class PermissionsSetMenuDto extends BaseDtoEntity {

    private static final long serialVersionUID = -40494711020479149L;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long menuId;

    private List<String> permissionIdList = new ArrayList<>();

}


