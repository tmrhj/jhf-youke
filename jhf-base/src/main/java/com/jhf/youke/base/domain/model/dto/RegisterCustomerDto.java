package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
public class RegisterCustomerDto extends BaseDtoEntity {

    private static final long serialVersionUID = -81308384793849034L;

    @NotNull(message = "不能为空")
    @ApiModelProperty(value = "销售员/团长")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long saleMan;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "用户昵称")
    private String loginName;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "姓名")
    private String name;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "地区")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long areaId;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "小程序OpenID")
    private String openId;

}


