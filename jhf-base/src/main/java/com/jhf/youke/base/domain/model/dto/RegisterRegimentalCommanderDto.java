package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
public class RegisterRegimentalCommanderDto extends BaseDtoEntity {

    private static final long serialVersionUID = -81308384793849034L;

    @NotNull(message = "不能为空")
    @ApiModelProperty(value = "邀请人id")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long referrer;

    @ApiModelProperty(value = "用户名【团长名称】")
    private String loginName;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "姓名")
    private String name;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "手机号")
    private String phone;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "小程序OpenID")
    private String openId;

}


