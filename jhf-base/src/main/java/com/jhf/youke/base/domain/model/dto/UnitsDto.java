package com.jhf.youke.base.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class UnitsDto extends BaseDtoEntity {

    private static final long serialVersionUID = -54780033394562461L;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "状态 0无效 1有效")
    private Integer status;



}


