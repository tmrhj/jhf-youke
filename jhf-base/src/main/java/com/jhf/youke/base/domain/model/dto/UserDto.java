package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class UserDto extends BaseDtoEntity {

    private static final long serialVersionUID = -81308384793849034L;


    @ApiModelProperty(value = "用户名")
    private String loginName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "二级密码")
    private String passwordTwo;

    @ApiModelProperty(value = "盐值")
    private String salt;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "头像")
    private String headImage;

    @ApiModelProperty(value = "性别 0未知 1男 2女")
    private Integer sex;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "地区ID 关联区域表")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long areaId;

    @ApiModelProperty(value = "公众号OpenID")
    private String openId;

    @ApiModelProperty(value = "单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "根单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long rootId;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "生日")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    @ApiModelProperty(value = "状态  0：禁用   1：正常")
    private Integer status;

    @ApiModelProperty(value = "部门ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long deptId;

    @ApiModelProperty(value = "用户类型 1管理员 2B端用户 3供应链 4厂商 5 C端")
    private Integer type;


    private String oldPassword;

    private String newPassword1;

    private String newPassword2;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date startTime;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date endTime;

    @ApiModelProperty(value = "用户角色id集合")
    List<String> roleIds = new ArrayList<>();


    @ApiModelProperty(value = "用户自己及下属单位")
    private String companyIds;

    @ApiModelProperty(value = "身份 1 客户 2 团长")
    private Integer position;
}


