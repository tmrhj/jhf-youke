package com.jhf.youke.base.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class UserRoleDto extends BaseDtoEntity {

    private static final long serialVersionUID = 443494795584343416L;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "角色ID")
    private Long roleId;


}


