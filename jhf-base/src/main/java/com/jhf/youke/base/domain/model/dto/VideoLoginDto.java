package com.jhf.youke.base.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class VideoLoginDto implements Serializable {

    private static final long serialVersionUID = 851338877485453821L;

    private String code;

    private String openid;

    private String nickname;

    private Integer sex;

    private String headimgurl;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long rootId;

    private String companyName;

}
