package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_area")
public class AreaPo extends BasePoEntity {

    private static final long serialVersionUID = 557202404049845327L;

    /** 名字 **/
    private String name;

    /** 编码  **/
    private String code;

    /** 上级  **/
    private Long parentId;

    /** 上级树  **/
    private String parentIds;

    /** 区域类型  **/
    private Integer type;



}


