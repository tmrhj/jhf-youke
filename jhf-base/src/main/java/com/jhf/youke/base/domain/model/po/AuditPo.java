package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author  makejava
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_audit")
public class AuditPo extends BasePoEntity {

    private static final long serialVersionUID = -33970136235840417L;

    /**
     * 类型 1 团长注册审核
     */
    private Integer type;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 团长名
     */
    private String recommendMan;

    /**
     * 0待审核 1审核通过
     */
    private Integer status;



}


