package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_auto_completed")
public class AutoCompletedPo extends BasePoEntity {

    private static final long serialVersionUID = 800439220300327343L;

    /** 名称 **/
    private String name;

    /** sql **/
    private String sqlstr;

    /** 微服务名 **/
    private String serverName;



}


