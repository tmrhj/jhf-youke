package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_banner")
public class BannerPo extends BasePoEntity {

    private static final long serialVersionUID = 796380651353963280L;

    /** 单位ID **/
    private Long companyId;

    /** 标题 **/
    private String title;

    /** 链接地址 **/
    private String link;

    /** 图片地址 **/
    private String imgUrl;

    /** 1正常 2禁用 **/
    private Integer status;

    /** 排序 **/
    private Integer sort;


}


