package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_button")
public class ButtonPo extends BasePoEntity {

    private static final long serialVersionUID = -92836108200780630L;

    /** 名字 **/
    private String name;

    /** 当前状态 **/
    private Integer status;

    /** 下一状态 **/
    private Integer nextStatus;

    /** 按钮类型 **/
    private String type;

    /** 样式 **/
    private String css;

    /** 行方法 **/
    private String url;

    /** 函数名 **/
    private String function;

    /** 执行权限 **/
    private String permissions;



}


