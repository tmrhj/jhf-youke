package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_company")
public class CompanyPo extends BasePoEntity {

    private static final long serialVersionUID = -92448315732473223L;

    /** 名称 **/
    private String name;

    /** 根上级ID **/
    private Long rootId;

    /** 上级ID **/
    private Long parentId;

    /** 上级单位树 **/
    private String parentIds;

    /** 负责人 **/
    private String leader;

    /** 手机号码 **/
    private String phone;

    /** 类型 1公司 2部门 **/
    private Integer type;

    /** 排序 **/
    private Integer sort;



}


