package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_config")
public class ConfigPo extends BasePoEntity {

    private static final long serialVersionUID = -12337800111717939L;

    /** 名字 **/
    private String name;

    /** 编码 **/
    private String code;

    /**值 **/
    private String value;

    /**描述 **/
    private String description;

    /**排序 **/
    private Integer sort;



}


