package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_dept")
public class DeptPo extends BasePoEntity {

    private static final long serialVersionUID = -86140029077832149L;

    /** 学校ID **/
    private Long schoolId;

    /** 班级名称 **/
    private String name;

    /** 排序 **/
    private Integer sort;

    /** 企业微信中的部门ID **/
    private String wxDeptId;



}


