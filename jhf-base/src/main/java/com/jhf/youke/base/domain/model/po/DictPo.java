package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_dict")
public class DictPo extends BasePoEntity {

    private static final long serialVersionUID = -70197042448785350L;

    /** 表名 **/
    private String tableName;

    /** 字段名 **/
    private String fieldName;

    /** 值 **/
    private Integer value;

    /** 标签 **/
    private String label;

    /** 类型，可表+字段，全局唯一 **/
    private String type;

    /** 前端列表回显样式 **/
    private String listClass;

    /** 排序 **/
    private Integer sort;

    /** 描述 **/
    private String description;

    /** 字典名称 **/
    private String name;



}


