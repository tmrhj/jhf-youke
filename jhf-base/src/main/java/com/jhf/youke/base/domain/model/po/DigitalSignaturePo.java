package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_digital_signature")
public class DigitalSignaturePo extends BasePoEntity {

    private static final long serialVersionUID = 396240462558760604L;

    /**单位 **/
    private Long companyId;

    /**盐值 **/
    private String salt;

    /**私钥 **/
    private String privateKey;

    /**公钥 **/
    private String publicKey;



}


