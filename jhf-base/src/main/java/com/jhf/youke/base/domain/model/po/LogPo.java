package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_log")
public class LogPo extends BasePoEntity {

    private static final long serialVersionUID = 863669389143339925L;

    /** 用户ID **/
    private Long userId;

    /** 用户昵称 **/
    private String userName;

    /** 用户类型 **/
    private Integer userType;

    /** IP **/
    private String loginIp;

    /** 登录地点 **/
    private String address;

    /** 浏览器 **/
    private String browser;

    /** 操作系统 **/
    private String systemName;

    /** 状态 0正常 1失败 **/
    private Integer status;



}


