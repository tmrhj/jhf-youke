package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_menu")
public class MenuPo extends BasePoEntity {

    private static final long serialVersionUID = -41185490004233762L;

    /** 父菜单ID，一级菜单为0 **/
    private Long parentId;

    /** 菜单名称 **/
    private String name;

    /** 授权(多个用逗号分隔，如：user:list,user:create) **/
    private String perms;

    /** 类型   0：目录   1：菜单   2：按钮 **/
    private Integer type;

    /** 菜单图标 **/
    private String icon;

    /** 排序 **/
    private Integer orderNum;

    /** 路由地址 **/
    private String path;

    /** 组件路径 **/
    private String component;

    /** 是否为外链 0否 1是 **/
    private Integer isFrame;



}


