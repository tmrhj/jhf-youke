package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_object_status")
public class ObjectStatusPo extends BasePoEntity {

    private static final long serialVersionUID = 317016010256285176L;

    /** 业务对象ID **/
    private Integer objectId;

    /** 业务状态名称 **/
    private String name;

    /** 是否新建 **/
    private Integer isNew;

    /** 状态 0无效 1正常 **/
    private Integer status;

    /** 值 **/
    private Integer value;



}


