package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_permissions")
public class PermissionsPo extends BasePoEntity {

    private static final long serialVersionUID = 332089823830906364L;

    /** 名称 **/
    private String name;

    /** 父类ID **/
    private Long parentId;

    /** 父类ID集合 **/
    private String parentIds;

    /** 1 目录 2 普通权限 3 数据权限 **/
    private Integer type;

    /** 编码 **/
    private String code;

    /** 排序 **/
    private Integer sort;

    /** 所在层级 **/
    private Integer layer;



}


