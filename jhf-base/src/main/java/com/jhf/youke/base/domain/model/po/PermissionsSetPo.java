package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_permissions_set")
public class PermissionsSetPo extends BasePoEntity {

    private static final long serialVersionUID = 363755407589756605L;

    private String name;

    private Long permissionId;

    /** 权限编码 **/
    private String permissionCode;

    /** 1 角色 2 菜单 3 方法 **/
    private Integer type;

    /** 菜单ID/角色ID **/
    private Long bizId;

    /** 方法路径 **/
    private String url;

    /** 内部方法状态 标识为1 **/
    private Integer insideStatus;

    /** 白名单方法状态 标识为1 **/
    private Integer whiteStatus;

}


