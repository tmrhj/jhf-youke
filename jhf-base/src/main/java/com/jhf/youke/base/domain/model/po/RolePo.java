package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_role")
public class RolePo extends BasePoEntity {

    private static final long serialVersionUID = -73573134743717947L;

    /** 角色名称 **/
    private String name;

    /** 排序 **/
    private Integer sort;



}


