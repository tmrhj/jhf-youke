package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_units")
public class UnitsPo extends BasePoEntity {

    private static final long serialVersionUID = -12974081391987353L;

    /** 名字  **/
    private String name;

    /** 状态 0无效 1有效  **/
    private Integer status;



}


