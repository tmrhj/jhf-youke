package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_user")
public class UserPo extends BasePoEntity {

    private static final long serialVersionUID = 314240427920643212L;

    /** 用户名  **/
    private String loginName;

    /** 密码  **/
    private String password;

    /** 二级密码  **/
    private String passwordTwo;

    /** 盐值  **/
    private String salt;

    /** 姓名  **/
    private String name;

    /** 头像  **/
    private String headImage;

    /** 性别 0未知 1男 2女  **/
    private Integer sex;

    /** 手机号  **/
    private String phone;

    /** 地区ID 关联区域表  **/
    private Long areaId;

    /** 公众号OpenID  **/
    private String openId;

    /** 单位ID  **/
    private Long companyId;

    /** 根单位ID  **/
    private Long rootId;

    /** 单位名称  **/
    private String companyName;

    /** 详细地址  **/
    private String address;

    /** 生日  **/
    private Date birthday;

    /** 状态  0：禁用   1：正常  **/
    private Integer status;

    /** 部门ID  **/
    private Long deptId;

    /** 部门名称  **/
    @TableField(exist=false)
    private String deptName;

    /** 用户类型 1管理员 2B端用户 3供应链 4厂商 5 C端  **/
    private Integer type;

    /**
     * 身份 1 客户 2 团长
     */
    private Integer position;

    @TableField(exist=false)
    private Date startTime;

    @TableField(exist=false)
    private Date endTime;

    @TableField(exist=false)
    private String companyIds;
}


