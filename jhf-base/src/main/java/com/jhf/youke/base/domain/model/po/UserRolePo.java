package com.jhf.youke.base.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "base_user_role")
public class UserRolePo extends BasePoEntity {

    private static final long serialVersionUID = 734087493153232682L;

    /** 用户ID  **/
    private Long userId;

    /** 角色ID  **/
    private Long roleId;



}


