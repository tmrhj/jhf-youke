package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AreaVo extends BaseVoEntity {

    private static final long serialVersionUID = -83358732097182251L;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "上级")
    private Long parentId;

    @ApiModelProperty(value = "上级树")
    private String parentIds;

    @ApiModelProperty(value = "区域类型")
    private Integer type;




}


