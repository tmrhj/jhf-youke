package com.jhf.youke.base.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author  makejava
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AuditVo extends BaseVoEntity {

    private static final long serialVersionUID = -79205972176644820L;


    @ApiModelProperty(value = "类型 1 团长注册审核")
    private Integer type;

    @ApiModelProperty(value = "用户ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "团长名")
    private String recommendMan;

    @ApiModelProperty(value = "0待审核 1审核通过")
    private Integer status;

}


