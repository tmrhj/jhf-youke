package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AutoCompletedVo extends BaseVoEntity {

    private static final long serialVersionUID = 208975521362445261L;


    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "sql")
    private String sqlstr;

    @ApiModelProperty(value = "微服务名")
    private String serverName;


}


