package com.jhf.youke.base.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class BannerVo extends BaseVoEntity {

    private static final long serialVersionUID = 193060428704812382L;

    @ApiModelProperty(value = "单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "链接地址")
    private String link;

    @ApiModelProperty(value = "图片地址")
    private String imgUrl;

    @ApiModelProperty(value = "1正常 2禁用")
    private Integer status;

    @ApiModelProperty(value = "排序")
    private Integer sort;


}


