package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ButtonVo extends BaseVoEntity {

    private static final long serialVersionUID = -17057230869430023L;



    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "当前状态")
    private Integer status;

    @ApiModelProperty(value = "下一状态")
    private Integer nextStatus;

    @ApiModelProperty(value = "按钮类型")
    private String type;

    @ApiModelProperty(value = "样式")
    private String css;

    @ApiModelProperty(value = "执行方法")
    private String url;


    @ApiModelProperty(value = "函数名")
    private String function;

    @ApiModelProperty(value = "执行权限")
    private String permissions;

}


