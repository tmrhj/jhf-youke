package com.jhf.youke.base.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class CompanyVo extends BaseVoEntity {

    private static final long serialVersionUID = 268134273441127996L;


    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "根上级ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long rootId;

    @ApiModelProperty(value = "上级ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long parentId;

    @ApiModelProperty(value = "上级单位树")
    private String parentIds;

    @ApiModelProperty(value = "负责人")
    private String leader;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "类型 1公司 2部门")
    private Integer type;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    List<CompanyVo> children = new ArrayList<>();

}


