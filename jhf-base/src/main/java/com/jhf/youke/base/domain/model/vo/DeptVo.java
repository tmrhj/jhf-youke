package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class DeptVo extends BaseVoEntity {

    private static final long serialVersionUID = 989092760499185762L;


    @ApiModelProperty(value = "学校ID")
    private Long schoolId;

    @ApiModelProperty(value = "班级名称")
    private String name;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "企业微信中的部门ID")
    private String wxDeptId;


}


