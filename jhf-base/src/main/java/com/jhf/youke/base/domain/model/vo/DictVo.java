package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class DictVo extends BaseVoEntity {

    private static final long serialVersionUID = 228160735580343812L;


    @ApiModelProperty(value = "字典名称")
    private String name;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "字段名")
    private String fieldName;

    @ApiModelProperty(value = "值")
    private Integer value;

    @ApiModelProperty(value = "标签")
    private String label;

    @ApiModelProperty(value = "类型，可表+字段，全局唯一")
    private String type;

    @ApiModelProperty(value = "前端列表回显样式")
    private String listClass;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "描述")
    private String description;

}


