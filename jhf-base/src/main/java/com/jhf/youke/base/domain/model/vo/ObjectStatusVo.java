package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ObjectStatusVo extends BaseVoEntity {

    private static final long serialVersionUID = -99304931292744157L;




    @ApiModelProperty(value = "业务对象ID")
    private Integer objectId;

    @ApiModelProperty(value = "业务状态名称")
    private String name;

    @ApiModelProperty(value = "是否新建")
    private Integer isNew;

    @ApiModelProperty(value = "状态 0无效 1正常")
    private Integer status;

    @ApiModelProperty(value = "值")
    private Integer value;



}


