package com.jhf.youke.base.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jhf.youke.core.utils.Constant;
import com.jhf.youke.core.utils.StringUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 路由配置信息
 * @author RHJ
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouterVo {
    /**
     * 路由名字
     */
    private String name;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现
     */
    private boolean hidden;

    /**
     * 重定向地址，当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
     */
    private String redirect;

    /**
     * 组件地址
     */
    private String component;

    /**
     * 路由参数：如 {"id": 1, "name": "ry"}
     */
    private String query;

    /**
     * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
     */
    private Boolean alwaysShow;

    /**
     * 其他元素
     */
    private MetaVo meta;

    /**
     * 子路由
     */
    private List<RouterVo> children;

    public RouterVo(){

    }

    public RouterVo(MenuVo menu) {
        this.name = getRouteName(menu);
        this.path = getRouterPath(menu);
        this.component = getComponent(menu);
        this.meta = new MetaVo(menu.getName(), menu.getIcon(), false, menu.getPath());
        if (!menu.getSubMenus().isEmpty() && menu.getSubMenus().size() > 0 && menu.getType() == 0) {
            this.alwaysShow = true;
            this.redirect = "noRedirect";
        } else if (isMenuFrame(menu)){
            this.meta = null;
            List<RouterVo> childrenList = new ArrayList<RouterVo>();
            RouterVo children = new RouterVo();
            children.setPath(menu.getPath());
            children.setComponent(menu.getComponent());
            children.setName(StringUtils.capitalize(menu.getPath()));
            children.setMeta(new MetaVo(menu.getName(), menu.getIcon(), false, menu.getPath()));
            childrenList.add(children);
            this.children = childrenList;
        }else if (menu.getParentId().intValue() == 0 && isInnerLink(menu)) {
            this.meta = new MetaVo(menu.getName(), menu.getIcon());
            this.path = "/";
            List<RouterVo> childrenList = new ArrayList<RouterVo>();
            RouterVo children = new RouterVo();
            String routerPath = innerLinkReplaceEach(menu.getPath());
            children.setPath(routerPath);
            children.setComponent(Constant.INNER_LINK);
            children.setName(StringUtils.capitalize(routerPath));
            children.setMeta(new MetaVo(menu.getName(), menu.getIcon(), menu.getPath()));
            childrenList.add(children);
            this.children = childrenList;
        }
    }

    /**
     * 获取路由名称
     *
     * @param menu 菜单信息
     * @return 路由名称
     */
    public String getRouteName(MenuVo menu)
    {
        String routerName = StringUtils.capitalize(menu.getPath());
        // 非外链并且是一级目录（类型为目录）
        if (isMenuFrame(menu))
        {
            routerName = StringUtils.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取路由地址
     *
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(MenuVo menu)
    {
        String routerPath = menu.getPath();
        // 内链打开外网方式
        if (menu.getParentId().intValue() != 0 && isInnerLink(menu))
        {
            routerPath = innerLinkReplaceEach(routerPath);
        }
        // 非外链并且是一级目录（类型为目录）
        if (0 == menu.getParentId().intValue() && menu.getType() == 0
                && menu.getIsFrame() == 0)
        {
            routerPath = "/" + menu.getPath();
        }
        // 非外链并且是一级目录（类型为菜单）
        else if (isMenuFrame(menu))
        {
            routerPath = "/";
        }
        return routerPath;
    }


    public String innerLinkReplaceEach(String path)
    {
        return StringUtils.replaceEach(path, new String[] { Constant.HTTP, Constant.HTTPS },
                new String[] { "", "" });
    }

    /**
     * 是否为内链组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isInnerLink(MenuVo menu)
    {
        return menu.getIsFrame() == 0 && StringUtils.ishttp(menu.getPath());
    }

    public boolean isMenuFrame(MenuVo menu)
    {
        return menu.getParentId().intValue() == 0 && menu.getType() == 1
                && menu.getIsFrame() == 0;
    }

    /**
     * 获取组件信息
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(MenuVo menu)
    {
        String component = Constant.LAYOUT;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isMenuFrame(menu))
        {
            component = menu.getComponent();
        }
        else if (StringUtils.isEmpty(menu.getComponent()) && menu.getParentId().intValue() != 0 && isInnerLink(menu))
        {
            component = Constant.INNER_LINK;
        }
        else if (StringUtils.isEmpty(menu.getComponent()) && isParentView(menu))
        {
            component = Constant.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为parent_view组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isParentView(MenuVo menu)
    {
        return menu.getParentId().intValue() != 0 && menu.getType() == 0;
    }




}
