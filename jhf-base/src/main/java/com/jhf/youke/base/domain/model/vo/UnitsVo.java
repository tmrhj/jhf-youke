package com.jhf.youke.base.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class UnitsVo extends BaseVoEntity {

    private static final long serialVersionUID = -82449924251005794L;


    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "状态 0无效 1有效")
    private Integer status;



}


