package com.jhf.youke.base.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class UserVo extends BaseVoEntity {

    private static final long serialVersionUID = 570280655529227149L;



    @ApiModelProperty(value = "用户名")
    private String loginName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "二级密码")
    private String passwordTwo;

    @ApiModelProperty(value = "盐值")
    private String salt;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "头像")
    private String headImage;

    @ApiModelProperty(value = "身份证")
    private String idCard;

    @ApiModelProperty(value = "性别 0未知 1男 2女")
    private Integer sex;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "地区ID 关联区域表")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long areaId;

    @ApiModelProperty(value = "公众号OpenID")
    private String openId;

    @ApiModelProperty(value = "单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "根单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long rootId;

    @ApiModelProperty(value = "单位名称")
    private String companyName;


    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "生日")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    @ApiModelProperty(value = "状态  0：禁用   1：正常")
    private Integer status;

    @ApiModelProperty(value = "部门ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "用户类型 1管理员 2B端用户 3供应链 4厂商 5 C端")
    private Integer type;

    @ApiModelProperty(value = "用户角色id集合")
    List<String> roleIds = new ArrayList<>();

    @ApiModelProperty(value = "身份 1 客户 2 团长")
    private Integer position;

}


