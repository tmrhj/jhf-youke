package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.AreaConverter;
import com.jhf.youke.base.domain.model.Do.AreaDo;
import com.jhf.youke.base.domain.gateway.AreaRepository;
import com.jhf.youke.base.domain.model.po.AreaPo;
import com.jhf.youke.base.domain.model.vo.AreaVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class AreaService extends AbstractDomainService<AreaRepository, AreaDo, AreaVo> {


    @Resource
    AreaConverter areaConverter;

    @Override
    public boolean update(AreaDo entity) {
        AreaPo areaPo = areaConverter.do2Po(entity);
        areaPo.preUpdate();
        return repository.update(areaPo);
    }

    @Override
    public boolean updateBatch(List<AreaDo> doList) {
        List<AreaPo> poList = areaConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(AreaDo entity) {
        AreaPo areaPo = areaConverter.do2Po(entity);
        return repository.delete(areaPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(AreaDo entity) {
        AreaPo areaPo = areaConverter.do2Po(entity);
        areaPo.preInsert();
        return repository.insert(areaPo);
    }

    @Override
    public boolean insertBatch(List<AreaDo> doList) {
        List<AreaPo> poList = areaConverter.do2PoList(doList);
        poList = AreaPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<AreaVo> findById(Long id) {
        Optional<AreaPo> areaPo =  repository.findById(id);
        AreaVo areaVo = areaConverter.po2Vo(areaPo.orElse(new AreaPo()));
        return Optional.ofNullable(areaVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<AreaVo> findAllMatching(AreaDo entity) {
        AreaPo areaPo = areaConverter.do2Po(entity);
        List<AreaPo>areaPoList =  repository.findAllMatching(areaPo);
        return areaConverter.po2VoList(areaPoList);
    }


    @Override
    public Pagination<AreaVo> selectPage(AreaDo entity){
        AreaPo areaPo = areaConverter.do2Po(entity);
        PageQuery<AreaPo> pageQuery = new PageQuery<>(areaPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<AreaPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<AreaVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                areaConverter.po2VoList(pagination.getList()));
    }


}

