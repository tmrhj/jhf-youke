package com.jhf.youke.base.domain.service;

import cn.hutool.core.util.NumberUtil;
import com.jhf.youke.base.domain.converter.AuditConverter;
import com.jhf.youke.base.domain.enums.AuditEnum;
import com.jhf.youke.base.domain.gateway.UserRepository;
import com.jhf.youke.base.domain.model.Do.AuditDo;
import com.jhf.youke.base.domain.gateway.AuditRepository;
import com.jhf.youke.base.domain.model.po.AuditPo;
import com.jhf.youke.base.domain.model.po.UserPo;
import com.jhf.youke.base.domain.model.vo.AuditVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.Constant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  makejava
 **/

@Service
public class AuditService extends AbstractDomainService<AuditRepository, AuditDo, AuditVo> {


    @Resource
    AuditConverter auditConverter;

    @Resource
    UserRepository userRepository;

    @Override
    public boolean update(AuditDo entity) {
        AuditPo auditPo = auditConverter.do2Po(entity);
        auditPo.preUpdate();
        return repository.update(auditPo);
    }

    @Override
    public boolean updateBatch(List<AuditDo> doList) {
        List<AuditPo> poList = auditConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(AuditDo entity) {
        AuditPo auditPo = auditConverter.do2Po(entity);
        return repository.delete(auditPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(AuditDo entity) {
        AuditPo auditPo = auditConverter.do2Po(entity);
        auditPo.preInsert();
        return repository.insert(auditPo);
    }

    @Override
    public boolean insertBatch(List<AuditDo> doList) {
        List<AuditPo> poList = auditConverter.do2PoList(doList);
        AuditPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<AuditVo> findById(Long id) {
        Optional<AuditPo> auditPo =  repository.findById(id);
        AuditVo auditVo = auditConverter.po2Vo(auditPo.orElse(new AuditPo()));
        return Optional.ofNullable(auditVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<AuditVo> findAllMatching(AuditDo entity) {
        AuditPo auditPo = auditConverter.do2Po(entity);
        List<AuditPo>auditPoList =  repository.findAllMatching(auditPo);
        return auditConverter.po2VoList(auditPoList);
    }


    @Override
    public Pagination<AuditVo> selectPage(AuditDo entity){
        AuditPo auditPo = auditConverter.do2Po(entity);
        PageQuery<AuditPo> pageQuery = new PageQuery<>(auditPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<AuditPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                auditConverter.po2VoList(pagination.getList()));
    }

    public AuditPo saveRegimentalCommander(AuditDo auditDo, Long referrer) {
        Optional<UserPo> userOption = userRepository.findById(referrer);
        assert !userOption.isEmpty() : "推荐人不能为空";
        auditDo.setRecommendMan(userOption.get().getName());
        AuditPo auditPo = auditConverter.do2Po(auditDo);
        auditPo.preInsert();
        repository.insert(auditPo);
        return auditPo;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean auditUser(Long id, Integer status) {
        Optional<AuditPo> auditPoOptional = repository.findById(id);
        assert !auditPoOptional.isEmpty() : "查询不到数据";
        AuditPo auditPo = auditPoOptional.get();
        Optional<UserPo> userPoOptional = userRepository.findById(auditPo.getUserId());
        assert !userPoOptional.isEmpty() : "查询不到数据";
        UserPo userPo = userPoOptional.get();
        if (status == AuditEnum.STATUS_AGREE.code()) {
            userPo.setStatus(1);
        } else if (status == AuditEnum.STATUS_DISAGREE.code()) {
            userPo.setDelFlag("1");
        }
        userPo.preUpdate();
        userRepository.update(userPo);
        auditPo.preUpdate();
        auditPo.setStatus(status);
        repository.update(auditPo);
        return true;
    }

}

