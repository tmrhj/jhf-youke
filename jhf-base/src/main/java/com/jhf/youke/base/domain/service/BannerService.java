package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.BannerConverter;
import com.jhf.youke.base.domain.model.Do.BannerDo;
import com.jhf.youke.base.domain.gateway.BannerRepository;
import com.jhf.youke.base.domain.model.po.BannerPo;
import com.jhf.youke.base.domain.model.vo.BannerVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class BannerService extends AbstractDomainService<BannerRepository, BannerDo, BannerVo> {


    @Resource
    BannerConverter bannerConverter;

    @Override
    public boolean update(BannerDo entity) {
        BannerPo bannerPo = bannerConverter.do2Po(entity);
        bannerPo.preUpdate();
        return repository.update(bannerPo);
    }

    @Override
    public boolean updateBatch(List<BannerDo> doList) {
        List<BannerPo> poList = bannerConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(BannerDo entity) {
        BannerPo bannerPo = bannerConverter.do2Po(entity);
        return repository.delete(bannerPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(BannerDo entity) {
        BannerPo bannerPo = bannerConverter.do2Po(entity);
        bannerPo.preInsert();
        return repository.insert(bannerPo);
    }

    @Override
    public boolean insertBatch(List<BannerDo> doList) {
        List<BannerPo> poList = bannerConverter.do2PoList(doList);
        poList = BannerPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<BannerVo> findById(Long id) {
        Optional<BannerPo> bannerPo =  repository.findById(id);
        BannerVo bannerVo = bannerConverter.po2Vo(bannerPo.orElse(new BannerPo()));
        return Optional.ofNullable(bannerVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<BannerVo> findAllMatching(BannerDo entity) {
        entity.setDelFlag("0");
        BannerPo bannerPo = bannerConverter.do2Po(entity);
        List<BannerPo>bannerPoList =  repository.findAllMatching(bannerPo);
        return bannerConverter.po2VoList(bannerPoList);
    }


    @Override
    public Pagination<BannerVo> selectPage(BannerDo entity){
        entity.setDelFlag("0");
        BannerPo bannerPo = bannerConverter.do2Po(entity);
        PageQuery<BannerPo> pageQuery = new PageQuery<>(bannerPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<BannerPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                bannerConverter.po2VoList(pagination.getList()));
    }


}

