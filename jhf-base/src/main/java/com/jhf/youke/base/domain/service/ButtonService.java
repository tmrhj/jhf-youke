package com.jhf.youke.base.domain.service;

import cn.hutool.json.JSONUtil;
import com.jhf.youke.base.domain.converter.ButtonConverter;
import com.jhf.youke.base.domain.model.Do.ButtonDo;
import com.jhf.youke.base.domain.gateway.ButtonRepository;
import com.jhf.youke.base.domain.model.po.ButtonPo;
import com.jhf.youke.base.domain.model.vo.ButtonVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.CacheUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class ButtonService extends AbstractDomainService<ButtonRepository, ButtonDo, ButtonVo> {


    @Resource
    ButtonConverter buttonConverter;

    @Override
    public boolean update(ButtonDo entity) {
        ButtonPo buttonPo = buttonConverter.do2Po(entity);
        buttonPo.preUpdate();
        return repository.update(buttonPo);
    }

    @Override
    public boolean updateBatch(List<ButtonDo> doList) {
        List<ButtonPo> poList = buttonConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ButtonDo entity) {
        ButtonPo buttonPo = buttonConverter.do2Po(entity);
        return repository.delete(buttonPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ButtonDo entity) {
        ButtonPo buttonPo = buttonConverter.do2Po(entity);
        buttonPo.preInsert();
        return repository.insert(buttonPo);
    }

    @Override
    public boolean insertBatch(List<ButtonDo> doList) {
        List<ButtonPo> poList = buttonConverter.do2PoList(doList);
        poList = ButtonPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ButtonVo> findById(Long id) {
        Optional<ButtonPo> buttonPo =  repository.findById(id);
        ButtonVo buttonVo = buttonConverter.po2Vo(buttonPo.orElse(new ButtonPo()));
        return Optional.ofNullable(buttonVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ButtonVo> findAllMatching(ButtonDo entity) {
        ButtonPo buttonPo = buttonConverter.do2Po(entity);
        List<ButtonPo>buttonPoList =  repository.findAllMatching(buttonPo);
        return buttonConverter.po2VoList(buttonPoList);
    }


    @Override
    public Pagination<ButtonVo> selectPage(ButtonDo entity){
        ButtonPo buttonPo = buttonConverter.do2Po(entity);
        PageQuery<ButtonPo> pageQuery = new PageQuery<>(buttonPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ButtonPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                buttonConverter.po2VoList(pagination.getList()));
    }

    public List<ButtonVo> getListByStatus(Integer status)throws Exception{
        String key = "button_list_by_" + status;
        List<ButtonVo> list = CacheUtils.getList(key, ButtonVo.class);
        if (list == null) {
            List<ButtonPo> buttonPoList = repository.getListByStatus(status);
            list = buttonConverter.po2VoList(buttonPoList);
            CacheUtils.set(key, JSONUtil.toJsonStr(list), 20 * 60 * 60);
        }
        return list;
    }


}

