package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.ConfigConverter;
import com.jhf.youke.base.domain.model.Do.ConfigDo;
import com.jhf.youke.base.domain.gateway.ConfigRepository;
import com.jhf.youke.base.domain.model.po.ConfigPo;
import com.jhf.youke.base.domain.model.vo.ConfigVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class ConfigService extends AbstractDomainService<ConfigRepository, ConfigDo, ConfigVo> {


    @Resource
    ConfigConverter configConverter;

    @Override
    public boolean update(ConfigDo entity) {
        ConfigPo configPo = configConverter.do2Po(entity);
        configPo.preUpdate();
        return repository.update(configPo);
    }

    @Override
    public boolean updateBatch(List<ConfigDo> doList) {
        List<ConfigPo> poList = configConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ConfigDo entity) {
        ConfigPo configPo = configConverter.do2Po(entity);
        return repository.delete(configPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ConfigDo entity) {
        ConfigPo configPo = configConverter.do2Po(entity);
        configPo.preInsert();
        return repository.insert(configPo);
    }

    @Override
    public boolean insertBatch(List<ConfigDo> doList) {
        List<ConfigPo> poList = configConverter.do2PoList(doList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ConfigVo> findById(Long id) {
        Optional<ConfigPo> configPo =  repository.findById(id);
        ConfigVo configVo = configConverter.po2Vo(configPo.orElse(new ConfigPo()));
        return Optional.ofNullable(configVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ConfigVo> findAllMatching(ConfigDo entity) {
        ConfigPo configPo = configConverter.do2Po(entity);
        List<ConfigPo>configPoList =  repository.findAllMatching(configPo);
        return configConverter.po2VoList(configPoList);
    }

    @Override
    public Pagination<ConfigVo> selectPage(ConfigDo entity){
        ConfigPo configPo = configConverter.do2Po(entity);
        PageQuery<ConfigPo> pageQuery = new PageQuery<>(configPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ConfigPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<ConfigVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                configConverter.po2VoList(pagination.getList()));
    }

}

