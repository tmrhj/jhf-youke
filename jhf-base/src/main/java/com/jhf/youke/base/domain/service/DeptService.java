package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.DeptConverter;
import com.jhf.youke.base.domain.model.Do.DeptDo;
import com.jhf.youke.base.domain.gateway.DeptRepository;
import com.jhf.youke.base.domain.model.po.DeptPo;
import com.jhf.youke.base.domain.model.vo.DeptVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class DeptService extends AbstractDomainService<DeptRepository, DeptDo, DeptVo> {


    @Resource
    DeptConverter deptConverter;

    @Override
    public boolean update(DeptDo entity) {
        DeptPo deptPo = deptConverter.do2Po(entity);
        return repository.update(deptPo);
    }

    @Override
    public boolean updateBatch(List<DeptDo> doList) {
        List<DeptPo> poList = deptConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(DeptDo entity) {
        DeptPo deptPo = deptConverter.do2Po(entity);
        return repository.delete(deptPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(DeptDo entity) {
        DeptPo deptPo = deptConverter.do2Po(entity);
        return repository.insert(deptPo);
    }

    @Override
    public boolean insertBatch(List<DeptDo> doList) {
        List<DeptPo> poList = deptConverter.do2PoList(doList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<DeptVo> findById(Long id) {
        Optional<DeptPo> deptPo =  repository.findById(id);
        DeptVo deptVo = deptConverter.po2Vo(deptPo.orElse(new DeptPo()));
        return Optional.ofNullable(deptVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<DeptVo> findAllMatching(DeptDo entity) {
        DeptPo deptPo = deptConverter.do2Po(entity);
        List<DeptPo>deptPoList =  repository.findAllMatching(deptPo);
        return deptConverter.po2VoList(deptPoList);
    }


    @Override
    public Pagination<DeptVo> selectPage(DeptDo entity){
        DeptPo deptPo = deptConverter.do2Po(entity);
        PageQuery<DeptPo> pageQuery = new PageQuery<>(deptPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<DeptPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<DeptVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                deptConverter.po2VoList(pagination.getList()));
    }


}

