package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.DictConverter;
import com.jhf.youke.base.domain.gateway.DictRepository;
import com.jhf.youke.base.domain.model.Do.DictDo;
import com.jhf.youke.base.domain.model.po.DictPo;
import com.jhf.youke.base.domain.model.vo.DictVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Service
public class DictService extends AbstractDomainService<DictRepository, DictDo, DictVo> {


    @Resource
    DictConverter dictConverter;

    @Override
    public boolean update(DictDo entity) {
        DictPo dictPo = dictConverter.do2Po(entity);
        dictPo.preUpdate();
        return repository.update(dictPo);
    }

    @Override
    public boolean updateBatch(List<DictDo> doList) {
        List<DictPo> poList = dictConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(DictDo entity) {
        DictPo dictPo = dictConverter.do2Po(entity);
        return repository.delete(dictPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(DictDo entity) {
        DictPo dictPo = dictConverter.do2Po(entity);
        dictPo.preInsert();
        return repository.insert(dictPo);
    }

    @Override
    public boolean insertBatch(List<DictDo> doList) {
        List<DictPo> poList = dictConverter.do2PoList(doList);
        DictPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<DictVo> findById(Long id) {
        Optional<DictPo> dictPo =  repository.findById(id);
        DictVo dictVo = dictConverter.po2Vo(dictPo.orElse(new DictPo()));
        return Optional.ofNullable(dictVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<DictVo> findAllMatching(DictDo entity) {
        DictPo dictPo = dictConverter.do2Po(entity);
        List<DictPo>dictPoList =  repository.findAllMatching(dictPo);
        return dictConverter.po2VoList(dictPoList);
    }


    @Override
    public Pagination<DictVo> selectPage(DictDo entity){
        DictPo dictPo = dictConverter.do2Po(entity);
        PageQuery<DictPo> pageQuery = new PageQuery<>(dictPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<DictPo> pagination = repository.selectPage(pageQuery);
        Pagination<DictVo> a = new Pagination<DictVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                dictConverter.po2VoList(pagination.getList()));
        return a;
    }


}

