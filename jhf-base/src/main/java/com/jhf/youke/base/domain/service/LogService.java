package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.LogConverter;
import com.jhf.youke.base.domain.gateway.LogRepository;
import com.jhf.youke.base.domain.model.Do.LogDo;
import com.jhf.youke.base.domain.model.po.LogPo;
import com.jhf.youke.base.domain.model.vo.LogVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;



/**
 * @author RHJ
 */
@Service
public class LogService extends AbstractDomainService<LogRepository, LogDo, LogVo> {


    @Resource
    LogConverter logConverter;

    @Override
    public boolean update(LogDo entity) {
        LogPo logPo = logConverter.do2Po(entity);
        return repository.update(logPo);
    }

    @Override
    public boolean updateBatch(List<LogDo> doList) {
        List<LogPo> poList = logConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(LogDo entity) {
        LogPo logPo = logConverter.do2Po(entity);
        return repository.delete(logPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(LogDo entity) {
        LogPo logPo = logConverter.do2Po(entity);
        return repository.insert(logPo);
    }

    @Override
    public boolean insertBatch(List<LogDo> doList) {
        List<LogPo> poList = logConverter.do2PoList(doList);
        LogPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<LogVo> findById(Long id) {
        Optional<LogPo> logPo =  repository.findById(id);
        LogVo logVo = logConverter.po2Vo(logPo.orElse(new LogPo()));
        return Optional.ofNullable(logVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<LogVo> findAllMatching(LogDo entity) {
        LogPo logPo = logConverter.do2Po(entity);
        List<LogPo>logPoList =  repository.findAllMatching(logPo);
        return logConverter.po2VoList(logPoList);
    }


    @Override
    public Pagination<LogVo> selectPage(LogDo entity){
        LogPo logPo = logConverter.do2Po(entity);
        PageQuery<LogPo> pageQuery = new PageQuery<>(logPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<LogPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<LogVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                logConverter.po2VoList(pagination.getList()));
    }


}

