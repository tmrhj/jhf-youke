package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.ObjectStatusConverter;
import com.jhf.youke.base.domain.model.Do.ObjectStatusDo;
import com.jhf.youke.base.domain.gateway.ObjectStatusRepository;
import com.jhf.youke.base.domain.model.po.ObjectStatusPo;
import com.jhf.youke.base.domain.model.vo.ObjectStatusVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Service
public class ObjectStatusService extends AbstractDomainService<ObjectStatusRepository, ObjectStatusDo, ObjectStatusVo> {


    @Resource
    ObjectStatusConverter objectStatusConverter;

    @Override
    public boolean update(ObjectStatusDo entity) {
        ObjectStatusPo objectStatusPo = objectStatusConverter.do2Po(entity);
        objectStatusPo.preUpdate();
        return repository.update(objectStatusPo);
    }

    @Override
    public boolean updateBatch(List<ObjectStatusDo> doList) {
        List<ObjectStatusPo> poList = objectStatusConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ObjectStatusDo entity) {
        ObjectStatusPo objectStatusPo = objectStatusConverter.do2Po(entity);
        return repository.delete(objectStatusPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ObjectStatusDo entity) {
        ObjectStatusPo objectStatusPo = objectStatusConverter.do2Po(entity);
        objectStatusPo.preInsert();
        return repository.insert(objectStatusPo);
    }

    @Override
    public boolean insertBatch(List<ObjectStatusDo> doList) {
        List<ObjectStatusPo> poList = objectStatusConverter.do2PoList(doList);
        poList = ObjectStatusPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ObjectStatusVo> findById(Long id) {
        Optional<ObjectStatusPo> objectStatusPo =  repository.findById(id);
        ObjectStatusVo objectStatusVo = objectStatusConverter.po2Vo(objectStatusPo.orElse(new ObjectStatusPo()));
        return Optional.ofNullable(objectStatusVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ObjectStatusVo> findAllMatching(ObjectStatusDo entity) {
        ObjectStatusPo objectStatusPo = objectStatusConverter.do2Po(entity);
        List<ObjectStatusPo>objectStatusPoList =  repository.findAllMatching(objectStatusPo);
        return objectStatusConverter.po2VoList(objectStatusPoList);
    }


    @Override
    public Pagination<ObjectStatusVo> selectPage(ObjectStatusDo entity){
        ObjectStatusPo objectStatusPo = objectStatusConverter.do2Po(entity);
        PageQuery<ObjectStatusPo> pageQuery = new PageQuery<>(objectStatusPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ObjectStatusPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                objectStatusConverter.po2VoList(pagination.getList()));
    }


}

