package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.RoleConverter;
import com.jhf.youke.base.domain.model.Do.RoleDo;
import com.jhf.youke.base.domain.gateway.RoleRepository;
import com.jhf.youke.base.domain.model.po.RolePo;
import com.jhf.youke.base.domain.model.vo.RoleVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class RoleService extends AbstractDomainService<RoleRepository, RoleDo, RoleVo> {


    @Resource
    RoleConverter roleConverter;

    @Resource
    UserRoleService userRoleService;

    @Override
    public boolean update(RoleDo entity) {
        RolePo rolePo = roleConverter.do2Po(entity);
        rolePo.preUpdate();
        return repository.update(rolePo);
    }

    @Override
    public boolean updateBatch(List<RoleDo> doList) {
        List<RolePo> poList = roleConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(RoleDo entity) {
        RolePo rolePo = roleConverter.do2Po(entity);
        return repository.delete(rolePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(RoleDo entity) {
        RolePo rolePo = roleConverter.do2Po(entity);
        rolePo.preInsert();
        return repository.insert(rolePo);
    }

    public RolePo save(RoleDo entity) {
        RolePo rolePo = roleConverter.do2Po(entity);
        rolePo.preInsert();
        repository.insert(rolePo);
        return rolePo;
    }

    @Override
    public boolean insertBatch(List<RoleDo> doList) {
        List<RolePo> poList = roleConverter.do2PoList(doList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<RoleVo> findById(Long id) {
        Optional<RolePo> rolePo =  repository.findById(id);
        RoleVo roleVo = roleConverter.po2Vo(rolePo.orElse(new RolePo()));
        return Optional.ofNullable(roleVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<RoleVo> findAllMatching(RoleDo entity) {
        RolePo rolePo = roleConverter.do2Po(entity);
        rolePo.setDelFlag("0");
        List<RolePo>rolePoList =  repository.findAllMatching(rolePo);
        return roleConverter.po2VoList(rolePoList);
    }


    @Override
    public Pagination<RoleVo> selectPage(RoleDo entity){
        RolePo rolePo = roleConverter.do2Po(entity);
        rolePo.setDelFlag("0");
        PageQuery<RolePo> pageQuery = new PageQuery<>(rolePo, entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<RolePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<RoleVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                roleConverter.po2VoList(pagination.getList()));
    }


}

