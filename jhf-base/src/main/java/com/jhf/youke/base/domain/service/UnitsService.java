package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.UnitsConverter;
import com.jhf.youke.base.domain.gateway.UnitsRepository;
import com.jhf.youke.base.domain.model.Do.UnitsDo;
import com.jhf.youke.base.domain.model.po.UnitsPo;
import com.jhf.youke.base.domain.model.vo.UnitsVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class UnitsService extends AbstractDomainService<UnitsRepository, UnitsDo, UnitsVo> {


    @Resource
    UnitsConverter unitsConverter;

    @Override
    public boolean update(UnitsDo entity) {
        UnitsPo unitsPo = unitsConverter.do2Po(entity);
        return repository.update(unitsPo);
    }

    @Override
    public boolean updateBatch(List<UnitsDo> doList) {
        List<UnitsPo> poList = unitsConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(UnitsDo entity) {
        UnitsPo unitsPo = unitsConverter.do2Po(entity);
        return repository.delete(unitsPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(UnitsDo entity) {
        UnitsPo unitsPo = unitsConverter.do2Po(entity);
        return repository.insert(unitsPo);
    }

    @Override
    public boolean insertBatch(List<UnitsDo> doList) {
        List<UnitsPo> poList = unitsConverter.do2PoList(doList);
        UnitsPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<UnitsVo> findById(Long id) {
        Optional<UnitsPo> unitsPo =  repository.findById(id);
        UnitsVo unitsVo = unitsConverter.po2Vo(unitsPo.orElse(new UnitsPo()));
        return Optional.ofNullable(unitsVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<UnitsVo> findAllMatching(UnitsDo entity) {
        UnitsPo unitsPo = unitsConverter.do2Po(entity);
        List<UnitsPo>unitsPoList =  repository.findAllMatching(unitsPo);
        return unitsConverter.po2VoList(unitsPoList);
    }


    @Override
    public Pagination<UnitsVo> selectPage(UnitsDo entity){
        UnitsPo unitsPo = unitsConverter.do2Po(entity);
        PageQuery<UnitsPo> pageQuery = new PageQuery<>(unitsPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<UnitsPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<UnitsVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                unitsConverter.po2VoList(pagination.getList()));
    }


}

