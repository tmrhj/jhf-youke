package com.jhf.youke.base.domain.service;

import com.jhf.youke.base.domain.converter.UserRoleConverter;
import com.jhf.youke.base.domain.gateway.UserRoleRepository;
import com.jhf.youke.base.domain.model.Do.UserRoleDo;
import com.jhf.youke.base.domain.model.po.UserRolePo;
import com.jhf.youke.base.domain.model.vo.UserRoleVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 * **/
@Service
public class UserRoleService extends AbstractDomainService<UserRoleRepository, UserRoleDo, UserRoleVo> {


    @Resource
    UserRoleConverter userRoleConverter;

    @Override
    public boolean update(UserRoleDo entity) {
        UserRolePo userRolePo = userRoleConverter.do2Po(entity);
        userRolePo.preUpdate();
        return repository.update(userRolePo);
    }

    @Override
    public boolean updateBatch(List<UserRoleDo> doList) {
        List<UserRolePo> poList = userRoleConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(UserRoleDo entity) {
        UserRolePo userRolePo = userRoleConverter.do2Po(entity);
        return repository.delete(userRolePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(UserRoleDo entity) {
        UserRolePo userRolePo = userRoleConverter.do2Po(entity);
        userRolePo.preInsert();
        return repository.insert(userRolePo);
    }

    @Override
    public boolean insertBatch(List<UserRoleDo> doList) {
        List<UserRolePo> poList = userRoleConverter.do2PoList(doList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<UserRoleVo> findById(Long id) {
        Optional<UserRolePo> userRolePo =  repository.findById(id);
        UserRoleVo userRoleVo = userRoleConverter.po2Vo(userRolePo.orElse(new UserRolePo()));
        return Optional.ofNullable(userRoleVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<UserRoleVo> findAllMatching(UserRoleDo entity) {
        UserRolePo userRolePo = userRoleConverter.do2Po(entity);
        List<UserRolePo>userRolePoList =  repository.findAllMatching(userRolePo);
        return userRoleConverter.po2VoList(userRolePoList);
    }


    @Override
    public Pagination<UserRoleVo> selectPage(UserRoleDo entity){
        UserRolePo userRolePo = userRoleConverter.do2Po(entity);
        PageQuery<UserRolePo> pageQuery = new PageQuery<>(userRolePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<UserRolePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<UserRoleVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                userRoleConverter.po2VoList(pagination.getList()));
    }

    public List<String> getRoleIdsByUserId(Long userId){
        List<String> ids = new ArrayList<>();
        UserRolePo po = new UserRolePo();
        po.setUserId(userId);
        List<UserRolePo> rolePoList =  repository.findAllMatching(po);
        for(UserRolePo item:rolePoList){
            ids.add(item.getRoleId().toString());
        }
        return ids;
    }

    public void updateRoleIdsByUserId(List<String> roleIds, Long userId, Boolean isUpdate){
        if(isUpdate) {
            //更新时，先删除原来的角色
            repository.deleteRoleByUserId(userId);
        }
        List<UserRolePo> saveList = new ArrayList<>();
        for(String roleId:roleIds){
            UserRolePo po = new UserRolePo();
            po.setUserId(userId);
            po.setRoleId(StringUtils.toLong(roleId));
            po.preInsert();
            saveList.add(po);
        }
        if(saveList.size() > 0){
            //添加新的角色
            repository.insertBatch(saveList);
        }
    }

}

