package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.AreaRepository;
import com.jhf.youke.base.domain.model.po.AreaPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "AreaRepository")
public class AreaRepositoryImpl extends ServiceImpl<AreaMapper, AreaPo> implements AreaRepository {


    @Override
    public boolean update(AreaPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<AreaPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(AreaPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(AreaPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<AreaPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<AreaPo> findById(Long id) {
        AreaPo areaPo = baseMapper.selectById(id);
        return Optional.ofNullable(areaPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<AreaPo> findAllMatching(AreaPo entity) {
        QueryWrapper<AreaPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<AreaPo> selectPage(PageQuery<AreaPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<AreaPo> areaPos = baseMapper.selectList(new QueryWrapper<AreaPo>(pageQuery.getParam()));
        PageInfo<AreaPo> pageInfo = new PageInfo<>(areaPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

