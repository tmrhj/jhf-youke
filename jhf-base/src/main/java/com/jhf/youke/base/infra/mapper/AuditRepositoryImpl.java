package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.AuditRepository;
import com.jhf.youke.base.domain.model.Do.AuditDo;
import com.jhf.youke.base.domain.model.po.AuditPo;
import com.jhf.youke.base.domain.exception.AuditException;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  makejava
 **/

@Service(value = "AuditRepository")
public class AuditRepositoryImpl extends ServiceImpl<AuditMapper, AuditPo> implements AuditRepository {


    @Override
    public boolean update(AuditPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<AuditPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(AuditPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(AuditPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<AuditPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<AuditPo> findById(Long id) {
        AuditPo auditPo = baseMapper.selectById(id);
        return Optional.ofNullable(auditPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new AuditException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<AuditPo> findAllMatching(AuditPo entity) {
        QueryWrapper<AuditPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<AuditPo> selectPage(PageQuery<AuditPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<AuditPo> auditPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<AuditPo> pageInfo = new PageInfo<>(auditPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

