package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.AutoCompletedPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;


/**
 * 自动完成映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface AutoCompletedMapper  extends BaseMapper<AutoCompletedPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_auto_completed set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_auto_completed set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 通过sql自动完成
     *
     * @param map 地图
     * @return {@link List}<{@link Map}<{@link String},{@link String}>>
     */
    List<Map<String,String>> autoCompleteBySql(Map<String,Object> map);

}

