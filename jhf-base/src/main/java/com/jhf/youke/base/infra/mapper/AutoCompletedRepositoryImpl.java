package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.exception.AutoCompletedException;
import com.jhf.youke.base.domain.gateway.AutoCompletedRepository;
import com.jhf.youke.base.domain.model.po.AutoCompletedPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "AutoCompletedRepository")
public class AutoCompletedRepositoryImpl extends ServiceImpl<AutoCompletedMapper, AutoCompletedPo> implements AutoCompletedRepository {


    @Override
    public boolean update(AutoCompletedPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<AutoCompletedPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(AutoCompletedPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(AutoCompletedPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<AutoCompletedPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<AutoCompletedPo> findById(Long id) {
        AutoCompletedPo autoCompletedPo = baseMapper.selectById(id);
        return Optional.ofNullable(autoCompletedPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new AutoCompletedException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<AutoCompletedPo> findAllMatching(AutoCompletedPo entity) {
        QueryWrapper<AutoCompletedPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<AutoCompletedPo> selectPage(PageQuery<AutoCompletedPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<AutoCompletedPo> autoCompletedPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<AutoCompletedPo> pageInfo = new PageInfo<>(autoCompletedPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<Map<String, String>> autoCompleteBySql(Map<String, Object> map) {
        return baseMapper.autoCompleteBySql(map);
    }
}

