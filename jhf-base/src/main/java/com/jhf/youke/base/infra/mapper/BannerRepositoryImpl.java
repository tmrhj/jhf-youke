package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.exception.BannerException;
import com.jhf.youke.base.domain.gateway.BannerRepository;
import com.jhf.youke.base.domain.model.po.BannerPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "BannerRepository")
public class BannerRepositoryImpl extends ServiceImpl<BannerMapper, BannerPo> implements BannerRepository {


    @Override
    public boolean update(BannerPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<BannerPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(BannerPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(BannerPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<BannerPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<BannerPo> findById(Long id) {
        BannerPo bannerPo = baseMapper.selectById(id);
        return Optional.ofNullable(bannerPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }
        if(StringUtils.isEmpty(ids)){
            throw new BannerException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<BannerPo> findAllMatching(BannerPo entity) {
        QueryWrapper<BannerPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        wrapper.orderByAsc("sort");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<BannerPo> selectPage(PageQuery<BannerPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<BannerPo> bannerPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()).orderByAsc("sort"));
        PageInfo<BannerPo> pageInfo = new PageInfo<>(bannerPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

