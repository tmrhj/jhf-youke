package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.ButtonPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 按钮映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface ButtonMapper  extends BaseMapper<ButtonPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_button set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_button set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 被状态列表
     *
     * @param status 状态
     * @return {@link List}<{@link ButtonPo}>
     */
    List<ButtonPo> getListByStatus(@Param("status") Integer status);

}

