package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.CompanyPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 公司映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface CompanyMapper  extends BaseMapper<CompanyPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_company set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_company set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 获得公司身份证
     *
     * @param companyId 公司标识
     * @return {@link String}
     */
    String getCompanyIds(@Param("companyId") Long companyId);

}

