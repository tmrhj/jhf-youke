package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.CompanyRepository;
import com.jhf.youke.base.domain.model.po.CompanyPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "CompanyRepository")
public class CompanyRepositoryImpl extends ServiceImpl<CompanyMapper, CompanyPo> implements CompanyRepository {


    @Override
    public boolean update(CompanyPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<CompanyPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(CompanyPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(CompanyPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<CompanyPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<CompanyPo> findById(Long id) {
        CompanyPo companyPo = baseMapper.selectById(id);
        return Optional.ofNullable(companyPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<CompanyPo> findAllMatching(CompanyPo entity) {
        QueryWrapper<CompanyPo> wrapper = new QueryWrapper<>();
        if(!StringUtils.ifNull(entity.getName())){
            wrapper.like("name", entity.getName());
            entity.setName(null);
        }
        entity.setDelFlag("0");
        wrapper.setEntity(entity);
        wrapper.orderByAsc("sort");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<CompanyPo> selectPage(PageQuery<CompanyPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        pageQuery.getParam().setDelFlag("0");
        List<CompanyPo> companyPos = baseMapper.selectList(new QueryWrapper<CompanyPo>(pageQuery.getParam()));
        PageInfo<CompanyPo> pageInfo = new PageInfo<>(companyPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public String getCompanyIds(Long companyId) {
        return baseMapper.getCompanyIds(companyId);
    }
}

