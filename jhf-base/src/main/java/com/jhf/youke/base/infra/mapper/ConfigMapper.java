package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.ConfigPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 配置映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface ConfigMapper  extends BaseMapper<ConfigPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_config set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_config set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

