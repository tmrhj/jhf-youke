package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.ConfigRepository;
import com.jhf.youke.base.domain.model.po.ConfigPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "ConfigRepository")
public class ConfigRepositoryImpl extends ServiceImpl<ConfigMapper, ConfigPo> implements ConfigRepository {


    @Override
    public boolean update(ConfigPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ConfigPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ConfigPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ConfigPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ConfigPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<ConfigPo> findById(Long id) {
        ConfigPo configPo = baseMapper.selectById(id);
        return Optional.ofNullable(configPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ConfigPo> findAllMatching(ConfigPo entity) {
        QueryWrapper<ConfigPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        wrapper.eq("del_flag", "0");
        wrapper.orderByAsc("sort");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ConfigPo> selectPage(PageQuery<ConfigPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        QueryWrapper<ConfigPo> qw = new QueryWrapper<ConfigPo>();
        qw.eq("del_flag", "0");
        if(pageQuery.getParam() != null && !StringUtils.ifNull(pageQuery.getParam().getName())){
            qw.like("name", pageQuery.getParam().getName());
        }
        if(pageQuery.getParam() != null && !StringUtils.ifNull(pageQuery.getParam().getCode())){
            qw.like("code", pageQuery.getParam().getCode());
        }
        qw.orderByAsc("sort");
        List<ConfigPo> configPos = baseMapper.selectList(qw);
        PageInfo<ConfigPo> pageInfo = new PageInfo<>(configPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

