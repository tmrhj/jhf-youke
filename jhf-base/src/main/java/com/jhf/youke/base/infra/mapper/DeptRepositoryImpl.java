package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.DeptRepository;
import com.jhf.youke.base.domain.model.po.DeptPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "DeptRepository")
public class DeptRepositoryImpl extends ServiceImpl<DeptMapper, DeptPo> implements DeptRepository {


    @Override
    public boolean update(DeptPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<DeptPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(DeptPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(DeptPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<DeptPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<DeptPo> findById(Long id) {
        DeptPo deptPo = baseMapper.selectById(id);
        return Optional.ofNullable(deptPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<DeptPo> findAllMatching(DeptPo entity) {
        QueryWrapper<DeptPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<DeptPo> selectPage(PageQuery<DeptPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<DeptPo> deptPos = baseMapper.selectList(new QueryWrapper<DeptPo>(pageQuery.getParam()));
        PageInfo<DeptPo> pageInfo = new PageInfo<>(deptPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

