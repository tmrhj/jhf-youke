package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.DictRepository;
import com.jhf.youke.base.domain.model.po.DictPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
/**
 * @author RHJ
 * **/
@Service(value = "DictRepository")
public class DictRepositoryImpl extends ServiceImpl<DictMapper, DictPo> implements DictRepository {


    @Override
    public boolean update(DictPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<DictPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(DictPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(DictPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<DictPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<DictPo> findById(Long id) {
        DictPo dictPo = baseMapper.selectById(id);
        return Optional.ofNullable(dictPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<DictPo> findAllMatching(DictPo entity) {
        entity.setDelFlag("0");
        QueryWrapper<DictPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<DictPo> selectPage(PageQuery<DictPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        QueryWrapper<DictPo> qw = new QueryWrapper<DictPo>();
        qw.eq("del_flag", "0");
        if(pageQuery.getParam() != null && !StringUtils.ifNull(pageQuery.getParam().getName())){
            qw.like("name", pageQuery.getParam().getName());
        }
        if(pageQuery.getParam() != null && !StringUtils.ifNull(pageQuery.getParam().getTableName())){
            qw.like("table_name", pageQuery.getParam().getTableName());
        }
        if(pageQuery.getParam() != null && !StringUtils.ifNull(pageQuery.getParam().getFieldName())){
            qw.like("field_name", pageQuery.getParam().getFieldName());
        }
        if(pageQuery.getParam() != null && !StringUtils.ifNull(pageQuery.getParam().getType())){
            qw.like("type", pageQuery.getParam().getType());
        }
        qw.orderByAsc("type");
        qw.orderByAsc("sort");
        List<DictPo> dictPos = baseMapper.selectList(qw);
        PageInfo<DictPo> pageInfo = new PageInfo<>(dictPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

