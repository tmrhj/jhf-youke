package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.DigitalSignatureRepository;
import com.jhf.youke.base.domain.model.po.DigitalSignaturePo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
/**
 * @author RHJ
 * **/
@Service(value = "DigitalSignatureRepository")
public class DigitalSignatureRepositoryImpl extends ServiceImpl<DigitalSignatureMapper, DigitalSignaturePo> implements DigitalSignatureRepository {


    @Override
    public boolean update(DigitalSignaturePo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<DigitalSignaturePo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(DigitalSignaturePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(DigitalSignaturePo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<DigitalSignaturePo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<DigitalSignaturePo> findById(Long id) {
        DigitalSignaturePo digitalSignaturePo = baseMapper.selectById(id);
        return Optional.ofNullable(digitalSignaturePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<DigitalSignaturePo> findAllMatching(DigitalSignaturePo entity) {
        QueryWrapper<DigitalSignaturePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<DigitalSignaturePo> selectPage(PageQuery<DigitalSignaturePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<DigitalSignaturePo> digitalSignaturePos = baseMapper.selectList(new QueryWrapper<DigitalSignaturePo>(pageQuery.getParam()));
        PageInfo<DigitalSignaturePo> pageInfo = new PageInfo<>(digitalSignaturePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public DigitalSignaturePo getByCompanyId(Long id) {
        return baseMapper.getByCompanyId(id);
    }

    @Override
    public DigitalSignaturePo getAllByCompanyId(Long id) {
        return baseMapper.getAllByCompanyId(id);
    }
}

