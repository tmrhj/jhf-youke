package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.LogRepository;
import com.jhf.youke.base.domain.model.po.LogPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Service(value = "LogRepository")
public class LogRepositoryImpl extends ServiceImpl<LogMapper, LogPo> implements LogRepository {


    @Override
    public boolean update(LogPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<LogPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(LogPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(LogPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<LogPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<LogPo> findById(Long id) {
        LogPo logPo = baseMapper.selectById(id);
        return Optional.ofNullable(logPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<LogPo> findAllMatching(LogPo entity) {
        QueryWrapper<LogPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<LogPo> selectPage(PageQuery<LogPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<LogPo> logPos = baseMapper.selectList(new QueryWrapper<LogPo>(pageQuery.getParam()));
        PageInfo<LogPo> pageInfo = new PageInfo<>(logPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

