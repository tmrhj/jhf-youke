package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.MenuRepository;
import com.jhf.youke.base.domain.model.po.MenuPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "MenuRepository")
public class MenuRepositoryImpl extends ServiceImpl<MenuMapper, MenuPo> implements MenuRepository {


    @Override
    public boolean update(MenuPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<MenuPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(MenuPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(MenuPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<MenuPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<MenuPo> findById(Long id) {
        MenuPo menuPo = baseMapper.selectById(id);
        return Optional.ofNullable(menuPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<MenuPo> findAllMatching(MenuPo entity) {
        QueryWrapper<MenuPo> wrapper = new QueryWrapper<>();
        wrapper.eq("del_flag", "0");
        if(!StringUtils.ifNull(entity.getName())){
            wrapper.like("name", entity.getName());
        }
        if(entity.getIsFrame() != null){
            wrapper.eq("is_frame", entity.getIsFrame());
        }
        wrapper.orderByAsc("order_num");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<MenuPo> selectPage(PageQuery<MenuPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        pageQuery.getParam().setDelFlag("0");
        List<MenuPo> menuPos = baseMapper.selectList(new QueryWrapper<MenuPo>(pageQuery.getParam()).orderByAsc("order_num"));
        PageInfo<MenuPo> pageInfo = new PageInfo<>(menuPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<MenuPo> list(QueryWrapper<MenuPo> qw) {
        List<MenuPo> menuPos = baseMapper.selectList(qw);
        return menuPos;
    }

    @Override
    public List<MenuPo> getListByUser(Long userId) {
        return baseMapper.getListByUser(userId);
    }
}

