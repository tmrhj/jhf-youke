package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.exception.ObjectStatusException;
import com.jhf.youke.base.domain.gateway.ObjectStatusRepository;
import com.jhf.youke.base.domain.model.po.ObjectStatusPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "ObjectStatusRepository")
public class ObjectStatusRepositoryImpl extends ServiceImpl<ObjectStatusMapper, ObjectStatusPo> implements ObjectStatusRepository {


    @Override
    public boolean update(ObjectStatusPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ObjectStatusPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ObjectStatusPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ObjectStatusPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ObjectStatusPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ObjectStatusPo> findById(Long id) {
        ObjectStatusPo objectStatusPo = baseMapper.selectById(id);
        return Optional.ofNullable(objectStatusPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new ObjectStatusException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ObjectStatusPo> findAllMatching(ObjectStatusPo entity) {
        QueryWrapper<ObjectStatusPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ObjectStatusPo> selectPage(PageQuery<ObjectStatusPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ObjectStatusPo> objectStatusPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<ObjectStatusPo> pageInfo = new PageInfo<>(objectStatusPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

