package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.PermissionsRepository;
import com.jhf.youke.base.domain.model.po.PermissionsPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "PermissionsRepository")
public class PermissionsRepositoryImpl extends ServiceImpl<PermissionsMapper, PermissionsPo> implements PermissionsRepository {


    @Override
    public boolean update(PermissionsPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<PermissionsPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(PermissionsPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(PermissionsPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<PermissionsPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<PermissionsPo> findById(Long id) {
        PermissionsPo permissionsPo = baseMapper.selectById(id);
        return Optional.ofNullable(permissionsPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<PermissionsPo> findAllMatching(PermissionsPo entity) {
        QueryWrapper<PermissionsPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        wrapper.eq("del_flag", "0");
        if(!StringUtils.ifNull(entity.getName())){
            wrapper.like("name", entity.getName());
        }
        wrapper.orderByAsc("sort");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<PermissionsPo> selectPage(PageQuery<PermissionsPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<PermissionsPo> permissionsPos = baseMapper.selectList(new QueryWrapper<PermissionsPo>(pageQuery.getParam()));
        PageInfo<PermissionsPo> pageInfo = new PageInfo<>(permissionsPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

