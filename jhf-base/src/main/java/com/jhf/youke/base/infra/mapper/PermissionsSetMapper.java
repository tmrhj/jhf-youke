package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.PermissionsPo;
import com.jhf.youke.base.domain.model.po.PermissionsSetPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 权限设置映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface PermissionsSetMapper  extends BaseMapper<PermissionsSetPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_permissions_set set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_permissions_set set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 被用户列表
     *
     * @param userId 用户id
     * @return {@link List}<{@link PermissionsPo}>
     */
    List<PermissionsPo> getListByUser(@Param("userId") Long userId);

    /**
     * 得到用户url列表
     *
     * @param userId 用户id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getUrlListByUser(@Param("userId") Long userId);

    /**
     * 被菜单id列表
     *
     * @param menuId 菜单id
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getListByMenuId(@Param("menuId") Long menuId);

    /**
     * 获取所有菜单设置列表
     *
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getAllMenuSetList();

    /**
     * 得到白名单
     *
     * @param userPo 用户订单
     * @return {@link List}<{@link PermissionsSetPo}>
     */
    List<PermissionsSetPo> getWhiteList(PermissionsSetPo userPo);

    /**
     * 角色权限列表
     * @param roleId 角色id
     * @return {@link List}<{@link PermissionsPo}>
     */
    List<PermissionsPo> getListByRoleId(@Param("roleId") Long roleId);
}

