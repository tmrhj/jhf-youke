package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.PermissionsSetRepository;
import com.jhf.youke.base.domain.model.po.PermissionsPo;
import com.jhf.youke.base.domain.model.po.PermissionsSetPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service(value = "PermissionsSetRepository")
public class PermissionsSetRepositoryImpl extends ServiceImpl<PermissionsSetMapper, PermissionsSetPo> implements PermissionsSetRepository {

    @Override
    public boolean update(PermissionsSetPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<PermissionsSetPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(PermissionsSetPo entity) {
        return baseMapper.deleteById(entity) > 0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(PermissionsSetPo entity) {
        return baseMapper.insert(entity) > 0;
    }

    @Override
    public boolean insertBatch(List<PermissionsSetPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<PermissionsSetPo> findById(Long id) {
        PermissionsSetPo permissionsSetPo = baseMapper.selectById(id);
        return Optional.ofNullable(permissionsSetPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for (Long id : idList) {
            if (ids == null) {
                ids = id.toString();
            } else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<PermissionsSetPo> findAllMatching(PermissionsSetPo entity) {
        QueryWrapper<PermissionsSetPo> wrapper = new QueryWrapper<>();
        entity.setDelFlag("0");
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<PermissionsSetPo> selectPage(PageQuery<PermissionsSetPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        pageQuery.getParam().setDelFlag("0");
        List<PermissionsSetPo> permissionsSetPos = baseMapper.selectList(new QueryWrapper<PermissionsSetPo>(pageQuery.getParam()));
        PageInfo<PermissionsSetPo> pageInfo = new PageInfo<>(permissionsSetPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Pagination<PermissionsSetPo> getWhiteList(PageQuery<PermissionsSetPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<PermissionsSetPo> userPos = baseMapper.getWhiteList(pageQuery.getParam());
        PageInfo<PermissionsSetPo> pageInfo = new PageInfo<>(userPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<PermissionsSetPo> getListByRole(Long roleId) {
        QueryWrapper<PermissionsSetPo> qw = new QueryWrapper<>();
        qw.eq("biz_id", roleId);
        qw.eq("type", 1);
        List<PermissionsSetPo> list = baseMapper.selectList(qw);
        return list;
    }

    @Override
    public List<PermissionsSetPo> getListByMenu(Long menuId) {
        QueryWrapper<PermissionsSetPo> qw = new QueryWrapper<>();
        qw.eq("biz_id", menuId);
        qw.eq("type", 2);
        List<PermissionsSetPo> list = baseMapper.selectList(qw);
        return list;
    }

    @Override
    public List<PermissionsSetPo> getListByMenuId(Long menuId) {
        return baseMapper.getListByMenuId(menuId);
    }

    @Override
    public List<PermissionsPo> getListByUser(Long userId) {
        return baseMapper.getListByUser(userId);
    }

    @Override
    public List<PermissionsSetPo> getUrlListByUser(Long userId) {
        return baseMapper.getUrlListByUser(userId);
    }

    @Override
    public List<PermissionsSetPo> getAllMenuSetList() {
        return baseMapper.getAllMenuSetList();
    }

    @Override
    public List<PermissionsPo> getListByRoleId(Long roleId) {
        return baseMapper.getListByRoleId(roleId);
    }
}

