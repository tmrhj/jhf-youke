package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.RoleRepository;
import com.jhf.youke.base.domain.model.Do.RoleDo;
import com.jhf.youke.base.domain.model.po.RolePo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "RoleRepository")
public class RoleRepositoryImpl extends ServiceImpl<RoleMapper, RolePo> implements RoleRepository<RoleDo> {


    @Override
    public boolean update(RolePo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<RolePo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(RolePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(RolePo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<RolePo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<RolePo> findById(Long id) {
        RolePo rolePo = baseMapper.selectById(id);
        return Optional.ofNullable(rolePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<RolePo> findAllMatching(RolePo entity) {
        QueryWrapper<RolePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        wrapper.orderByAsc("sort");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<RolePo> selectPage(PageQuery<RolePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<RolePo> rolePos = baseMapper.selectList(new QueryWrapper<RolePo>(pageQuery.getParam()).orderByAsc("sort"));
        PageInfo<RolePo> pageInfo = new PageInfo<>(rolePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

