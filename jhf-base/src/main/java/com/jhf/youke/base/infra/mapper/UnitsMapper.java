package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.UnitsPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 单位映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface UnitsMapper  extends BaseMapper<UnitsPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_units set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_units set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

