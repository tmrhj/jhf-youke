package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.UnitsRepository;
import com.jhf.youke.base.domain.model.po.UnitsPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "UnitsRepository")
public class UnitsRepositoryImpl extends ServiceImpl<UnitsMapper, UnitsPo> implements UnitsRepository {


    @Override
    public boolean update(UnitsPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<UnitsPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(UnitsPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(UnitsPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<UnitsPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<UnitsPo> findById(Long id) {
        UnitsPo unitsPo = baseMapper.selectById(id);
        return Optional.ofNullable(unitsPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<UnitsPo> findAllMatching(UnitsPo entity) {
        QueryWrapper<UnitsPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<UnitsPo> selectPage(PageQuery<UnitsPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<UnitsPo> unitsPos = baseMapper.selectList(new QueryWrapper<UnitsPo>(pageQuery.getParam()));
        PageInfo<UnitsPo> pageInfo = new PageInfo<>(unitsPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

