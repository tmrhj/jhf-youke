package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.UserPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 用户映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface UserMapper  extends BaseMapper<UserPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update base_user set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update base_user set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 通过pwd
     *
     * @param loginName 登录名
     * @param password  密码
     * @return {@link UserPo}
     */
    UserPo getByPwd(@Param("loginName") String loginName, @Param("password")String password);

    /**
     * 通过移动
     *
     * @param openId 开放id
     * @return {@link UserPo}
     */
    UserPo getByMobile(@Param("openId") String openId);

    /**
     * 获得页面列表
     *
     * @param userPo 用户订单
     * @return {@link List}<{@link UserPo}>
     */
    List<UserPo> getPageList(UserPo userPo);

}
