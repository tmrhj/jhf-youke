package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.UserRepository;
import com.jhf.youke.base.domain.model.po.UserPo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "UserRepository")
public class UserRepositoryImpl extends ServiceImpl<UserMapper, UserPo> implements UserRepository {


    @Override
    public boolean update(UserPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<UserPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(UserPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(UserPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<UserPo> list) {
        list.forEach(entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<UserPo> findById(Long id) {
        UserPo userPo = baseMapper.selectById(id);
        return Optional.ofNullable(userPo);
    }

    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<UserPo> findAllMatching(UserPo entity) {
        entity.setDelFlag("0");
        QueryWrapper<UserPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<UserPo> selectPage(PageQuery<UserPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<UserPo> userPos = baseMapper.getPageList(pageQuery.getParam());
        PageInfo<UserPo> pageInfo = new PageInfo<>(userPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }


    @Override
    public UserPo getByPwd(String loginName, String password) {
        return baseMapper.getByPwd(loginName, password);
    }

    @Override
    public UserPo getByLoginName(String loginName) {
        QueryWrapper<UserPo> qw = new QueryWrapper<>();
        qw.eq("login_name", loginName);
        qw.last("limit 1");
        return getOne(qw);
    }

    @Override
    public UserPo getByMobile(String openId) {
        return  baseMapper.getByMobile(openId);
    }

}
