package com.jhf.youke.base.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.base.domain.model.po.UserRolePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * @author RHJ
 */
@Mapper
public interface UserRoleMapper  extends BaseMapper<UserRolePo> {

    /**
     * 单个删除
     * @param id
     * @return
     */
    @Update("update base_user_role set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @Update("update base_user_role set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 删除角色
     * @param userId
     * @return
     */
    @Update("delete from base_user_role where user_id = #{userId} ")
    int deleteRoleByUserId(@Param("userId") Long userId);

}

