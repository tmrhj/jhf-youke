package com.jhf.youke.base.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.base.domain.gateway.UserRoleRepository;
import com.jhf.youke.base.domain.model.Do.UserRoleDo;
import com.jhf.youke.base.domain.model.po.UserRolePo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Service(value = "UserRoleRepository")
public class UserRoleRepositoryImpl extends ServiceImpl<UserRoleMapper, UserRolePo> implements UserRoleRepository<UserRoleDo> {


    @Override
    public boolean update(UserRolePo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<UserRolePo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(UserRolePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(UserRolePo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<UserRolePo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<UserRolePo> findById(Long id) {
        UserRolePo userRolePo = baseMapper.selectById(id);
        return Optional.ofNullable(userRolePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = null;
        for(Long id:idList){
            if(ids == null) {
                ids = id.toString();
            }else {
                ids += "," + id.toString();
            }
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<UserRolePo> findAllMatching(UserRolePo entity) {
        QueryWrapper<UserRolePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<UserRolePo> selectPage(PageQuery<UserRolePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<UserRolePo> userRolePos = baseMapper.selectList(new QueryWrapper<UserRolePo>(pageQuery.getParam()));
        PageInfo<UserRolePo> pageInfo = new PageInfo<>(userRolePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<UserRolePo> getListByUser(Long userId) {
        QueryWrapper<UserRolePo> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        List<UserRolePo> list = baseMapper.selectList(qw);
        return  list;
    }

    @Override
    public Boolean deleteRoleByUserId(Long userId) {
        return baseMapper.deleteRoleByUserId(userId) > 0;
    }
}

