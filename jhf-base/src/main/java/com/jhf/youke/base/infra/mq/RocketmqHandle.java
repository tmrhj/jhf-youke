package com.jhf.youke.base.infra.mq;

import ch.qos.logback.classic.Logger;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.jhf.youke.core.ddd.DomainMq;
import com.jhf.youke.core.entity.SendResult;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author RHJ
 */
@Component
public class RocketmqHandle implements DomainMq {
    private final static Logger logger = (Logger) LoggerFactory.getLogger(RocketmqHandle.class);

    @Value("${feign.mqUrl}")
    public String mqUrl;

    @Override
    public SendResult send(String msg) {
        HttpResponse response = HttpRequest.post(mqUrl + "/rocketmq/send").body(JSONUtil.toJsonStr(msg)).execute();
        SendResult result = JSONUtil.toBean(response.body(), SendResult.class);
        logger.info(" send rocketmq success {}", result.toString());
        return result;
    }

}
