import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.DES;
import cn.hutool.json.JSONUtil;
import com.jhf.youke.BaseApplication;
import com.jhf.youke.base.domain.converter.UserConverter;
import com.jhf.youke.base.domain.gateway.UserRepository;
import com.jhf.youke.base.domain.model.Do.MenuDo;
import com.jhf.youke.base.domain.model.vo.MenuVo;
import com.jhf.youke.base.domain.service.DigitalSignatureService;
import com.jhf.youke.base.domain.service.MenuService;
import com.jhf.youke.base.domain.service.PermissionsSetService;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.core.utils.OkHttpUtil;
import com.jhf.youke.core.utils.RsaUtils;
import com.jhf.youke.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@SpringBootTest(classes = BaseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {

    @Resource
    UserRepository userRepository;

    @Resource
    UserConverter userConverter;

    @Resource
    MenuService menuService;

    @Resource
    DigitalSignatureService digitalSignatureService;

    @Resource
    PermissionsSetService permissionsSetService;


    @Test
    void contextLoads() {
//        UserDto dto = new UserDto();
//        dto.setCurrentPage(1);
//        dto.setPageSize(20);
//        UserDo userDo = userConverter.dto2Do(dto);
//        System.out.println(userDo);
//        System.out.println(userDo.getPageSize() + " " + userDo.getCurrentPage());


//        Optional<UserPo> referrerOption = Optional.ofNullable(null);
//        assert  !referrerOption.isEmpty() : "推荐人不能为空";
//        System.out.println("测试");

//        String my = CacheUtils.get("aes_key_156dcd0a-8ced-46ba-a5ad-a0d494846967");
//        log.info("my {}", my);
        //构建
        DES des =  SecureUtil.des( java.util.Base64.getDecoder().decode("RavFeap1IrrreYB7xYLBiQ=="));

        //解密为原字符串
        String sql = des.decryptStr("eed0c9c1cf45533e54c2144b2d97a45a90177283bf04692645d1b8f65228348465b1182e8d2bb1ff7d9e27ef422730e365fb82da0187559259580374645ffd49da0eb7f676e11517eda2a30cfa7fbcb58efee21d003c027aff452a878ab0fcb50ba75f33d34b17766db9e91fe113c5ca651a75f0ef8b05f225a4a11b97692fee232e10a8f74df2a22f66583b7a6a0aa1f36ac308d06090c45366ac3d7fe81cade166e1968b3671a055de9a8028cb099c113a81996f495669aa10379eca3883c2e29beec8981079d6eb1514c3f4273a7275870f15f960f996e5a1c9c09dfad320e4a4e7fc678aea50b1c3119f9e40b2cc713bf8c721afa77791b18795eb45cb295a01581777bfb2f1ea7f8f81a753426a3969cb330586d4278d527c1642ff05dccfab67e7deaa0b44318439831f927d39fb28b3b0b5f14bafe7a7decb9cba48dcbdfecfb66f8079a225972f8874c10f48115e1b3b87cd1e895fea7e830f7711e1");
        System.out.println(sql);
    }

    @Test
    void test1(){
        KeyPair pair = SecureUtil.generateKeyPair("RSA");
        System.out.println(pair);
        String privateK = Base64.encodeBase64String(pair.getPrivate().getEncoded());
        // 将private 转为string
        System.out.println(privateK);
        System.out.println(Base64.encodeBase64String(pair.getPublic().getEncoded()));

        PrivateKey privateKey = RsaUtils.getPrivateKey(privateK);
        System.out.println(privateKey.equals(pair.getPrivate()));
    }

    @Test
    void test2(){
        String key = "test";
        CacheUtils.set(key, "test", 1000);
        System.out.println(menuService.getAllMenuToMap());
    }

    @Test
    void test3(){
        System.out.println(permissionsSetService.getAllMenuPermissionsToMap());
    }

    @Test
    void test4(){
        List<MenuVo> allMatching = menuService.findAllMatching(new MenuDo());
        System.out.println(allMatching);
        Long begin = System.currentTimeMillis();
        List<MenuVo> list = menuService.getTree(allMatching);
        Long end = System.currentTimeMillis();
        System.out.println(list);
        System.out.println(end-begin);
    }



    @Test
    void test6(){
        String ids = "";
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(1L);
        idList.add(1L);
        idList = null;
        if(!idList.isEmpty())
        ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        log.info(ids);
        Object obj = Object.class;


    }

    @Test
    void test7(){
        String ids = "";
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(3L);
        idList.add(2L);
        idList.forEach(id ->{
            if(id == 5){

            }
            System.out.println(id);
        });

        log.info(ids);
    }

    @Test
    public void test8(){
        String sql = "select * from test AND WHERE 1=2 AND 1=2";
        String str = StringUtils.substringAfter(sql, "WHERE");
        log.info(str);
        Boolean a = str.contains("AND");
        int b = sql.indexOf("AND");
        log.info("a {}, b {}",a, b);
    }

    @Test
    public void encrypt() throws Exception {
        //long companyId = 10000L;
        //String nonceStr = RandomUtil.randomString(10);
        //Map<String, String> param = Map.of("loginName", "admin", "password", "admin123");
        //String paramStr = JSONUtil.toJsonStr(param);
        //PublicKey publicKey = RSAUtils.getPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCll5j7tRJRohJ3UHokLhWrXPjkPYTsTyKj7eJk3sLEzDjb7f+q/5Ck1G+lf/WMFFmfVd7va4IGOnrPGkSA9KH+Yq1uHHeJCela50nJv2h5LFFAnGs3i3hB6h3O1x6Xum39aRNGdb+zKsCaHjoJ4C7GI+39tj5Ttx0GjUWWg6sF1wIDAQAB");
        //String dataStr = RSAUtils.encryption(paramStr, publicKey);
        //String sign = DigestUtil.md5Hex(dataStr + nonceStr + companyId, "UTF-8");
        //var login = digitalSignatureService.verification(sign, dataStr, nonceStr, companyId);
        //System.out.println(login);
        //System.out.println(companyId);
        //System.out.println(dataStr);
        //System.out.println(nonceStr);
        //System.out.println(sign);


        Response data = OkHttpUtil.getInstance().getData("https://ms.qifumc.cn/ms/base/digitalSignature/getByCompany?companyId=10000");
        Map<String, String> code1 = Map.of("code", "11");
        String code = OkHttpUtil.getInstance().postJson("https://ms.qifumc.cn/ms/pay/wx/getOpenIdByCode", JSONUtil.toJsonStr(code1));
        System.out.println(code);
        System.out.println(data.body().string());
    }



}
