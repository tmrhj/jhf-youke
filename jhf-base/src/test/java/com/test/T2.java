package com.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class T2 implements Runnable {

    private int count = 10;

    public synchronized void run() {
        count--;
        System.out.println(Thread.currentThread().getName() + " count = " + count);
        try {
            Thread.sleep(1000);
        }catch (Exception e){

        }
    }

    public static void main(String[] args) {
        ExecutorService executorService= Executors.newFixedThreadPool(5);
        T2 t = new T2();
        for(int i = 0;i<10;i++){
            //向线程池提交一个任务（其实就是通过线程池来启动一个线程）
            executorService.execute(new Runnable(){
                public void run() {
                    t.run();
                }
            });
            System.out.println("============  "+i);
        }
        executorService.shutdown();
    }

}