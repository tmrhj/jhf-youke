package com.test;

import com.jhf.youke.BaseApplication;
import com.jhf.youke.base.domain.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@SpringBootTest(classes = BaseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class T3 implements Runnable {

    private int count = 10;

    @Resource
    private UserService userService;

    public synchronized void run() {
        count--;
        System.out.println(Thread.currentThread().getName() + " count = " + count);
        try {
            Thread.sleep(1000);
        }catch (Exception e){

        }
    }



    @Test
    public static void main(String[] args) {
        ExecutorService executorService= Executors.newFixedThreadPool(5);

        for(int i = 0;i<10;i++){
            //向线程池提交一个任务（其实就是通过线程池来启动一个线程）
            executorService.execute(new Runnable(){
                public void run() {
                  // test();
                }
            });
            System.out.println("============  "+i);
        }
        executorService.shutdown();
    }

}