package com.jhf.youke.core.common.localChace;

/**
 * @author RHJ
 */
public class LocalCache {

    public static LfuCache cache = LfuCache.getInstance();

    public static Object get(String key){
        return cache.get(key);
    }

    public static void put(String key,Object value,int time){
           cache.put(key,value,time);
    }

    public static void put(String key,Object value){
        cache.put(key,value);
    }

    public static void del(String key){
        cache.del(key);
    }


    public static void main(String[] args) throws Exception {

          LocalCache.put("t1","0k");
          String str = (String) LocalCache.get("t1");
          System.out.println(str);
          LocalCache.del("t1");
          str = (String) LocalCache.get("t1");
          System.out.println("del:"+str);
    }
}
