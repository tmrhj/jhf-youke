package com.jhf.youke.core.ddd;

import com.jhf.youke.core.entity.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

/**
 * 抽象域服务
 *
 * @author RHJ
 * @date 2022/11/17
 */
public abstract class  AbstractDomainService<M extends Repository, E extends BaseDoEntity, V>  {

    /**
     * 此处需要使用@Autowired, 不能使用@Resource
     */
    @Autowired
    protected M repository;

    /**
     * 更新
     *
     * @param entity 实体
     * @return boolean
     */
    public abstract boolean update(E entity);

    /**
     * 批处理更新
     *
     * @param doList 做列表
     * @return boolean
     */
    public abstract boolean updateBatch(List<E> doList);

    /**
     * 删除
     *
     * @param entity 实体
     * @return boolean
     */
    public abstract boolean delete(E entity);

    /**
     * 删除批处理
     *
     * @param list 列表
     * @return boolean
     */
    public abstract boolean deleteBatch(List<Long> list);

    /**
     * 插入
     *
     * @param entity 实体
     * @return boolean
     */
    public abstract boolean insert(E entity);

    /**
     * 插入批
     *
     * @param doList 做列表
     * @return boolean
     */
    public abstract boolean insertBatch(List<E> doList);

    /**
     * 发现通过id
     *
     * @param id id
     * @return {@link Optional}<{@link V}>
     */
    public abstract Optional<V> findById(Long id);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    public abstract boolean remove(Long id);

    /**
     * 删除批处理
     *
     * @param idList id列表
     * @return boolean
     */
    public abstract boolean removeBatch(List<Long> idList);

    /**
     * 找到所有匹配
     *
     * @param entity 实体
     * @return {@link List}<{@link V}>
     */
    public abstract List<V> findAllMatching(E entity);

    /**
     * 选择页面
     *
     * @param entity 实体
     * @return {@link Pagination}<{@link V}>
     */
    public abstract Pagination<V> selectPage(E entity);


}
