package com.jhf.youke.core.ddd;

import java.util.Collection;
import java.util.List;

/**
 * 通用转换基类
 * @author RHJ
 * 2022/11/8
*/
@SuppressWarnings("unused")
public interface  BaseConverter<DO extends BaseDoEntity, PO extends BasePoEntity, DTO extends BaseDtoEntity, VO extends BaseVoEntity> {

    /**
     * collection 转 Object
     * @param collection
     * @return object
     **/
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto 转 do
     * @param dto
     * @return do
     **/
    DO dto2Do(DTO dto);

    /**
     * do 转 po
     * @param domainObject
     * @return po
     **/
    PO do2Po(DO domainObject);

    /**
     * do 转 vo
     * @param domainObject
     * @return vo
     * **/
    VO do2Vo(DO domainObject);

    /**
     * do List 转 PO List
     *  @param doList
     *  @return list<po>
     * **/
    List<PO> do2PoList(List<DO> doList);

    /**
     * PO  转 DO
     *  @param po
     *  @return do
     * **/
    DO po2Do(PO po);

    /**
     * PO  转 VO
     * @param po
     * @return vo
     * **/
    VO po2Vo(PO po);

    /**
     * PO List 转 DO List
     * @param poList
     * @return list<do>
     * **/
    List<DO> po2DoList(List<PO> poList);

    /**
     * PO List 转 VO List
     * @param poList
     *  @return list<vo>
     * **/
    List<VO> po2VoList(List<PO> poList);



}
