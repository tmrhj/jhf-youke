package com.jhf.youke.core.ddd;

import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.util.Date;


/**
 * @author RHJ
 */
@Data
public abstract class BaseDoEntity implements Entity<T> {

    private static final long serialVersionUID = 1L;

    protected Long id;
    /**  备注 **/
    protected String remark;
    /**  创建日期 **/
    protected Date createTime;
    /**  更新日期 **/
    protected Date updateTime;
    /**  删除标记（0：正常；1：删除；2：审核） **/
    protected String delFlag = "0";
    /**  不是新记录  1 是新记录 **/
    protected String ifNew = "0";
    protected Integer pageSize = 20;
    protected Integer currentPage = 1;
    /**  查询排序 **/
    protected String querySort;





}
