package com.jhf.youke.core.ddd;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author RHJ
 */
@Getter
public class BaseEvent extends ApplicationEvent {
    private static final Long serialVersionUID = 1L;
    /** 领域事件  **/
    public static final String EVENT_DOMAIN = "1";
    /** 应用事件，发送MQ  **/
    public static final String EVENT_APPLICATION = "2";
    protected String type;
    protected String topic;
    protected String message;

    public BaseEvent(Object source){
        super(source);
    }


}
