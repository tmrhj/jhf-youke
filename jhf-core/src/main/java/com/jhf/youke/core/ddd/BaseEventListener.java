package com.jhf.youke.core.ddd;

import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.annotation.Resource;


/**
 * 基本事件监听器
 *
 * @author RHJ
 * @date 2022/11/17
 */
public abstract class  BaseEventListener <M extends BaseEvent> {

    /**
     * 日志记录器
     */
    private final static Logger logger = (Logger) LoggerFactory.getLogger(BaseEventListener.class);
    /**
     * 事件域
     */
    public static final String EVENT_DOMAIN = "1";
    /**
     * 事件应用程序
     */
    public static final String EVENT_APPLICATION = "2";

    /**
     * 域mq
     */
    @Resource
    private DomainMq domainMq;


    /**
     * 处理事件
     *
     * @param event 事件
     */
    @TransactionalEventListener(fallbackExecution = true)
    public void handleEvent(M event) {
        String message = event.getMessage();
        logger.info("已接受的信息:{}", message);

        if(EVENT_DOMAIN.equals(event.getType())){
            handleEventDomain(event);
        }else if(EVENT_APPLICATION.equals(event.getType())){
            handleEventApplication(event);
        }
    }

    /**
     * 处理事件域
     * fetch 领域事件处理，用于处理相同的界限上下文
     *
     * @param event 事件
     * @Description: 领域事件处理，用于处理相同的界限上下文
     * @return: void
     * @Author: RHJ
     * @Date: 2022/9/6
     */
    public abstract void handleEventDomain(M event);

    /**
     * 处理事件应用程序
     *
     * @param event 事件
     * @Description: 应用事件处理，发送MQ，处理跨界限上下文
     * @Param: [event]
     * @return: void
     * @Author: RHJ
     * @Date: 2022/9/6
     */
    public void handleEventApplication(M event){
        try {
            domainMq.send(event.getMessage());
        }catch (Exception e){
            logger.info("send event application error {}", e.getMessage());
        }

    }



}
