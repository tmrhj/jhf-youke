package com.jhf.youke.core.ddd;

import com.jhf.youke.core.utils.IdGen;
import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.util.Date;
import java.util.List;

/**
 * @author rhj
 * **/

@Data
public abstract class BasePoEntity implements Entity<T> {

    private static final long serialVersionUID = 1L;

    protected Long id;
    protected String remark;
    protected Date createTime;
    protected Date updateTime;
    protected String delFlag = "0";


    public void preInsert(){
        // 不限制ID为UUID，调用setIsNewRecord()使用自定义ID
        if(this.id == null || this.id == 0){
            setId(IdGen.id());
        }
        this.createTime = new Date();
        this.updateTime = this.createTime;
    }

    public void preUpdate(){
        this.updateTime = new Date();
    }

    public static <T extends BasePoEntity> List<T> getInsertListId(List<T> list){
        list.forEach( entity -> entity.preInsert());
        return list;
    }

 }
