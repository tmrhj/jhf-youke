package com.jhf.youke.core.ddd;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.util.Date;

/**
 * @author RHJ
 * **/
@Data
@SuppressWarnings("serial")
public abstract class BaseVoEntity implements Entity<T> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    protected Long id;

    protected Integer pageSize = 20;
    protected Integer currentPage = 1;
    protected String delFlag = "0";
    protected String querySort;
    protected String remark;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date createTime;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date updateTime;

    protected String ifNew = "0";

}
