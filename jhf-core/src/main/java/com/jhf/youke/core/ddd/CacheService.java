package com.jhf.youke.core.ddd;

import com.jhf.youke.core.entity.RedisHashDto;
import com.jhf.youke.core.entity.RedisHashItemDto;
import java.util.List;
import java.util.Map;

/**
 * 缓存服务
 *
 * @author RHJ
 * @date 2022/11/17
 */

public interface CacheService{

    /**
     * 得到
     *
     * @param key 关键
     * @return {@link String}
     */
    String get(String key);

    /**
     * 集
     *
     * @param key   关键
     * @param value 价值
     * @param time  时间
     * @return {@link String}
     */
    String set(String key, String value, long time);

    /**
     * 设定json
     *
     * @param map 地图
     * @return {@link String}
     */
    String setByJson(Map<String,Object> map);

    /**
     * ▽
     *
     * @param key 关键
     * @return {@link String}
     */
    String del(String key);

    /**
     * 有时间
     *
     * @param key 关键
     * @return {@link String}
     */
    String getTime(String key);

    /**
     * 锁
     *
     * @param key       关键
     * @param requestId 请求id
     * @param timeout   超时
     * @return {@link Boolean}
     */
    Boolean lock(String key,String requestId, Integer timeout);

    /**
     * 释放
     *
     * @param key           关键
     * @param retIdentifier ret标识符
     * @return {@link Boolean}
     */
    Boolean release(String key,String retIdentifier);

    /**
     * 设置列表
     *
     * @param map 地图
     * @return {@link Object}
     */
    Object setList(Map<String,Object> map);

    /**
     * 设置一个列表
     *
     * @param map 地图
     * @return {@link Object}
     */
    Object setOneForList(Map<String,Object> map);

    /**
     * 得到列表
     *
     * @param key   关键
     * @param start 开始
     * @param end   结束
     * @return {@link List}<{@link Object}>
     */
    List<Object> getList(String key, Long start, Long end );

    /**
     * lpop
     *
     * @param key 关键
     * @return {@link Object}
     */
    Object lpop(String key);

    /**
     * 获取散列
     *
     * @param key 关键
     * @return {@link Map}<{@link Object}, {@link Object}>
     */
    Map<Object, Object> getHash(String key);

    /**
     * 将哈希
     *
     * @param dto dto
     * @return {@link String}
     */
    String setHash(RedisHashDto dto);

    /**
     * 得到哈希条目
     *
     * @param key  关键
     * @param item 项
     * @return {@link String}
     */
    String getHashItem(String key ,String item);

    /**
     * 将哈希条目
     *
     * @param dto dto
     * @return {@link String}
     */
    String setHashItem(RedisHashItemDto dto);

    /**
     * 德尔·哈希条目
     *
     * @param dto dto
     * @return {@link String}
     */
    String delHashItem(RedisHashItemDto dto);
}
