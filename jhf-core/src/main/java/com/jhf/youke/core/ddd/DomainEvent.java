package com.jhf.youke.core.ddd;

/**
 * 域事件
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface DomainEvent{

    /**
     * 发布
     *
     * @param topic     主题
     * @param tag       标签
     * @param groupName 组名称
     * @param msg       味精
     * @param id        id
     * @Description: 通用领域事件
     * @Param: topic 主题,
     * @Param: tag 主题,
     * @Param: groupName 分组 ,
     * @Param: msg 消息,
     * @Param: id 标识，
     * @return: void
     * @Author: RHJ
     * @Date: 2022/11/3
     */
    void publish(String topic, String tag, String groupName, String msg, Long id);

}


