package com.jhf.youke.core.ddd;


import com.jhf.youke.core.entity.SendResult;

/**
 * 域mq
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface DomainMq {

    /**
     * 发送
     *
     * @param msg 味精
     * @return {@link SendResult}
     * @Description: 能完MQ发送
     * @Param: [msg]  发送消息
     * @return: com.jhf.youke.core.entity.SendResult  返回发送结果
     * @Author: RHJ
     * @Date: 2022/11/3
     */
    SendResult send(String msg);

}
