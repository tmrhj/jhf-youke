package com.jhf.youke.core.ddd;

import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;

import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
public interface  Repository<T extends BasePoEntity> {


      /** 单条更新
       * @param  entity
       * @return  boolean
       * **/
      boolean update(T entity);

      /** 批量更新
       * @param  list
       * @return  boolean
       * **/
      boolean updateBatch(List<T> list);

      /** 单条物理删除
       * @param  entity
       * @return  boolean
       * **/
      boolean delete(T entity);

      /** 批量物理删除
       * @param  list
       * @return  boolean
       * **/
      boolean deleteBatch(List<Long> list);

      /** 单条插入
       * @param  entity
       * @return  boolean
       * **/
      boolean insert(T entity);

      /** 批量插入
       * @param  list
       * @return  boolean
       * **/
      boolean insertBatch(List<T> list);

      /** 根据ID查询
       * @param  id
       * @return  boolean
       * **/
      Optional<T> findById(Long id);

      /** 标记删除
       * @param  id
       * @return  boolean
       * **/
      boolean remove(Long id);

      /** 批量标记删除
       * @param  idList ids
       * @return  boolean
       * **/
      boolean removeBatch(List<Long> idList);

      /** 查询list
       * @param  entity 查询 条件
       * @return  List<T> 返回分面对象
       * **/
      List<T> findAllMatching(T entity);

      /** 分页查询
       * @param  pageQuery 查询 条件
       * @return  Pagination<T> 返回分面对象
       * **/
      Pagination<T> selectPage(PageQuery<T> pageQuery);



}


