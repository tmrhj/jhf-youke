
package com.jhf.youke.core.entity;


import com.jhf.youke.core.utils.IdGen;
import lombok.Data;
import java.util.Date;

/**
 * @author RHJ
 * **/
@Data
public abstract class BaseDataEntity<T extends BaseDataEntity<T>> {

	private static final long serialVersionUID = 1L;


	protected Long id;
	/** 备注  **/
	protected String remark;
	/** 创建日期  **/
	protected Date createTime;
	/** 更新日期  **/
	protected Date updateTime;
	/** 删除标记（0：正常；1：删除；2：审核）  **/
	protected String delFlag;
	/** 0 不是新记录  1 是新记录  **/
	protected String ifNew = "0";

	public BaseDataEntity() {
		super();
		this.delFlag = "0";
	}

	public void preInsert(){
		// 不限制ID为UUID，调用setIsNewRecord()使用自定义ID
		if(this.id == null || this.id == 0){
			setId(IdGen.id());
		}
		this.createTime = new Date();
		this.updateTime = this.createTime;
	}

	public void preUpdate(){
		this.updateTime = new Date();
	}

}
