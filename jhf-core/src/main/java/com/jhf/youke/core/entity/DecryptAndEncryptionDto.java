package com.jhf.youke.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author RHJ
 */
@Data
public class DecryptAndEncryptionDto implements Serializable {

    @ApiModelProperty(value = "随机字符串", required=true)
    private String nonceStr;

    @ApiModelProperty(value = "签名", required=true)
    private String sign;

    @ApiModelProperty(value = "加密数据", required=true)
    private String data;

}
