package com.jhf.youke.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author RHJ
 */
@Data
public class DeleteDto implements Serializable{

    @ApiModelProperty(value = "需要删除的id", required=true)
    List<Long> idList = new ArrayList();

}

