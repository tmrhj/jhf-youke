package com.jhf.youke.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * @author RHJ
 */
@Data
public class DetailsDto implements Serializable{

    @ApiModelProperty(value = "id", required=true)
    private Long id;

}

