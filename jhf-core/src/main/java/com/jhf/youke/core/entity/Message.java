package com.jhf.youke.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * ClassName: message <br/>
 * Description: <br/>
 * date: 2019/6/25 0025 下午 9:48<br/>
 *
 * @author Administrator<br />
 * @since JDK 1.8
 */
@Data
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String groupName;
    protected String topic;
    protected String tag;
    protected Object messages;
    /** 延时消息单位秒 **/
    private Long delayTime;

}
