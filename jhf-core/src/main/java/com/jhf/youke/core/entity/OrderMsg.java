package com.jhf.youke.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

/**
 * @author created by rhj
 * @since 2022/4/22 17:04
 */
@Data
@Accessors(chain = true)
public class OrderMsg implements Serializable {

	/** 订单创建时间，即该笔订单真实的创建时间。时间格式为 yyyy-MM-dd HH:mm:ss **/
	private String orderModifiedTime;
	/** 订单修改时间，时间格式为 yyyy-MM-dd HH:mm:ss.SSS **/
	private String orderCreateTime;
	/** 商品信息列表 **/
	private List<ItemOrderInfo> itemOrderList;
	/** 订单扩展字段 **/
	private List<OrderExtInfo> extInfo;
	/** 订单号  **/
	private String outBizNo;
	/** 支付宝用户 UID **/
	private String buyerId;
	/** 订单类型。固定为SERVICE_ORDER（服务订单）。 **/
	private String orderType;


	@Data
	public static class ItemOrderInfo implements Serializable{
		//商品名称
		private String itemName;
		//商品扩展参数。该参数用于传入用户购买商品图片及商家商品详情页地址等信息。
		private List<OrderExtInfo> extInfo;

	}

	@Data
	public static class OrderExtInfo implements Serializable{
		//key
		private String extKey;
		//value
		private String extValue;
	}


}
