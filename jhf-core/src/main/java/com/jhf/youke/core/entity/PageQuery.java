package com.jhf.youke.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;

/**
 *
 * @author RHJ
 * **/
@Data
public class PageQuery<T>{

    @ApiModelProperty(value = "查询条件", required=false)
    @Valid
    private T param;

    @ApiModelProperty(value = "页码", required=true)
    private int currentPage;

    @ApiModelProperty(value = "一页多少条", required=true)
    private int pageSize;

    @ApiModelProperty(value = "排序", required=false)
    private String sort;

    public PageQuery(){

    }

    public PageQuery(T t, int currentPage, int pageSize, String sort){
        this.param = t;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.sort = sort;
    }
}
