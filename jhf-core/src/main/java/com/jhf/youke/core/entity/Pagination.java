package com.jhf.youke.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

/**
 * 索引分页
 * @author rhj
 */
@Getter
public class Pagination<T>{

    @ApiModelProperty(value = "页码")
    private long pageNum;

    @ApiModelProperty(value = "一页多少条")
    private long pageSize;

    @ApiModelProperty(value = "总条数")
    private long totalSize;

    @ApiModelProperty(value = "列表数据")
    private List<T> list = Collections.emptyList();

    private Pagination(){}

    public Pagination(Long pageNum,Long pageSize, Long totalSize, List<T> list){
        this.pageNum =pageNum;
        this.pageSize = pageSize;
        this.totalSize = totalSize;
        this.list = list;
    }

    public static <T> Pagination<T> empty(){
        Pagination<T> indexedPagination = new Pagination();
        return indexedPagination;
    }

    public static <T> Pagination<T> create(long pageNum, long pageSize, long totalSize, List<T> list){
        Pagination<T> indexedPagination = new Pagination();
        indexedPagination.pageNum = pageNum;
        indexedPagination.pageSize = pageSize;
        indexedPagination.totalSize = totalSize;
        indexedPagination.list = list;
        return indexedPagination;
    }

}
