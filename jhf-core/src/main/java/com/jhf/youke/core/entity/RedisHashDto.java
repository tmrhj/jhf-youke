package com.jhf.youke.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RedisHashDto implements Serializable{

    private String key;
    private Map<String, Object> value;
    private long time;


    public RedisHashDto(){
    }

    public RedisHashDto(String key, Map<String, Object> value, long time){
        this.key = key;
        this.value = value;
        this.time = time;
    }

}

