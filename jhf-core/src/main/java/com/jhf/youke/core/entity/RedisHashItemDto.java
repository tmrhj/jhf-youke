package com.jhf.youke.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RedisHashItemDto implements Serializable{

    private String key;
    private String item;
    private Object value;
    private long time;
    private String[] itemList;

    public RedisHashItemDto(){
    }

    public RedisHashItemDto(String key, String[] itemList){
        this.key = key;
        this.itemList = itemList;
    }

    public RedisHashItemDto(String key, String item, Object value, long time){
        this.key = key;
        this.item = item;
        this.value = value;
        this.time = time;
    }

    public RedisHashItemDto(String key, String item, Object value, long time, String[] itemList){
        this.key = key;
        this.item = item;
        this.value = value;
        this.time = time;
        this.itemList = itemList;
    }
}

