package com.jhf.youke.core.entity;

import lombok.Getter;


/**
 * @author RHJ
 */
@Getter
public class Response<T>{

    private int code;

    private T data;

    private String error;

    private Response(){}

    public Response(int code, String error){
        this.code = code;
        this.error = error;
    }

    public static <T> Response<T> ok(T data){
        Response<T> result = new Response<>();
        result.code = 0;
        result.data = data;
        return result;
    }

    public static <T> Response<T> fail(String error){
        return fail(-1, error);
    }

    public static <T> Response<T> fail(int code, String error){
        Response<T> result = new Response<>();
        result.code = code;
        result.error = error;
        return result;
    }
}
