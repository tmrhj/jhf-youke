package com.jhf.youke.core.entity;


import com.jhf.youke.core.utils.StringUtils;
import lombok.Data;

import java.util.Map;

/**
 * @author RHJ
 */
@Data
public class SagaMsg {
    private Long bizId;
    private Integer objectId;
    private String code;
    private Map<String,Object> message;

    public SagaMsg(){

    }

    public SagaMsg(Map<String, Object> map) {
        this.bizId = StringUtils.toLong(map.get("bizId"));
        this.objectId = StringUtils.toInteger(map.get("objectId"));
        this.code = StringUtils.chgNull(map.get("code"));
        this.message = (Map<String,Object>) map.get("message");
    }

}
