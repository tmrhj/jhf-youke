package com.jhf.youke.core.entity;

import lombok.Data;

/**
 * @author RHJ
 */
@Data
public class SendResult {
    private MqData data;
    private Integer code;
    private String error;

    @Data
    public class MqData{
        private String sendStatus;
        private String msgId;
        private String queueOffset;
        private String offsetMsgId;
        private String regionId;
        private MessageQueue messageQueue;

    }

    @Data
    public class MessageQueue{
        private String topic ;
        private String brokerName ;
        private Integer queueId ;
    }

};