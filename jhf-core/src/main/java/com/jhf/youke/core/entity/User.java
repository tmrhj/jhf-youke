package com.jhf.youke.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.Set;


/**
 * 系统用户(User)实体类
 * @author rhj
 * @since 2021-12-15 15:18:02
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class User {

    private static final long serialVersionUID = 861098541016940491L;

    private Set<String> permissionsMap;

    private Long id;

    /** 用户名 **/
    private String loginName;

    /**密码 **/
    private String password;

    /**二级密码 **/
    private String passwordTwo;

    /**盐值 **/
    private String salt;

    /**姓名 **/
    private String name;

    /**头像 **/
    private String headImage;

    /**性别 0未知 1男 2女 **/
    private Integer sex;

    /**手机号 **/
    private String phone;

    /**地区ID 关联区域表 **/
    private Long areaId;

    /**公众号OpenID **/
    private String openId;

    /**单位ID **/
    private Long companyId;

    /**单位名称 **/
    private String companyName;

    /**根单位ID **/
    private Long rootId;

    /**详细地址 **/
    private String address;

    /**详细地址 **/
    private String companyIds;

    /**生日 **/
    private Date birthday;

    /**状态  0：禁用   1：正常 **/
    private Integer status;

    /**部门ID **/
    private Long deptId;

    /**用户类型 1管理员 2B端用户 3供应链 4厂商 5 C端 **/
    private Integer type;
    /** 备注 **/
    private String remark;
    /** 创建日期 **/
    private Date createTime;
    /** 更新日期 **/
    private Date updateTime;
    /** 删除标记（0：正常；1：删除；2：审核） **/
    private String delFlag = "0";
}

