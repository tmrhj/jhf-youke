package com.jhf.youke.core.exception;

/**
 * @author RHJ
 * **/
public abstract class BaseApplicationException extends RuntimeException {
    public BaseApplicationException(String message) {
        super(message);
    }

    public BaseApplicationException(String message, Exception ex) {
        super(message, ex);
    }

}