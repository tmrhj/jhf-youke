package com.jhf.youke.core.exception;

/**
 * @author ZJ
 * @description: 幂等重复提交判断异常
 * @date 2022/1/410:16
 */
public class CheckKeyExceptionBase extends BaseApplicationException {

    public CheckKeyExceptionBase(String message)
    {
        super(message);
    }

}
