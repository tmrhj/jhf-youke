package com.jhf.youke.core.feign;


import com.jhf.youke.core.ddd.CacheService;
import com.jhf.youke.core.entity.RedisHashDto;
import com.jhf.youke.core.entity.RedisHashItemDto;
import com.jhf.youke.core.feign.hystrix.RedisFeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 复述,假装
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Component
@FeignClient(name="jhf-redis-service", url = "${feign.redisUrl}" ,fallback = RedisFeignHystrix.class)
public interface RedisFeign extends CacheService {

    /**
     * 得到
     *
     * @param key 关键
     * @return {@link String}
     */
    @RequestMapping(value="/redis/get")
    @Override
    String get(@RequestParam("key") String key);

    /**
     * 集
     *
     * @param key   关键
     * @param value 价值
     * @param time  时间
     * @return {@link String}
     */
    @RequestMapping(value="/redis/set")
    @Override
    String set(@RequestParam("key") String key, @RequestParam("value") String value,
                      @RequestParam("time") long time);

    /**
     * 设定json
     *
     * @param map 地图
     * @return {@link String}
     */
    @RequestMapping(value="/redis/setByJson")
    @Override
    String setByJson(@RequestBody Map<String,Object> map);


    /**
     * ▽
     *
     * @param key 关键
     * @return {@link String}
     */
    @RequestMapping(value="/redis/del")
    @Override
    String del(@RequestParam("key") String key);

    /**
     * 有时间
     * 获取缓存过期时间
     *
     * @param key 关键
     * @return {@link String}
     */
    @GetMapping(value="/redis/getTime")
    @Override
    String getTime(@RequestParam("key") String key);

    /**
     * 锁
     * @param key       关键
     * @param requestId 请求id
     * @param tiemout   tiemout
     * @return {@link Boolean}
     */
    @GetMapping(value="/redis/lock")
    @Override
    Boolean lock(@RequestParam("key") String key,@RequestParam("requestId") String requestId ,@RequestParam("timeout") Integer tiemout);


    /**
     * 释放
     *
     * @param key           关键
     * @param retIdentifier ret标识符
     * @return {@link Boolean}
     */
    @GetMapping(value="/redis/release")
    @Override
    Boolean release(@RequestParam("key") String key,@RequestParam("retIdentifier") String retIdentifier);


    /**
     * 设置列表
     *
     * @param map 地图
     * @return {@link Object}
     */
    @PostMapping(value="/redis/setList")
    @Override
    Object setList(@RequestBody  Map<String,Object> map);

    /**
     * 设置一个列表
     *
     * @param map 地图
     * @return {@link Object}
     */
    @PostMapping(value="/redis/setOneForList")
    @Override
    Object setOneForList(@RequestBody  Map<String,Object> map);


    /**
     * 得到列表
     *
     * @param key   关键
     * @param start 开始
     * @param end   结束
     * @return {@link List}<{@link Object}>
     */
    @RequestMapping(value="/redis/getList")
    @Override
     List<Object> getList(@RequestParam("key") String key, @RequestParam("start") Long start, @RequestParam("end") Long end );


    /**
     * lpop
     *
     * @param key 关键
     * @return {@link Object}
     */
    @RequestMapping(value="/redis/lpop")
    @Override
    Object lpop(@RequestParam("key") String key);


    /**
     * 获取散列
     *
     * @param key 关键
     * @return {@link Map}<{@link Object}, {@link Object}>
     */
    @RequestMapping(value="/redis/getHash")
    @Override
    Map<Object, Object> getHash(@RequestParam("key") String key);

    /**
     * 将哈希
     *
     * @param dto key
     * @return {@link String}
     */
    @RequestMapping(value="/redis/setHash")
    @Override
    String setHash(@RequestBody RedisHashDto dto);

    /**
     * 得到哈希条目
     *
     * @param key  关键
     * @param item 项
     * @return {@link String}
     */
    @RequestMapping(value="/redis/getHashItem")
    @Override
    String getHashItem(@RequestParam("key") String key ,@RequestParam("item") String item);


    /**
     * 将哈希条目
     *
     * @param dto key
     * @return {@link String}
     */
    @RequestMapping(value="/redis/setHashItem")
    @Override
    String setHashItem(@RequestBody RedisHashItemDto dto);

    /**
     * 德尔·哈希条目
     *
     * @param dto key
     * @return {@link String}
     */
    @RequestMapping(value="/redis/delHashItem")
    @Override
    String delHashItem(@RequestBody RedisHashItemDto dto);

}
