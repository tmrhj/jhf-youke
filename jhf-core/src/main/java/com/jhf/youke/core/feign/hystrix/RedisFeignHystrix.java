package com.jhf.youke.core.feign.hystrix;


import com.jhf.youke.core.entity.RedisHashDto;
import com.jhf.youke.core.entity.RedisHashItemDto;
import com.jhf.youke.core.feign.RedisFeign;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;


/**
 * @author RHJ
 */
@Component
public class RedisFeignHystrix implements RedisFeign {

    @Override
    public String get(String key) {
        return null;
    }

    @Override
    public String set(String key, String value, long time) {
        return null;
    }

    @Override
    public String setByJson(Map<String,Object> map) {
        return null;
    }

    @Override
    public String del(String key) {
        return null;
    }

    @Override
    public String getTime(String key) {
        return null;
    }

    @Override
    public Boolean lock(String key, String requestId, Integer tiemout) {
        return null;
    }

    @Override
    public Boolean release(@RequestParam("key") String key,@RequestParam("retIdentifier") String retIdentifier){
        return null;
    }


    @Override
    public Object setList(@RequestBody  Map<String,Object> map){
        return null;
    }

    @Override
    public Object setOneForList(@RequestBody  Map<String,Object> map){
        return null;
    }


    @Override
    public List<Object> getList(@RequestParam("key") String key, @RequestParam("start") Long start, @RequestParam("end") Long end ){
        return null;
    }


    @Override
    public Object lpop(@RequestParam("key") String key){
        return null;
    }

    @Override
    public Map<Object, Object> getHash(@RequestParam("key") String key){
        return null;
    }

    @Override
    public String setHash(@RequestBody RedisHashDto dto){
        return null;
    }

    @Override
    public String getHashItem(@RequestParam("key") String key ,@RequestParam("item") String item){
        return null;
    }

    @Override
    public String setHashItem(@RequestBody RedisHashItemDto dto){
        return null;
    }

    @Override
    public String delHashItem(@RequestBody RedisHashItemDto dto){
        return null;
    }

}
