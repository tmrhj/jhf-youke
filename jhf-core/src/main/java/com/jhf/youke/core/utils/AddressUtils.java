package com.jhf.youke.core.utils;


import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 根据IP获取地址
 *  @author  ZJ
 *  2022-01-14
 */
public class AddressUtils
{
    private static final Logger log = LoggerFactory.getLogger(AddressUtils.class);

    /** IP地址查询 **/
    public static final String IP_URL = "http:/**whois.pconline.com.cn/ipJson.jsp";
    /** 未知地址 **/
    public static final String UNKNOWN = "XX XX";

    public static String getRealAddressByIp(String ip)
    {
        String address = UNKNOWN;
        // 内网不查询
        if (IpUtils.internalIp(ip))
        {
            return "内网IP";
        }
        try
        {
            String rspStr = HttpUtil.get(IP_URL + "?ip=" + ip + "&json=true", CharsetUtil.CHARSET_GBK);
            if (StringUtils.isEmpty(rspStr))
            {
                return UNKNOWN;
            }
            JSONObject obj = JSONObject.parseObject(rspStr);
            String region = obj.getString("pro");
            String city = obj.getString("city");
            return String.format("%s %s", region, city);
        }
        catch (Exception e) {
        }
        return address;
    }

    public static void main(String[] args) {
        System.out.println(getRealAddressByIp("175.0.74.185"));
    }

}
