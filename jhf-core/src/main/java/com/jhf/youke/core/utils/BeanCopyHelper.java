package com.jhf.youke.core.utils;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
public class BeanCopyHelper {

    public static <S, T> T copy(S s, T t){
        BeanUtils.copyProperties(s, t);
        return t;
    }

    public static <S, T> List<T> copy(List<S> list, Supplier<T> supplier){
        return list.stream().map(s -> copy(s, supplier.get())).collect(Collectors.toList());
    }
}
