package com.jhf.youke.core.utils;



import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.jhf.youke.core.ddd.CacheService;
import com.jhf.youke.core.entity.RedisHashDto;
import com.jhf.youke.core.entity.RedisHashItemDto;
import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.feign.RedisFeign;
import java.util.List;
import java.util.Map;

/**
 * @author RHJ
 */
public class CacheUtils {

    private static CacheService cacheService = SpringUtil.getBean(RedisFeign.class);

    public static String get(String key){
        return cacheService.get(key);
    }

    public static String del(String key){
        return cacheService.del(key);
    }

    public static String set(String key, String value, long time){
        return cacheService.set(key, value, time);
    }

    public static String setByJson(Map<String,Object> map){
        return cacheService.setByJson(map);
    }

    public static String getTime(String key){
        return cacheService.getTime(key);
    }

    public static<T> T getObject(String key, Class<T> toValueType)throws Exception{
        String json = cacheService.get(key);
        if(json == null){
            return null;
        }
        return  JSONUtil.toBean(json,toValueType);
    }

    /**
     * @author : RHJ
     * @description : 得到一个LIST ,返回值为ArrayList<LinkedHashMap<>>
     * @param
     * @return
     * @date   2018/9/14 0014 10:29
     */
    public static<T> List<T> getList(String key, Class<T> toValueType)throws Exception{
        String json = cacheService.get(key);
        if(json == null){
            return null;
        }
        return JSONUtil.toList(json,toValueType);
    }

    public static List<Object> getList(String key, Long start, Long end){
        return cacheService.getList(key, start, end);
    }


    public static Object setList(Map<String,Object> map){
        return cacheService.setList(map);
    }


   public Object setOneForList(Map<String,Object> map){
        return cacheService.setOneForList(map);
    }

    public Object lpop( String key){
        return cacheService.lpop(key);
    }


    public static User getUser(String token){
        String json = get(token);
        if(json == null){
            return null;
        }
        User user = JSONUtil.toBean(json, User.class);
        return user;
    }

    public static Boolean lock(String key, String requestId, Integer timeout){
        return  cacheService.lock(key,requestId,timeout);
    }

    public static Boolean release(String key, String retIdentifier){
        return  cacheService.release(key, retIdentifier);
    }

    public static  Map<Object, Object> getHash(String key){
        return  cacheService.getHash(key);
    }

    /**
    * @Description: 设置多条数据
    * @Param: [dto]
    * @return: java.lang.String
    * @Author: RHJ
    * @Date: 2022/3/5
    */
    public static String setHash(RedisHashDto dto){
        return  cacheService.setHash(dto);
    }

    public static String getHashItem( String key , String item){
        return  cacheService.getHashItem(key, item);
    }

    public static String setHashItem(RedisHashItemDto dto){
        return  cacheService.setHashItem(dto);
    }

    public static String delHashItem(RedisHashItemDto dto){
        return  cacheService.delHashItem(dto);
    }


}
