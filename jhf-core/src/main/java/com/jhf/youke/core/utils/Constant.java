package com.jhf.youke.core.utils;

/**
 * @author RHJ
 */
public class Constant {

    /**http请求 **/
    public static final String HTTP = "http:/**";

    /** https请求 **/
    public static final String HTTPS = "https:/**";

    /** Layout组件标识 */
    public final static String LAYOUT = "Layout";

    /** ParentView组件标识 */
    public final static String PARENT_VIEW = "ParentView";

    /** InnerLink组件标识 */
    public final static String INNER_LINK = "InnerLink";

    public static final Integer RESPONSE_OK = 0;
    public static final Integer RESPONSE_FAIL = -1;

    public static final Long SUPER_ADMINISTRATOR = 10000L;

    public static final String IF_NEW = "1";

    public static final String DEFAULT_DEL_FLAG = "0";

    public static final String RESPONSE_SUCCESS = "0";
    public static final String RESPONSE_ERROR = "-1";
    public static final String RESPONSE_CODE = "code";
    public static final String RESPONSE_DATA = "data";

    public static final Integer CODE_NOT_REGISTERED = -101;
    public static final Integer CODE_UNDER_REVIEW = -102;
    public static final Integer CODE_SIGNATURE_EXCEPTION =-100;
    public static final Integer CODE_PASSWORD_ERROR =-103;
    public static final Integer CODE_NORMAL = 0;


    public static final String FIND_NOT_DATA = "find not data";

    public static final String RESPONSE_WX_ERROR_CODE = "errcode";

    /** 视频用户默认角色 **/
    public static final Long VIDEO_USER_DEFAULT_ROLE = 77595139028549632L;
    /** 团长默认角色 **/
    public static final Long REGIMENT_USER_DEFAULT_ROLE = 76941343665422336L;
    /** 客户默认角色 **/
    public static final Long CUSTOMER_USER_DEFAULT_ROLE = 78207250380357632L;
}
