package com.jhf.youke.core.utils;



import java.util.ArrayList;

/**
* @Description:  自定义有界先进先出队列
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/1/10
*/
public class FoFiArrayList<T>{
    private ArrayList<T> arrayList;
    private int limit;

    public FoFiArrayList(int limit){
        this.limit = limit;
    }

    /**
     * 入队
     */
    public void add(T t) {
        if (arrayList == null) {
            arrayList = new ArrayList<T>();
        }
        if(arrayList.size() == limit){
            arrayList.remove(0);
        }
        arrayList.add(t);
    }

    public T get(int index) {
        return arrayList == null ? null : arrayList.get(index);
    }

    public int size() {
        return arrayList == null ? 0 : arrayList.size();
    }



    public static void main(String[] args) {
        int init = 10;
        FoFiArrayList list = new FoFiArrayList<String>(3);
        for(int i=0; i<init; i++){
            list.add(String.valueOf(i));
        }
        for(int i=0; i<list.size(); i++){
            System.out.println(list.get(i));
        }

    }

}
