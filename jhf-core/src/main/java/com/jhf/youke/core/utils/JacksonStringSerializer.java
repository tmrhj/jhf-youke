package com.jhf.youke.core.utils;


import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;


/**
 * @author RHJ
 */
public class JacksonStringSerializer extends JsonSerializer<Long> {

    @Override
    public void serialize(Long aLong, com.fasterxml.jackson.core.JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, com.fasterxml.jackson.core.JsonProcessingException {
        jsonGenerator.writeString(String.valueOf(aLong));
    }
}
