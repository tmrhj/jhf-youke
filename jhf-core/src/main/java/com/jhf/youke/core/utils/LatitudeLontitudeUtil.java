package com.jhf.youke.core.utils;

/**
 * @author RHJ
 * **/
public class LatitudeLontitudeUtil {
	
	/** 地球半径 */  
    private static final double EARTH_RADIUS = 6371370;  
    /** 范围距离 */  
    private double distance;  
    /** 左上角 */  
    private Location leftTop = null;  
    /** 右上角 */  
    private Location rightTop = null;  
    /** 左下角 */  
    private Location leftBottom = null;  
    /** 右下角 */  
    private Location rightBottom = null;  
  
    private LatitudeLontitudeUtil(double distance) {  
        this.distance = distance;  
    }  
  
    private void getRectangle4Point(double lat, double lng) {  
  
        // float dlng = 2 * asin(sin(distance / (2 * EARTH_RADIUS)) / cos(lat));  
        // float dlng = degrees(dlng) // 弧度转换成角度  
        double dlng = 2 * Math.asin(Math.sin(distance / (2 * EARTH_RADIUS))  
                / Math.cos(lat));  
        dlng = Math.toDegrees(dlng);  
  
        // dlat = distance / EARTH_RADIUS  
        // dlng = degrees(dlat) # 弧度转换成角度  
        double dlat = distance / EARTH_RADIUS;
        // # 弧度转换成角度
        dlat = Math.toDegrees(dlat);

        leftTop = new Location(lat + dlat, lng - dlng);  
        rightTop = new Location(lat + dlat, lng + dlng);  
        leftBottom = new Location(lat - dlat, lng - dlng);  
        rightBottom = new Location(lat - dlat, lng + dlng);  
  
    }  
  
    public static double hav(double theta) {  
        double s = Math.sin(theta / 2);  
        return s * s;  
    }  
  
    public static Integer getDistance(double lat0, double lng0, double lat1,
            double lng1) {  
      
        lat0 = Math.toRadians(lat0);  
        lat1 = Math.toRadians(lat1);  
        lng0 = Math.toRadians(lng0);  
        lng1 = Math.toRadians(lng1);

        Double dlng = Math.abs(lng0 - lng1);
        Double dlat = Math.abs(lat0 - lat1);
        Double h = hav(dlat) + Math.cos(lat0) * Math.cos(lat1) * hav(dlng);
        Double distance = 2 * EARTH_RADIUS * Math.asin(Math.sqrt(h));

  
        return distance.intValue();
    }  
        
    
    public static double getDistance1(double lat1, double lng1, double lat2, double lng2) {
		double radLat1 = Math.toRadians(lat1);
		double radLat2 = Math.toRadians(lat2);
        // 两点纬度差
		double a = Math.abs(radLat1 - radLat2);
        // 两点的经度差
		double b = Math.abs(Math.toRadians(lng1) - Math.toRadians(lng2));
		double s =  2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) +
				Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
		
		s = s * EARTH_RADIUS;
		return s ;
		
	
		
	}
  
    public static Location[] getRectangle4Point(double lat, double lng,  
            double distance) {  
        LatitudeLontitudeUtil llu = new LatitudeLontitudeUtil(distance);

        llu.getRectangle4Point(lat, lng);  
        Location[] locations = new Location[4];  
        locations[0] = llu.leftTop;  
        locations[1] = llu.rightTop;  
        locations[2] = llu.leftBottom;  
        locations[3] = llu.rightBottom;  
        
        return locations;  
    }


    public static  double[] mapTx2bd(double lat, double lon){
        double bdLat;
        double bdLon;
        double xPi=3.14159265358979324;
        double x = lon, y = lat;
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * xPi);
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * xPi);
        bdLon = z * Math.cos(theta) + 0.0065;
        bdLat = z * Math.sin(theta) + 0.006;

        System.out.println("bdLat:"+bdLat);
        System.out.println("bdLon:"+bdLon);

        return new double[]{bdLat,bdLon};
    }

    
    
     public static void main(String[] args) {

    	 Location location = new Location(28.174932d,113.04834d);
    	 Location[] locations = LatitudeLontitudeUtil.getRectangle4Point(location.getLatitude(), location.getLongitude(), 100);
         System.out.println(location.getLongitude() +","+location.getLatitude());
    	 System.out.println("leftTop:"+locations[0].getLongitude()+","+locations[0].getLatitude());
    	 System.out.println("rightTop:"+locations[1].getLongitude()+","+locations[1].getLatitude());
    	 System.out.println("leftBottom:"+locations[2].getLongitude()+","+locations[2].getLatitude());
    	 System.out.println("rightBottom:"+locations[3].getLongitude()+","+locations[3].getLatitude());
   
   
         double d1 = LatitudeLontitudeUtil.getDistance(location.getLatitude(), location.getLongitude(), locations[0].getLatitude(), locations[0].getLongitude());  
         double d2 = LatitudeLontitudeUtil.getDistance(location.getLatitude(), location.getLongitude(), locations[1].getLatitude(), locations[1].getLongitude());  
         double d5 = LatitudeLontitudeUtil.getDistance(location.getLatitude(), location.getLongitude(), locations[2].getLatitude(), locations[2].getLongitude()); 
         double d6 = LatitudeLontitudeUtil.getDistance(location.getLatitude(), location.getLongitude(), locations[3].getLatitude(), locations[3].getLongitude()); 
         
         System.out.println(d1);  
         System.out.println(d2);       
         System.out.println(d5);  
         System.out.println(d6);

         mapTx2bd(28.172129,113.03625);
    
     
     }
  


}
