package com.jhf.youke.core.utils;

import java.util.ArrayList;


/**
 * @author RHJ
 */
public class LimitQueue<T> {
    private ArrayList<T> arrayList;
    private int limit;
    private static final Integer MAX_COUNT =10;

    public LimitQueue(int limit){
        this.limit = limit;
    }

    /**
     * 入队
     */
    public void add(T t) {
        if (arrayList == null) {
            arrayList = new ArrayList<T>();
        }
        if(arrayList.size() == limit){
            arrayList.remove(0);
        }
        arrayList.add(t);
    }

    public T get(int index) {
        return arrayList == null ? null : arrayList.get(index);
    }

    public int size() {
        return arrayList == null ? 0 : arrayList.size();
    }


    public static void main(String[] args) {
        LimitQueue demo = new LimitQueue<String>(5);
        for(int i=0; i<MAX_COUNT; i++){
            demo.add(String.valueOf(i));
        }
        for(int i=0; i<demo.size(); i++){
            System.out.println(demo.get(i));
        }

    }



}


