package com.jhf.youke.core.utils;

import cn.hutool.json.JSONObject;

/**
 * @author RHJ
 */
public class ResultUtils {

    public static Boolean getOkFlag(JSONObject res) {
        return res != null && 0 == Convert.toInt(res.get("code").toString());
    }

}
