package com.jhf.youke.crm.adapter.web;

import com.jhf.youke.crm.app.executor.CustomerAddressAppService;
import com.jhf.youke.crm.domain.model.dto.CustomerAddressDto;
import com.jhf.youke.crm.domain.model.vo.CustomerAddressVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/customerAddress")
public class CustomerAddressController {

    @Resource
    private CustomerAddressAppService customerAddressAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody CustomerAddressDto dto) {
        return Response.ok(customerAddressAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody CustomerAddressDto dto) {
        return Response.ok(customerAddressAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody CustomerAddressDto dto) {
        return Response.ok(customerAddressAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<CustomerAddressVo>> findById(Long id) {
        return Response.ok(customerAddressAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(customerAddressAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<CustomerAddressVo>> list(@RequestBody CustomerAddressDto dto, @RequestHeader("token") String token) {

        return Response.ok(customerAddressAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<CustomerAddressVo>> selectPage(@RequestBody  CustomerAddressDto dto, @RequestHeader("token") String token) {
        return Response.ok(customerAddressAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(customerAddressAppService.getPreData());
    }
    

}

