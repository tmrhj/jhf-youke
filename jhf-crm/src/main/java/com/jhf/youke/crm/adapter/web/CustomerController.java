package com.jhf.youke.crm.adapter.web;

import com.jhf.youke.crm.app.executor.CustomerAppService;
import com.jhf.youke.crm.domain.model.dto.CustomerDto;
import com.jhf.youke.crm.domain.model.vo.CustomerVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author  RHJ
 * **/
@Api(tags = "")
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Resource
    private CustomerAppService customerAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody CustomerDto dto) {
        return Response.ok(customerAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody CustomerDto dto) {
        return Response.ok(customerAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody CustomerDto dto) {
        return Response.ok(customerAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<CustomerVo>> findById(Long id) {
        return Response.ok(customerAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(customerAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<CustomerVo>> list(@RequestBody CustomerDto dto, @RequestHeader("token") String token) {

        return Response.ok(customerAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<CustomerVo>> selectPage(@RequestBody  CustomerDto dto, @RequestHeader("token") String token) {
        return Response.ok(customerAppService.selectPage(dto));
    }

    @PostMapping("/getListBySale")
    @ApiOperation("根据销售员/团长获取用户列表")
    public Response<List<CustomerVo>> getListBySale(@RequestBody CustomerDto dto) {
        return Response.ok(customerAppService.getListBySale(dto.getSaleMan()));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(customerAppService.getPreData());
    }
    

}

