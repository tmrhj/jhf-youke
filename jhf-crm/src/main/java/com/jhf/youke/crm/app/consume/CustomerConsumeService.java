package com.jhf.youke.crm.app.consume;

import com.jhf.youke.crm.domain.model.Do.CustomerDo;
import com.jhf.youke.crm.domain.service.CustomerService;
import com.jhf.youke.saga.client.annotation.SagaReply;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author RHJ
 */
@Component
public class CustomerConsumeService {
    @Resource
    CustomerService customerService;

    @SagaReply(abnormal = "create_customer_abnormal")
    public void create(Map<String, Object> map) {
        Map<String,Object> msg = (Map<String,Object>) map.get("message");
        CustomerDo customerDo = new CustomerDo(msg);
        customerService.create(customerDo);
    }

}
