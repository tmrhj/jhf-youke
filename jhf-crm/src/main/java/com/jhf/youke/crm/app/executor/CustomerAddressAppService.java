package com.jhf.youke.crm.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.crm.domain.converter.CustomerAddressConverter;
import com.jhf.youke.crm.domain.model.Do.CustomerAddressDo;
import com.jhf.youke.crm.domain.model.dto.CustomerAddressDto;
import com.jhf.youke.crm.domain.model.vo.CustomerAddressVo;
import com.jhf.youke.crm.domain.service.CustomerAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class CustomerAddressAppService extends BaseAppService {

    @Resource
    private CustomerAddressService customerAddressService;

    @Resource
    private CustomerAddressConverter customerAddressConverter;


    public boolean update(CustomerAddressDto dto) {
        CustomerAddressDo customerAddressDo =  customerAddressConverter.dto2Do(dto);
        return customerAddressService.update(customerAddressDo);
    }

    public boolean delete(CustomerAddressDto dto) {
        CustomerAddressDo customerAddressDo =  customerAddressConverter.dto2Do(dto);
        return customerAddressService.delete(customerAddressDo);
    }


    public boolean insert(CustomerAddressDto dto) {
        CustomerAddressDo customerAddressDo =  customerAddressConverter.dto2Do(dto);
        return customerAddressService.insert(customerAddressDo);
    }

    public Optional<CustomerAddressVo> findById(Long id) {
        return customerAddressService.findById(id);
    }


    public boolean remove(Long id) {
        return customerAddressService.remove(id);
    }


    public List<CustomerAddressVo> findAllMatching(CustomerAddressDto dto) {
        CustomerAddressDo customerAddressDo =  customerAddressConverter.dto2Do(dto);
        return customerAddressService.findAllMatching(customerAddressDo);
    }


    public Pagination<CustomerAddressVo> selectPage(CustomerAddressDto dto) {
        CustomerAddressDo customerAddressDo =  customerAddressConverter.dto2Do(dto);
        return customerAddressService.selectPage(customerAddressDo);
    }



}

