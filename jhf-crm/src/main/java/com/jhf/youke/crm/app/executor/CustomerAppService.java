package com.jhf.youke.crm.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.crm.domain.converter.CustomerConverter;
import com.jhf.youke.crm.domain.model.Do.CustomerDo;
import com.jhf.youke.crm.domain.model.dto.CustomerDto;
import com.jhf.youke.crm.domain.model.vo.CustomerVo;
import com.jhf.youke.crm.domain.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Slf4j
@Service
public class CustomerAppService extends BaseAppService {

    @Resource
    private CustomerService customerService;

    @Resource
    private CustomerConverter customerConverter;


    public boolean update(CustomerDto dto) {
        CustomerDo customerDo =  customerConverter.dto2Do(dto);
        return customerService.update(customerDo);
    }

    public boolean delete(CustomerDto dto) {
        CustomerDo customerDo =  customerConverter.dto2Do(dto);
        return customerService.delete(customerDo);
    }


    public boolean insert(CustomerDto dto) {
        CustomerDo customerDo =  customerConverter.dto2Do(dto);
        return customerService.insert(customerDo);
    }

    public Optional<CustomerVo> findById(Long id) {
        return customerService.findById(id);
    }


    public boolean remove(Long id) {
        return customerService.remove(id);
    }


    public List<CustomerVo> findAllMatching(CustomerDto dto) {
        CustomerDo customerDo =  customerConverter.dto2Do(dto);
        return customerService.findAllMatching(customerDo);
    }


    public Pagination<CustomerVo> selectPage(CustomerDto dto) {
        CustomerDo customerDo =  customerConverter.dto2Do(dto);
        return customerService.selectPage(customerDo);
    }
    


    public List<CustomerVo> getListBySale(Long saleMan) {
        return customerService.getListBySale(saleMan);
    }
}

