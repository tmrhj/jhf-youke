package com.jhf.youke.crm.domain.converter;

import com.jhf.youke.crm.domain.model.Do.CustomerAddressDo;
import com.jhf.youke.crm.domain.model.dto.CustomerAddressDto;
import com.jhf.youke.crm.domain.model.po.CustomerAddressPo;
import com.jhf.youke.crm.domain.model.vo.CustomerAddressVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 客户地址转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface CustomerAddressConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param customerAddressDto 客户地址dto
     * @return {@link CustomerAddressDo}
     */
    CustomerAddressDo dto2Do(CustomerAddressDto customerAddressDto);

    /**
     * 洗阿宝
     *
     * @param customerAddressDo 客户地址做
     * @return {@link CustomerAddressPo}
     */
    CustomerAddressPo do2Po(CustomerAddressDo customerAddressDo);

    /**
     * 洗签证官
     *
     * @param customerAddressDo 客户地址做
     * @return {@link CustomerAddressVo}
     */
    CustomerAddressVo do2Vo(CustomerAddressDo customerAddressDo);


    /**
     * 洗订单列表
     *
     * @param customerAddressDoList 客户地址做列表
     * @return {@link List}<{@link CustomerAddressPo}>
     */
    List<CustomerAddressPo> do2PoList(List<CustomerAddressDo> customerAddressDoList);

    /**
     * 警察乙做
     *
     * @param customerAddressPo 客户地址阿宝
     * @return {@link CustomerAddressDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    CustomerAddressDo po2Do(CustomerAddressPo customerAddressPo);

    /**
     * 警察乙签证官
     *
     * @param customerAddressPo 客户地址阿宝
     * @return {@link CustomerAddressVo}
     */
    CustomerAddressVo po2Vo(CustomerAddressPo customerAddressPo);

    /**
     * 警察乙做列表
     *
     * @param customerAddressPoList 客户地址订单列表
     * @return {@link List}<{@link CustomerAddressDo}>
     */
    List<CustomerAddressDo> po2DoList(List<CustomerAddressPo> customerAddressPoList);

    /**
     * 警察乙vo列表
     *
     * @param customerAddressPoList 客户地址订单列表
     * @return {@link List}<{@link CustomerAddressVo}>
     */
    List<CustomerAddressVo> po2VoList(List<CustomerAddressPo> customerAddressPoList);


}

