package com.jhf.youke.crm.domain.converter;

import com.jhf.youke.crm.domain.model.Do.CustomerDo;
import com.jhf.youke.crm.domain.model.dto.CustomerDto;
import com.jhf.youke.crm.domain.model.po.CustomerPo;
import com.jhf.youke.crm.domain.model.vo.CustomerVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;
import java.util.List;


/**
 * 客户转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface CustomerConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param customerDto 客户dto
     * @return {@link CustomerDo}
     */
    CustomerDo dto2Do(CustomerDto customerDto);

    /**
     * 洗阿宝
     *
     * @param customerDo 客户做
     * @return {@link CustomerPo}
     */
    CustomerPo do2Po(CustomerDo customerDo);

    /**
     * 洗签证官
     *
     * @param customerDo 客户做
     * @return {@link CustomerVo}
     */
    CustomerVo do2Vo(CustomerDo customerDo);


    /**
     * 洗订单列表
     *
     * @param customerDoList 客户做列表
     * @return {@link List}<{@link CustomerPo}>
     */
    List<CustomerPo> do2PoList(List<CustomerDo> customerDoList);

    /**
     * 警察乙做
     *
     * @param customerPo 客户订单
     * @return {@link CustomerDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    CustomerDo po2Do(CustomerPo customerPo);

    /**
     * 警察乙签证官
     *
     * @param customerPo 客户订单
     * @return {@link CustomerVo}
     */
    CustomerVo po2Vo(CustomerPo customerPo);

    /**
     * 警察乙做列表
     *
     * @param customerPoList 客户订单列表
     * @return {@link List}<{@link CustomerDo}>
     */
    List<CustomerDo> po2DoList(List<CustomerPo> customerPoList);

    /**
     * 警察乙vo列表
     *
     * @param customerPoList 客户订单列表
     * @return {@link List}<{@link CustomerVo}>
     */
    List<CustomerVo> po2VoList(List<CustomerPo> customerPoList);


}

