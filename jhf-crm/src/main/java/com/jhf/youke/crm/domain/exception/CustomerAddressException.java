package com.jhf.youke.crm.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class CustomerAddressException extends DomainException {

    public CustomerAddressException(String message) { super(message); }
}

