package com.jhf.youke.crm.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class CustomerException extends DomainException {

    public CustomerException(String message) { super(message); }
}

