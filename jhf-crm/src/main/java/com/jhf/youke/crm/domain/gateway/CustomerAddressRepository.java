package com.jhf.youke.crm.domain.gateway;


import com.jhf.youke.crm.domain.model.po.CustomerAddressPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface CustomerAddressRepository extends Repository<CustomerAddressPo> {


}

