package com.jhf.youke.crm.domain.gateway;


import com.jhf.youke.crm.domain.model.po.CustomerPo;
import com.jhf.youke.core.ddd.Repository;

import java.util.List;

/**
 * 客户库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface CustomerRepository extends Repository<CustomerPo> {

    /**
     * 被销售列表
     *
     * @param saleMan 销售人
     * @return {@link List}<{@link CustomerPo}>
     */
    List<CustomerPo> getListBySale(Long saleMan);

    /**
     * 通过公司
     *
     * @param companyId 公司标识
     * @param phone     电话
     * @return {@link CustomerPo}
     */
    CustomerPo getByCompany(Long companyId, String phone);

}

