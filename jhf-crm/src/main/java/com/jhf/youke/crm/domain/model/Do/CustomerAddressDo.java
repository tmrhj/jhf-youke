package com.jhf.youke.crm.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.crm.domain.exception.CustomerAddressException;
import lombok.Data;

import java.util.Date;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class CustomerAddressDo extends BaseDoEntity {

  private static final long serialVersionUID = 395053995835790885L;

    /** 客户标识 **/
    private Long customerId;

    /** 用户标识 **/
    private Long userId;

    /** 省 **/
    private String province;

    /** 市 **/
    private String city;

    /** 区 **/
    private String district;

    /** 街道 **/
    private String street;

    /** 详情地址 **/
    private String address;

    /** 是否默认 **/
    private Integer ifDefault;

    private Date createTime;

    private Date updateTime;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new CustomerAddressException(errorMessage);
        }
        return obj;
    }

    private CustomerAddressDo validateNull(CustomerAddressDo customerAddressDo){
          // 可使用链式法则进行为空检查
          customerAddressDo.
          requireNonNull(customerAddressDo, customerAddressDo.getRemark(),"不能为NULL");
                
        return customerAddressDo;
    }


}


