package com.jhf.youke.crm.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.crm.domain.exception.CustomerException;
import lombok.Data;

import java.util.Date;
import java.util.Map;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class CustomerDo extends BaseDoEntity {

    private static final long serialVersionUID = -31844352665804457L;
    private static final String REFERRER = "referrer";

    /** 名字 **/
    private String name;

    /**手机 **/
    private String phone;

    /** 单位标识 **/
    private Long companyId;

    /** 性别 **/
    private Integer sex;

    /** 地区 **/
    private Long areaId;

    /** 用户标识 **/
    private Long userId;

    /** 销售员/团长 **/
    private Long saleMan;

    private Date createTime;

    private Date updateTime;

    public CustomerDo(){

    }

    /** 新建user时创建customer **/
    public CustomerDo(Map<String, Object> map) {
        if (map.containsKey(REFERRER)) {
            // 团长
            this.name = StringUtils.chgNull(map.get("userName"));
            this.saleMan = StringUtils.toLong(StringUtils.chgZero(map.get("referrer")));
        } else {
            // 客户
            this.name = StringUtils.chgNull(map.get("name"));
            this.saleMan = StringUtils.toLong(StringUtils.chgZero(map.get("saleMan")));
        }
        this.areaId = map.containsKey("areaId") ? StringUtils.toLong(StringUtils.chgZero(map.get("areaId"))) : null;
        this.phone = StringUtils.chgNull(map.get("phone"));
        this.companyId = StringUtils.toLong(StringUtils.chgZero(map.get("companyId")));
        this.sex = StringUtils.toInteger(StringUtils.chgZero(map.get("sex")));
        this.userId = StringUtils.toLong(StringUtils.chgZero(map.get("userId")));
    }


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new CustomerException(errorMessage);
        }
        return obj;
    }

    private CustomerDo validateNull(CustomerDo customerDo){
          //可使用链式法则进行为空检查
          customerDo.
          requireNonNull(customerDo, customerDo.getRemark(),"不能为NULL");
                
        return customerDo;
    }


}


