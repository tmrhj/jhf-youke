package com.jhf.youke.crm.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "crm_customer_address")
public class CustomerAddressPo extends BasePoEntity {

    private static final long serialVersionUID = -57892542558111915L;

    /** 客户标识 **/
    private Long customerId;

    /** 用户标识 **/
    private Long userId;

    /** 省 **/
    private String province;

    /** 市 **/
    private String city;

    /** 区 **/
    private String district;

    /** 街道 **/
    private String street;

    /** 详情地址 **/
    private String address;

    /** 是否默认 **/
    private Integer ifDefault;

}


