package com.jhf.youke.crm.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "crm_customer")
public class CustomerPo extends BasePoEntity {

    private static final long serialVersionUID = -52670076172978487L;

    /**名字 **/
    private String name;

    /**手机 **/
    private String phone;

    /**单位标识 **/
    private Long companyId;

    /**性别 **/
    private Integer sex;

    /**地区 **/
    private Long areaId;

    /**用户标识 **/
    private Long userId;

    /**销售员/团长 **/
    private Long saleMan;

}


