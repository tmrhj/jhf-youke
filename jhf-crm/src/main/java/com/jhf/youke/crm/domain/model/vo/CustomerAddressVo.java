package com.jhf.youke.crm.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class CustomerAddressVo extends BaseVoEntity {

    private static final long serialVersionUID = -58725231953153547L;

    @ApiModelProperty(value = "客户标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long customerId;

    @ApiModelProperty(value = "用户标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "区")
    private String district;

    @ApiModelProperty(value = "街道")
    private String street;

    @ApiModelProperty(value = "详情地址")
    private String address;

    @ApiModelProperty(value = "是否默认")
    private Integer ifDefault;



}


