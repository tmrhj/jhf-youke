package com.jhf.youke.crm.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class CustomerVo extends BaseVoEntity {

    private static final long serialVersionUID = 302037566004540933L;


    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "地区")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long areaId;

    @ApiModelProperty(value = "用户标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "销售员/团长")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long saleMan;


}


