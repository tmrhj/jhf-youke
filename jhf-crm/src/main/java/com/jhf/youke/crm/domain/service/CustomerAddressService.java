package com.jhf.youke.crm.domain.service;

import com.jhf.youke.crm.domain.converter.CustomerAddressConverter;
import com.jhf.youke.crm.domain.model.Do.CustomerAddressDo;
import com.jhf.youke.crm.domain.gateway.CustomerAddressRepository;
import com.jhf.youke.crm.domain.model.po.CustomerAddressPo;
import com.jhf.youke.crm.domain.model.vo.CustomerAddressVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  Rhj
 * **/
@Service
public class CustomerAddressService extends AbstractDomainService<CustomerAddressRepository, CustomerAddressDo, CustomerAddressVo> {


    @Resource
    CustomerAddressConverter customerAddressConverter;

    @Override
    public boolean update(CustomerAddressDo entity) {
        CustomerAddressPo customerAddressPo = customerAddressConverter.do2Po(entity);
        customerAddressPo.preUpdate();
        return repository.update(customerAddressPo);
    }

    @Override
    public boolean updateBatch(List<CustomerAddressDo> doList) {
        List<CustomerAddressPo> poList = customerAddressConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(CustomerAddressDo entity) {
        CustomerAddressPo customerAddressPo = customerAddressConverter.do2Po(entity);
        return repository.delete(customerAddressPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(CustomerAddressDo entity) {
        CustomerAddressPo customerAddressPo = customerAddressConverter.do2Po(entity);
        customerAddressPo.preInsert();
        return repository.insert(customerAddressPo);
    }

    @Override
    public boolean insertBatch(List<CustomerAddressDo> doList) {
        List<CustomerAddressPo> poList = customerAddressConverter.do2PoList(doList);
        poList = CustomerAddressPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<CustomerAddressVo> findById(Long id) {
        Optional<CustomerAddressPo> customerAddressPo =  repository.findById(id);
        CustomerAddressVo customerAddressVo = customerAddressConverter.po2Vo(customerAddressPo.orElse(new CustomerAddressPo()));
        return Optional.ofNullable(customerAddressVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<CustomerAddressVo> findAllMatching(CustomerAddressDo entity) {
        CustomerAddressPo customerAddressPo = customerAddressConverter.do2Po(entity);
        List<CustomerAddressPo>customerAddressPoList =  repository.findAllMatching(customerAddressPo);
        return customerAddressConverter.po2VoList(customerAddressPoList);
    }


    @Override
    public Pagination<CustomerAddressVo> selectPage(CustomerAddressDo entity){
        CustomerAddressPo customerAddressPo = customerAddressConverter.do2Po(entity);
        PageQuery<CustomerAddressPo> pageQuery = new PageQuery<>(customerAddressPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<CustomerAddressPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                customerAddressConverter.po2VoList(pagination.getList()));
    }


}

