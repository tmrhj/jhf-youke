package com.jhf.youke.crm.domain.service;

import com.jhf.youke.crm.domain.converter.CustomerConverter;
import com.jhf.youke.crm.domain.model.Do.CustomerDo;
import com.jhf.youke.crm.domain.gateway.CustomerRepository;
import com.jhf.youke.crm.domain.model.po.CustomerPo;
import com.jhf.youke.crm.domain.model.vo.CustomerVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class CustomerService extends AbstractDomainService<CustomerRepository, CustomerDo, CustomerVo> {


    @Resource
    CustomerConverter customerConverter;

    @Override
    public boolean update(CustomerDo entity) {
        CustomerPo customerPo = customerConverter.do2Po(entity);
        customerPo.preUpdate();
        return repository.update(customerPo);
    }

    @Override
    public boolean updateBatch(List<CustomerDo> doList) {
        List<CustomerPo> poList = customerConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(CustomerDo entity) {
        CustomerPo customerPo = customerConverter.do2Po(entity);
        return repository.delete(customerPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(CustomerDo entity) {
        CustomerPo customerPo = customerConverter.do2Po(entity);
        customerPo.preInsert();
        return repository.insert(customerPo);
    }

    @Override
    public boolean insertBatch(List<CustomerDo> doList) {
        List<CustomerPo> poList = customerConverter.do2PoList(doList);
        poList = CustomerPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<CustomerVo> findById(Long id) {
        Optional<CustomerPo> customerPo =  repository.findById(id);
        CustomerVo customerVo = customerConverter.po2Vo(customerPo.orElse(new CustomerPo()));
        return Optional.ofNullable(customerVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<CustomerVo> findAllMatching(CustomerDo entity) {
        CustomerPo customerPo = customerConverter.do2Po(entity);
        List<CustomerPo>customerPoList =  repository.findAllMatching(customerPo);
        return customerConverter.po2VoList(customerPoList);
    }


    @Override
    public Pagination<CustomerVo> selectPage(CustomerDo entity){
        CustomerPo customerPo = customerConverter.do2Po(entity);
        PageQuery<CustomerPo> pageQuery = new PageQuery<>(customerPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<CustomerPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                customerConverter.po2VoList(pagination.getList()));
    }


    public List<CustomerVo> getListBySale(Long saleMan) {
        List<CustomerPo> poList = repository.getListBySale(saleMan);
        List<CustomerVo> list = customerConverter.po2VoList(poList);
        return list;
    }

    /** 幂等检查,如果客户不存在，再进行创建 **/
    public void create(CustomerDo customerDo) {
        CustomerPo customerPo = repository.getByCompany(customerDo.getCompanyId(), customerDo.getPhone());
        if(customerPo != null){
            return;
        }else {
            insert(customerDo);
        }
    }
}

