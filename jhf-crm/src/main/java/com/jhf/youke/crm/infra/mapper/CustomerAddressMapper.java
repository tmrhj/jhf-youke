package com.jhf.youke.crm.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.crm.domain.model.po.CustomerAddressPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 客户地址映射器
 *
 * @author RHJ
 * @date 2022/11/17
 **/
@Mapper
public interface CustomerAddressMapper  extends BaseMapper<CustomerAddressPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update crm_customer_address set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update crm_customer_address set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

