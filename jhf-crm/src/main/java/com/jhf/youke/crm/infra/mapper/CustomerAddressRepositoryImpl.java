package com.jhf.youke.crm.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.crm.domain.exception.CustomerAddressException;
import com.jhf.youke.crm.domain.gateway.CustomerAddressRepository;
import com.jhf.youke.crm.domain.model.po.CustomerAddressPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "CustomerAddressRepository")
public class CustomerAddressRepositoryImpl extends ServiceImpl<CustomerAddressMapper, CustomerAddressPo> implements CustomerAddressRepository {


    @Override
    public boolean update(CustomerAddressPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<CustomerAddressPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(CustomerAddressPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(CustomerAddressPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<CustomerAddressPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<CustomerAddressPo> findById(Long id) {
        CustomerAddressPo customerAddressPo = baseMapper.selectById(id);
        return Optional.ofNullable(customerAddressPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new CustomerAddressException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<CustomerAddressPo> findAllMatching(CustomerAddressPo entity) {
        QueryWrapper<CustomerAddressPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        wrapper.orderByDesc("create_time");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<CustomerAddressPo> selectPage(PageQuery<CustomerAddressPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<CustomerAddressPo> customerAddressPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<CustomerAddressPo> pageInfo = new PageInfo<>(customerAddressPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

