package com.jhf.youke.crm.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.crm.domain.model.po.CustomerPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 客户映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface CustomerMapper  extends BaseMapper<CustomerPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update crm_customer set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update crm_customer set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 被销售列表
     *
     * @param saleMan 销售人
     * @return {@link List}<{@link CustomerPo}>
     */
    List<CustomerPo> getListBySale(@Param("saleMan") Long saleMan);

    /**
     * 通过公司
     *
     * @param companyId 公司标识
     * @param phone     电话
     * @return {@link CustomerPo}
     */
    CustomerPo getByCompany(@Param("companyId")Long companyId, @Param("phone")String phone);

}

