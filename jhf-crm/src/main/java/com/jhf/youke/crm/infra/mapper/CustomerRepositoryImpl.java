package com.jhf.youke.crm.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.crm.domain.exception.CustomerException;
import com.jhf.youke.crm.domain.gateway.CustomerRepository;
import com.jhf.youke.crm.domain.model.po.CustomerPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "CustomerRepository")
public class CustomerRepositoryImpl extends ServiceImpl<CustomerMapper, CustomerPo> implements CustomerRepository {


    @Override
    public boolean update(CustomerPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<CustomerPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(CustomerPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(CustomerPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<CustomerPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<CustomerPo> findById(Long id) {
        CustomerPo customerPo = baseMapper.selectById(id);
        return Optional.ofNullable(customerPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new CustomerException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<CustomerPo> findAllMatching(CustomerPo entity) {
        QueryWrapper<CustomerPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<CustomerPo> selectPage(PageQuery<CustomerPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<CustomerPo> customerPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<CustomerPo> pageInfo = new PageInfo<>(customerPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<CustomerPo> getListBySale(Long saleMan) {
        return baseMapper.getListBySale(saleMan);
    }

    @Override
    public CustomerPo getByCompany(Long companyId, String phone) {
        return baseMapper.getByCompany(companyId, phone);
    }

}

