package com.jhf.youke.db.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;

import java.util.Objects;

/**
 * @author RHJ
 * **/
public class DomainQueryWrapper<T> extends QueryWrapper<T> {

    public  DomainQueryWrapper(T t){
        super(t);
    }

    @Override
    protected QueryWrapper<T> addCondition(boolean condition, String column, SqlKeyword sqlKeyword, Object val) {
        if(Objects.isNull(val) || "".equals(val)){
            condition = false;
        }
        return super.addCondition(condition, column, sqlKeyword, val);
    }

}
