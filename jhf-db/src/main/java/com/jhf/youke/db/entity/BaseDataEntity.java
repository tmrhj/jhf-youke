
package com.jhf.youke.db.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.jhf.youke.core.utils.IdGen;
import lombok.Data;

import java.util.Date;

/**
 * @author RHJ
 * **/
@Data
public abstract class BaseDataEntity<T extends BaseDataEntity<T>> extends Model<T> {

	private static final long serialVersionUID = 1L;

	@TableId
	protected Long id;
	/** 备注 **/
	protected String remark;
	protected Date createTime;
	protected Date updateTime;
	protected String delFlag;

	/** 0 不是新记录  1 是新记录 **/
	@TableField(exist=false)
	protected String ifNew = "0";

	public BaseDataEntity() {
		super();
		this.delFlag = "0";
	}

	public void preInsert(){
		// 不限制ID为UUID，调用setIsNewRecord()使用自定义ID
		if(this.id == null || this.id == 0){
			setId(IdGen.id());
		}
		this.createTime = new Date();
		this.updateTime = this.createTime;
	}

	public void preUpdate(){
		this.updateTime = new Date();
	}

}
