package com.jhf.youke.files.adapter.web;


import com.jhf.youke.core.entity.Response;
import com.jhf.youke.files.domain.model.dto.ZipUploadDto;
import com.jhf.youke.files.domain.service.FilesService;
import com.jhf.youke.files.utils.ConvertM3U8;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;


/**
 * @author RHJ
 */
@Api(tags = "文件上传")
@RestController
public class FilesController {

    @Resource
    FilesService filesService;

    @Resource
    ConvertM3U8 convertM3U8Service;

    @ApiOperation("单文件上传")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Response<String> upload(@RequestParam("file") MultipartFile file) {
        try {
            return Response.ok(filesService.upload(file));
        } catch (Exception e) {
            return Response.fail(e.getMessage());
        }
    }

    @ApiOperation("单文件上传,指定目录,文件名UUID")
    @RequestMapping(value = "/uploadByDir", method = RequestMethod.POST)
    public Response<String> uploadByDir(@RequestParam("dir") String dir, @RequestParam("file") MultipartFile file) {
        try {
            return Response.ok(filesService.upload(dir, file,false));
        } catch (Exception e) {
            return Response.fail(e.getMessage());
        }
    }

    @ApiOperation("单文件上传,指定目录,文件名")
    @RequestMapping(value = "/uploadByDirAndFileName", method = RequestMethod.POST)
    public Response<String> uploadByDirAndFileName(@RequestParam("dir") String dir, @RequestParam("file") MultipartFile file) {
        try {
            return Response.ok(filesService.upload(dir, file,true));
        } catch (Exception e) {
            return Response.fail(e.getMessage());
        }
    }

    @ApiOperation("视频文件切片上传")
    @PostMapping(value = "/uploadVideo")
    public Response<String> uploadVideo(@RequestParam("file") MultipartFile file) {
        try {
            return Response.ok(convertM3U8Service.convertM3U8(file));
        } catch (Exception e) {
            return Response.fail(e.getMessage());
        }
    }


    @ApiOperation("压缩文件内图片上传")
    @PostMapping(value = "/zipUpload")
    public Response<String> zipUpload(@RequestBody ZipUploadDto dto) {
        try {
            return Response.ok(filesService.zipUpload(dto));
        } catch (Exception e) {
            return Response.fail(e.getMessage());
        }
    }

}

