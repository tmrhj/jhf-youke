package com.jhf.youke.files.domain;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductException extends DomainException {

    public ProductException(String message) { super(message); }
}

