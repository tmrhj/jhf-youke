package com.jhf.youke.files.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class FilesException extends DomainException {

    public FilesException(String message) { super(message); }
}

