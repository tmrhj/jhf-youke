package com.jhf.youke.files.domain.model.dto;

import lombok.Data;

/**
 * description:
 * date: 2022/7/5 10:25
 * @author: cyx
 */
@Data
public class ZipUploadDto {

    private static final long serialVersionUID = 672888122027119800L;

    private byte[] bytes;

    private Long size;

}
