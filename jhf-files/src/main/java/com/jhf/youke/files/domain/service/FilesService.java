package com.jhf.youke.files.domain.service;


import cn.hutool.core.date.DateUtil;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.files.domain.config.MinIoConfig;
import com.jhf.youke.files.domain.exception.FilesException;
import com.jhf.youke.files.domain.model.dto.ZipUploadDto;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.util.UUID;

/**
 * @author RHJ
 */
@Service
public class FilesService {


    @Resource
    MinIoConfig minIoConfig;

    @Resource
    MinioClient minioClient;

    public String upload(MultipartFile file) {
        String dir = DateUtil.today();
        return upload(dir, file, false);
    }

    /**
    * @Description:   文件上传
    * @Param: [dir 目录, file, ifChangeName true 使用原文件名 false 使用uuid文件名]
    * @return: java.lang.String
    * @Author: RHJ
    * @Date: 2022/9/29
    */
    public String upload(String dir, MultipartFile file, boolean ifChangeName){
        String adImgUrl = null;
        String fileName = "";
        if(ifChangeName){
            fileName = file.getName();
        }else {
            fileName = UUID.randomUUID().toString();
        }
        //首先判断是不是空的文件
        if (!file.isEmpty()) {
            String suffix = StringUtils.substringAfter (file.getOriginalFilename(), ".");
            try {
                // 文件流上传
                ObjectWriteResponse objectWriteResponse = minioClient.putObject(
                        PutObjectArgs.builder()
                                //桶名称
                                .bucket(minIoConfig.getBucket())
                                .object(dir + "/" + fileName)
                                // 流 ， 流大小 ， 需要上传部分 -1 全传
                                .stream(file.getInputStream(), file.getSize(), -1)
                                .build());
                adImgUrl = minIoConfig.getBucket() + "/" + objectWriteResponse.object();
            } catch (Exception e) {
                e.getMessage();
            }
            if (StringUtils.ifNull(adImgUrl)) {
                throw new FilesException("上传失败！");
            }
        } else {
            throw new FilesException("请选择需要上传的文件！");
        }
        return adImgUrl;
    }

    public String zipUpload(ZipUploadDto dto) throws Exception {
        if (dto.getBytes() == null || dto.getSize() == null) {
            return null;
        }
        // 文件流上传
        ObjectWriteResponse objectWriteResponse = minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket(minIoConfig.getBucket())
                        .object("dir/" + System.currentTimeMillis() + ".png")
                        // 流 ， 流大小 ， 需要上传部分 -1 全传
                        .stream(new ByteArrayInputStream(dto.getBytes()), dto.getSize(), -1)
                        .contentType(MimeTypeUtils.IMAGE_PNG.toString())
                        .build());
        return minIoConfig.getBucket() + "/" + objectWriteResponse.object();
    }

}
