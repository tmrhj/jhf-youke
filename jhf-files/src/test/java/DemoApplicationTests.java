import com.jhf.youke.FilesApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

@Slf4j
@SpringBootTest(classes = FilesApplication.class)
class DemoApplicationTests {




    @Test
    void test1(){

        // 如果文件路径名有空格会发生异常，需要使用 nextLine 去获取输入的内容
        String pathName = "E:\\tools\\node-v16.17.0-x64.msi";
        System.out.println(pathName);
        File file = new File(pathName);

        System.out.println("当前路径的文件是否存在：" + file.exists());
        System.out.println("文件名称：" + file.getName());
        System.out.println("getParent：" + file.getParent());
        System.out.println("文件的长度：" + file.length());
        System.out.println("文件最后一次修改的时间：" + file.lastModified());
        System.out.println("文件的绝对路径：" + file.getAbsolutePath());
        System.out.println("=============================");

        File files =  new File(file.getParent());
        for(File f : files.listFiles()){
            System.out.println("文件名称：" + file.getName());
        }

    }




}
