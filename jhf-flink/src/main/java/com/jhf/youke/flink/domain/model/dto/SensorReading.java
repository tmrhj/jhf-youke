package com.jhf.youke.flink.domain.model.dto;

import lombok.Data;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/11/21
*/

@Data
public class SensorReading {
    private Long id;
    private String name;
    private Double num;

    public SensorReading(){

    }
    public SensorReading(Long id, String name, Double num){
        this.id = id;
        this.name = name;
        this.num = num;
    }
}
