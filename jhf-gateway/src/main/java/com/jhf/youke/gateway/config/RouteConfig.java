package com.jhf.youke.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author RHJ
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "app.route")
public class RouteConfig {
    private List<String> whitelist;
}
