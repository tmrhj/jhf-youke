package com.jhf.youke.gateway.config.filter;

import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhf.youke.core.entity.Response;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.gateway.constants.GatewayConstant;
import com.jhf.youke.gateway.entity.PermissionsSetVo;
import com.jhf.youke.gateway.utils.BaseUtils;
import lombok.SneakyThrows;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author RHJ
 */
@Component
@RefreshScope
public class AuthGlobFilter implements GlobalFilter, Ordered {

    @Resource
    ObjectMapper objectMapper;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        var request = exchange.getRequest();
        var path = request.getURI().getPath();
        var authenticationHeader = request.getHeaders().getFirst("token");

        //检查白名单
        if (BaseUtils.checkWhite(path)) {
            return chain.filter(exchange);
        }

        try {
            //用户所有权限
            Map<String, Object> map = JSONUtil.toBean(CacheUtils.get(authenticationHeader), Map.class);
            //超级管理员放行
            Long userId = StringUtils.toLong(map.get("id"));
            if(GatewayConstant.SUPER_MANAGE_ID.equals(userId)){
                return chain.filter(exchange);
            }
            //检查内部方法
            if (BaseUtils.checkInside(path)) {
                return insideError(exchange.getResponse());
            }

            List<String> permissionsMap = (List<String>)map.get("permissionsMap");

            //获取系统全部权限
            Map<String, Object> allPermissions = BaseUtils.getPermissionsListToMap();
            List<PermissionsSetVo> permissionsSetVoList = JSONUtil.toList(JSONUtil.parseArray(allPermissions.get(path)), PermissionsSetVo.class);
            // 没有配置权限，则默认所有人可以访问
            if (CollectionUtils.isEmpty(permissionsSetVoList)) {
                return chain.filter(exchange);
            }
            //用户没有配置权限
            if (permissionsMap.isEmpty()) {
                return unauthorized(exchange.getResponse());
            }
            //权限比对
            for (PermissionsSetVo permissionsSetVo : permissionsSetVoList) {
                if (permissionsMap.contains(permissionsSetVo.getPermissionCode())) {
                    return chain.filter(exchange);
                }
            }
            return unauthorized(exchange.getResponse());
        } catch (Exception e) {
            return unauthorized(exchange.getResponse());
        }
    }

    @SneakyThrows
    private Mono<Void> unauthorized(ServerHttpResponse response) {
        var r = Response.fail(402, "暂无接口权限");
        var buffer = response.bufferFactory().wrap(objectMapper.writeValueAsBytes(r));
        response.setStatusCode(HttpStatus.OK);
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

    @SneakyThrows
    private Mono<Void> insideError(ServerHttpResponse response) {
        var r = Response.fail(403, "内部方法不允许访问");
        var buffer = response.bufferFactory().wrap(objectMapper.writeValueAsBytes(r));
        response.setStatusCode(HttpStatus.OK);
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

    @Override
    public int getOrder() {
        return -2;
    }
}

