package com.jhf.youke.gateway.consume;

import com.jhf.youke.core.common.localChace.LocalCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class UpdatePermissionsCacheConsume {

    /**
     * 删除权限缓存
     * @return
     */
    @Bean
    public Consumer<Message<String>> updatePermissionsCache() {
        return message -> {
            LocalCache.del("all_permissions_map");
            LocalCache.del("all_permissions_inside");
            LocalCache.del("all_permissions_white");
        };
    }

}
