package com.jhf.youke.gateway.controller;

import com.jhf.youke.core.common.localChace.LocalCache;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author RHJ
 */
@RestController
public class PermissionsController {

    @RequestMapping("/cleanPermissions/{key}")
    public void cleanPermissions(@PathVariable("key") String key) {
        LocalCache.del(key);
    }
}
