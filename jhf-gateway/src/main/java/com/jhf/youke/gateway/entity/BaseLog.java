package com.jhf.youke.gateway.entity;


import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import java.util.Date;

/**
 * @author RHJ
 */
@Data
public class BaseLog {

    /** Id **/
    private Long id;
    /** 操作人ID **/
    private Long userId;
    /** 操作人名称 **/
    private String userName;
    /** 用户类型 **/
    private Integer userType;
    /** 操作人 IP **/
    private String loginIp;
    /** 操作模块 **/
    private String title;
    /** 登录地点 **/
    private String address;
    /** 浏览器 **/
    private String browser;
    /** 操作系统 **/
    private String systemName;
    /** 操作类型 **/
    private String operation;
    /** 请求参数 **/
    private String requestParam;
    /** 返回结果 **/
    private String resultParam;
    /** 创建时间 **/
    private Date createTime;
    /** 请求方式 **/
    private String method;
    /** 请求地址 **/
    private String url;
    /** 更新时间 **/
    private Date updateTime;
    /** 响应结果0成功 1失败 **/
    private Integer status;
    /** 备注 **/
    private String remark;
    /** 更新类型 **/
    private Integer updateType;

    public void setAppendResponse(String response){
        if (StringUtils.isNotBlank(this.resultParam)) {
            this.resultParam = this.resultParam + response;
        } else {
            this.resultParam = response;
        }
    }
}
