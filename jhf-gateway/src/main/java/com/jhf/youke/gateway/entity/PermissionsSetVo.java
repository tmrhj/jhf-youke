package com.jhf.youke.gateway.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class PermissionsSetVo implements Serializable{

    private static final long serialVersionUID = 540417130084476711L;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long id;

    private String name;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long permissionId;

    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    @ApiModelProperty(value = "1 角色 2 菜单 3 方法")
    private Integer type;

    @ApiModelProperty(value = "菜单ID/角色ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "方法路径")
    private String url;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(value = "是否删除 0正常 1删除")
    private String delFlag;

    @ApiModelProperty(value = "内部方法状态 标识为1")
    private Integer insideStatus;

    @ApiModelProperty(value = "白名单方法状态 标识为1")
    private Integer whiteStatus;

}


