package com.jhf.youke.gateway.feign;


import com.jhf.youke.gateway.feign.hystrix.BaseFeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;


/**
 * 基地假装
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Component
@FeignClient(name="jhf-base-service", url = "${feign.baseUrl}", fallback = BaseFeignHystrix.class)
public interface BaseFeign {

    /**
     * 把所有映射列表
     *
     * @param map 地图
     * @return {@link Object}
     */
    @PostMapping(value="/base/permissionsSet/getAllListToMap")
    Object getAllListToMap(@RequestBody Map<String, Object> map);

    /**
     * 列表
     *
     * @param map 地图
     * @return {@link Object}
     */
    @PostMapping(value="/base/permissionsSet/list")
    Object list(@RequestBody Map<String, Object> map);

}
