package com.jhf.youke.gateway.feign;

import com.jhf.youke.core.entity.Message;
import com.jhf.youke.gateway.feign.hystrix.RocketMqHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * 火箭mq假装
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Component
@FeignClient(name = "jhf-rocketmq-service", url = "${feign.mqUrl}", fallback = RocketMqHystrix.class)
public interface RocketMqFeign {

    /**
     * 发送
     * @param msg 味精
     * @return {@link Object}
     */
    @PostMapping("/rocketmq/send")
    Object send(@RequestBody Message msg);

}
