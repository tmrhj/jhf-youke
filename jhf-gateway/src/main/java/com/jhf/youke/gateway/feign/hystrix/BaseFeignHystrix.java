package com.jhf.youke.gateway.feign.hystrix;


import com.jhf.youke.gateway.feign.BaseFeign;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * @author RHJ
 */
@Component
public class BaseFeignHystrix implements BaseFeign {

    @Override
    public Object getAllListToMap(Map<String, Object> map) {
        return null;
    }

    @Override
    public Object list(Map<String, Object> map) {
        return null;
    }
}

