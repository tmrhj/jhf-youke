package com.jhf.youke.gateway.utils;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.jhf.youke.core.common.localChace.LocalCache;
import com.jhf.youke.core.utils.Constant;
import com.jhf.youke.core.utils.Convert;
import com.jhf.youke.gateway.entity.PermissionsSetVo;
import com.jhf.youke.gateway.feign.BaseFeign;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
public class BaseUtils {

    private static BaseFeign baseFeign = SpringContextHolder.getBean(BaseFeign.class);

    private static List<PermissionsSetVo> insideList = new ArrayList<>(10);

    private static List<PermissionsSetVo> whiteList = new ArrayList<>(10);

    private static final String CODE = "code";

    private static final String DATA = "data";

    public static Map<String, Object> getPermissionsListToMap() {
        String key = "all_permissions_map";
        Object permissionsObject = LocalCache.get(key);
        Map<String, Object> data = new HashMap<>(8);
        if (ObjectUtil.isNotEmpty(permissionsObject)) {
            data = BeanUtil.beanToMap(permissionsObject, data, false, true);
            return data;
        }
        Map<String, Object> param = new HashMap<>(8);
        param.put("type", 3);
        Map<String, Object> map = (Map<String, Object>) baseFeign.getAllListToMap(param);
        if (map != null && map.get(CODE) != null && Constant.RESPONSE_SUCCESS.equals(Convert.toStr(map.get(CODE))) && map.get(DATA) != null) {
            String json = JSONUtil.toJsonStr(map.get("data"));
            data = JSONUtil.toBean(json, Map.class);
        }
        LocalCache.put(key, data);
        return data;
    }

    public static void initInside() {
        Map<String, Object> param = new HashMap<>(8);
        param.put("insideStatus", 1);
        Map<String, Object> map = (Map<String, Object>) baseFeign.list(param);
        if (map != null && map.get(Constant.RESPONSE_CODE) != null && Constant.RESPONSE_SUCCESS.equals(Convert.toStr(map.get(Constant.RESPONSE_CODE))) && map.get(Constant.RESPONSE_DATA) != null) {
            String json = JSONUtil.toJsonStr(map.get(Constant.RESPONSE_DATA));
            insideList = JSONUtil.toList(json, PermissionsSetVo.class);
        }
    }

    public static void initWhite() {
        Map<String, Object> param = new HashMap<>(8);
        param.put("whiteStatus", 1);
        Map<String, Object> map = (Map<String, Object>) baseFeign.list(param);
        if (map != null && map.get(Constant.RESPONSE_CODE) != null && Constant.RESPONSE_SUCCESS.equals(Convert.toStr(map.get(Constant.RESPONSE_CODE))) && map.get(Constant.RESPONSE_DATA) != null) {
            String json = JSONUtil.toJsonStr(map.get(Constant.RESPONSE_DATA));
            whiteList = JSONUtil.toList(json, PermissionsSetVo.class);
        }
    }

    public static List<PermissionsSetVo> getInsideList() {
        String key = "all_permissions_inside";
        Object permissionsInside = LocalCache.get(key);
        if (ObjectUtil.isNotEmpty(permissionsInside)) {
            insideList = JSONUtil.toList(JSONUtil.parseArray(permissionsInside), PermissionsSetVo.class);
            return insideList;
        } else {
            initInside();
        }
        LocalCache.put(key, insideList);
        return insideList;
    }

    public static List<PermissionsSetVo> getWhiteList() {
        String key = "all_permissions_white";
        Object permissionsWhite = LocalCache.get(key);
        if (ObjectUtil.isNotEmpty(permissionsWhite)) {
            whiteList = JSONUtil.toList(JSONUtil.parseArray(permissionsWhite), PermissionsSetVo.class);
            return whiteList;
        } else {
            initWhite();
        }
        LocalCache.put(key, whiteList);
        return whiteList;
    }

    public static Boolean checkWhite(String path) {
        var antPathMatcher = new AntPathMatcher();
        List<PermissionsSetVo> whiteList = getWhiteList();
        if (!CollectionUtils.isEmpty(whiteList)) {
            List<String> collect = whiteList.stream().map(PermissionsSetVo::getUrl).filter(StrUtil::isNotBlank).collect(Collectors.toList());
            for (String openRoute : collect) {
                if (antPathMatcher.match(openRoute, path)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean checkInside(String path) {
        var antPathMatcher = new AntPathMatcher();
        List<PermissionsSetVo> insideList = getInsideList();
        if (!CollectionUtils.isEmpty(insideList)) {
            List<String> collect = insideList.stream().map(PermissionsSetVo::getUrl).filter(StrUtil::isNotBlank).collect(Collectors.toList());
            for (String openRoute : collect) {
                if (antPathMatcher.match(openRoute, path)) {
                    return true;
                }
            }
        }
        return false;
    }

}
