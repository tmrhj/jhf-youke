package com.jhf.youke.gateway.utils;


import com.jhf.youke.core.entity.Message;
import com.jhf.youke.gateway.feign.RocketMqFeign;

/**
 * @author RHJ
 */
public class MqUtils {

    private static RocketMqFeign rocketMqFeign = SpringContextHolder.getBean(RocketMqFeign.class);

    public static Object send(Message msg){
        return rocketMqFeign.send(msg);
    }

}
