package com.jhf.youke.hadoop.adapter.web;

import com.jhf.youke.hadoop.domain.service.HdfsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @author RHJ
 */
@RestController
@RequestMapping("/hdfs")
public class HdfsController {

    @Resource
    private HdfsService hdfsService;


    @PostMapping("/test")
    public String selectPage() {

        return  hdfsService.getPath();
    }

}

