package com.jhf.youke.hadoop.config;
 
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
 
/**
 * HDFS配置类
 * @author rhj
 */
@Configuration
@Data
public class HdfsConfig {
    @Value("${hdfs.path}")
    private String path;

}
 