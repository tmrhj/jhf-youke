package com.jhf.youke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author RHJ
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class HbaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HbaseApplication.class);
    }

}
