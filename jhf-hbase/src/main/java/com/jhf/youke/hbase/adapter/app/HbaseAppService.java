package com.jhf.youke.hbase.adapter.app;

import com.jhf.youke.hbase.domain.service.PhoenixService;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/12/9
*/
@Component
public class HbaseAppService {

    @Resource
    private PhoenixService phoenixService;

    public void createTable(String tableName, List<String> columns) throws Exception {
        phoenixService.createTable(tableName, columns);
    }

    public void dropTable(String tableName) throws Exception {
       phoenixService.dropTable(tableName);
    }


    public void insertData(String tableName, String values) throws Exception {
        phoenixService.insertData(tableName, values);
    }

    public void insertBatchData(String tableName, List<String> values) throws Exception {
        phoenixService.insertBatchData(tableName,values);
    }

    public void deleteData(String tableName, String id) throws Exception {
         phoenixService.deleteData(tableName, id);
    }

    public List<Map<String, String>> getData(String tableName) throws Exception {
        List<Map<String, String>> list = phoenixService.getData(tableName);
        return list;
    }

}
