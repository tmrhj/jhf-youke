package com.jhf.youke.hbase.adapter.web;


import com.jhf.youke.core.entity.Response;
import com.jhf.youke.hbase.adapter.app.HbaseAppService;
import com.jhf.youke.hbase.domain.model.dto.HbaseDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @author RHJ
 */
@RestController
@RequestMapping("/hbase")
public class HbaseController {

    @Resource
    private HbaseAppService hbaseAppService;


    @PostMapping("/createTable")
    public Response<Boolean> createTable(@RequestBody HbaseDto dto){
        try{
            hbaseAppService.createTable(dto.getTableName(),dto.getValues());
            return Response.ok(true);
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
    }


    @PostMapping("/dropTable")
    public Response<Boolean> dropTable(@RequestBody HbaseDto dto){
        try{
            hbaseAppService.dropTable(dto.getTableName());
            return Response.ok(true);
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
    }


    @PostMapping("/insertData")
    public Response<Boolean> insertData(@RequestBody HbaseDto dto){
        try{
            hbaseAppService.insertData(dto.getTableName(),dto.getValue());
            return Response.ok(true);
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
    }


    @PostMapping("/insertBatchData")
    public Response<Boolean> insertBatchData(@RequestBody HbaseDto dto){
        try{
            hbaseAppService.insertBatchData(dto.getTableName(),dto.getValues());
            return Response.ok(true);
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
    }


    @PostMapping("/deleteData")
    public Response<Boolean> deleteData(@RequestBody HbaseDto dto){
        try{
            hbaseAppService.deleteData(dto.getTableName(),dto.getId());
            return Response.ok(true);
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
    }

    @PostMapping("/getData")
    public Response<List<Map<String, String>>> getData(@RequestBody HbaseDto dto){
        try{
            List<Map<String, String>> list = hbaseAppService.getData(dto.getTableName());
            return Response.ok(list);
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail(e.getMessage());
        }
    }

}

