package com.jhf.youke.hbase.domain.model.dto;

import lombok.Data;

/** 
* @Description:  
* @Param:  
* @return:  
* @Author: RHJ
* @Date: 2022/12/13 
*/ 

@Data
public class Column{
    public Column(){

    }
    private String name;
    private String value;
}