package com.jhf.youke.hbase.domain.model.dto;

import lombok.Data;

import java.util.List;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/12/13
*/

@Data
public class ColumnFamily{
    public ColumnFamily(){

    }
    String familyName;
    List<Column> columns;

}