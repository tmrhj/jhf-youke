package com.jhf.youke.hbase.domain.model.dto;

import lombok.Data;

import java.util.List;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/12/9
*/
@Data
public class HbaseDto {
    private String nameSpace;
    private String tableName;
    private String value;
    private String startRow;
    private  String stopRow;
    private String id;
    private List<String> values;
    private String[] columnFamilies;
    private List<ColumnFamily> columnFamilyList;


}
