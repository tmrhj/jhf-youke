package com.jhf.youke.hbase.domain.service;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/12/13
*/
public class HbaseConnection {

    public static Connection connection = null;

    static {
        try {
            connection  = ConnectionFactory.createConnection();
        }catch (Exception e){

        }
    }

    public static Connection getConnection(){
        return connection;
    }

    public static void closeConnection() throws Exception{
        if(connection != null){
            connection.close();
        }
    }

    public static void main(String[] args) throws IOException {
        Connection conn = HbaseConnection.getConnection();
        System.out.println(conn);
    }

}
