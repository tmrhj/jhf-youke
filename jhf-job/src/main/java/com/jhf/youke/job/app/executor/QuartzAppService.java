package com.jhf.youke.job.app.executor;


import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.job.app.fegin.SagaFeign;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Quartz 定时任务
 *
 * @author RHJ
 */
@Component
@Log4j2
public class QuartzAppService {


    @Resource
    SagaFeign sagaFeign;


    /**
     * 每分钟执行一次
     *
     * @Description: Saga定时任务每分钟一次，将发送异常saga重新发送
     * @Param: []
     * @return: void
     * @Author: RHJ
     * @Date: 2022/11/8
     */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void sagaAbnormalHandling() {
        try {
            Map<Object, Object> map = CacheUtils.getHash("saga_abnormal");
            if (map != null && !map.isEmpty()) {
                log.error("saga 异常处理定时任务执行");
                for (Map.Entry<Object, Object> m : map.entrySet()) {
                    Map<String, Object> saga = new HashMap<>(8);
                    saga.put("id", m.getKey());
                    sagaFeign.abnormalHandling(saga);
                }
            }
        } catch (Exception e) {
            log.error("异常，{}", e.getMessage());
        }
    }


}
