package com.jhf.youke.job.app.fegin;


import com.jhf.youke.job.app.fegin.hystrix.SagaHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Map;


/**
 * 传奇假装
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Component
@FeignClient(name="jhf-saga-service", url = "${feign.sagaUrl}", fallback = SagaHystrix.class)

public interface SagaFeign {

    /**
     * 异常处理
     *
     * @param map 地图
     * @return {@link Object}
     */
    @PostMapping(value="/sagas/saga/abnormalHandling")
    Object abnormalHandling(@RequestBody Map<String,Object> map);

}
