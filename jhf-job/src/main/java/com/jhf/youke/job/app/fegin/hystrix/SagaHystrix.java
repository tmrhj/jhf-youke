package com.jhf.youke.job.app.fegin.hystrix;


import com.jhf.youke.core.entity.Response;
import com.jhf.youke.job.app.fegin.SagaFeign;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author RHJ
 * **/
@Component
public class SagaHystrix implements FallbackFactory<SagaFeign> {

    @Override
    public SagaFeign create(Throwable arg0) {

        return new SagaFeign() {
            @Override
            public Object abnormalHandling(Map<String,Object> map) {
                return  Response.fail( arg0.getMessage());
            }

        };

    }
}
