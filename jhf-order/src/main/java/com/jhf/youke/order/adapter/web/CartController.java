package com.jhf.youke.order.adapter.web;

import com.jhf.youke.order.app.executor.CartAppService;
import com.jhf.youke.order.domain.model.dto.CartDto;
import com.jhf.youke.order.domain.model.vo.CartVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/cart")
public class CartController {

    @Resource
    private CartAppService cartAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody CartDto dto) {
        return Response.ok(cartAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody CartDto dto) {
        return Response.ok(cartAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody CartDto dto) {
        return Response.ok(cartAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<CartVo>> findById(Long id) {
        return Response.ok(cartAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(cartAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<CartVo>> list(@RequestBody CartDto dto) {

        return Response.ok(cartAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<CartVo>> selectPage(@RequestBody  CartDto dto) {
        return Response.ok(cartAppService.selectPage(dto));
    }

}

