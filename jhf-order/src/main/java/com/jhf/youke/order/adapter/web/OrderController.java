package com.jhf.youke.order.adapter.web;

import com.jhf.youke.core.entity.DetailsDto;
import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.order.app.executor.OrderAppService;
import com.jhf.youke.order.domain.model.dto.OrderDto;
import com.jhf.youke.order.domain.model.dto.VideoOrderDto;
import com.jhf.youke.order.domain.model.vo.OrderVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Api(tags = "")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderAppService orderAppService;

    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody OrderDto dto) {
        return Response.ok(orderAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody OrderDto dto) {
        return Response.ok(orderAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody OrderDto dto) {
        return Response.ok(orderAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<OrderVo>> findById(Long id) {
        return Response.ok(orderAppService.findById(id));
    }

    @GetMapping("/detailById")
    @ApiOperation("根据ID查询")
    public Response<Optional<OrderVo>> detailById(Long id) {
        return Response.ok(orderAppService.detailById(id));
    }

    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(orderAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<OrderVo>> list(@RequestBody OrderDto dto) {

        return Response.ok(orderAppService.findAllMatching(dto));
    }

    @PostMapping("/listBySale")
    @ApiOperation("根据销售员/团长查询订单列表")
    public Response<Pagination<OrderVo>> listBySale(@RequestHeader("token") String token, @RequestBody OrderDto dto) {
        Pagination<OrderVo> orderVo = null;
        try{
             orderVo =  orderAppService.listBySale(token,dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Response.ok(orderVo);
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<OrderVo>> selectPage(@RequestBody  OrderDto dto) {
        return Response.ok(orderAppService.selectPage(dto));
    }

    @PostMapping("/confirm")
    @ApiOperation("订单确认")
    public Response<Boolean> confirm(@RequestBody  OrderDto dto) {
        return Response.ok(orderAppService.confirm(dto));
    }

    @PostMapping("/deliverGoods")
    @ApiOperation("订单发货")
    public Response<Boolean> deliverGoods(@RequestBody  OrderDto dto) {
        return Response.ok(orderAppService.deliverGoods(dto));
    }

    @PostMapping("/save")
    @ApiOperation("订单报单保存")
    public Response<Boolean> save(@RequestBody  OrderDto dto) {
        return Response.ok(orderAppService.save(dto));
    }


    @PostMapping("/submit")
    @ApiOperation("订单发货")
    public Response<Boolean> submit(@RequestBody  OrderDto dto) {
        return Response.ok(orderAppService.submit(dto));
    }


    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(orderAppService.getPreData());
    }

    @PostMapping("/payNotify")
    @ApiOperation("订单支付成功回调")
    public Response<Boolean> payNotify(@RequestBody DetailsDto dto) {
        return Response.ok(orderAppService.payNotify(dto.getId()));
    }

    @PostMapping("/videoSave")
    @ApiOperation("视频下单")
    public Response<Map<String, String>> videoSave(@RequestHeader("token") String token, @RequestBody VideoOrderDto dto) {
        return Response.ok(orderAppService.videoSave(token, dto));
    }

    @PostMapping("/videoAgainPay")
    @ApiOperation("视频订单重新支付")
    public Response<Map<String, String>> videoAgainPay(@RequestHeader("token") String token, @RequestBody VideoOrderDto dto) {
        User user = CacheUtils.getUser(token);
        return Response.ok(orderAppService.videoAgainPay(dto.getOrderId(), user));
    }

    @PostMapping("/videoGoodsInfo")
    @ApiOperation("视频商品详情")
    public Response<Map<String, Object>> videoGoodsInfo(@RequestHeader("token") String token, @RequestBody VideoOrderDto dto) {
        return Response.ok(orderAppService.videoProductReleaseInfo(token, dto));
    }

    @PostMapping("/getVideoPageList")
    @ApiOperation("用户视频订单列表")
    public Response<Pagination<OrderVo>> getVideoPageList(@RequestHeader("token") String token, @RequestBody OrderDto dto) {
        User user = CacheUtils.getUser(token);
        dto.setCustomerId(user.getId());
        return Response.ok(orderAppService.getVideoPageList(dto));
    }

}

