package com.jhf.youke.order.app.consume;


import cn.hutool.core.convert.Convert;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.order.app.executor.OrderAppService;
import com.jhf.youke.saga.client.annotation.SagaReply;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author RHJ
 */
@Component
public class ImmediatelyPaySuccessConsumeService {

    @Resource
    OrderAppService orderAppService;

    @SagaReply(abnormal = "immediately_pay_success_abnormal")
    public void create(Map<String, Object> map) {
        Map<String,Object> msg = (Map<String,Object>) map.get("message");
        String attach = StringUtils.chgNull(msg.get("attach"));
        String videoCode = "videoOrder";
        if(StringUtils.ifNull(attach)) {
            orderAppService.payNotify(Convert.toLong(msg.get("orderId")));
        }else if(videoCode.equals(attach)){
            orderAppService.videoOrderPayNotify(StringUtils.chgNull(msg.get("orderId")));
        }
    }

}
