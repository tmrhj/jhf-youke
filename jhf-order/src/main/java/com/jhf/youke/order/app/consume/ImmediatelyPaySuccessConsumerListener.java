package com.jhf.youke.order.app.consume;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;
import java.util.function.Consumer;

/**
 * description:
 * date: 2022/9/23 14:12
 * @author cyx
 */
@Slf4j
@Service
public class ImmediatelyPaySuccessConsumerListener {

    @Resource
    private ImmediatelyPaySuccessConsumeService immediatelyPaySuccessConsumeService;

    /**
     * 订单支付
     * @return
     */
    @Bean
    public Consumer<Message<String>> immediatelyPaySuccessConsume() {
        return message -> {

            Map<String,Object> map = JSONUtil.toBean(message.getPayload(), Map.class);
            immediatelyPaySuccessConsumeService.create(map);

        };
    }

}
