package com.jhf.youke.order.app.executor;

import com.jhf.youke.order.domain.converter.CartConverter;
import com.jhf.youke.order.domain.model.Do.CartDo;
import com.jhf.youke.order.domain.model.dto.CartDto;
import com.jhf.youke.order.domain.model.vo.CartVo;
import com.jhf.youke.order.domain.service.CartService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */

@Slf4j
@Service
public class CartAppService {

    @Resource
    private CartService cartService;

    @Resource
    private CartConverter cartConverter;


    public boolean update(CartDto dto) {
        CartDo cartDo =  cartConverter.dto2Do(dto);
        return cartService.update(cartDo);
    }

    public boolean delete(CartDto dto) {
        CartDo cartDo =  cartConverter.dto2Do(dto);
        return cartService.delete(cartDo);
    }


    public boolean insert(CartDto dto) {
        CartDo cartDo =  cartConverter.dto2Do(dto);
        return cartService.insert(cartDo);
    }

    public Optional<CartVo> findById(Long id) {
        return cartService.findById(id);
    }


    public boolean remove(Long id) {
        return cartService.remove(id);
    }


    public List<CartVo> findAllMatching(CartDto dto) {
        CartDo cartDo =  cartConverter.dto2Do(dto);
        return cartService.findAllMatching(cartDo);
    }


    public Pagination<CartVo> selectPage(CartDto dto) {
        CartDo cartDo =  cartConverter.dto2Do(dto);
        return cartService.selectPage(cartDo);
    }

}

