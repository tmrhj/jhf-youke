package com.jhf.youke.order.app.feign;


import com.jhf.youke.order.app.feign.hystrix.ProductHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;


/**
 * 产品装
 *
 * @author Administrator
 * @date 2022/11/17
 */
@Component
@FeignClient(name = "jhf-product-service", url = "${feign.productUrl}", fallback = ProductHystrix.class)
public interface ProductFeign {

    /**
     * 得到视频
     *
     * @param map 地图
     * @return {@link Map}<{@link String},{@link Object}>
     */
    @GetMapping("/products/productRelease/getVideo")
    Map<String,Object> getVideo(@RequestHeader("token") String token, @RequestBody Map<String,Object> map);

}
