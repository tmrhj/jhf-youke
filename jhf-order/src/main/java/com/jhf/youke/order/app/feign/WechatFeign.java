package com.jhf.youke.order.app.feign;


import com.jhf.youke.core.entity.Response;
import com.jhf.youke.order.app.feign.hystrix.WechatHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Map;


/**
 * 微信假装
 *
 * @author Administrator
 * @date 2022/11/17
 */
@Component
@FeignClient(name = "jhf-wechat-service", url = "${feign.wechatUrl}", fallback = WechatHystrix.class)
public interface WechatFeign {

    /**
     * js api支付
     *
     * @param map 地图
     * @return {@link Response}<{@link Map}<{@link String}, {@link String}>>
     */
    @PostMapping("/wechat/wx/jsApiPay")
    Response<Map<String, String>> jsApiPay(@RequestBody Map<String,Object> map);

}
