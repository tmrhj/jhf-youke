package com.jhf.youke.order.domain.converter;

import com.jhf.youke.order.domain.model.Do.CartDo;
import com.jhf.youke.order.domain.model.dto.CartDto;
import com.jhf.youke.order.domain.model.po.CartPo;
import com.jhf.youke.order.domain.model.vo.CartVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 购物车转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface CartConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param cartDto 购物车dto
     * @return {@link CartDo}
     */
    CartDo dto2Do(CartDto cartDto);

    /**
     * 洗阿宝
     *
     * @param cartDo 车做
     * @return {@link CartPo}
     */
    CartPo do2Po(CartDo cartDo);

    /**
     * 洗签证官
     *
     * @param cartDo 车做
     * @return {@link CartVo}
     */
    CartVo do2Vo(CartDo cartDo);

    /**
     * 洗订单列表
     *
     * @param cartDoList
     * @return {@link List}<{@link CartPo}>
     */
    List<CartPo> do2PoList(List<CartDo> cartDoList);

    /**
     * 警察乙做
     *
     * @param cartPo 购物车订单
     * @return {@link CartDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    CartDo po2Do(CartPo cartPo);

    /**
     * 警察乙签证官
     *
     * @param cartPo 购物车订单
     * @return {@link CartVo}
     */
    CartVo po2Vo(CartPo cartPo);

    /**
     * 警察乙做列表
     *
     * @param cartPoList
     * @return {@link List}<{@link CartDo}>
     */
    List<CartDo> po2DoList(List<CartPo> cartPoList);

    /**
     * 警察乙vo列表
     *
     * @param cartPoList 购物车订单列表
     * @return {@link List}<{@link CartVo}>
     */
    List<CartVo> po2VoList(List<CartPo> cartPoList);


}

