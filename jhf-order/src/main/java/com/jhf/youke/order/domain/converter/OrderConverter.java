package com.jhf.youke.order.domain.converter;

import com.jhf.youke.core.ddd.BaseConverter;
import com.jhf.youke.order.domain.model.Do.OrderDo;
import com.jhf.youke.order.domain.model.dto.OrderDto;
import com.jhf.youke.order.domain.model.po.OrderPo;
import com.jhf.youke.order.domain.model.vo.OrderVo;
import org.mapstruct.Mapper;


/**
 * @author RHJ
 */
@Mapper(componentModel = "spring")
public interface OrderConverter extends BaseConverter<OrderDo,OrderPo,OrderDto,OrderVo>{

}

