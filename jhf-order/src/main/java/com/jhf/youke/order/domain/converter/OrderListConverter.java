package com.jhf.youke.order.domain.converter;

import com.jhf.youke.order.domain.model.Do.OrderListDo;
import com.jhf.youke.order.domain.model.dto.OrderListDto;
import com.jhf.youke.order.domain.model.po.OrderListPo;
import com.jhf.youke.order.domain.model.vo.OrderListVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 订单列表转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface OrderListConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param orderListDto 订单列表dto
     * @return {@link OrderListDo}
     */
    OrderListDo dto2Do(OrderListDto orderListDto);

    /**
     * 洗阿宝
     *
     * @param orderListDo 订单列表做
     * @return {@link OrderListPo}
     */
    OrderListPo do2Po(OrderListDo orderListDo);

    /**
     * 洗签证官
     *
     * @param orderListDo 订单列表做
     * @return {@link OrderListVo}
     */
    OrderListVo do2Vo(OrderListDo orderListDo);


    /**
     * 洗订单列表
     *
     * @param orderListDoList 订单列表做
     * @return {@link List}<{@link OrderListPo}>
     */
    List<OrderListPo> do2PoList(List<OrderListDo> orderListDoList);

    /**
     * 警察乙做
     *
     * @param orderListPo 订单列表阿宝
     * @return {@link OrderListDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    OrderListDo po2Do(OrderListPo orderListPo);

    /**
     * 警察乙签证官
     *
     * @param orderListPo 订单列表阿宝
     * @return {@link OrderListVo}
     */
    OrderListVo po2Vo(OrderListPo orderListPo);

    /**
     * 警察乙做列表
     *
     * @param orderListPoList 订单列表订单列表
     * @return {@link List}<{@link OrderListDo}>
     */
    List<OrderListDo> po2DoList(List<OrderListPo> orderListPoList);

    /**
     * 警察乙vo列表
     *
     * @param orderListPoList 订单列表订单列表
     * @return {@link List}<{@link OrderListVo}>
     */
    List<OrderListVo> po2VoList(List<OrderListPo> orderListPoList);


}

