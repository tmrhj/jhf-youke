package com.jhf.youke.order.domain.event;

import com.jhf.youke.core.ddd.BaseEvent;
import lombok.Getter;


/**
 * @author RHJ
 */
@Getter
public class OrderEvent extends BaseEvent {

    public OrderEvent(Object source, String type, String topic, String message) {
        super(source);
        this.topic = topic;
        this.type = type;
        this.message = message;
    }

}



