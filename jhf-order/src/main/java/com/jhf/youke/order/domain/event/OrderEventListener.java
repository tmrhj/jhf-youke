package com.jhf.youke.order.domain.event;

import com.jhf.youke.core.ddd.BaseEventListener;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;


/**
 * @author RHJ
 */
@Log4j2
@Component
public class OrderEventListener extends BaseEventListener<OrderEvent> {

    @Override
    public void handleEventDomain(OrderEvent event) {
        String topic = event.getTopic();
        log.info("domain event topic {}", topic);
        switch (topic) {
            case "spring":

                break;
            case "update":
                log.info("domain event topic is {}", topic);
                break;
            default:
                log.info("not equals topic");
        }
    }




}
