package com.jhf.youke.order.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class CartException extends DomainException {

    public CartException(String message) { super(message); }
}

