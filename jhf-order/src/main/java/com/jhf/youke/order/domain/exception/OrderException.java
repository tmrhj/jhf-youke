package com.jhf.youke.order.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class OrderException extends DomainException {

    public OrderException(String message) { super(message); }
}

