package com.jhf.youke.order.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class OrderListException extends DomainException {

    public OrderListException(String message) { super(message); }
}

