package com.jhf.youke.order.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.order.domain.model.po.CartPo;

/**
 * @author RHJ
 */

public interface CartRepository extends Repository<CartPo> {


}

