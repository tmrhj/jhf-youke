package com.jhf.youke.order.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.order.domain.model.po.OrderListPo;

import java.util.List;

/**
 * 订单列表存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface OrderListRepository extends Repository<OrderListPo> {

    /**
     * 被订单id列表
     *
     * @param orderId 订单id
     * @return {@link List}<{@link OrderListPo}>
     * @Description: 根据父表ID查询明细
     * @Param: [orderId] 订单ID
     * @return: java.util.List<com.jhf.youke.order.domain.model.po.OrderListPo>
     * @Author: RHJ
     * @Date: 2022/11/9
     */
    List<OrderListPo> getListByOrderId(Long orderId);


}

