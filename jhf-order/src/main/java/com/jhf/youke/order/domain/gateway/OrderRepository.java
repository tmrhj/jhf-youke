package com.jhf.youke.order.domain.gateway;


import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.order.domain.model.dto.OrderDto;
import com.jhf.youke.order.domain.model.po.OrderPo;
import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.order.domain.model.vo.OrderVo;

import java.util.List;

/**
 * 顺序存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface OrderRepository extends Repository<OrderPo> {

    /**
     * 得到产品版本id
     *
     * @param productReleaseId 产品版本id
     * @param customerId       客户id
     * @return {@link List}<{@link OrderPo}>
     */
    List<OrderPo> getPaidByProductReleaseId(Long productReleaseId, Long customerId);

    /**
     * 获取视频列表页
     *
     * @param dto dto
     * @return {@link Pagination}<{@link OrderVo}>
     */
    Pagination<OrderVo> getVideoPageList(OrderDto dto);

}

