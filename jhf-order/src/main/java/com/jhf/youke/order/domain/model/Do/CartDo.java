package com.jhf.youke.order.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.order.domain.exception.CartException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class CartDo extends BaseDoEntity {

  private static final long serialVersionUID = 530579477591359249L;

    /** 客户标识 **/
    private Long customerId;

    /** 单位标识  **/
    private Long companyId;

    /** 商品发布ID  **/
    private Long productReleaseId;

    /** 产品ID  **/
    private Long productId;

    /** 产品名  **/
    private String productName;

    /** 计量单位名  **/
    private String unitName;

    /** 规格1  **/
    private Long propertyId1;

    /** 规格2  **/
    private Long propertyId2;

    /** 规格3  **/
    private Long propertyId3;

    /** 单价  **/
    private BigDecimal price;

    /** 数量  **/
    private BigDecimal num;

    /** 金额  **/
    private BigDecimal fee;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new CartException(errorMessage);
        }
        return obj;
    }

    private CartDo validateNull(CartDo cartDo){
          /** 可使用链式法则进行为空检查  **/
          cartDo.
          requireNonNull(cartDo, cartDo.getRemark(),"不能为NULL");
                
        return cartDo;
    }


}


