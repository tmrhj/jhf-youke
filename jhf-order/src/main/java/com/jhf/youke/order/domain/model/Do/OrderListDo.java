package com.jhf.youke.order.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.order.domain.exception.OrderListException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class OrderListDo extends BaseDoEntity {

    private static final long serialVersionUID = -88194009292310792L;

    private Long orderId;

    /** 产品大类id **/
    private Long productStyleId;

    /** 产品大类名字 **/
    private String productStyleName;

    /** 产品Id **/
    private Long productId;

    /** 产品名 **/
    private String productName;

    /** 计量单位 **/
    private String unitName;

    /** 商品发布ID **/
    private Long productReleaseId;

    /** 商品图片**/
    private String productImg;

    /** 规格1 **/
    private Long propertyId1;

    /** 规格2 **/
    private Long propertyId2;

    /** 规格3 **/
    private Long propertyId3;

    private String propertyName1;

    private String propertyName2;

    private String propertyName3;

    /** 市场价 **/
    private BigDecimal marketPrice;

    /** 实际单价 **/
    private BigDecimal price;

    /** 数量 **/
    private BigDecimal num;

    /** 金额 **/
    private BigDecimal fee;


    public OrderListDo(){
        super();
    }

    public OrderListDo(Map<String,Object> productRelease, Long orderId){
        super();
        this.orderId = orderId;
        this.marketPrice = new BigDecimal(StringUtils.chgNull(productRelease.get("marketPrice")));
        this.price = new BigDecimal(StringUtils.chgNull(productRelease.get("price")));
        this.fee = this.price;
        this.num = new BigDecimal(1);
        this.productReleaseId = StringUtils.toLong(productRelease.get("id"));
        this.productId = StringUtils.toLong(productRelease.get("productId"));
        this.productName = StringUtils.chgNull(productRelease.get("name"));
        this.productStyleId = StringUtils.toLong(productRelease.get("productStyleId"));
        this.productStyleName = StringUtils.chgNull(productRelease.get("productStyleName"));
        this.productImg = StringUtils.chgNull(productRelease.get("listImg"));
    }

    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new OrderListException(errorMessage);
        }
        return obj;
    }

    private OrderListDo validateNull(OrderListDo orderListDo){
          //可使用链式法则进行为空检查
          orderListDo.
          requireNonNull(orderListDo, orderListDo.getRemark(),"不能为NULL");

        return orderListDo;
    }


}


