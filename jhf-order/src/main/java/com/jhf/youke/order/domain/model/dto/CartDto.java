package com.jhf.youke.order.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class CartDto extends BaseDtoEntity {

    private static final long serialVersionUID = 297558672164703368L;


    @ApiModelProperty(value = "客户标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long customerId;

    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "商品发布ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "产品ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;

    @ApiModelProperty(value = "产品名")
    private String productName;

    @ApiModelProperty(value = "计量单位名")
    private String unitName;

    @ApiModelProperty(value = "规格1")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId1;

    @ApiModelProperty(value = "规格2")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId2;

    @ApiModelProperty(value = "规格3")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId3;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "数量")
    private BigDecimal num;

    @ApiModelProperty(value = "金额")
    private BigDecimal fee;




}


