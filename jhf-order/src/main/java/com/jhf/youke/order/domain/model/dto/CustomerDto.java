package com.jhf.youke.order.domain.model.dto;

import com.jhf.youke.order.domain.model.value.Address;
import lombok.Data;

/**
 * @author RHJ
 */
@Data
public class CustomerDto {
    private Long id;
    private String name;
    private String phone;
    private Address address;
    private Long salesman;
    private String ifNew;


}
