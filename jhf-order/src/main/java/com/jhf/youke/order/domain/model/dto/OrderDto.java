package com.jhf.youke.order.domain.model.dto;

import cn.hutool.core.util.BooleanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.Constant;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.order.domain.model.vo.OrderListVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class OrderDto extends BaseDtoEntity {

    private static final long serialVersionUID = 177257800917852345L;


    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "类型")
    private Integer type;

    @ApiModelProperty(value = "用户标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long customerId;

    @ApiModelProperty(value = "客户标识")
    private String customerName;

    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "单位名")
    private String companyName;

    @ApiModelProperty(value = "总金额")
    private BigDecimal sumFee;

    @ApiModelProperty(value = "实际金额")
    private BigDecimal fee;

    @ApiModelProperty(value = "优惠金额")
    private BigDecimal couponFee;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "支付状态")
    private Integer payStatus;

    @ApiModelProperty(value = "支付时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    @ApiModelProperty(value = "团长ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long salesman;

    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "收货人")
    private String consignee;

    @ApiModelProperty(value = "收货地址")
    private String receiveAddress;

    @ApiModelProperty(value = "团长名")
    private String salesmanName;

    @ApiModelProperty(value = "团长支付状态")
    private Integer salesPayStatus;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;


    /** 完成时间 **/
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;

    private List<OrderListVo> itemList;

    private CustomerDto customer;

    public Boolean isNew(){
        return BooleanUtil.toBoolean(ifNew);
    }

    public void check() {
        if(Constant.IF_NEW.equals(customer.getIfNew()) && customer.getId() ==0){
            customer.setId(IdGen.id());
        }
        receiveAddress = customer.getAddress().getAddress();
        name = companyName + "的订单";
    }
}


