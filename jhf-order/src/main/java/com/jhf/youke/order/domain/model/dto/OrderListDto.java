package com.jhf.youke.order.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class OrderListDto extends BaseDtoEntity {

    private static final long serialVersionUID = 292704941538219119L;


    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long orderId;

    @ApiModelProperty(value = "产品大类id")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productStyleId;

    @ApiModelProperty(value = "产品大类名字")
    private String productStyleName;

    @ApiModelProperty(value = "产品Id")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;

    @ApiModelProperty(value = "产品名")
    private String productName;

    @ApiModelProperty(value = "计量单位")
    private String unitName;

    @ApiModelProperty(value = "商品发布ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "规格1")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId1;

    @ApiModelProperty(value = "规格2")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId2;

    @ApiModelProperty(value = "规格3")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId3;

    @ApiModelProperty(value = "规格1")
    private String propertyName1;

    @ApiModelProperty(value = "规格2")
    private String propertyName2;

    @ApiModelProperty(value = "规格3")
    private String propertyName3;

    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;

    @ApiModelProperty(value = "实际单价")
    private BigDecimal price;

    @ApiModelProperty(value = "数量")
    private BigDecimal num;

    @ApiModelProperty(value = "金额")
    private BigDecimal fee;


    /** 商品图片**/
    private String productImg;


}


