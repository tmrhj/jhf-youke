package com.jhf.youke.order.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Administrator
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class VideoOrderDto extends BaseDtoEntity {

    private static final long serialVersionUID = 292704941538219119L;

    @ApiModelProperty(value = "商品发布ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "订单ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long orderId;

}


