package com.jhf.youke.order.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "ord_cart")
public class CartPo extends BasePoEntity {

    private static final long serialVersionUID = 418164764894760957L;

    /** 客户标识 **/
    private Long customerId;

    /** 单位标识 **/
    private Long companyId;

    /** 商品发布ID **/
    private Long productReleaseId;

    /** 产品ID **/
    private Long productId;

    /** 产品名 **/
    private String productName;

    /** 计量单位名 **/
    private String unitName;

    /** 规格1 **/
    private Long propertyId1;

    /** 规格2 **/
    private Long propertyId2;

    /** 规格3 **/
    private Long propertyId3;

    /** 单价 **/
    private BigDecimal price;

    /** 数量 **/
    private BigDecimal num;

    /** 金额 **/
    private BigDecimal fee;



}


