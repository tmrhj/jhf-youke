package com.jhf.youke.order.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "ord_order_list")
public class OrderListPo extends BasePoEntity {

    private static final long serialVersionUID = 930080709128829150L;

    private Long orderId;

    /** 产品大类id **/
    private Long productStyleId;

    /** 产品大类名字 **/
    private String productStyleName;

    /** 产品Id **/
    private Long productId;

    /** 产品名 **/
    private String productName;

    /** 计量单位 **/
    private String unitName;

    /** 商品发布ID **/
    private Long productReleaseId;

    /** 商品图片**/
    private String productImg;

    /** 规格1 **/
    private Long propertyId1;

    /** 规格2 **/
    private Long propertyId2;

    /** 规格3 **/
    private Long propertyId3;

    /** 规格3 **/
    private String propertyName1;

    private String propertyName2;

    private String propertyName3;

    /** 市场价 **/
    private BigDecimal marketPrice;

    /** 实际单价 **/
    private BigDecimal price;

    /** 数量 **/
    private BigDecimal num;

    /** 金额 **/
    private BigDecimal fee;



}


