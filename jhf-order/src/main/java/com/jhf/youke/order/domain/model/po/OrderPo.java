package com.jhf.youke.order.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "ord_order")
public class OrderPo extends BasePoEntity {

    private static final long serialVersionUID = 138337694486649148L;

    /** 编码 **/
    private String code;

    /** 名字 **/
    private String name;

    /** 类型 **/
    private Integer type;

    /** 用户标识 **/
    private Long customerId;

    /** 客户标识 **/
    private String customerName;

    /** 单位标识 **/
    private Long companyId;

    /** 单位名 **/
    private String companyName;

    /** 总金额 **/
    private BigDecimal sumFee;

    /** 实际金额 **/
    private BigDecimal fee;

    /** 优惠金额 **/
    private BigDecimal couponFee;

    /** 状态 **/
    private Integer status;

    /** 支付状态 **/
    private Integer payStatus;

    /** 支付时间 **/
    private Date payTime;

    /** 完成时间 **/
    private Date finishTime;

    /** 团长ID **/
    private Long salesman;

    /** 团长名 **/
    private String salesmanName;

    /** 团长支付状态 **/
    private Integer salesPayStatus;

    private String phone;
    /**  收货人 **/
    private String consignee;
    /**  收货地址 **/
    private String receiveAddress;
    /** 经度 **/
    private String longitude;
    /** 纬度 **/
    private String latitude;



}


