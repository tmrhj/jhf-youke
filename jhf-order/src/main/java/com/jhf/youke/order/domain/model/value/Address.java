package com.jhf.youke.order.domain.model.value;

import com.jhf.youke.core.utils.StringUtils;
import lombok.Data;

/**
 * @author RHJ
 */
@Data
public class Address {
    private Long id;
    private String province;
    private String city;
    private String district;
    private String street;
    private String address;
    private String ifNew;

    public Address(){

    }

    public Address(String province, String city, String district, String street,  String address){
        this.province = province;
        this.city = city;
        this.district = district;
        this.street = street;
        this.address = address;
    }

    public String getAddress(){
        StringBuilder  addr = new StringBuilder();
        addr = StringUtils.isNotBlank(this.province) ? addr.append(this.province) : addr ;
        addr = StringUtils.isNotBlank(this.city) ? addr.append(this.city) : addr ;
        addr = StringUtils.isNotBlank(this.district) ? addr.append(this.district) : addr ;
        addr = StringUtils.isNotBlank(this.street) ? addr.append(this.street) : addr ;
        addr = StringUtils.isNotBlank(this.address) ? addr.append(this.address) : addr ;
        return addr.toString();
    }


}
