package com.jhf.youke.order.domain.service;

import com.jhf.youke.order.domain.converter.CartConverter;
import com.jhf.youke.order.domain.model.Do.CartDo;
import com.jhf.youke.order.domain.gateway.CartRepository;
import com.jhf.youke.order.domain.model.po.CartPo;
import com.jhf.youke.order.domain.model.vo.CartVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class CartService extends AbstractDomainService<CartRepository, CartDo, CartVo> {


    @Resource
    CartConverter cartConverter;

    @Override
    public boolean update(CartDo entity) {
        CartPo cartPo = cartConverter.do2Po(entity);
        cartPo.preUpdate();
        return repository.update(cartPo);
    }

    @Override
    public boolean updateBatch(List<CartDo> doList) {
        List<CartPo> poList = cartConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(CartDo entity) {
        CartPo cartPo = cartConverter.do2Po(entity);
        return repository.delete(cartPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(CartDo entity) {
        CartPo cartPo = cartConverter.do2Po(entity);
        cartPo.preInsert();
        return repository.insert(cartPo);
    }

    @Override
    public boolean insertBatch(List<CartDo> doList) {
        List<CartPo> poList = cartConverter.do2PoList(doList);
        poList = CartPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<CartVo> findById(Long id) {
        Optional<CartPo> cartPo =  repository.findById(id);
        CartVo cartVo = cartConverter.po2Vo(cartPo.orElse(new CartPo()));
        return Optional.ofNullable(cartVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<CartVo> findAllMatching(CartDo entity) {
        CartPo cartPo = cartConverter.do2Po(entity);
        List<CartPo>cartPoList =  repository.findAllMatching(cartPo);
        return cartConverter.po2VoList(cartPoList);
    }


    @Override
    public Pagination<CartVo> selectPage(CartDo entity){
        CartPo cartPo = cartConverter.do2Po(entity);
        PageQuery<CartPo> pageQuery = new PageQuery<>(cartPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<CartPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                cartConverter.po2VoList(pagination.getList()));
    }


}

