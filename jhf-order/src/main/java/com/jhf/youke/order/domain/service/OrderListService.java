package com.jhf.youke.order.domain.service;

import com.jhf.youke.order.domain.converter.OrderListConverter;
import com.jhf.youke.order.domain.model.Do.OrderListDo;
import com.jhf.youke.order.domain.gateway.OrderListRepository;
import com.jhf.youke.order.domain.model.po.OrderListPo;
import com.jhf.youke.order.domain.model.vo.OrderListVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class OrderListService extends AbstractDomainService<OrderListRepository, OrderListDo, OrderListVo> {


    @Resource
    OrderListConverter orderListConverter;

    @Override
    public boolean update(OrderListDo entity) {
        OrderListPo orderListPo = orderListConverter.do2Po(entity);
        orderListPo.preUpdate();
        return repository.update(orderListPo);
    }

    @Override
    public boolean updateBatch(List<OrderListDo> doList) {
        List<OrderListPo> poList = orderListConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(OrderListDo entity) {
        OrderListPo orderListPo = orderListConverter.do2Po(entity);
        return repository.delete(orderListPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(OrderListDo entity) {
        OrderListPo orderListPo = orderListConverter.do2Po(entity);
        orderListPo.preInsert();
        return repository.insert(orderListPo);
    }

    @Override
    public boolean insertBatch(List<OrderListDo> doList) {
        List<OrderListPo> poList = orderListConverter.do2PoList(doList);
        poList = OrderListPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    public boolean insertBatch(List<OrderListDo> doList,Long orderId) {
        List<OrderListPo> poList = orderListConverter.do2PoList(doList);
        poList.forEach( entity -> {
            entity.preInsert();
            entity.setOrderId(orderId);
        });
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<OrderListVo> findById(Long id) {
        Optional<OrderListPo> orderListPo =  repository.findById(id);
        OrderListVo orderListVo = orderListConverter.po2Vo(orderListPo.orElse(new OrderListPo()));
        return Optional.ofNullable(orderListVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<OrderListVo> findAllMatching(OrderListDo entity) {
        OrderListPo orderListPo = orderListConverter.do2Po(entity);
        List<OrderListPo>orderListPoList =  repository.findAllMatching(orderListPo);
        return orderListConverter.po2VoList(orderListPoList);
    }


    @Override
    public Pagination<OrderListVo> selectPage(OrderListDo entity){
        OrderListPo orderListPo = orderListConverter.do2Po(entity);
        PageQuery<OrderListPo> pageQuery = new PageQuery<>(orderListPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<OrderListPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                orderListConverter.po2VoList(pagination.getList()));
    }


    public List<OrderListPo> getListByOrderId(Long id) {
        return repository.getListByOrderId(id);
    }
}

