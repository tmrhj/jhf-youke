package com.jhf.youke.order.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.order.domain.model.po.CartPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 购物车映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface CartMapper  extends BaseMapper<CartPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update ord_cart set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update ord_cart set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

