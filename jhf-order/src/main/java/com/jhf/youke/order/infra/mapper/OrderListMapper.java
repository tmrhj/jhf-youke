package com.jhf.youke.order.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.order.domain.model.po.OrderListPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 订单列表映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface OrderListMapper  extends BaseMapper<OrderListPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update ord_order_list set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update ord_order_list set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 被订单id列表
     *
     * @param orderId 订单id
     * @return {@link List}<{@link OrderListPo}>
     */
    List<OrderListPo> getListByOrderId(@Param("orderId") Long orderId);

}

