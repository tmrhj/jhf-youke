package com.jhf.youke.order.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.order.domain.exception.OrderListException;
import com.jhf.youke.order.domain.gateway.OrderListRepository;
import com.jhf.youke.order.domain.model.po.OrderListPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "OrderListRepository")
public class OrderListRepositoryImpl extends ServiceImpl<OrderListMapper, OrderListPo> implements OrderListRepository {


    @Override
    public boolean update(OrderListPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<OrderListPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(OrderListPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(OrderListPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<OrderListPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<OrderListPo> findById(Long id) {
        OrderListPo orderListPo = baseMapper.selectById(id);
        return Optional.ofNullable(orderListPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new OrderListException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<OrderListPo> findAllMatching(OrderListPo entity) {
        QueryWrapper<OrderListPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<OrderListPo> selectPage(PageQuery<OrderListPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        QueryWrapper<OrderListPo> qw = new QueryWrapper<>(pageQuery.getParam());
        qw.orderBy(!StringUtils.isEmpty(pageQuery.getSort()),false, pageQuery.getSort());
        List<OrderListPo> orderListPos = baseMapper.selectList(qw);
        PageInfo<OrderListPo> pageInfo = new PageInfo<>(orderListPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<OrderListPo> getListByOrderId(Long orderId) {
        return baseMapper.getListByOrderId(orderId);
    }


}

