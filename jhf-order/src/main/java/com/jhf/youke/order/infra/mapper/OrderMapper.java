package com.jhf.youke.order.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.order.domain.model.dto.OrderDto;
import com.jhf.youke.order.domain.model.po.OrderPo;
import com.jhf.youke.order.domain.model.vo.OrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 顺序映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface OrderMapper  extends BaseMapper<OrderPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update ord_order set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update ord_order set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 得到产品版本id
     *
     * @param productReleaseId 产品版本id
     * @param customerId       客户id
     * @return {@link List}<{@link OrderPo}>
     */
    @Select("SELECT a.* FROM ord_order a  LEFT JOIN ord_order_list b on  b.order_id = a.id " +
            "  where b.product_release_id = #{productReleaseId} and a.pay_status = 3 and a.customer_id = #{customerId} ")
    List<OrderPo> getPaidByProductReleaseId(@Param("productReleaseId") Long productReleaseId,@Param("customerId") Long  customerId);

    /**
     * 获取视频列表页
     *
     * @param dto dto
     * @return {@link List}<{@link OrderVo}>
     */
    List<OrderVo> getVideoPageList(OrderDto dto);

}

