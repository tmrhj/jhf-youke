package com.jhf.youke.order.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.order.domain.exception.OrderException;
import com.jhf.youke.order.domain.gateway.OrderRepository;
import com.jhf.youke.order.domain.model.dto.OrderDto;
import com.jhf.youke.order.domain.model.po.OrderPo;
import com.jhf.youke.order.domain.model.vo.OrderVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "OrderRepository")
public class OrderRepositoryImpl extends ServiceImpl<OrderMapper, OrderPo> implements OrderRepository {


    @Override
    public boolean update(OrderPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<OrderPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(OrderPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(OrderPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<OrderPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<OrderPo> findById(Long id) {
        OrderPo orderPo = baseMapper.selectById(id);
        return Optional.ofNullable(orderPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }
        if(StringUtils.isEmpty(ids)){
            throw new OrderException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<OrderPo> findAllMatching(OrderPo entity) {
        QueryWrapper<OrderPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<OrderPo> selectPage(PageQuery<OrderPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        QueryWrapper<OrderPo> qw = new QueryWrapper<>(pageQuery.getParam());
        qw.orderBy(!StringUtils.isEmpty(pageQuery.getSort()),false, pageQuery.getSort());
        List<OrderPo> orderPos = baseMapper.selectList(qw);
        PageInfo<OrderPo> pageInfo = new PageInfo<>(orderPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<OrderPo> getPaidByProductReleaseId(Long productReleaseId, Long customerId) {
        return baseMapper.getPaidByProductReleaseId(productReleaseId, customerId);
    }

    @Override
    public Pagination<OrderVo> getVideoPageList(OrderDto dto) {
        PageHelper.startPage(dto.getCurrentPage(), dto.getPageSize());
        List<OrderVo> list = baseMapper.getVideoPageList(dto);
        PageInfo<OrderVo> pageInfo = new PageInfo<>(list);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

