import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.DES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import com.jhf.youke.OrderApplication;
import com.jhf.youke.order.domain.event.OrderEvent;
import com.jhf.youke.order.domain.model.Do.OrderDo;
import com.jhf.youke.order.domain.model.Do.OrderListDo;
import com.jhf.youke.order.domain.model.dto.CustomerDto;
import com.jhf.youke.order.domain.model.value.Address;
import com.jhf.youke.order.domain.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Slf4j
@SpringBootTest(classes = OrderApplication.class)
class OrderTestApplication {


    @Resource
    OrderService orderService;

    @Resource
    ApplicationContext applicationContext;

    @Test
    void test1(){
       BigDecimal fee = BigDecimal.ZERO;
       fee.add(BigDecimal.TEN);
       log.info(fee.toString());
    }

    @Test
    void test2(){
        Address address = new Address("湖南省","长沙市","东城区","东湖街道","车站北路19号");
        log.info(address.getAddress());
    }

    @Test
    void test3(){
        OrderEvent orderEvent = new OrderEvent(this,"1","test","test");
        applicationContext.publishEvent(orderEvent);
    }

    @Test
    void test4(){
        OrderDo orderDo = new OrderDo();
        orderDo.setId(1L);
        orderDo.setCompanyId(1L);
        orderDo.setSalesman(1L);
        CustomerDto customerDto = new CustomerDto();
        Address address = new Address("湖南省","长沙市","东城区","东湖街道","车站北路19号");
        address.setId(1L);
        customerDto.setAddress(address);
        customerDto.setId(1L);
        customerDto.setName("张三");
        customerDto.setPhone("18999999999");
        customerDto.setSalesman(100L);
        OrderListDo orderListDo = new OrderListDo();
        orderListDo.setNum(BigDecimal.ONE);
        orderListDo.setPropertyId1(1L);
        orderListDo.setPropertyId2(2L);
        orderListDo.setPropertyId2(3L);
        orderListDo.setProductName("苹果");
        orderListDo.setUnitName("斤");
        orderListDo.setProductReleaseId(1L);
        List<OrderListDo> list = new ArrayList<>();
        list.add(orderListDo);
        orderDo.setCustomer(customerDto);
        orderDo.setItemList(list);

        String msg1 = orderDo.sendCustomerMsg();
        String msg2 = orderDo.sendCommissionMsg();

        log.info(msg1);
        log.info(msg2);

    }


    @Test
    public void test(){
        String content = "test中文111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";

        log.info("明文 {}", content);

        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

        //构建
        AES aes = SecureUtil.aes(key);

        //加密
        byte[] encrypt = aes.encrypt(content);
        //解密
        byte[] decrypt = aes.decrypt(encrypt);

        //构建
        DES des = SecureUtil.des(key);

        //加密为16进制表示
        String encryptHex = des.encryptHex(content);
        log.info("加密 {}", encryptHex);
        //解密为原字符串
        String decryptStr = des.decryptStr(encryptHex);
        log.info("解密 {}", decryptStr);

    }

    @Test
    public void test5(){
      byte[]  my  = {-79, -109, -122, 86, -51, -83, 97, -91, -66, 92, 35, 116, 63, -81, 19, 59};
      log.info(""+ my.toString());
      String str =  Base64.getEncoder().encodeToString(my);
      log.info(str);
      byte[] my1 = Base64.getDecoder().decode(str);
      log.info(""+ my1.toString());

    }





}
