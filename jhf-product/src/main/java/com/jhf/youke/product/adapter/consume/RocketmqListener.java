package com.jhf.youke.product.adapter.consume;
import ch.qos.logback.classic.Logger;
import com.jhf.youke.core.entity.Message;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

/**
 * @author RHJ
 */
@Component
public class RocketmqListener {

    private final static Logger logger = (Logger) LoggerFactory.getLogger(RocketmqListener.class);


    @Bean
    public Consumer<Message> inputKafka1() {
        return msg -> {
            // 收到消息在这里做一些处理
            logger.info("inputKafka1 message: {}", msg.getMessages());
        };
    }


}
