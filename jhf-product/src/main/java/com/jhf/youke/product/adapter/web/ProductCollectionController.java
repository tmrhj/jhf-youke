package com.jhf.youke.product.adapter.web;

import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.product.app.executor.ProductCollectionAppService;
import com.jhf.youke.product.domain.model.dto.ProductCollectionDto;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


@Api(tags = "商品收藏")
@RestController
@RequestMapping("/productCollection")
public class ProductCollectionController {

    @Resource
    private ProductCollectionAppService productCollectionAppService;


    @PostMapping("/delete")
    @ApiOperation("用户取消收藏")
    public Response<Boolean> delete(@RequestHeader("token") String token, @RequestBody ProductCollectionDto dto) {
        User user = CacheUtils.getUser(token);
        dto.setUserId(user.getId());
        return Response.ok(productCollectionAppService.delete(dto));
    }

    @PostMapping("/collectGoods")
    @ApiOperation("收藏商品")
    public Response<Boolean> insert(@RequestHeader("token") String token, @RequestBody ProductCollectionDto dto) {
        User user = CacheUtils.getUser(token);
        dto.setUserId(user.getId());
        dto.setUserName(user.getName());
        return Response.ok(productCollectionAppService.insert(dto));
    }

    @PostMapping("/selectPageByUser")
    @ApiOperation("用户查询自己的收藏记录")
    public Response<Pagination<ProductCollectionVo>> selectPage(@RequestBody  ProductCollectionDto dto, @RequestHeader("token") String token) {
        User user = CacheUtils.getUser(token);
        dto.setUserId(user.getId());
        return Response.ok(productCollectionAppService.getPageList(dto));
    }

}

