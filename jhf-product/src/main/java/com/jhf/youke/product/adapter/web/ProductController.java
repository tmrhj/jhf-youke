package com.jhf.youke.product.adapter.web;

import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.product.app.executor.ProductAppService;
import com.jhf.youke.product.domain.model.dto.ProductDto;
import com.jhf.youke.product.domain.model.vo.ProductVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "产品管理")
@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    private ProductAppService productAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductDto dto) {
        return Response.ok(productAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductDto dto) {
        return Response.ok(productAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ProductDto dto) {
        return Response.ok(productAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductVo>> findById(Long id) {
        return Response.ok(productAppService.findById(id));
    }

    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductVo>> list(@RequestBody ProductDto dto, @RequestHeader("token") String token) {
        User user = CacheUtils.getUser(token);
        dto.setCompanyId(user.getRootId());
        return Response.ok(productAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductVo>> selectPage(@RequestBody  ProductDto dto, @RequestHeader("token") String token) {
        return Response.ok(productAppService.selectPage(dto));
    }

    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(productAppService.getPreData());
    }


}

