package com.jhf.youke.product.adapter.web;

import com.jhf.youke.product.app.executor.ProductPropertyAppService;
import com.jhf.youke.product.domain.model.dto.ProductPropertyDto;
import com.jhf.youke.product.domain.model.vo.ProductPropertyVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/productProperty")
public class ProductPropertyController {

    @Resource
    private ProductPropertyAppService productPropertyAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductPropertyDto dto) {
        return Response.ok(productPropertyAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductPropertyDto dto) {
        return Response.ok(productPropertyAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ProductPropertyDto dto) {
        return Response.ok(productPropertyAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductPropertyVo>> findById(Long id) {
        return Response.ok(productPropertyAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productPropertyAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductPropertyVo>> list(@RequestBody ProductPropertyDto dto) {

        return Response.ok(productPropertyAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductPropertyVo>> selectPage(@RequestBody  ProductPropertyDto dto) {
        return Response.ok(productPropertyAppService.selectPage(dto));
    }

}

