package com.jhf.youke.product.adapter.web;

import com.jhf.youke.product.app.executor.ProductPropertyListAppService;
import com.jhf.youke.product.domain.model.dto.ProductPropertyListDto;
import com.jhf.youke.product.domain.model.vo.ProductPropertyListVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/productPropertyList")
public class ProductPropertyListController {

    @Resource
    private ProductPropertyListAppService productPropertyListAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductPropertyListDto dto) {
        return Response.ok(productPropertyListAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductPropertyListDto dto) {
        return Response.ok(productPropertyListAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ProductPropertyListDto dto) {
        return Response.ok(productPropertyListAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductPropertyListVo>> findById(Long id) {
        return Response.ok(productPropertyListAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productPropertyListAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductPropertyListVo>> list(@RequestBody ProductPropertyListDto dto) {

        return Response.ok(productPropertyListAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductPropertyListVo>> selectPage(@RequestBody  ProductPropertyListDto dto) {
        return Response.ok(productPropertyListAppService.selectPage(dto));
    }

}

