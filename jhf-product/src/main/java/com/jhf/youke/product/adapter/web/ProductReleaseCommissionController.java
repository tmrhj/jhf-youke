package com.jhf.youke.product.adapter.web;

import com.jhf.youke.product.app.executor.ProductReleaseCommissionAppService;
import com.jhf.youke.product.domain.model.dto.ProductReleaseCommissionDto;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/productReleaseCommission")
public class ProductReleaseCommissionController {

    @Resource
    private ProductReleaseCommissionAppService productReleaseCommissionAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductReleaseCommissionDto dto) {
        return Response.ok(productReleaseCommissionAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductReleaseCommissionDto dto) {
        return Response.ok(productReleaseCommissionAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ProductReleaseCommissionDto dto) {
        return Response.ok(productReleaseCommissionAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductReleaseCommissionVo>> findById(Long id) {
        return Response.ok(productReleaseCommissionAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productReleaseCommissionAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductReleaseCommissionVo>> list(@RequestBody ProductReleaseCommissionDto dto) {

        return Response.ok(productReleaseCommissionAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductReleaseCommissionVo>> selectPage(@RequestBody  ProductReleaseCommissionDto dto) {
        return Response.ok(productReleaseCommissionAppService.selectPage(dto));
    }

}

