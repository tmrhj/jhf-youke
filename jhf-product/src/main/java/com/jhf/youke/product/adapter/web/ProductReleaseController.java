package com.jhf.youke.product.adapter.web;

import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.app.executor.ProductReleaseAppService;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseInfoVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/productRelease")
public class ProductReleaseController {

    @Resource
    private ProductReleaseAppService productReleaseAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductReleaseEditDto dto) {
        productReleaseAppService.updateGoods(dto);
        return Response.ok(true);
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestHeader("token") String token, @RequestBody ProductReleaseEditDto dto) {
        User user = CacheUtils.getUser(token);
        dto.setCompanyId(user.getCompanyId());
        productReleaseAppService.addGoods(dto);
        return Response.ok(true);
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<ProductReleaseInfoVo> findById(@RequestHeader("token") String token, Long id) {
        return Response.ok(productReleaseAppService.findById(token, id));
    }

    @GetMapping("/detailById")
    @ApiOperation("根据ID查询,包括子表数据")
    public Response<ProductReleaseVo> getById(Long id) {
        return Response.ok(productReleaseAppService.instanceOf(id));
    }

    @GetMapping("/insertAll")
    @ApiOperation("插入数据,包括子表数据")
    public Response<Boolean> insertAll(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.insertAll(dto));
    }

    @GetMapping("/updateAll")
    @ApiOperation("更新数据,包括子表数据")
    public Response<Boolean> updateAll(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.updateAll(dto));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productReleaseAppService.remove(id));
    }


    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody List<Long> idList) {
        return  Response.ok(productReleaseAppService.removeBatch(idList));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductReleaseVo>> list(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductReleaseVo>> selectPage(@RequestHeader( value = "token", required = false) String token,
                                                             @RequestBody  ProductReleaseDto dto) {
        if(!StringUtils.ifNull(token) && StringUtils.ifNull(dto.getCompanyId())) {
            User user = CacheUtils.getUser(token);
            dto.setCompanyId(user.getRootId());
        }
        return Response.ok(productReleaseAppService.selectPage(dto));
    }


    @GetMapping("/release")
    @ApiOperation("商品发布")
    public Response<Boolean> release(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.release(dto));
    }

    @GetMapping("/soldOut")
    @ApiOperation("商品下架")
    public Response<Boolean> soldOut(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.soldOut(dto));
    }

    @GetMapping("/getProductReleaseCommission")
    @ApiOperation("商品下架")
    public Response<List<ProductReleaseCommissionVo>> getProductReleaseCommission(Long id) {
        return Response.ok(productReleaseAppService.getProductReleaseCommission(id));
    }

    @PostMapping("/getAllInfoList")
    @ApiOperation("查询带详情的列表")
    public Response<Pagination<ProductReleaseVo>> getAllInfoList(@RequestBody ProductReleaseDto dto) {
        return Response.ok(productReleaseAppService.getAllInfoList(dto));
    }

    @PostMapping("/getVideo")
    public ProductReleaseVo getVideo(@RequestHeader( value = "token", required = false) String token, @RequestBody ProductReleaseDto dto) {
        return productReleaseAppService.getVideo(token, dto.getId());
    }

}

