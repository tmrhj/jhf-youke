package com.jhf.youke.product.adapter.web;

import com.jhf.youke.product.app.executor.ProductReleaseImageAppService;
import com.jhf.youke.product.domain.model.dto.ProductReleaseImageDto;
import com.jhf.youke.product.domain.model.vo.ProductReleaseImageVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/productReleaseImage")
public class ProductReleaseImageController {

    @Resource
    private ProductReleaseImageAppService productReleaseImageAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductReleaseImageDto dto) {
        return Response.ok(productReleaseImageAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductReleaseImageDto dto) {
        return Response.ok(productReleaseImageAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ProductReleaseImageDto dto) {
        return Response.ok(productReleaseImageAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductReleaseImageVo>> findById(Long id) {
        return Response.ok(productReleaseImageAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productReleaseImageAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductReleaseImageVo>> list(@RequestBody ProductReleaseImageDto dto) {

        return Response.ok(productReleaseImageAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductReleaseImageVo>> selectPage(@RequestBody  ProductReleaseImageDto dto) {
        return Response.ok(productReleaseImageAppService.selectPage(dto));
    }

}

