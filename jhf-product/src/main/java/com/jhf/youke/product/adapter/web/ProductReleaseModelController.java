package com.jhf.youke.product.adapter.web;

import com.jhf.youke.product.app.executor.ProductReleaseModelAppService;
import com.jhf.youke.product.domain.model.dto.ProductReleaseModelDto;
import com.jhf.youke.product.domain.model.vo.ProductReleaseModelVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/productReleaseModel")
public class ProductReleaseModelController {

    @Resource
    private ProductReleaseModelAppService productReleaseModelAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductReleaseModelDto dto) {
        return Response.ok(productReleaseModelAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductReleaseModelDto dto) {
        return Response.ok(productReleaseModelAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ProductReleaseModelDto dto) {
        return Response.ok(productReleaseModelAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductReleaseModelVo>> findById(Long id) {
        return Response.ok(productReleaseModelAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productReleaseModelAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductReleaseModelVo>> list(@RequestBody ProductReleaseModelDto dto) {

        return Response.ok(productReleaseModelAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductReleaseModelVo>> selectPage(@RequestBody  ProductReleaseModelDto dto) {
        return Response.ok(productReleaseModelAppService.selectPage(dto));
    }

}

