package com.jhf.youke.product.adapter.web;

import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.app.executor.ProductStyleAppService;
import com.jhf.youke.product.domain.model.dto.ProductStyleDto;
import com.jhf.youke.product.domain.model.vo.ProductStyleVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "产品大类管理")
@RestController
@RequestMapping("/productStyle")
public class ProductStyleController {

    @Resource
    private ProductStyleAppService productStyleAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ProductStyleDto dto) {
        return Response.ok(productStyleAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ProductStyleDto dto) {
        return Response.ok(productStyleAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestHeader("token") String token, @RequestBody ProductStyleDto dto) {
        return Response.ok(productStyleAppService.insert(dto, token));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ProductStyleVo>> findById(Long id) {
        return Response.ok(productStyleAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(productStyleAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ProductStyleVo>> list(@RequestHeader( value = "token", required = false) String token, @RequestBody ProductStyleDto dto) {
        if(!StringUtils.ifNull(token) && StringUtils.ifNull(dto.getCompanyId())) {
            User user = CacheUtils.getUser(token);
            dto.setCompanyId(user.getRootId());
        }
        return Response.ok(productStyleAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ProductStyleVo>> selectPage(@RequestBody ProductStyleDto dto) {
        return Response.ok(productStyleAppService.selectPage(dto));
    }

    @PostMapping("/tree")
    @ApiOperation("树形列表")
    public Response<List<ProductStyleVo>> tree(@RequestBody ProductStyleDto dto) {
        return Response.ok(productStyleAppService.getTreeByCompanyId(dto.getCompanyId()));
    }

}

