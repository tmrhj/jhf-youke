package com.jhf.youke.product.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.product.domain.converter.ProductConverter;
import com.jhf.youke.product.domain.model.Do.ProductDo;
import com.jhf.youke.product.domain.model.dto.ProductDto;
import com.jhf.youke.product.domain.model.vo.ProductVo;
import com.jhf.youke.product.domain.service.ProductService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductAppService {

    @Resource
    private ProductService productService;

    @Resource
    private ProductConverter productConverter;


    public boolean update(ProductDto dto) {
        ProductDo productDo =  productConverter.dto2Do(dto);
        return productService.update(productDo);
    }

    public boolean delete(ProductDto dto) {
        ProductDo productDo =  productConverter.dto2Do(dto);
        return productService.delete(productDo);
    }


    public boolean insert(ProductDto dto) {
        ProductDo productDo =  productConverter.dto2Do(dto);
        return productService.insert(productDo);
    }

    public Optional<ProductVo> findById(Long id) {
        return productService.findById(id);
    }


    public boolean remove(Long id) {
        return productService.remove(id);
    }


    public List<ProductVo> findAllMatching(ProductDto dto) {
        ProductDo productDo =  productConverter.dto2Do(dto);
        return productService.findAllMatching(productDo);
    }


    public Pagination<ProductVo> selectPage(ProductDto dto) {
        ProductDo productDo =  productConverter.dto2Do(dto);
        return productService.selectPage(productDo);
    }
    
     public Long getPreData() {
        return IdGen.id();
    }
        

}

