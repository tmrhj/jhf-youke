package com.jhf.youke.product.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.product.domain.converter.ProductCollectionConverter;
import com.jhf.youke.product.domain.model.Do.ProductCollectionDo;
import com.jhf.youke.product.domain.model.dto.ProductCollectionDto;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;
import com.jhf.youke.product.domain.service.ProductCollectionService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  makejava
 **/

@Slf4j
@Service
public class ProductCollectionAppService extends BaseAppService {

    @Resource
    private ProductCollectionService productCollectionService;

    @Resource
    private ProductCollectionConverter productCollectionConverter;


    public boolean update(ProductCollectionDto dto) {
        ProductCollectionDo productCollectionDo =  productCollectionConverter.dto2Do(dto);
        return productCollectionService.update(productCollectionDo);
    }

    public boolean delete(ProductCollectionDto dto) {
        ProductCollectionDo productCollectionDo =  productCollectionConverter.dto2Do(dto);
        return productCollectionService.delete(productCollectionDo);
    }


    public boolean insert(ProductCollectionDto dto) {
        ProductCollectionDo productCollectionDo =  productCollectionConverter.dto2Do(dto);
        return productCollectionService.insert(productCollectionDo);
    }

    public Optional<ProductCollectionVo> findById(Long id) {
        return productCollectionService.findById(id);
    }


    public boolean remove(Long id) {
        return productCollectionService.remove(id);
    }


    public List<ProductCollectionVo> findAllMatching(ProductCollectionDto dto) {
        ProductCollectionDo productCollectionDo =  productCollectionConverter.dto2Do(dto);
        return productCollectionService.findAllMatching(productCollectionDo);
    }


    public Pagination<ProductCollectionVo> selectPage(ProductCollectionDto dto) {
        ProductCollectionDo productCollectionDo =  productCollectionConverter.dto2Do(dto);
        return productCollectionService.selectPage(productCollectionDo);
    }


    public Pagination<ProductCollectionVo> getPageList(ProductCollectionDto dto) {
        ProductCollectionDo productCollectionDo =  productCollectionConverter.dto2Do(dto);
        return productCollectionService.getPageList(productCollectionDo);
    }

}

