package com.jhf.youke.product.app.executor;

import com.jhf.youke.product.domain.converter.ProductPropertyConverter;
import com.jhf.youke.product.domain.model.Do.ProductPropertyDo;
import com.jhf.youke.product.domain.model.dto.ProductPropertyDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.vo.ProductPropertyVo;
import com.jhf.youke.product.domain.service.ProductPropertyService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductPropertyAppService {

    @Resource
    private ProductPropertyService productPropertyService;

    @Resource
    private ProductPropertyConverter productPropertyConverter;


    public boolean update(ProductPropertyDto dto) {
        ProductPropertyDo productPropertyDo =  productPropertyConverter.dto2Do(dto);
        return productPropertyService.update(productPropertyDo);
    }

    public boolean delete(ProductPropertyDto dto) {
        ProductPropertyDo productPropertyDo =  productPropertyConverter.dto2Do(dto);
        return productPropertyService.delete(productPropertyDo);
    }


    public boolean insert(ProductPropertyDto dto) {
        ProductPropertyDo productPropertyDo =  productPropertyConverter.dto2Do(dto);
        return productPropertyService.insert(productPropertyDo);
    }

    public Optional<ProductPropertyVo> findById(Long id) {
        return productPropertyService.findById(id);
    }


    public boolean remove(Long id) {
        return productPropertyService.remove(id);
    }


    public List<ProductPropertyVo> findAllMatching(ProductPropertyDto dto) {
        ProductPropertyDo productPropertyDo =  productPropertyConverter.dto2Do(dto);
        return productPropertyService.findAllMatching(productPropertyDo);
    }


    public Pagination<ProductPropertyVo> selectPage(ProductPropertyDto dto) {
        ProductPropertyDo productPropertyDo =  productPropertyConverter.dto2Do(dto);
        return productPropertyService.selectPage(productPropertyDo);
    }

    public Map<String, ProductPropertyListPo> addProductProperty(ProductReleaseEditDto dto){
        return productPropertyService.addProductProperty(dto);
    }

    public Map<String, ProductPropertyListPo> updateProductProperty(ProductReleaseEditDto dto){
        return productPropertyService.updateProductProperty(dto);
    }

}

