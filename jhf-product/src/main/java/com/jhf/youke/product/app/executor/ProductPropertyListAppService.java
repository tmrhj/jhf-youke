package com.jhf.youke.product.app.executor;

import com.jhf.youke.product.domain.converter.ProductPropertyListConverter;
import com.jhf.youke.product.domain.model.Do.ProductPropertyListDo;
import com.jhf.youke.product.domain.model.dto.ProductPropertyListDto;
import com.jhf.youke.product.domain.model.vo.ProductPropertyListVo;
import com.jhf.youke.product.domain.service.ProductPropertyListService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductPropertyListAppService {

    @Resource
    private ProductPropertyListService productPropertyListService;

    @Resource
    private ProductPropertyListConverter productPropertyListConverter;


    public boolean update(ProductPropertyListDto dto) {
        ProductPropertyListDo productPropertyListDo =  productPropertyListConverter.dto2Do(dto);
        return productPropertyListService.update(productPropertyListDo);
    }

    public boolean delete(ProductPropertyListDto dto) {
        ProductPropertyListDo productPropertyListDo =  productPropertyListConverter.dto2Do(dto);
        return productPropertyListService.delete(productPropertyListDo);
    }


    public boolean insert(ProductPropertyListDto dto) {
        ProductPropertyListDo productPropertyListDo =  productPropertyListConverter.dto2Do(dto);
        return productPropertyListService.insert(productPropertyListDo);
    }

    public Optional<ProductPropertyListVo> findById(Long id) {
        return productPropertyListService.findById(id);
    }


    public boolean remove(Long id) {
        return productPropertyListService.remove(id);
    }


    public List<ProductPropertyListVo> findAllMatching(ProductPropertyListDto dto) {
        ProductPropertyListDo productPropertyListDo =  productPropertyListConverter.dto2Do(dto);
        return productPropertyListService.findAllMatching(productPropertyListDo);
    }


    public Pagination<ProductPropertyListVo> selectPage(ProductPropertyListDto dto) {
        ProductPropertyListDo productPropertyListDo =  productPropertyListConverter.dto2Do(dto);
        return productPropertyListService.selectPage(productPropertyListDo);
    }

}

