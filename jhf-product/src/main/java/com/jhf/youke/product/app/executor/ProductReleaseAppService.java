package com.jhf.youke.product.app.executor;

import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.core.utils.Constant;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.app.feign.SalesFeign;
import com.jhf.youke.product.domain.converter.ProductReleaseConverter;
import com.jhf.youke.product.domain.model.Do.ProductCollectionDo;
import com.jhf.youke.product.domain.model.Do.ProductReleaseDo;
import com.jhf.youke.product.domain.model.dto.ProductPropertyDto;
import com.jhf.youke.product.domain.model.dto.ProductPropertyListDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.vo.*;
import com.jhf.youke.product.domain.service.ProductCollectionService;
import com.jhf.youke.product.domain.service.ProductReleaseFactory;
import com.jhf.youke.product.domain.service.ProductReleaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


/**
 * @author RHJ
 * **/
@Slf4j
@Service
public class ProductReleaseAppService {

    @Resource
    private ProductReleaseService productReleaseService;

    @Resource
    private ProductReleaseConverter productReleaseConverter;

    @Resource
    private ProductReleaseFactory productReleaseFactory;

    @Resource
    private ProductPropertyAppService productPropertyAppService;

    @Resource
    private ProductPropertyListAppService productPropertyListAppService;

    @Resource
    private ProductReleaseModelAppService productReleaseModelAppService;

    @Resource
    private ProductReleaseCommissionAppService productReleaseCommissionAppService;

    @Resource
    ProductReleaseImageAppService productReleaseImageAppService;

    @Resource
    ProductCollectionService productCollectionService;

    @Resource
    SalesFeign salesFeign;

    public boolean update(ProductReleaseDto dto) {
        ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
        return productReleaseService.update(productReleaseDo);
    }

    public boolean delete(ProductReleaseDto dto) {
        ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
        return productReleaseService.delete(productReleaseDo);
    }


    public boolean insert(ProductReleaseDto dto) {
        ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
        return productReleaseService.insert(productReleaseDo);
    }

    public boolean removeBatch(List<Long> idList) {
        return productReleaseService.removeBatch(idList);
    }

    public ProductReleaseInfoVo findById(String token, Long id) {
        ProductReleaseVo vo = instanceOf(id);
        ProductReleaseInfoVo infoVo =  productReleaseConverter.vo2InfoVo(vo);
        List<Map<String,Object>> gradeList = null;
        Map<String,Object> map = (Map<String,Object>) salesFeign.gradeList(token, new HashMap<>(8));
        if (map != null && Constant.RESPONSE_SUCCESS.equals(StringUtils.chgNull(map.get(Constant.RESPONSE_CODE)))) {
            gradeList = (List<Map<String,Object>>)map.get("data");
        }
        if(vo.getSpecType() == 1){
            //单规格
            for(ProductReleaseCommissionVo item:vo.getProductReleaseCommissionDos()){
                if(item.getPropertyId1() == null) {
                    //第一个规格是空，则为单规格分佣设置
                    GoodsCommissionVo commissionVo = new GoodsCommissionVo();
                    commissionVo.setId(item.getId());
                    commissionVo.setCommission(item.getCommission());
                    commissionVo.setPeersCommission(item.getPeersCommission());
                    commissionVo.setType(item.getType());
                    commissionVo.setLevel(item.getLevel());
                    infoVo.getSingleSpecCommissionList().add(commissionVo);
                }
            }
            infoVo.setSingleSpecCommissionList(addNewGradeCommission(gradeList, infoVo.getSingleSpecCommissionList()));
        }else {
            //多规格
            //规格选项
            for(ProductPropertyVo item:vo.getProductPropertyList()){
                GoodsSpecVo goodsSpecVo = new GoodsSpecVo();
                goodsSpecVo.setId(item.getId());
                goodsSpecVo.setName(item.getName());
                for(ProductPropertyListVo listItem:item.getList()){
                    GoodsSpecItemVo itemVo = new GoodsSpecItemVo();
                    itemVo.setId(listItem.getId());
                    itemVo.setName(listItem.getName());
                    goodsSpecVo.getItemList().add(itemVo);
                }
                infoVo.getMultipleSpecList().add(goodsSpecVo);
            }

            for(ProductReleaseModelVo item:vo.getProductReleaseModelDos()){
                MultipleSpecGoodsVo specGoodsVo = new MultipleSpecGoodsVo();
                specGoodsVo.setId(item.getId());
                if(item.getPropertyId1() != null) {
                    ProductPropertyListVo spec1 = productPropertyListAppService.findById(item.getPropertyId1()).get();
                    specGoodsVo.setSpecName1(spec1.getName());
                }
                if(item.getPropertyId2() != null) {
                    ProductPropertyListVo spec2 = productPropertyListAppService.findById(item.getPropertyId2()).get();
                    specGoodsVo.setSpecName2(spec2.getName());
                }
                if(item.getPropertyId3() != null) {
                    ProductPropertyListVo spec3 = productPropertyListAppService.findById(item.getPropertyId3()).get();
                    specGoodsVo.setSpecName3(spec3.getName());
                }
                specGoodsVo.setMarketPrice(item.getMarketPrice());
                specGoodsVo.setPrice(item.getPrice());
                specGoodsVo.setNum(item.getNum());

                for(ProductReleaseCommissionVo commissionItem:vo.getProductReleaseCommissionDos()){

                    if(StringUtils.chgNull(commissionItem.getPropertyId1()).equals(StringUtils.chgNull(item.getPropertyId1())) &&
                        StringUtils.chgNull(commissionItem.getPropertyId2()).equals(StringUtils.chgNull(item.getPropertyId2())) &&
                        StringUtils.chgNull(commissionItem.getPropertyId3()).equals(StringUtils.chgNull(item.getPropertyId3()))){
                        GoodsCommissionVo commissionVo = new GoodsCommissionVo();
                        commissionVo.setId(commissionItem.getId());
                        commissionVo.setCommission(commissionItem.getCommission());
                        commissionVo.setPeersCommission(commissionItem.getPeersCommission());
                        commissionVo.setType(commissionItem.getType());
                        commissionVo.setLevel(commissionItem.getLevel());
                        specGoodsVo.getCommissionList().add(commissionVo);
                    }
                }
                specGoodsVo.setCommissionList(addNewGradeCommission(gradeList, specGoodsVo.getCommissionList()));
                infoVo.getMultipleSpecGoodsList().add(specGoodsVo);
            }
        }
        for(ProductReleaseImageVo item:vo.getProductReleaseImageDos()){
            if(StringUtils.ifNull(infoVo.getImgs())) {
                infoVo.setImgs(item.getLarge());
            }else {
                infoVo.setImgs(infoVo.getImgs() + ","+ item.getLarge());
            }
        }
        return infoVo;
    }

    public List<GoodsCommissionVo> addNewGradeCommission(List<Map<String,Object>> gradeList, List<GoodsCommissionVo> commissionList) {

        if(gradeList != null && gradeList.size() > 0){
            for(Map<String,Object> grade:gradeList){
                //有新级别,需要添加进来
                Integer level = StringUtils.toInteger(grade.get("level"));
                String name = StringUtils.chgNull(grade.get("name"));
                boolean is = true;
                for(GoodsCommissionVo vo:commissionList){
                    if(vo.getLevel().equals(level)){
                        vo.setName(name);
                        is = false;
                        break;
                    }
                }
                if(is){
                    GoodsCommissionVo commissionVo = new GoodsCommissionVo();
                    commissionVo.setType(1);
                    commissionVo.setLevel(level);
                    commissionVo.setName(name);
                    commissionList.add(commissionVo);
                }
            }

            for (int i = 0; i < commissionList.size(); i++) {
                //商品有设置佣金的级别被删除了，不需要显示在前端
                Boolean is = true;
                for(Map<String,Object> grade:gradeList){
                    Integer level = StringUtils.toInteger(grade.get("level"));
                    if(commissionList.get(i).getLevel().equals(level)){
                        is = false;
                        break;
                    }
                }
                if(is) {
                    commissionList.remove(i);
                    i--;
                }
            }
        }
        return commissionList;
    }
    public boolean remove(Long id) {
        return productReleaseService.remove(id);
    }


    public List<ProductReleaseVo> findAllMatching(ProductReleaseDto dto) {
        ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
        return productReleaseService.findAllMatching(productReleaseDo);
    }


    public Pagination<ProductReleaseVo> selectPage(ProductReleaseDto dto) {
        ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
        return productReleaseService.selectPage(productReleaseDo);
    }

    public ProductReleaseVo instanceOf(Long id) {
        ProductReleaseVo vo = productReleaseFactory.instanceOf(id);
        ProductPropertyDto param = new ProductPropertyDto();
        param.setProductReleaseId(id);
        List<ProductPropertyVo> propertyList = productPropertyAppService.findAllMatching(param);
        for(ProductPropertyVo item:propertyList){
            ProductPropertyListDto itemParam = new ProductPropertyListDto();
            itemParam.setProductPropertyId(item.getId());
            item.setList(productPropertyListAppService.findAllMatching(itemParam));
        }
        vo.setProductPropertyList(propertyList);
        return vo;
    }

    public Boolean updateAll(ProductReleaseDto dto) {
        Boolean res = false;
        try{
            ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
            productReleaseFactory.updateAll(productReleaseDo);
            res = true;
        }catch (Exception e){
            log.info(e.getMessage());
        }
        return res;
    }


    public Boolean insertAll(ProductReleaseDto dto) {
        Boolean res = false;
        try{
            ProductReleaseDo productReleaseDo =  productReleaseConverter.dto2Do(dto);
            productReleaseFactory.insertAll(productReleaseDo);
            res = true;
        }catch (Exception e){
            log.info(e.getMessage());
        }
        return res;
    }

    public Boolean release(ProductReleaseDto dto){
        ProductReleaseDo productReleaseDo = productReleaseConverter.dto2Do(dto);
        return productReleaseService.release(productReleaseDo);
    }

    public Boolean soldOut(ProductReleaseDto dto){
        ProductReleaseDo productReleaseDo = productReleaseConverter.dto2Do(dto);
        return productReleaseService.soldOut(productReleaseDo);
    }

    public List<ProductReleaseCommissionVo> getProductReleaseCommission(Long id){
        return productReleaseService.getProductReleaseCommission(id);
    }

    public void addGoods(ProductReleaseEditDto dto){
        productReleaseService.saveGoods(dto);
        //保存多规格选项
        Map<String, ProductPropertyListPo> map = productPropertyAppService.addProductProperty(dto);
        //保存多规格商品
        productReleaseModelAppService.addProductReleaseModel(dto, map);
        //保存商品分佣设置
        productReleaseCommissionAppService.addProductReleaseCommission(dto, map);
        //添加轮播图
        productReleaseImageAppService.addProductReleaseImage(dto);
    }

    public void updateGoods(ProductReleaseEditDto dto) {
        productReleaseService.updateGoods(dto);
        //保存多规格选项
        Map<String, ProductPropertyListPo> map = productPropertyAppService.updateProductProperty(dto);
        //保存多规格商品
        productReleaseModelAppService.updateProductReleaseModel(dto, map);
        //保存商品分佣设置
        productReleaseCommissionAppService.updateProductReleaseCommission(dto, map);
        //保存轮播图
        productReleaseImageAppService.updateProductReleaseImage(dto);
    }


    public Pagination<ProductReleaseVo> getAllInfoList(ProductReleaseDto dto){
        return  productReleaseService.getAllInfoList(dto);
    }

    public ProductReleaseVo getVideo(String token, Long id) {
        ProductReleaseVo data = productReleaseService.findById(id).get();
        data.setIsCollection(0);
        if(!StringUtils.ifNull(token)) {
            User user = CacheUtils.getUser(token);
            ProductCollectionDo param = new ProductCollectionDo();
            param.setUserId(user.getId());
            param.setProductReleaseId(data.getId());
            param.setDelFlag("0");
            List<ProductCollectionVo> list = productCollectionService.findAllMatching(param);
            if(list.size() > 0){
                data.setIsCollection(1);
            }
        }
        return data;
    }

}

