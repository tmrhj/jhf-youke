package com.jhf.youke.product.app.executor;

import com.jhf.youke.product.domain.converter.ProductReleaseCommissionConverter;
import com.jhf.youke.product.domain.model.Do.ProductReleaseCommissionDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseCommissionDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import com.jhf.youke.product.domain.service.ProductReleaseCommissionService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;



/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductReleaseCommissionAppService {

    @Resource
    private ProductReleaseCommissionService productReleaseCommissionService;

    @Resource
    private ProductReleaseCommissionConverter productReleaseCommissionConverter;


    public boolean update(ProductReleaseCommissionDto dto) {
        ProductReleaseCommissionDo productReleaseCommissionDo =  productReleaseCommissionConverter.dto2Do(dto);
        return productReleaseCommissionService.update(productReleaseCommissionDo);
    }

    public boolean delete(ProductReleaseCommissionDto dto) {
        ProductReleaseCommissionDo productReleaseCommissionDo =  productReleaseCommissionConverter.dto2Do(dto);
        return productReleaseCommissionService.delete(productReleaseCommissionDo);
    }


    public boolean insert(ProductReleaseCommissionDto dto) {
        ProductReleaseCommissionDo productReleaseCommissionDo =  productReleaseCommissionConverter.dto2Do(dto);
        return productReleaseCommissionService.insert(productReleaseCommissionDo);
    }

    public Optional<ProductReleaseCommissionVo> findById(Long id) {
        return productReleaseCommissionService.findById(id);
    }


    public boolean remove(Long id) {
        return productReleaseCommissionService.remove(id);
    }


    public List<ProductReleaseCommissionVo> findAllMatching(ProductReleaseCommissionDto dto) {
        ProductReleaseCommissionDo productReleaseCommissionDo =  productReleaseCommissionConverter.dto2Do(dto);
        return productReleaseCommissionService.findAllMatching(productReleaseCommissionDo);
    }


    public Pagination<ProductReleaseCommissionVo> selectPage(ProductReleaseCommissionDto dto) {
        ProductReleaseCommissionDo productReleaseCommissionDo =  productReleaseCommissionConverter.dto2Do(dto);
        return productReleaseCommissionService.selectPage(productReleaseCommissionDo);
    }

    public void addProductReleaseCommission(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        productReleaseCommissionService.addProductReleaseCommission(dto, map);
    }

    public void updateProductReleaseCommission(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        productReleaseCommissionService.updateProductReleaseCommission(dto, map);
    }
}

