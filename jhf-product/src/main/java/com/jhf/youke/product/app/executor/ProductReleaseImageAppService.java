package com.jhf.youke.product.app.executor;

import com.jhf.youke.product.domain.converter.ProductReleaseImageConverter;
import com.jhf.youke.product.domain.model.Do.ProductReleaseImageDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseImageDto;
import com.jhf.youke.product.domain.model.vo.ProductReleaseImageVo;
import com.jhf.youke.product.domain.service.ProductReleaseImageService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;



/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductReleaseImageAppService {

    @Resource
    private ProductReleaseImageService productReleaseImageService;

    @Resource
    private ProductReleaseImageConverter productReleaseImageConverter;


    public boolean update(ProductReleaseImageDto dto) {
        ProductReleaseImageDo productReleaseImageDo =  productReleaseImageConverter.dto2Do(dto);
        return productReleaseImageService.update(productReleaseImageDo);
    }

    public boolean delete(ProductReleaseImageDto dto) {
        ProductReleaseImageDo productReleaseImageDo =  productReleaseImageConverter.dto2Do(dto);
        return productReleaseImageService.delete(productReleaseImageDo);
    }


    public boolean insert(ProductReleaseImageDto dto) {
        ProductReleaseImageDo productReleaseImageDo =  productReleaseImageConverter.dto2Do(dto);
        return productReleaseImageService.insert(productReleaseImageDo);
    }

    public Optional<ProductReleaseImageVo> findById(Long id) {
        return productReleaseImageService.findById(id);
    }


    public boolean remove(Long id) {
        return productReleaseImageService.remove(id);
    }


    public List<ProductReleaseImageVo> findAllMatching(ProductReleaseImageDto dto) {
        ProductReleaseImageDo productReleaseImageDo =  productReleaseImageConverter.dto2Do(dto);
        return productReleaseImageService.findAllMatching(productReleaseImageDo);
    }


    public Pagination<ProductReleaseImageVo> selectPage(ProductReleaseImageDto dto) {
        ProductReleaseImageDo productReleaseImageDo =  productReleaseImageConverter.dto2Do(dto);
        return productReleaseImageService.selectPage(productReleaseImageDo);
    }

    public void addProductReleaseImage(ProductReleaseEditDto dto){
        productReleaseImageService.addProductReleaseImage(dto);
    }

    public void updateProductReleaseImage(ProductReleaseEditDto dto){
        productReleaseImageService.updateProductReleaseImage(dto);
    }
}

