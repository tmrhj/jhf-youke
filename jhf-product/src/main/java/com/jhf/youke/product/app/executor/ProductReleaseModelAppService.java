package com.jhf.youke.product.app.executor;

import com.jhf.youke.product.domain.converter.ProductReleaseModelConverter;
import com.jhf.youke.product.domain.model.Do.ProductReleaseModelDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseModelDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseModelVo;
import com.jhf.youke.product.domain.service.ProductReleaseModelService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;



/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductReleaseModelAppService {

    @Resource
    private ProductReleaseModelService productReleaseModelService;

    @Resource
    private ProductReleaseModelConverter productReleaseModelConverter;


    public boolean update(ProductReleaseModelDto dto) {
        ProductReleaseModelDo productReleaseModelDo =  productReleaseModelConverter.dto2Do(dto);
        return productReleaseModelService.update(productReleaseModelDo);
    }

    public boolean delete(ProductReleaseModelDto dto) {
        ProductReleaseModelDo productReleaseModelDo =  productReleaseModelConverter.dto2Do(dto);
        return productReleaseModelService.delete(productReleaseModelDo);
    }


    public boolean insert(ProductReleaseModelDto dto) {
        ProductReleaseModelDo productReleaseModelDo =  productReleaseModelConverter.dto2Do(dto);
        return productReleaseModelService.insert(productReleaseModelDo);
    }

    public Optional<ProductReleaseModelVo> findById(Long id) {
        return productReleaseModelService.findById(id);
    }


    public boolean remove(Long id) {
        return productReleaseModelService.remove(id);
    }


    public List<ProductReleaseModelVo> findAllMatching(ProductReleaseModelDto dto) {
        ProductReleaseModelDo productReleaseModelDo =  productReleaseModelConverter.dto2Do(dto);
        return productReleaseModelService.findAllMatching(productReleaseModelDo);
    }


    public Pagination<ProductReleaseModelVo> selectPage(ProductReleaseModelDto dto) {
        ProductReleaseModelDo productReleaseModelDo =  productReleaseModelConverter.dto2Do(dto);
        return productReleaseModelService.selectPage(productReleaseModelDo);
    }

    public void addProductReleaseModel(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        productReleaseModelService.addProductReleaseModel(dto, map);
    }

    public void updateProductReleaseModel(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        productReleaseModelService.updateProductReleaseModel(dto, map);
    }

}

