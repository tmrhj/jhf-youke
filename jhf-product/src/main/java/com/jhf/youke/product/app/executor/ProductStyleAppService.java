package com.jhf.youke.product.app.executor;

import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.product.domain.converter.ProductStyleConverter;
import com.jhf.youke.product.domain.model.Do.ProductStyleDo;
import com.jhf.youke.product.domain.model.dto.ProductStyleDto;
import com.jhf.youke.product.domain.model.vo.ProductStyleVo;
import com.jhf.youke.product.domain.service.ProductStyleService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class ProductStyleAppService {

    @Resource
    private ProductStyleService productStyleService;

    @Resource
    private ProductStyleConverter productStyleConverter;


    public boolean update(ProductStyleDto dto) {
        ProductStyleDo productStyleDo =  productStyleConverter.dto2Do(dto);
        return productStyleService.update(productStyleDo);
    }

    public boolean delete(ProductStyleDto dto) {
        ProductStyleDo productStyleDo =  productStyleConverter.dto2Do(dto);
        return productStyleService.delete(productStyleDo);
    }


    public boolean insert(ProductStyleDto dto, String token) {
        User user = CacheUtils.getUser(token);
        ProductStyleDo productStyleDo =  productStyleConverter.dto2Do(dto);
        //用户rootId为分类的companyId
        productStyleDo.setCompanyId(user.getRootId());
        return productStyleService.insert(productStyleDo);
    }

    public Optional<ProductStyleVo> findById(Long id) {
        return productStyleService.findById(id);
    }


    public boolean remove(Long id) {
        return productStyleService.remove(id);
    }


    public List<ProductStyleVo> findAllMatching(ProductStyleDto dto) {
        ProductStyleDo productStyleDo =  productStyleConverter.dto2Do(dto);
        return productStyleService.findAllMatching(productStyleDo);
    }


    public Pagination<ProductStyleVo> selectPage(ProductStyleDto dto) {
        ProductStyleDo productStyleDo =  productStyleConverter.dto2Do(dto);
        return productStyleService.selectPage(productStyleDo);
    }

    public List<ProductStyleVo> getTreeByCompanyId(Long companyId) {
        return productStyleService.getTreeByCompanyId(companyId);
    }

}

