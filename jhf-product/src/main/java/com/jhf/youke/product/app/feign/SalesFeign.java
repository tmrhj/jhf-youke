package com.jhf.youke.product.app.feign;

import com.jhf.youke.product.app.feign.hystrix.SalesHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;


/**
 * 销售假装
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Component
@FeignClient(name = "jhf-sales-service", url = "${feign.salesUrl}", fallback = SalesHystrix.class)
public interface SalesFeign {


    /**
     * 等级列表
     *
     * @param token 令牌
     * @param map   地图
     * @return {@link Object}
     */
    @PostMapping(value="/sales/grade/list")
    Object gradeList(@RequestHeader("token") String token, @RequestBody Map<String, Object> map);

}
