package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductCollectionDo;
import com.jhf.youke.product.domain.model.dto.ProductCollectionDto;
import com.jhf.youke.product.domain.model.po.ProductCollectionPo;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  makejava
 **/

@Mapper(componentModel = "spring")
public interface ProductCollectionConverter extends BaseConverter<ProductCollectionDo,ProductCollectionPo,ProductCollectionDto,ProductCollectionVo> {


}

