package com.jhf.youke.product.domain.converter;


import com.jhf.youke.product.domain.model.Do.ProductDo;
import com.jhf.youke.product.domain.model.dto.ProductDto;
import com.jhf.youke.product.domain.model.po.ProductPo;
import com.jhf.youke.product.domain.model.vo.ProductVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;
import java.util.List;


/**
 * 产品转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productDto 产品dto
     * @return {@link ProductDo}
     */
    ProductDo dto2Do(ProductDto productDto);

    /**
     * 洗阿宝
     *
     * @param productDo 产品做
     * @return {@link ProductPo}
     */
    ProductPo do2Po(ProductDo productDo);

    /**
     * 洗签证官
     *
     * @param productDo 产品做
     * @return {@link ProductVo}
     */
    ProductVo do2Vo(ProductDo productDo);


    /**
     * 洗订单列表
     *
     * @param productDoList 产品做列表
     * @return {@link List}<{@link ProductPo}>
     */
    List<ProductPo> do2PoList(List<ProductDo> productDoList);

    /**
     * 警察乙做
     *
     * @param productPo 产品订单
     * @return {@link ProductDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductDo po2Do(ProductPo productPo);

    /**
     * 警察乙签证官
     *
     * @param productPo 产品订单
     * @return {@link ProductVo}
     */
    ProductVo po2Vo(ProductPo productPo);

    /**
     * 警察乙做列表
     *
     * @param productPoList 产品订单列表
     * @return {@link List}<{@link ProductDo}>
     */
    List<ProductDo> po2DoList(List<ProductPo> productPoList);

    /**
     * 警察乙vo列表
     *
     * @param productPoList 产品订单列表
     * @return {@link List}<{@link ProductVo}>
     */
    List<ProductVo> po2VoList(List<ProductPo> productPoList);


}

