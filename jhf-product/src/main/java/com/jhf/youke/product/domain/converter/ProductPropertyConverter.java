package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductPropertyDo;
import com.jhf.youke.product.domain.model.dto.ProductPropertyDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyPo;
import com.jhf.youke.product.domain.model.vo.ProductPropertyVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品属性转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductPropertyConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productPropertyDto 产品属性dto
     * @return {@link ProductPropertyDo}
     */
    ProductPropertyDo dto2Do(ProductPropertyDto productPropertyDto);

    /**
     * 洗阿宝
     *
     * @param productPropertyDo 产品属性做
     * @return {@link ProductPropertyPo}
     */
    ProductPropertyPo do2Po(ProductPropertyDo productPropertyDo);

    /**
     * 洗签证官
     *
     * @param productPropertyDo 产品属性做
     * @return {@link ProductPropertyVo}
     */
    ProductPropertyVo do2Vo(ProductPropertyDo productPropertyDo);


    /**
     * 洗订单列表
     *
     * @param productPropertyDoList 产品属性做列表
     * @return {@link List}<{@link ProductPropertyPo}>
     */
    List<ProductPropertyPo> do2PoList(List<ProductPropertyDo> productPropertyDoList);

    /**
     * 警察乙做
     *
     * @param productPropertyPo 产品属性阿宝
     * @return {@link ProductPropertyDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductPropertyDo po2Do(ProductPropertyPo productPropertyPo);

    /**
     * 警察乙签证官
     *
     * @param productPropertyPo 产品属性阿宝
     * @return {@link ProductPropertyVo}
     */
    ProductPropertyVo po2Vo(ProductPropertyPo productPropertyPo);

    /**
     * 警察乙做列表
     *
     * @param productPropertyPoList
     * @return {@link List}<{@link ProductPropertyDo}>
     */
    List<ProductPropertyDo> po2DoList(List<ProductPropertyPo> productPropertyPoList);

    /**
     * 警察乙vo列表
     *
     * @param productPropertyPoList 产品属性订单列表
     * @return {@link List}<{@link ProductPropertyVo}>
     */
    List<ProductPropertyVo> po2VoList(List<ProductPropertyPo> productPropertyPoList);


}

