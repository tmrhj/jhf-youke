package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductPropertyListDo;
import com.jhf.youke.product.domain.model.dto.ProductPropertyListDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.vo.ProductPropertyListVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品属性列表转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductPropertyListConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productPropertyListDto 产品属性列表dto
     * @return {@link ProductPropertyListDo}
     */
    ProductPropertyListDo dto2Do(ProductPropertyListDto productPropertyListDto);

    /**
     * 洗阿宝
     *
     * @param productPropertyListDo 产品属性列表做
     * @return {@link ProductPropertyListPo}
     */
    ProductPropertyListPo do2Po(ProductPropertyListDo productPropertyListDo);

    /**
     * 洗签证官
     *
     * @param productPropertyListDo 产品属性列表做
     * @return {@link ProductPropertyListVo}
     */
    ProductPropertyListVo do2Vo(ProductPropertyListDo productPropertyListDo);


    /**
     * 洗订单列表
     *
     * @param productPropertyListDoList
     * @return {@link List}<{@link ProductPropertyListPo}>
     */
    List<ProductPropertyListPo> do2PoList(List<ProductPropertyListDo> productPropertyListDoList);

    /**
     * 警察乙做
     *
     * @param productPropertyListPo 产品属性列表阿宝
     * @return {@link ProductPropertyListDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductPropertyListDo po2Do(ProductPropertyListPo productPropertyListPo);

    /**
     * 警察乙签证官
     *
     * @param productPropertyListPo 产品属性列表阿宝
     * @return {@link ProductPropertyListVo}
     */
    ProductPropertyListVo po2Vo(ProductPropertyListPo productPropertyListPo);

    /**
     * 警察乙做列表
     *
     * @param productPropertyListPoList 产品属性列表订单列表
     * @return {@link List}<{@link ProductPropertyListDo}>
     */
    List<ProductPropertyListDo> po2DoList(List<ProductPropertyListPo> productPropertyListPoList);

    /**
     * 警察乙vo列表
     *
     * @param productPropertyListPoList 产品属性列表订单列表
     * @return {@link List}<{@link ProductPropertyListVo}>
     */
    List<ProductPropertyListVo> po2VoList(List<ProductPropertyListPo> productPropertyListPoList);


}

