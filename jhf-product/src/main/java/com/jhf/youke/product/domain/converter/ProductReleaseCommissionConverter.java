package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductReleaseCommissionDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseCommissionDto;
import com.jhf.youke.product.domain.model.po.ProductReleaseCommissionPo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品发布委员会转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductReleaseCommissionConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productReleaseCommissionDto 产品发布委员会dto
     * @return {@link ProductReleaseCommissionDo}
     */
    ProductReleaseCommissionDo dto2Do(ProductReleaseCommissionDto productReleaseCommissionDto);

    /**
     * 洗阿宝
     *
     * @param productReleaseCommissionDo 产品发布委员会做
     * @return {@link ProductReleaseCommissionPo}
     */
    ProductReleaseCommissionPo do2Po(ProductReleaseCommissionDo productReleaseCommissionDo);

    /**
     * 洗签证官
     *
     * @param productReleaseCommissionDo 产品发布委员会做
     * @return {@link ProductReleaseCommissionVo}
     */
    ProductReleaseCommissionVo do2Vo(ProductReleaseCommissionDo productReleaseCommissionDo);


    /**
     * 洗订单列表
     *
     * @param productReleaseCommissionDoList 产品发布委员会名单
     * @return {@link List}<{@link ProductReleaseCommissionPo}>
     */
    List<ProductReleaseCommissionPo> do2PoList(List<ProductReleaseCommissionDo> productReleaseCommissionDoList);

    /**
     * 警察乙做
     *
     * @param productReleaseCommissionPo 产品发布委员会阿宝
     * @return {@link ProductReleaseCommissionDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductReleaseCommissionDo po2Do(ProductReleaseCommissionPo productReleaseCommissionPo);

    /**
     * 警察乙签证官
     *
     * @param productReleaseCommissionPo 产品发布委员会阿宝
     * @return {@link ProductReleaseCommissionVo}
     */
    ProductReleaseCommissionVo po2Vo(ProductReleaseCommissionPo productReleaseCommissionPo);

    /**
     * 警察乙做列表
     *
     * @param productReleaseCommissionPoList 产品发布委员会阿宝列表
     * @return {@link List}<{@link ProductReleaseCommissionDo}>
     */
    List<ProductReleaseCommissionDo> po2DoList(List<ProductReleaseCommissionPo> productReleaseCommissionPoList);

    /**
     * 警察乙vo列表
     *
     * @param productReleaseCommissionPoList 产品发布委员会阿宝列表
     * @return {@link List}<{@link ProductReleaseCommissionVo}>
     */
    List<ProductReleaseCommissionVo> po2VoList(List<ProductReleaseCommissionPo> productReleaseCommissionPoList);


}

