package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductReleaseDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseInfoVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品版本转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductReleaseConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productReleaseDto 产品发布dto
     * @return {@link ProductReleaseDo}
     */
    ProductReleaseDo dto2Do(ProductReleaseDto productReleaseDto);

    /**
     * 洗阿宝
     *
     * @param productReleaseDo
     * @return {@link ProductReleasePo}
     */
    ProductReleasePo do2Po(ProductReleaseDo productReleaseDo);

    /**
     * 洗签证官
     *
     * @param productReleaseDo 产品发布做
     * @return {@link ProductReleaseVo}
     */
    ProductReleaseVo do2Vo(ProductReleaseDo productReleaseDo);


    /**
     * 洗订单列表
     *
     * @param productReleaseDoList 产品发布做列表
     * @return {@link List}<{@link ProductReleasePo}>
     */
    List<ProductReleasePo> do2PoList(List<ProductReleaseDo> productReleaseDoList);

    /**
     * 警察乙做
     *
     * @param productReleasePo 产品发布订单
     * @return {@link ProductReleaseDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductReleaseDo po2Do(ProductReleasePo productReleasePo);

    /**
     * 警察乙签证官
     *
     * @param productReleasePo 产品发布订单
     * @return {@link ProductReleaseVo}
     */
    ProductReleaseVo po2Vo(ProductReleasePo productReleasePo);

    /**
     * 警察乙做列表
     *
     * @param productReleasePoList 产品发布订单列表
     * @return {@link List}<{@link ProductReleaseDo}>
     */
    List<ProductReleaseDo> po2DoList(List<ProductReleasePo> productReleasePoList);

    /**
     * 警察乙vo列表
     *
     * @param productReleasePoList 产品发布订单列表
     * @return {@link List}<{@link ProductReleaseVo}>
     */
    List<ProductReleaseVo> po2VoList(List<ProductReleasePo> productReleasePoList);

    /**
     * 编辑dto2做
     *
     * @param productReleaseEditDto 产品发布编辑dto
     * @return {@link ProductReleaseDo}
     */
    ProductReleaseDo editDto2Do(ProductReleaseEditDto productReleaseEditDto);

    /**
     * 最大信息签证官
     *
     * @param productReleaseVo 产品发布签证官
     * @return {@link ProductReleaseInfoVo}
     */
    ProductReleaseInfoVo vo2InfoVo(ProductReleaseVo productReleaseVo);
}

