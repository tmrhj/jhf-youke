package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductReleaseImageDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseImageDto;
import com.jhf.youke.product.domain.model.po.ProductReleaseImagePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseImageVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品发布图像转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductReleaseImageConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productReleaseImageDto 产品发布图像dto
     * @return {@link ProductReleaseImageDo}
     */
    ProductReleaseImageDo dto2Do(ProductReleaseImageDto productReleaseImageDto);

    /**
     * 洗阿宝
     *
     * @param productReleaseImageDo 产品发布图片做
     * @return {@link ProductReleaseImagePo}
     */
    ProductReleaseImagePo do2Po(ProductReleaseImageDo productReleaseImageDo);

    /**
     * 洗签证官
     *
     * @param productReleaseImageDo 产品发布图片做
     * @return {@link ProductReleaseImageVo}
     */
    ProductReleaseImageVo do2Vo(ProductReleaseImageDo productReleaseImageDo);


    /**
     * 洗订单列表
     *
     * @param productReleaseImageDoList 产品发布图像列表
     * @return {@link List}<{@link ProductReleaseImagePo}>
     */
    List<ProductReleaseImagePo> do2PoList(List<ProductReleaseImageDo> productReleaseImageDoList);

    /**
     * 警察乙做
     *
     * @param productReleaseImagePo 产品发布图像阿宝
     * @return {@link ProductReleaseImageDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductReleaseImageDo po2Do(ProductReleaseImagePo productReleaseImagePo);

    /**
     * 警察乙签证官
     *
     * @param productReleaseImagePo 产品发布图像阿宝
     * @return {@link ProductReleaseImageVo}
     */
    ProductReleaseImageVo po2Vo(ProductReleaseImagePo productReleaseImagePo);

    /**
     * 警察乙做列表
     *
     * @param productReleaseImagePoList 产品发布图像列表
     * @return {@link List}<{@link ProductReleaseImageDo}>
     */
    List<ProductReleaseImageDo> po2DoList(List<ProductReleaseImagePo> productReleaseImagePoList);

    /**
     * 警察乙vo列表
     *
     * @param productReleaseImagePoList 产品发布图像列表
     * @return {@link List}<{@link ProductReleaseImageVo}>
     */
    List<ProductReleaseImageVo> po2VoList(List<ProductReleaseImagePo> productReleaseImagePoList);


}

