package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductReleaseModelDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseModelDto;
import com.jhf.youke.product.domain.model.po.ProductReleaseModelPo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseModelVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品发布模式转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductReleaseModelConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productReleaseModelDto 产品发布模型dto
     * @return {@link ProductReleaseModelDo}
     */
    ProductReleaseModelDo dto2Do(ProductReleaseModelDto productReleaseModelDto);

    /**
     * 洗阿宝
     *
     * @param productReleaseModelDo
     * @return {@link ProductReleaseModelPo}
     */
    ProductReleaseModelPo do2Po(ProductReleaseModelDo productReleaseModelDo);

    /**
     * 洗签证官
     *
     * @param productReleaseModelDo 产品发布模式做
     * @return {@link ProductReleaseModelVo}
     */
    ProductReleaseModelVo do2Vo(ProductReleaseModelDo productReleaseModelDo);


    /**
     * 洗订单列表
     *
     * @param productReleaseModelDoList 产品发布模式列表
     * @return {@link List}<{@link ProductReleaseModelPo}>
     */
    List<ProductReleaseModelPo> do2PoList(List<ProductReleaseModelDo> productReleaseModelDoList);

    /**
     * 警察乙做
     *
     * @param productReleaseModelPo 产品发布模型阿宝
     * @return {@link ProductReleaseModelDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductReleaseModelDo po2Do(ProductReleaseModelPo productReleaseModelPo);

    /**
     * 警察乙签证官
     *
     * @param productReleaseModelPo 产品发布模型阿宝
     * @return {@link ProductReleaseModelVo}
     */
    ProductReleaseModelVo po2Vo(ProductReleaseModelPo productReleaseModelPo);

    /**
     * 警察乙做列表
     *
     * @param productReleaseModelPoList 产品发布模式列表
     * @return {@link List}<{@link ProductReleaseModelDo}>
     */
    List<ProductReleaseModelDo> po2DoList(List<ProductReleaseModelPo> productReleaseModelPoList);

    /**
     * 警察乙vo列表
     *
     * @param productReleaseModelPoList 产品发布模式列表
     * @return {@link List}<{@link ProductReleaseModelVo}>
     */
    List<ProductReleaseModelVo> po2VoList(List<ProductReleaseModelPo> productReleaseModelPoList);


}

