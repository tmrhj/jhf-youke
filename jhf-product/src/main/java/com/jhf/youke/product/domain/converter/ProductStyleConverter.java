package com.jhf.youke.product.domain.converter;

import com.jhf.youke.product.domain.model.Do.ProductStyleDo;
import com.jhf.youke.product.domain.model.dto.ProductStyleDto;
import com.jhf.youke.product.domain.model.po.ProductStylePo;
import com.jhf.youke.product.domain.model.vo.ProductStyleVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 产品风格转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface ProductStyleConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param productStyleDto 产品风格dto
     * @return {@link ProductStyleDo}
     */
    ProductStyleDo dto2Do(ProductStyleDto productStyleDto);

    /**
     * 洗阿宝
     *
     * @param productStyleDo 产品风格做
     * @return {@link ProductStylePo}
     */
    ProductStylePo do2Po(ProductStyleDo productStyleDo);

    /**
     * 洗签证官
     *
     * @param productStyleDo 产品风格做
     * @return {@link ProductStyleVo}
     */
    ProductStyleVo do2Vo(ProductStyleDo productStyleDo);


    /**
     * 洗订单列表
     *
     * @param productStyleDoList 产品风格做列表
     * @return {@link List}<{@link ProductStylePo}>
     */
    List<ProductStylePo> do2PoList(List<ProductStyleDo> productStyleDoList);

    /**
     * 警察乙做
     *
     * @param productStylePo 产品风格阿宝
     * @return {@link ProductStyleDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    ProductStyleDo po2Do(ProductStylePo productStylePo);

    /**
     * 警察乙签证官
     *
     * @param productStylePo 产品风格阿宝
     * @return {@link ProductStyleVo}
     */
    ProductStyleVo po2Vo(ProductStylePo productStylePo);

    /**
     * 警察乙做列表
     *
     * @param productStylePoList 产品风格订单列表
     * @return {@link List}<{@link ProductStyleDo}>
     */
    List<ProductStyleDo> po2DoList(List<ProductStylePo> productStylePoList);

    /**
     * 警察乙vo列表
     *
     * @param productStylePoList 产品风格订单列表
     * @return {@link List}<{@link ProductStyleVo}>
     */
    List<ProductStyleVo> po2VoList(List<ProductStylePo> productStylePoList);


}

