package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  makejava
 **/

public class ProductCollectionException extends DomainException {

    public ProductCollectionException(String message) { super(message); }
}

