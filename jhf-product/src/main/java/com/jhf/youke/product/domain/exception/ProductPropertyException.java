package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductPropertyException extends DomainException {

    public ProductPropertyException(String message) { super(message); }
}

