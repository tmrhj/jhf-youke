package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductPropertyListException extends DomainException {

    public ProductPropertyListException(String message) { super(message); }
}

