package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductReleaseCommissionException extends DomainException {

    public ProductReleaseCommissionException(String message) { super(message); }
}

