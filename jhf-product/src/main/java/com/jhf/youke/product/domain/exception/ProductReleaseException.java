package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductReleaseException extends DomainException {

    public ProductReleaseException(String message) { super(message); }
}

