package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductReleaseImageException extends DomainException {

    public ProductReleaseImageException(String message) { super(message); }
}

