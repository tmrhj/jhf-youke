package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductReleaseModelException extends DomainException {

    public ProductReleaseModelException(String message) { super(message); }
}

