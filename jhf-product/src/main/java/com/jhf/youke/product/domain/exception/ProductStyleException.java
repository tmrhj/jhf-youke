package com.jhf.youke.product.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class ProductStyleException extends DomainException {

    public ProductStyleException(String message) { super(message); }
}

