package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.model.po.ProductCollectionPo;
import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;

/**
 * @author  makejava
 **/
public interface ProductCollectionRepository extends Repository<ProductCollectionPo> {

    Pagination<ProductCollectionVo> getPageList(PageQuery<ProductCollectionPo> pageQuery);

}

