package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface ProductPropertyListRepository extends Repository<ProductPropertyListPo> {


}

