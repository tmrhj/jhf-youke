package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.product.domain.model.po.ProductPropertyPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface ProductPropertyRepository extends Repository<ProductPropertyPo> {


}

