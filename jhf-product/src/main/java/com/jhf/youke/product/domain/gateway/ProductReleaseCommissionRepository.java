package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.product.domain.model.po.ProductReleaseCommissionPo;
import java.util.List;


/**
 * 产品发布委员会库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ProductReleaseCommissionRepository extends Repository<ProductReleaseCommissionPo> {

      /**
       * 被发布列表
       *
       * @param id id
       * @return {@link List}<{@link ProductReleaseCommissionPo}>
       */
      List<ProductReleaseCommissionPo> getListByRelease(Long id);
}

