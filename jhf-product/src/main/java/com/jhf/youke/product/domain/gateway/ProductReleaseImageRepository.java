package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.product.domain.model.po.ProductReleaseImagePo;

import java.util.List;


/**
 * 产品发布图像库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ProductReleaseImageRepository extends Repository<ProductReleaseImagePo> {


    /**
     * 被发布列表
     *
     * @param id id
     * @return {@link List}<{@link ProductReleaseImagePo}>
     */
    List<ProductReleaseImagePo> getListByRelease(Long id);


}

