package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.product.domain.model.po.ProductReleaseModelPo;

import java.util.List;


/**
 * 产品发布模型库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ProductReleaseModelRepository extends Repository<ProductReleaseModelPo> {

    /**
     * 被发布列表
     *
     * @param id id
     * @return {@link List}<{@link ProductReleaseModelPo}>
     */
    List<ProductReleaseModelPo> getListByRelease(Long id);


}

