package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;


/**
 * 产品版本库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ProductReleaseRepository extends Repository<ProductReleasePo> {

    /**
     * 被产品样式列表
     *
     * @param productReleasePo 产品发布订单
     * @return {@link Pagination}<{@link ProductReleasePo}>
     */
    Pagination<ProductReleasePo> getListByProductStyle(ProductReleasePo productReleasePo);

    /**
     * 得到所有信息列表
     *
     * @param dto dto
     * @return {@link Pagination}<{@link ProductReleaseVo}>
     * @Description: 查询带详情的商品发布列表
     * @param: [dto]
     * @return: com.jhf.youke.core.entity.Pagination<com.jhf.youke.product.domain.model.vo.ProductReleaseVo>
     * @Author: RHJ
     * @Date: 2022/11/16
     */
    Pagination<ProductReleaseVo> getAllInfoList(ProductReleaseDto dto);

}

