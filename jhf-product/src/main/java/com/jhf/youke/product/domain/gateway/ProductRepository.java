package com.jhf.youke.product.domain.gateway;



import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.product.domain.model.po.ProductPo;

/**
 * 产品库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ProductRepository extends Repository<ProductPo> {

    /**
     * 得到名字
     *
     * @param name 名字
     * @return {@link ProductPo}
     */
    ProductPo getByName(String name);

}

