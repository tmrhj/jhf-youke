package com.jhf.youke.product.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.product.domain.model.po.ProductStylePo;

import java.util.List;

/**
 * 产品样式存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface ProductStyleRepository extends Repository<ProductStylePo> {

     /**
      * 被发布列表
      *
      * @param companyId 公司标识
      * @return {@link List}<{@link ProductStylePo}>
      */
     List<ProductStylePo> getListByRelease(Long companyId);
}

