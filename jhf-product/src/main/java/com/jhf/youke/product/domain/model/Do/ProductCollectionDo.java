package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.exception.ProductCollectionException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.model.po.ProductCollectionPo;
import lombok.Data;

import java.util.List;
import java.util.Objects;
import java.util.Date;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author  makejava
 **/

@Data
public class ProductCollectionDo extends BaseDoEntity {

    private static final long serialVersionUID = -67633090945360316L;

    /**
     * 商品发布ID
     */
    private Long productReleaseId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户昵称
     */
    private String userName;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductCollectionException(errorMessage);
        }
        return obj;
    }

    private ProductCollectionDo validateNull(ProductCollectionDo productCollectionDo){
          //可使用链式法则进行为空检查
          productCollectionDo.
          requireNonNull(productCollectionDo, productCollectionDo.getRemark(),"不能为NULL");

        return productCollectionDo;
    }

    public void requireUser(List<ProductCollectionPo> list) {
        if(list == null || list.size() == 0){
            throw new ProductCollectionException("收藏记录不存在");
        }
        if(!StringUtils.chgNull(list.get(0).getUserId()).equals(StringUtils.chgNull(this.userId))){
            throw new ProductCollectionException("只能删除自己的收藏记录");
        }
    }

}


