package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductException;
import lombok.Data;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductDo extends BaseDoEntity {

  private static final long serialVersionUID = -64090955013232618L;

    private String name;

    private String code;

    private Long companyId;

    private String companyName;

    private String productStyleName;

    private Long productStyleId;

    private Integer type;

    private Integer status;


    private <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductException(errorMessage);
        }
        return obj;
    }

    private ProductDo validateNull(ProductDo productDo){
          //可使用链式法则进行为空检查
          productDo.
          requireNonNull(productDo, productDo.getRemark(),"不能为NULL");
                
        return productDo;
    }


}


