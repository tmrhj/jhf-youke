package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductPropertyException;
import lombok.Data;

import java.util.List;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductPropertyDo extends BaseDoEntity {

  private static final long serialVersionUID = 136407874551249215L;

    /** 名字 **/
    private String name;

    /** 供应链标识 **/
    private Long companyId;

    /** 商品ID **/
    private Long productReleaseId;

    /** 状态 **/
    private Integer status;

    List<ProductPropertyListDo> productPropertyListDos;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductPropertyException(errorMessage);
        }
        return obj;
    }

    private ProductPropertyDo validateNull(ProductPropertyDo productPropertyDo){
          //可使用链式法则进行为空检查
          productPropertyDo.
          requireNonNull(productPropertyDo, productPropertyDo.getRemark(),"不能为NULL");

        return productPropertyDo;
    }

    public ProductPropertyDo setParentId(){
        requireNonNull(this, this.productPropertyListDos,"明细表不能为空");
        this.productPropertyListDos.forEach(
                productPropertyList-> productPropertyList.setProductPropertyId(this.id));
        return this;
    }


}


