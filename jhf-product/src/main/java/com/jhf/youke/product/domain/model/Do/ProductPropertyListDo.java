package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductPropertyListException;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductPropertyListDo extends BaseDoEntity {

  private static final long serialVersionUID = -88906572494501097L;

    /**主表ID **/
    private Long productPropertyId;

    /** 名字 */
    private String name;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductPropertyListException(errorMessage);
        }
        return obj;
    }

    private ProductPropertyListDo validateNull(ProductPropertyListDo productPropertyListDo){
          // 可使用链式法则进行为空检查
          productPropertyListDo.
             requireNonNull(productPropertyListDo, productPropertyListDo.getRemark(),"不能为NULL");
                
        return productPropertyListDo;
    }


}


