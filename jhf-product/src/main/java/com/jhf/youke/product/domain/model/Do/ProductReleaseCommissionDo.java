package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductReleaseCommissionException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductReleaseCommissionDo extends BaseDoEntity {

  private static final long serialVersionUID = -47563131351511790L;

    /** 商品发布标识 **/
    private Long productReleaseId;

    /** 等级 **/
    private Integer level;

    /** 佣金 **/
    private BigDecimal commission;

    /** 平级佣金 **/
    private BigDecimal peersCommission;

    /**1 按金额 2 按比率 **/
    private Integer type;

    /** 多规格1 **/
    private Long propertyId1;

    /** 多规格2 **/
    private Long propertyId2;

    /** 多规格3 **/
    private Long propertyId3;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductReleaseCommissionException(errorMessage);
        }
        return obj;
    }

    private ProductReleaseCommissionDo validateNull(ProductReleaseCommissionDo productReleaseCommissionDo){
          // 可使用链式法则进行为空检查
          productReleaseCommissionDo.
          requireNonNull(productReleaseCommissionDo, productReleaseCommissionDo.getRemark(),"不能为NULL");
                
        return productReleaseCommissionDo;
    }


}


