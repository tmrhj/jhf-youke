package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.exception.ProductReleaseException;
import com.jhf.youke.product.domain.model.dto.GoodsCommissionDto;
import com.jhf.youke.product.domain.model.dto.MultipleSpecGoodsDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.*;



/**
 * @author RHJ
 */
@Data
public class ProductReleaseDo extends BaseDoEntity {

    private static final long serialVersionUID = 645996227503209281L;

    /** 名字 **/
    private String name;

    /** 产品标识 **/
    private Long productId;

    /** 产品分类标识 **/
    private Long productStyleId;

    private Integer num;

    /** 供应链ID **/
    private Long companyId;

    /** 类型 1实体2服务 **/
    private Integer type;

    /** 市场价 **/
    private BigDecimal marketPrice;

    /** 价格 **/
    private BigDecimal price;

    /** 活动价 **/
    private BigDecimal activityPrice;

    /** 成本价 **/
    private BigDecimal costPrice;

    /** 产品介绍 **/
    private String introduction;

    /** 列表主图 **/
    private String listImg;

    /** 单规格 2 多规格 **/
    private Integer specType;

    /** 排序 **/
    private Integer sort;

    private Integer status;

    /** 产品分类名称 **/
    private String productStyleName;

    /** 视频地址 **/
    private String videoUrl;

    List<ProductReleaseCommissionDo> productReleaseCommissionDos;

    List<ProductReleaseImageDo> productReleaseImageDos;

    List<ProductReleaseModelDo> productReleaseModelDos;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductReleaseException(errorMessage);
        }
        return obj;
    }

    public void validateNotNull(ProductReleaseDo productReleaseDo) {
        productReleaseDo.requireNonNull(productReleaseDo, productReleaseDo.getName(),"商品名称不能为空")
                .requireNonNull(productReleaseDo,productReleaseDo.getProductId(), "请选择所属产品")
                .requireNonNull(productReleaseDo,productReleaseDo.getProductStyleId(), "请选择商品分类")
                .requireNonNull(productReleaseDo,productReleaseDo.getType(),"请选择类型")
                .requireNonNull(productReleaseDo,productReleaseDo.getListImg(),"请上传列表展示图片")
                .requireNonNull(productReleaseDo,productReleaseDo.getStatus(),"请选择状态")
                .requireNonNull(productReleaseDo,productReleaseDo.getSort(),"排序不能为空")
                .requireNonNull(productReleaseDo,productReleaseDo.getIntroduction(),"商品详情不能为空")
                .requireNonNull(productReleaseDo, productReleaseDo.getSpecType(),"规格类型不能为空");
        if(productReleaseDo.getSpecType() == 1){
            productReleaseDo.requireNonNull(productReleaseDo, productReleaseDo.getMarketPrice(),"请输入市场价")
                    .requireNonNull(productReleaseDo,productReleaseDo.getPrice(), "请输入购买价")
                    .requireNonNull(productReleaseDo,productReleaseDo.getProductStyleId(), "请选择商品分类")
                    .requireNonNull(productReleaseDo,productReleaseDo.getNum(),"请输入限购数量");
        }
    }

    public void validateSpecCommission(ProductReleaseEditDto dto) {
        if(dto.getSpecType() == 1){
            for(GoodsCommissionDto item:dto.getSingleSpecCommissionList()){
                if(item.getCommission() == null){
                    throw new ProductReleaseException("请输入级别["+item.getName() +"]的佣金");
                }
                if(item.getPeersCommission() == null){
                    throw new ProductReleaseException("请输入级别["+item.getName() +"]的平级佣金");
                }
            }
        }else {
            Map<String, List<MultipleSpecGoodsDto>> map = new HashMap<>(8);

            for(MultipleSpecGoodsDto item:dto.getMultipleSpecGoodsList()){

                String key = StringUtils.chgNull(item.getSpecName1()) +
                        StringUtils.chgNull(item.getSpecName2()) + StringUtils.chgNull(item.getSpecName3());
                List<MultipleSpecGoodsDto> list = map.get(key);
                if(list == null){
                    list = new ArrayList<>();
                }
                list.add(item);
                map.put(key, list);

                if(StringUtils.ifNull(item.getSpecName1())){
                    throw new ProductReleaseException("请选择商品多规格属性");
                }
                if(dto.getMultipleSpecList().size() >= 2 && StringUtils.ifNull(item.getSpecName2())){
                    throw new ProductReleaseException("请选择商品多规格属性");
                }
                if(dto.getMultipleSpecList().size() >= 3 && StringUtils.ifNull(item.getSpecName3())){
                    throw new ProductReleaseException("请选择商品多规格属性");
                }
                String specName = getSpecName(item);
                if(item.getMarketPrice() == null){
                    throw new ProductReleaseException("请输入规格为"+specName+"，的市场价");
                }
                if(item.getPrice() == null){
                    throw new ProductReleaseException("请输入规格为"+specName+"，的购买价");
                }
                if(item.getNum() == null){
                    throw new ProductReleaseException("请输入规格为"+specName+"，的限购数量");
                }
                for(GoodsCommissionDto commissionDto:item.getCommissionList()){
                    if(commissionDto.getCommission() == null){
                        throw new ProductReleaseException("请输入规格为"+specName+"，级别为["+commissionDto.getName() +"]的佣金");
                    }
                    if(commissionDto.getPeersCommission() == null){
                        throw new ProductReleaseException("请输入规格为"+specName+"，级别为["+commissionDto.getName() +"]的平级佣金");
                    }
                }
            }
            for (String key : map.keySet()) {
                List<MultipleSpecGoodsDto> list = map.get(key);
                if(list.size() > 1){

                    throw new ProductReleaseException("规格选项为" + getSpecName(list.get(0)) + "的商品，有重复");
                }
            }
        }

    }

    private String getSpecName(MultipleSpecGoodsDto dto){
        String specName = "";
        if(!StringUtils.ifNull(dto.getSpecName1())){
            specName = "[" + dto.getSpecName1() + "]";
        }
        if(!StringUtils.ifNull(dto.getSpecName2())){
            specName += "[" + dto.getSpecName2() + "]";
        }
        if(!StringUtils.ifNull(dto.getSpecName3())){
            specName += "[" + dto.getSpecName3() + "]";
        }
        return specName;
    }

    private ProductReleaseDo validateNull(ProductReleaseDo productReleaseDo){
          // 可使用链式法则进行为空检查
          productReleaseDo.
          requireNonNull(productReleaseDo, productReleaseDo.getRemark(),"不能为NULL");

        return productReleaseDo;
    }

    public ProductReleaseDo release(){
      this.status = 102;
      return this;
    }

    public ProductReleaseDo soldOut(){
      this.status = 101;
      return this;
    }

    public ProductReleaseDo setParentId(){
        if(this.getProductReleaseImageDos()!= null){
            this.getProductReleaseImageDos().forEach( img -> img.setProductReleaseId(this.id));
        }
        if(this.getProductReleaseModelDos()!= null){
            this.getProductReleaseModelDos().forEach( model -> model.setProductReleaseId(this.id));
        }
        if(this.getProductReleaseCommissionDos() != null){
            this.getProductReleaseCommissionDos().forEach( commission -> commission.setProductReleaseId(this.id));
        }
      return this;
    }

}


