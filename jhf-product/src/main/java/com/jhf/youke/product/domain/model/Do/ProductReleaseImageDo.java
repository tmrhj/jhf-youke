package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductReleaseImageException;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductReleaseImageDo extends BaseDoEntity {

  private static final long serialVersionUID = 164442922676200276L;

    /** 发布标识 **/
    private Long productReleaseId;

    /** 大图 **/
    private String large;

    /** 中图 **/
    private String medium;

    private Integer orders;

    private String source;

    /** 缩略图 **/
    private String thumbnail;

    /** 类型 **/
    private Integer type;

    private String title;

    private Integer isDefault;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductReleaseImageException(errorMessage);
        }
        return obj;
    }

    private ProductReleaseImageDo validateNull(ProductReleaseImageDo productReleaseImageDo){
          // 可使用链式法则进行为空检查
          productReleaseImageDo.
          requireNonNull(productReleaseImageDo, productReleaseImageDo.getRemark(),"不能为NULL");
                
        return productReleaseImageDo;
    }


}


