package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductReleaseModelException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductReleaseModelDo extends BaseDoEntity {

  private static final long serialVersionUID = -58448834571844812L;

    private Long productReleaseId;

    private Long propertyId1;

    private Long propertyId2;

    private Long propertyId3;

    private String propertyName1;

    private String propertyName2;

    private String propertyName3;

    private Integer num;

    /** 成本价 **/
    private BigDecimal costPrice;

    /** 市场价 **/
    private BigDecimal marketPrice;

    /** 价格 **/
    private BigDecimal price;

    /** 活动价 **/
    private BigDecimal activityPrice;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductReleaseModelException(errorMessage);
        }
        return obj;
    }

    private ProductReleaseModelDo validateNull(ProductReleaseModelDo productReleaseModelDo){
          //可使用链式法则进行为空检查
          productReleaseModelDo.
          requireNonNull(productReleaseModelDo, productReleaseModelDo.getRemark(),"不能为NULL");

        return productReleaseModelDo;
    }


}


