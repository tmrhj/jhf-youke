package com.jhf.youke.product.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.product.domain.exception.ProductStyleException;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class ProductStyleDo extends BaseDoEntity {

  private static final long serialVersionUID = 465843193747564056L;

    /** 编码 **/
    private String code;

    /** 名额 **/
    private String name;

    /** 类型 1 实物 2 服务 **/
    private Integer type;

    /** 单位标识 **/
    private Long companyId;

    /** 单位名 **/
    private String companyName;

    /** 上级ID **/
    private Long parentId;

    /** 上级树 **/
    private String parentIds;

    /** 展示图片 **/
    private String imgUrl;

    /** 排序 **/
    private Integer sort;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ProductStyleException(errorMessage);
        }
        return obj;
    }

    private ProductStyleDo validateNull(ProductStyleDo productStyleDo){
          // 可使用链式法则进行为空检查
          productStyleDo.
          requireNonNull(productStyleDo, productStyleDo.getRemark(),"不能为NULL");

        return productStyleDo;
    }


}


