package com.jhf.youke.product.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class GoodsCommissionDto extends BaseDtoEntity {

    private static final long serialVersionUID = -80468237834964896L;


    @ApiModelProperty(value = "级别名称")
    private String name;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "1按金额 2按比例")
    private Integer type;

    @ApiModelProperty(value = "佣金")
    private BigDecimal commission;

    @ApiModelProperty(value = "平级佣金")
    private BigDecimal peersCommission;

}


