package com.jhf.youke.product.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class GoodsSpecDto extends BaseDtoEntity {

    private static final long serialVersionUID = -80468237834964896L;



    @ApiModelProperty(value = "名字")
    private String name;

    List<GoodsSpecItemDto> itemList = new ArrayList<>();

}


