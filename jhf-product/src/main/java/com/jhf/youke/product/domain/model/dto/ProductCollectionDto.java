package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  makejava
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductCollectionDto extends BaseDtoEntity{

    private static final long serialVersionUID = 463477785011760968L;

    @ApiModelProperty(value = "商品发布ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "用户ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "用户昵称")
    private String userName;

}


