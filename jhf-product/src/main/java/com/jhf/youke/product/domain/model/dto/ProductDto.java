package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductDto extends BaseDtoEntity {

    private static final long serialVersionUID = -63353799937266892L;

    private String name;

    private String code;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productStyleId;

    private Integer type;

    private Integer status;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    private String companyName;

    private String productStyleName;


}


