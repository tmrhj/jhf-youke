package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.product.domain.model.Do.ProductPropertyListDo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductPropertyDto extends BaseDtoEntity {

    private static final long serialVersionUID = -33443580134943502L;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "供应链标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "商品ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "状态")
    private Integer status;


    List<ProductPropertyListDo> productPropertyListDos;


}


