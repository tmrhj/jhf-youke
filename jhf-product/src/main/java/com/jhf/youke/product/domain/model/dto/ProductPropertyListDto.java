package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductPropertyListDto extends BaseDtoEntity {

    private static final long serialVersionUID = 835230834133026217L;

    @ApiModelProperty(value = "主表ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productPropertyId;

    @ApiModelProperty(value = "名字")
    private String name;




}


