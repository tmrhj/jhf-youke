package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductReleaseCommissionDto extends BaseDtoEntity {

    private static final long serialVersionUID = 420906426718727145L;


    @ApiModelProperty(value = "商品发布标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "佣金")
    private BigDecimal commission;

    @ApiModelProperty(value = "平级佣金")
    private BigDecimal peersCommission;


    @ApiModelProperty(value = "1 按金额 2 按比率")
    private Integer type;

    @ApiModelProperty(value = "多规格1")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId1;

    @ApiModelProperty(value = "多规格2")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId2;

    @ApiModelProperty(value = "多规格3")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId3;

    @ApiModelProperty(value = "多规格1")
    private String propertyName1;

    @ApiModelProperty(value = "多规格2")
    private String propertyName2;

    @ApiModelProperty(value = "多规格3")
    private String propertyName3;

}


