package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductReleaseDto extends BaseDtoEntity {

    private static final long serialVersionUID = -11519332498695171L;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "产品标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;

    @ApiModelProperty(value = "产品分类标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productStyleId;

    @ApiModelProperty(value = "供应链ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "厂商ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long providerId;

    @ApiModelProperty(value = "类型 1实体2服务")
    private Integer type;

    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;


    @ApiModelProperty(value = "价格")
    private BigDecimal price;


    @ApiModelProperty(value = "活动价")
    private BigDecimal activityPrice;


    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;


    @ApiModelProperty(value = "产品介绍")
    private String introduction;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "1单规格 2 多规格")
    private Integer specType;

    @ApiModelProperty(value = "限购数量")
    private Integer num;

    @ApiModelProperty(value = "视频地址")
    private String videoUrl;

}


