package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductReleaseModelDto extends BaseDtoEntity {

    private static final long serialVersionUID = 888696381695243120L;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId1;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId2;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId3;

    private Integer num;

    @ApiModelProperty(value = "多规格1")
    private String propertyName1;

    @ApiModelProperty(value = "多规格2")
    private String propertyName2;

    @ApiModelProperty(value = "多规格3")
    private String propertyName3;

    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;


    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;


    @ApiModelProperty(value = "价格")
    private BigDecimal price;


    @ApiModelProperty(value = "活动价")
    private BigDecimal activityPrice;



}


