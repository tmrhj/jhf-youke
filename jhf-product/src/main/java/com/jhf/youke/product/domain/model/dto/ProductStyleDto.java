package com.jhf.youke.product.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductStyleDto extends BaseDtoEntity {

    private static final long serialVersionUID = 641105291685959809L;


    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "名额")
    private String name;

    @ApiModelProperty(value = "类型 1 实物 2 服务")
    private Integer type;

    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "单位名")
    private String companyName;

    @ApiModelProperty(value = "上级ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long parentId;

    @ApiModelProperty(value = "上级树")
    private String parentIds;

    @ApiModelProperty(value = "展示图片")
    private String imgUrl;

    @ApiModelProperty(value = "排序")
    private Integer sort;


}


