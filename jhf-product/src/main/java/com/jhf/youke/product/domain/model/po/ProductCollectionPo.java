package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  makejava
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_collection")
public class ProductCollectionPo extends BasePoEntity {

    private static final long serialVersionUID = 275184309408368817L;

    /**
     * 商品发布ID
     */
    private Long productReleaseId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户昵称
     */
    private String userName;



}


