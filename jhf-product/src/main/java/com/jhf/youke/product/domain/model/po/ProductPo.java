package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product")
public class ProductPo extends BasePoEntity {

    private static final long serialVersionUID = 467366464392830261L;

    private String name;

    private String code;

    private Long productStyleId;

    private Integer type;

    private Integer status;

    private String companyName;

    private String productStyleName;

    private Long companyId;



}


