package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_property_list")
public class ProductPropertyListPo extends BasePoEntity {

    private static final long serialVersionUID = 866773583314364105L;

    /** 主表ID **/
    private Long productPropertyId;

    /** 名字 **/
    private String name;



}


