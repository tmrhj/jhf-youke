package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_property")
public class ProductPropertyPo extends BasePoEntity {

    private static final long serialVersionUID = -27148518493222578L;

    /** 名字 **/
    private String name;

    /** 供应链标识 **/
    private Long companyId;

    /** 商品ID **/
    private Long productReleaseId;

    /** 状态 **/
    private Integer status;

    @TableField(exist=false)
    List<ProductPropertyListPo> productPropertyListPos;

}


