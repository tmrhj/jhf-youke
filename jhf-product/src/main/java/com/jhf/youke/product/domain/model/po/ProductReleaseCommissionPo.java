package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_release_commission")
public class ProductReleaseCommissionPo extends BasePoEntity {

    private static final long serialVersionUID = 795212722461098044L;

    /** 商品发布标识 **/
    private Long productReleaseId;

    /** 等级 **/
    private Integer level;

    /** 佣金 **/
    private BigDecimal commission;

    /** 平级佣金 **/
    private BigDecimal peersCommission;

    /** 1 按金额 2 按比率 **/
    private Integer type;

    /** 多规格1 **/
    private Long propertyId1;

    /** 多规格2 **/
    private Long propertyId2;

    /** 多规格3 **/
    private Long propertyId3;

    private String propertyName1;

    private String propertyName2;

    private String propertyName3;



}


