package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_release_image")
public class ProductReleaseImagePo extends BasePoEntity {

    private static final long serialVersionUID = -81316811982811942L;

    /** 发布标识 **/
    private Long productReleaseId;

    /**大图 **/
    private String large;

    /** 中图 **/
    private String medium;

    private Integer orders;

    private String source;

    /**缩略图 **/
    private String thumbnail;

    /**类型 **/
    private Integer type;

    private String title;

    private Integer isDefault;



}


