package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_release_model")
public class ProductReleaseModelPo extends BasePoEntity {

    private static final long serialVersionUID = 178068461010604141L;

    private Long productReleaseId;

    private Long propertyId1;

    private Long propertyId2;

    private Long propertyId3;

    private String propertyName1;

    private String propertyName2;

    private String propertyName3;

    private Integer num;

    /** 成本价 **/
    private BigDecimal costPrice;

    /** 市场价 **/
    private BigDecimal marketPrice;

    /** 价格 **/
    private BigDecimal price;

    /** 活动价 **/
    private BigDecimal activityPrice;



}


