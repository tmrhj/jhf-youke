package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_release")
public class ProductReleasePo extends BasePoEntity {

    private static final long serialVersionUID = -97179973806232637L;

    /** 名字 **/
    private String name;

    /** 产品标识 **/
    private Long productId;

    /** 产品分类标识 **/
    private Long productStyleId;

    /** 产品分类名称 **/
    @TableField(exist=false)
    private String productStyleName;

    /** 供应链ID **/
    private Long companyId;

    /** 类型 1实体2服务 **/
    private Integer type;

    /** 市场价 **/
    private BigDecimal marketPrice;

    /** 价格 **/
    private BigDecimal price;

    /** 活动价 **/
    private BigDecimal activityPrice;

    /** 成本价 **/
    private BigDecimal costPrice;

    /**  产品介绍 **/
    private String introduction;

    /** 排序 **/
    private Integer sort;

    private String listImg;

    /** 1单规格 2 多规格 **/
    private Integer specType;

    /** 状态 **/
    private Integer status;

    private Integer num;

    /**
     * 视频地址
     */
    private String videoUrl;

}


