package com.jhf.youke.product.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "prod_product_style")
public class ProductStylePo extends BasePoEntity {

    private static final long serialVersionUID = 345369918071300429L;

    /** 编码 **/
    private String code;

    /** 名额 **/
    private String name;

    /** 类型 1 实物 2 服务 **/
    private Integer type;

    /** 单位标识 **/
    private Long companyId;

    /** 单位名 **/
    private String companyName;

    /** 上级ID  **/
    private Long parentId;

    /** 上级树  **/
    private String parentIds;

    /** 展示图片  **/
    private String imgUrl;

    /** 排序 **/
    private Integer sort;

}


