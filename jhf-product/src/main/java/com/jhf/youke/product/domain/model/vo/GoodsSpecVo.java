package com.jhf.youke.product.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class GoodsSpecVo extends BaseVoEntity {

    private static final long serialVersionUID = -80468237834964896L;


    @ApiModelProperty(value = "名字")
    private String name;

    List<GoodsSpecItemVo> itemList = new ArrayList<>();

}


