package com.jhf.youke.product.domain.model.vo;

import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class MultipleSpecGoodsVo extends BaseVoEntity {

    private static final long serialVersionUID = -80468237834964896L;


    @ApiModelProperty(value = "规格1")
    private String specName1;

    @ApiModelProperty(value = "规格2")
    private String specName2;

    @ApiModelProperty(value = "规格3")
    private String specName3;

    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;

    @ApiModelProperty(value = "购买价")
    private BigDecimal price;

    @ApiModelProperty(value = "限购数量")
    private Integer num;

    List<GoodsCommissionVo> commissionList = new ArrayList<>();

}


