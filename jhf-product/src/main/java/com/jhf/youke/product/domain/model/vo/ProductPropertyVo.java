package com.jhf.youke.product.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductPropertyVo extends BaseVoEntity {

    private static final long serialVersionUID = -80468237834964896L;



    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "供应链标识")
    private Long companyId;

    @ApiModelProperty(value = "商品ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "状态")
    private Integer status;

    List<ProductPropertyListVo> list;
}


