package com.jhf.youke.product.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductReleaseImageVo extends BaseVoEntity {

    private static final long serialVersionUID = 519228678403910181L;


    @ApiModelProperty(value = "发布标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @ApiModelProperty(value = "大图")
    private String large;

    @ApiModelProperty(value = "中图")
    private String medium;

    private Integer orders;

    private String source;

    @ApiModelProperty(value = "缩略图")
    private String thumbnail;

    @ApiModelProperty(value = "类型")
    private Integer type;

    private String title;

    private Integer isDefault;


}


