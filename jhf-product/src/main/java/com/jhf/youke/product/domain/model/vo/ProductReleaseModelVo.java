package com.jhf.youke.product.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductReleaseModelVo extends BaseVoEntity {

    private static final long serialVersionUID = -23178869853875412L;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productReleaseId;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId1;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId2;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long propertyId3;

    @ApiModelProperty(value = "多规格名字1")
    private String propertyName1;

    @ApiModelProperty(value = "多规格名字2")
    private String propertyName2;

    @ApiModelProperty(value = "多规格名字3")
    private String propertyName3;

    private Integer num;

    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;

    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;

    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    @ApiModelProperty(value = "活动价")
    private BigDecimal activityPrice;


}


