package com.jhf.youke.product.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductReleaseVo extends BaseVoEntity {

    private static final long serialVersionUID = -85058269884074955L;



    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "产品标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;

    @ApiModelProperty(value = "产品分类标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productStyleId;

    @ApiModelProperty(value = "供应链ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "厂商ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long providerId;

    @ApiModelProperty(value = "类型 1实体2服务")
    private Integer type;

    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;

    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    @ApiModelProperty(value = "活动价")
    private BigDecimal activityPrice;

    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;

    @ApiModelProperty(value = "产品介绍")
    private String introduction;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "列表主图")
    private String listImg;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "1单规格 2 多规格")
    private Integer specType;

    @ApiModelProperty(value = "限购数量")
    private Integer num;

    /** 产品分类名称 **/
    private String productStyleName;

    @ApiModelProperty(value = "视频地址")
    private String videoUrl;

    //商品是否已收藏 0否 1是
    private Integer isCollection;

    List<ProductReleaseCommissionVo> productReleaseCommissionDos;

    List<ProductReleaseImageVo> productReleaseImageDos;

    List<ProductReleaseModelVo> productReleaseModelDos;

    List<ProductPropertyVo> productPropertyList;

}


