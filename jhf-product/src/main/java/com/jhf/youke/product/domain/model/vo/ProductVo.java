package com.jhf.youke.product.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductVo extends BaseVoEntity {

    private static final long serialVersionUID = 975810335763637030L;


    private String name;

    private String code;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productStyleId;

    private Integer type;

    private Integer status;

    private String companyName;

    private String productStyleName;

    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;


}


