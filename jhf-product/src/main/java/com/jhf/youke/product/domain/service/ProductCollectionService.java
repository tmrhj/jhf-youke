package com.jhf.youke.product.domain.service;

import com.jhf.youke.product.domain.converter.ProductCollectionConverter;
import com.jhf.youke.product.domain.model.Do.ProductCollectionDo;
import com.jhf.youke.product.domain.gateway.ProductCollectionRepository;
import com.jhf.youke.product.domain.model.po.ProductCollectionPo;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  makejava
 **/

@Service
public class ProductCollectionService extends AbstractDomainService<ProductCollectionRepository, ProductCollectionDo, ProductCollectionVo> {


    @Resource
    ProductCollectionConverter productCollectionConverter;

    @Override
    public boolean update(ProductCollectionDo entity) {
        ProductCollectionPo productCollectionPo = productCollectionConverter.do2Po(entity);
        productCollectionPo.preUpdate();
        return repository.update(productCollectionPo);
    }

    @Override
    public boolean updateBatch(List<ProductCollectionDo> doList) {
        List<ProductCollectionPo> poList = productCollectionConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductCollectionDo entity) {
        ProductCollectionPo param = new ProductCollectionPo();
        param.setProductReleaseId(entity.getProductReleaseId());
        param.setUserId(entity.getUserId());
        List<ProductCollectionPo> list = repository.findAllMatching(param);
        entity.requireUser(list);
        return repository.delete(list.get(0));
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductCollectionDo entity) {
        ProductCollectionPo productCollectionPo = productCollectionConverter.do2Po(entity);
        productCollectionPo.preInsert();
        return repository.insert(productCollectionPo);
    }

    @Override
    public boolean insertBatch(List<ProductCollectionDo> doList) {
        List<ProductCollectionPo> poList = productCollectionConverter.do2PoList(doList);
        ProductCollectionPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductCollectionVo> findById(Long id) {
        Optional<ProductCollectionPo> productCollectionPo =  repository.findById(id);
        ProductCollectionVo productCollectionVo = productCollectionConverter.po2Vo(productCollectionPo.orElse(new ProductCollectionPo()));
        return Optional.ofNullable(productCollectionVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductCollectionVo> findAllMatching(ProductCollectionDo entity) {
        ProductCollectionPo productCollectionPo = productCollectionConverter.do2Po(entity);
        List<ProductCollectionPo>productCollectionPoList =  repository.findAllMatching(productCollectionPo);
        return productCollectionConverter.po2VoList(productCollectionPoList);
    }


    @Override
    public Pagination<ProductCollectionVo> selectPage(ProductCollectionDo entity){
        ProductCollectionPo productCollectionPo = productCollectionConverter.do2Po(entity);
        PageQuery<ProductCollectionPo> pageQuery = new PageQuery<>(productCollectionPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductCollectionPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productCollectionConverter.po2VoList(pagination.getList()));
    }

    public Pagination<ProductCollectionVo> getPageList(ProductCollectionDo entity){
        ProductCollectionPo productCollectionPo = productCollectionConverter.do2Po(entity);
        PageQuery<ProductCollectionPo> pageQuery = new PageQuery<>(productCollectionPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductCollectionVo> pagination = repository.getPageList(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(), pagination.getList());
    }

}

