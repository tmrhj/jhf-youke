package com.jhf.youke.product.domain.service;

import com.jhf.youke.product.domain.converter.ProductPropertyListConverter;
import com.jhf.youke.product.domain.model.Do.ProductPropertyListDo;
import com.jhf.youke.product.domain.gateway.ProductPropertyListRepository;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.vo.ProductPropertyListVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class ProductPropertyListService extends AbstractDomainService<ProductPropertyListRepository, ProductPropertyListDo, ProductPropertyListVo> {


    @Resource
    ProductPropertyListConverter productPropertyListConverter;

    @Override
    public boolean update(ProductPropertyListDo entity) {
        ProductPropertyListPo productPropertyListPo = productPropertyListConverter.do2Po(entity);
        productPropertyListPo.preUpdate();
        return repository.update(productPropertyListPo);
    }

    @Override
    public boolean updateBatch(List<ProductPropertyListDo> doList) {
        List<ProductPropertyListPo> poList = productPropertyListConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductPropertyListDo entity) {
        ProductPropertyListPo productPropertyListPo = productPropertyListConverter.do2Po(entity);
        return repository.delete(productPropertyListPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductPropertyListDo entity) {
        ProductPropertyListPo productPropertyListPo = productPropertyListConverter.do2Po(entity);
        productPropertyListPo.preInsert();
        return repository.insert(productPropertyListPo);
    }

    @Override
    public boolean insertBatch(List<ProductPropertyListDo> doList) {
        List<ProductPropertyListPo> poList = productPropertyListConverter.do2PoList(doList);
        poList = ProductPropertyListPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductPropertyListVo> findById(Long id) {
        Optional<ProductPropertyListPo> productPropertyListPo =  repository.findById(id);
        ProductPropertyListVo productPropertyListVo = productPropertyListConverter.po2Vo(productPropertyListPo.orElse(new ProductPropertyListPo()));
        return Optional.ofNullable(productPropertyListVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductPropertyListVo> findAllMatching(ProductPropertyListDo entity) {
        ProductPropertyListPo productPropertyListPo = productPropertyListConverter.do2Po(entity);
        List<ProductPropertyListPo>productPropertyListPoList =  repository.findAllMatching(productPropertyListPo);
        return productPropertyListConverter.po2VoList(productPropertyListPoList);
    }


    @Override
    public Pagination<ProductPropertyListVo> selectPage(ProductPropertyListDo entity){
        ProductPropertyListPo productPropertyListPo = productPropertyListConverter.do2Po(entity);
        PageQuery<ProductPropertyListPo> pageQuery = new PageQuery<>(productPropertyListPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductPropertyListPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<ProductPropertyListVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productPropertyListConverter.po2VoList(pagination.getList()));
    }


}

