package com.jhf.youke.product.domain.service;

import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.converter.ProductReleaseCommissionConverter;
import com.jhf.youke.product.domain.model.Do.ProductReleaseCommissionDo;
import com.jhf.youke.product.domain.gateway.ProductReleaseCommissionRepository;
import com.jhf.youke.product.domain.model.dto.GoodsCommissionDto;
import com.jhf.youke.product.domain.model.dto.MultipleSpecGoodsDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.po.ProductReleaseCommissionPo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.*;


/**
 * @author RHJ
 */
@Service
public class ProductReleaseCommissionService extends AbstractDomainService<ProductReleaseCommissionRepository, ProductReleaseCommissionDo, ProductReleaseCommissionVo> {


    @Resource
    ProductReleaseCommissionConverter productReleaseCommissionConverter;

    @Override
    public boolean update(ProductReleaseCommissionDo entity) {
        ProductReleaseCommissionPo productReleaseCommissionPo = productReleaseCommissionConverter.do2Po(entity);
        productReleaseCommissionPo.preUpdate();
        return repository.update(productReleaseCommissionPo);
    }

    @Override
    public boolean updateBatch(List<ProductReleaseCommissionDo> doList) {
        List<ProductReleaseCommissionPo> poList = productReleaseCommissionConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductReleaseCommissionDo entity) {
        ProductReleaseCommissionPo productReleaseCommissionPo = productReleaseCommissionConverter.do2Po(entity);
        return repository.delete(productReleaseCommissionPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductReleaseCommissionDo entity) {
        ProductReleaseCommissionPo productReleaseCommissionPo = productReleaseCommissionConverter.do2Po(entity);
        productReleaseCommissionPo.preInsert();
        return repository.insert(productReleaseCommissionPo);
    }

    @Override
    public boolean insertBatch(List<ProductReleaseCommissionDo> doList) {
        List<ProductReleaseCommissionPo> poList = productReleaseCommissionConverter.do2PoList(doList);
        poList = ProductReleaseCommissionPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductReleaseCommissionVo> findById(Long id) {
        Optional<ProductReleaseCommissionPo> productReleaseCommissionPo =  repository.findById(id);
        ProductReleaseCommissionVo productReleaseCommissionVo = productReleaseCommissionConverter.po2Vo(productReleaseCommissionPo.orElse(new ProductReleaseCommissionPo()));
        return Optional.ofNullable(productReleaseCommissionVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductReleaseCommissionVo> findAllMatching(ProductReleaseCommissionDo entity) {
        ProductReleaseCommissionPo productReleaseCommissionPo = productReleaseCommissionConverter.do2Po(entity);
        List<ProductReleaseCommissionPo>productReleaseCommissionPoList =  repository.findAllMatching(productReleaseCommissionPo);
        return productReleaseCommissionConverter.po2VoList(productReleaseCommissionPoList);
    }

    @Override
    public Pagination<ProductReleaseCommissionVo> selectPage(ProductReleaseCommissionDo entity){
        ProductReleaseCommissionPo productReleaseCommissionPo = productReleaseCommissionConverter.do2Po(entity);
        PageQuery<ProductReleaseCommissionPo> pageQuery = new PageQuery<>(productReleaseCommissionPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductReleaseCommissionPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productReleaseCommissionConverter.po2VoList(pagination.getList()));
    }


    public void addProductReleaseCommission(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        if(dto.getSpecType() == 1) {
            //单规格
            for (GoodsCommissionDto commissionDto : dto.getSingleSpecCommissionList()) {
                ProductReleaseCommissionPo po = new ProductReleaseCommissionPo();
                po.setProductReleaseId(dto.getId());
                po.setLevel(commissionDto.getLevel());
                po.setCommission(commissionDto.getCommission());
                po.setPeersCommission(commissionDto.getPeersCommission());
                po.setType(commissionDto.getType());
                po.preInsert();
                repository.insert(po);
            }
        }else {
            //多规格
            for (MultipleSpecGoodsDto item : dto.getMultipleSpecGoodsList()) {
                Long propertyId1 = null;
                Long propertyId2 = null;
                Long propertyId3 = null;

                String propertyName1 = null;
                String propertyName2 = null;
                String propertyName3 = null;

                if (!StringUtils.ifNull(item.getSpecName1())) {
                    ProductPropertyListPo poLlist = map.get(item.getSpecName1());
                    if(poLlist != null) {
                        propertyId1 = poLlist.getId();
                        propertyName1 = poLlist.getName();
                    }
                }
                if (!StringUtils.ifNull(item.getSpecName2())) {
                    ProductPropertyListPo poLlist = map.get(item.getSpecName2());
                    if(poLlist != null) {
                        propertyId2 = poLlist.getId();
                        propertyName2 = poLlist.getName();
                    }
                }
                if (!StringUtils.ifNull(item.getSpecName3())) {
                    ProductPropertyListPo poLlist = map.get(item.getSpecName3());
                    if(poLlist != null) {
                        propertyId3 = poLlist.getId();
                        propertyName3 = poLlist.getName();
                    }
                }
                for (GoodsCommissionDto commissionDto : item.getCommissionList()) {
                    ProductReleaseCommissionPo po = new ProductReleaseCommissionPo();
                    po.setPropertyId1(propertyId1);
                    po.setPropertyId2(propertyId2);
                    po.setPropertyId3(propertyId3);
                    po.setPropertyName1(propertyName1);
                    po.setPropertyName2(propertyName2);
                    po.setPropertyName3(propertyName3);
                    po.setProductReleaseId(dto.getId());
                    po.setLevel(commissionDto.getLevel());
                    po.setCommission(commissionDto.getCommission());
                    po.setPeersCommission(commissionDto.getPeersCommission());
                    po.setType(commissionDto.getType());
                    po.preInsert();
                    repository.insert(po);
                }
            }
        }
    }

    public void updateProductReleaseCommission(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        ProductReleaseCommissionPo param = new ProductReleaseCommissionPo();
        param.setProductReleaseId(dto.getId());
        List<ProductReleaseCommissionPo> commissionPos = repository.findAllMatching(param);

        List<Long> delList = new ArrayList<>();

        Map<Long, ProductReleaseCommissionPo> commissionMap = new HashMap<>(32);

        if(dto.getSpecType() == 1) {
            //单规格
            for (GoodsCommissionDto commissionDto : dto.getSingleSpecCommissionList()) {
                ProductReleaseCommissionPo po = new ProductReleaseCommissionPo();
                po.setLevel(commissionDto.getLevel());
                po.setCommission(commissionDto.getCommission());
                po.setPeersCommission(commissionDto.getPeersCommission());
                po.setType(commissionDto.getType());
                if(commissionDto.getId() == null){
                    po.setProductReleaseId(dto.getId());
                    po.preInsert();
                    repository.insert(po);
                }else {
                    po.setId(commissionDto.getId());
                    po.preUpdate();
                    repository.update(po);
                }
                commissionMap.put(po.getId(), po);
            }

        }else {
            //多规格
            for (MultipleSpecGoodsDto item : dto.getMultipleSpecGoodsList()) {
                Long propertyId1 = null;
                Long propertyId2 = null;
                Long propertyId3 = null;

                String propertyName1 = null;
                String propertyName2 = null;
                String propertyName3 = null;

                if (!StringUtils.ifNull(item.getSpecName1())) {
                    ProductPropertyListPo poLlist = map.get(item.getSpecName1());
                    if(poLlist != null) {
                        propertyId1 = poLlist.getId();
                        propertyName1 = poLlist.getName();
                    }
                }
                if (!StringUtils.ifNull(item.getSpecName2())) {
                    ProductPropertyListPo poLlist = map.get(item.getSpecName2());
                    if(poLlist != null) {
                        propertyId2 = poLlist.getId();
                        propertyName2 = poLlist.getName();
                    }
                }
                if (!StringUtils.ifNull(item.getSpecName3())) {
                    ProductPropertyListPo poLlist = map.get(item.getSpecName3());
                    if(poLlist != null) {
                        propertyId3 = poLlist.getId();
                        propertyName3 = poLlist.getName();
                    }
                }
                for (GoodsCommissionDto commissionDto : item.getCommissionList()) {
                    ProductReleaseCommissionPo po = new ProductReleaseCommissionPo();
                    po.setPropertyId1(propertyId1);
                    po.setPropertyId2(propertyId2);
                    po.setPropertyId3(propertyId3);
                    po.setPropertyName1(propertyName1);
                    po.setPropertyName2(propertyName2);
                    po.setPropertyName3(propertyName3);
                    po.setLevel(commissionDto.getLevel());
                    po.setCommission(commissionDto.getCommission());
                    po.setPeersCommission(commissionDto.getPeersCommission());
                    po.setType(commissionDto.getType());
                    if(commissionDto.getId() == null) {
                        po.setProductReleaseId(dto.getId());
                        po.preInsert();
                        repository.insert(po);
                    }else {
                        po.setId(commissionDto.getId());
                        po.preUpdate();
                        repository.update(po);
                    }
                    commissionMap.put(po.getId(), po);
                }
            }
        }
        for(ProductReleaseCommissionPo item:commissionPos){
            ProductReleaseCommissionPo po = commissionMap.get(item.getId());
            if(po == null){
                delList.add(item.getId());
            }
        }
        if(delList.size() > 0){
            repository.deleteBatch(delList);
        }
    }
}

