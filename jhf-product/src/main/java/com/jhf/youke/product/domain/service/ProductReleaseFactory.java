package com.jhf.youke.product.domain.service;

import com.jhf.youke.product.domain.converter.ProductReleaseCommissionConverter;
import com.jhf.youke.product.domain.converter.ProductReleaseConverter;
import com.jhf.youke.product.domain.converter.ProductReleaseImageConverter;
import com.jhf.youke.product.domain.converter.ProductReleaseModelConverter;
import com.jhf.youke.product.domain.exception.ProductReleaseException;
import com.jhf.youke.product.domain.gateway.ProductReleaseCommissionRepository;
import com.jhf.youke.product.domain.gateway.ProductReleaseImageRepository;
import com.jhf.youke.product.domain.gateway.ProductReleaseModelRepository;
import com.jhf.youke.product.domain.gateway.ProductReleaseRepository;
import com.jhf.youke.product.domain.model.Do.ProductReleaseDo;
import com.jhf.youke.product.domain.model.po.ProductReleaseCommissionPo;
import com.jhf.youke.product.domain.model.po.ProductReleaseImagePo;
import com.jhf.youke.product.domain.model.po.ProductReleaseModelPo;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseImageVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseModelVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Component
public class ProductReleaseFactory {

    @Resource
    private ProductReleaseRepository productReleaseRepository;

    @Resource
    private ProductReleaseModelRepository productReleaseModelRepository;

    @Resource
    private ProductReleaseCommissionRepository productReleaseCommissionRepository;

    @Resource
    private ProductReleaseImageRepository productReleaseImageRepository;

    @Resource
    private ProductReleaseConverter productReleaseConverter;

    @Resource
    private ProductReleaseImageConverter productReleaseImageConverter;

    @Resource
    private ProductReleaseCommissionConverter productReleaseCommissionConverter;

    @Resource
    private ProductReleaseModelConverter productReleaseModelConverter;


    public ProductReleaseVo instanceOf(Long productReleaseId) {

        Optional<ProductReleasePo> productRelease = productReleaseRepository.findById(productReleaseId);

        if(productRelease.isEmpty()){
           throw new ProductReleaseException("ID找不到对应实例");
        }

        ProductReleaseVo productReleaseVo = productReleaseConverter.po2Vo(productRelease.get());

        List<ProductReleaseImagePo> productReleaseImages = productReleaseImageRepository.getListByRelease(productReleaseId);

        List<ProductReleaseModelPo> productReleaseModels = productReleaseModelRepository.getListByRelease(productReleaseId);

        List<ProductReleaseCommissionPo> productReleaseCommissions = productReleaseCommissionRepository.getListByRelease(productReleaseId);

        List<ProductReleaseImageVo> productReleaseImageDoList = productReleaseImageConverter.po2VoList(productReleaseImages);

        List<ProductReleaseModelVo> productReleaseModelDoList = productReleaseModelConverter.po2VoList(productReleaseModels);

        List<ProductReleaseCommissionVo> productReleaseCommissionDoList = productReleaseCommissionConverter.po2VoList(productReleaseCommissions);

        productReleaseVo.setProductReleaseImageDos(productReleaseImageDoList);
        productReleaseVo.setProductReleaseModelDos(productReleaseModelDoList);
        productReleaseVo.setProductReleaseCommissionDos(productReleaseCommissionDoList);

       return productReleaseVo;
    }

    /**
    * @Description: 全表单插入
    * @Param: [entity]
    * @return: void
    * @Author: RHJ
    * @Date: 2022/9/29
    */
    public void insertAll(ProductReleaseDo entity) {

        entity.setParentId();
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        productReleasePo.preInsert();
        productReleaseRepository.insert(productReleasePo);

        List<ProductReleaseImagePo> productReleaseImages = productReleaseImageConverter.do2PoList(entity.getProductReleaseImageDos()); ;

        List<ProductReleaseModelPo> productReleaseModels = productReleaseModelConverter.do2PoList(entity.getProductReleaseModelDos());

        List<ProductReleaseCommissionPo> productReleaseCommissions = productReleaseCommissionConverter.do2PoList(entity.getProductReleaseCommissionDos());

        productReleaseImageRepository.insertBatch(productReleaseImages);

        productReleaseModelRepository.insertBatch(productReleaseModels);

        productReleaseCommissionRepository.insertBatch(productReleaseCommissions);


    }

    /**
    * @Description:  全表单更新
    * @Param: [entity]
    * @return: void
    * @Author: RHJ
    * @Date: 2022/9/29
    */
    public void updateAll(ProductReleaseDo entity) {

        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);

        productReleaseRepository.update(productReleasePo);

        List<ProductReleaseImagePo> productReleaseImages = productReleaseImageConverter.do2PoList(entity.getProductReleaseImageDos()); ;

        List<ProductReleaseModelPo> productReleaseModels = productReleaseModelConverter.do2PoList(entity.getProductReleaseModelDos());

        List<ProductReleaseCommissionPo> productReleaseCommissions = productReleaseCommissionConverter.do2PoList(entity.getProductReleaseCommissionDos());

        productReleaseImageRepository.updateBatch(productReleaseImages);

        productReleaseModelRepository.updateBatch(productReleaseModels);

        productReleaseCommissionRepository.updateBatch(productReleaseCommissions);

    }


}
