package com.jhf.youke.product.domain.service;

import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.converter.ProductReleaseImageConverter;
import com.jhf.youke.product.domain.exception.ProductReleaseImageException;
import com.jhf.youke.product.domain.model.Do.ProductReleaseImageDo;
import com.jhf.youke.product.domain.gateway.ProductReleaseImageRepository;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductReleaseImagePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseImageVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.*;


/**
 * @author RHJ
 */
@Service
public class ProductReleaseImageService extends AbstractDomainService<ProductReleaseImageRepository, ProductReleaseImageDo, ProductReleaseImageVo> {


    @Resource
    ProductReleaseImageConverter productReleaseImageConverter;

    @Override
    public boolean update(ProductReleaseImageDo entity) {
        ProductReleaseImagePo productReleaseImagePo = productReleaseImageConverter.do2Po(entity);
        productReleaseImagePo.preUpdate();
        return repository.update(productReleaseImagePo);
    }

    @Override
    public boolean updateBatch(List<ProductReleaseImageDo> doList) {
        List<ProductReleaseImagePo> poList = productReleaseImageConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductReleaseImageDo entity) {
        ProductReleaseImagePo productReleaseImagePo = productReleaseImageConverter.do2Po(entity);
        return repository.delete(productReleaseImagePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductReleaseImageDo entity) {
        ProductReleaseImagePo productReleaseImagePo = productReleaseImageConverter.do2Po(entity);
        productReleaseImagePo.preInsert();
        return repository.insert(productReleaseImagePo);
    }

    @Override
    public boolean insertBatch(List<ProductReleaseImageDo> doList) {
        List<ProductReleaseImagePo> poList = productReleaseImageConverter.do2PoList(doList);
        poList = ProductReleaseImagePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductReleaseImageVo> findById(Long id) {
        Optional<ProductReleaseImagePo> productReleaseImagePo =  repository.findById(id);
        ProductReleaseImageVo productReleaseImageVo = productReleaseImageConverter.po2Vo(productReleaseImagePo.orElse(new ProductReleaseImagePo()));
        return Optional.ofNullable(productReleaseImageVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductReleaseImageVo> findAllMatching(ProductReleaseImageDo entity) {
        ProductReleaseImagePo productReleaseImagePo = productReleaseImageConverter.do2Po(entity);
        List<ProductReleaseImagePo>productReleaseImagePoList =  repository.findAllMatching(productReleaseImagePo);
        return productReleaseImageConverter.po2VoList(productReleaseImagePoList);
    }


    @Override
    public Pagination<ProductReleaseImageVo> selectPage(ProductReleaseImageDo entity){
        ProductReleaseImagePo productReleaseImagePo = productReleaseImageConverter.do2Po(entity);
        PageQuery<ProductReleaseImagePo> pageQuery = new PageQuery<>(productReleaseImagePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductReleaseImagePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<ProductReleaseImageVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productReleaseImageConverter.po2VoList(pagination.getList()));
    }

    public void addProductReleaseImage(ProductReleaseEditDto dto){
        if(StringUtils.ifNull(dto.getImgs())){
            throw new ProductReleaseImageException("轮播图不能为空");
        }
        String[] imgs = dto.getImgs().split(",");
        int sort = 1;
        for(String img:imgs){
            ProductReleaseImagePo po = new ProductReleaseImagePo();
            po.setProductReleaseId(dto.getId());
            po.setLarge(img);
            po.setMedium(img);
            po.setThumbnail(img);
            po.setOrders(sort);
            po.setType(2);
            repository.insert(po);
            sort ++;
        }
    }

    public void updateProductReleaseImage(ProductReleaseEditDto dto){
        if(StringUtils.ifNull(dto.getImgs())){
            throw new ProductReleaseImageException("轮播图不能为空");
        }
        String[] imgs = dto.getImgs().split(",");

        ProductReleaseImagePo param = new ProductReleaseImagePo();
        param.setProductReleaseId(dto.getId());
        List<ProductReleaseImagePo> imgList =  repository.findAllMatching(param);

        List<Long> delList = new ArrayList<>();

        Map<String, ProductReleaseImagePo> map = new HashMap<>(32);
        for(ProductReleaseImagePo item:imgList){
            map.put(item.getLarge(), item);
            Boolean isDel = true;
            for(String img:imgs){
                if(item.getLarge().equals(img)){
                    isDel = false;
                    break;
                }
            }
            if(isDel) {
                delList.add(item.getId());
            }
        }

        int sort = 1;
        for(String img:imgs){
            ProductReleaseImagePo poMap = map.get(img);
            if(poMap == null){
                ProductReleaseImagePo po = new ProductReleaseImagePo();
                po.setProductReleaseId(dto.getId());
                po.setLarge(img);
                po.setMedium(img);
                po.setThumbnail(img);
                po.setOrders(sort);
                po.setType(2);
                repository.insert(po);
            }else {
                if(poMap.getOrders() != sort) {
                    poMap.setOrders(sort);
                    repository.update(poMap);
                }
            }
            sort ++;
        }
        if(delList.size() > 0){
            repository.deleteBatch(delList);
        }
    }

}

