package com.jhf.youke.product.domain.service;

import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.converter.ProductReleaseModelConverter;
import com.jhf.youke.product.domain.model.Do.ProductReleaseModelDo;
import com.jhf.youke.product.domain.gateway.ProductReleaseModelRepository;
import com.jhf.youke.product.domain.model.dto.MultipleSpecGoodsDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductPropertyListPo;
import com.jhf.youke.product.domain.model.po.ProductReleaseModelPo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseModelVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class ProductReleaseModelService extends AbstractDomainService<ProductReleaseModelRepository, ProductReleaseModelDo, ProductReleaseModelVo> {


    @Resource
    ProductReleaseModelConverter productReleaseModelConverter;

    @Override
    public boolean update(ProductReleaseModelDo entity) {
        ProductReleaseModelPo productReleaseModelPo = productReleaseModelConverter.do2Po(entity);
        productReleaseModelPo.preUpdate();
        return repository.update(productReleaseModelPo);
    }

    @Override
    public boolean updateBatch(List<ProductReleaseModelDo> doList) {
        List<ProductReleaseModelPo> poList = productReleaseModelConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductReleaseModelDo entity) {
        ProductReleaseModelPo productReleaseModelPo = productReleaseModelConverter.do2Po(entity);
        return repository.delete(productReleaseModelPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductReleaseModelDo entity) {
        ProductReleaseModelPo productReleaseModelPo = productReleaseModelConverter.do2Po(entity);
        productReleaseModelPo.preInsert();
        return repository.insert(productReleaseModelPo);
    }

    @Override
    public boolean insertBatch(List<ProductReleaseModelDo> doList) {
        List<ProductReleaseModelPo> poList = productReleaseModelConverter.do2PoList(doList);
        poList = ProductReleaseModelPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductReleaseModelVo> findById(Long id) {
        Optional<ProductReleaseModelPo> productReleaseModelPo =  repository.findById(id);
        ProductReleaseModelVo productReleaseModelVo = productReleaseModelConverter.po2Vo(productReleaseModelPo.orElse(new ProductReleaseModelPo()));
        return Optional.ofNullable(productReleaseModelVo);

    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductReleaseModelVo> findAllMatching(ProductReleaseModelDo entity) {
        ProductReleaseModelPo productReleaseModelPo = productReleaseModelConverter.do2Po(entity);
        List<ProductReleaseModelPo>productReleaseModelPoList =  repository.findAllMatching(productReleaseModelPo);
        return productReleaseModelConverter.po2VoList(productReleaseModelPoList);
    }


    @Override
    public Pagination<ProductReleaseModelVo> selectPage(ProductReleaseModelDo entity){
        ProductReleaseModelPo productReleaseModelPo = productReleaseModelConverter.do2Po(entity);
        PageQuery<ProductReleaseModelPo> pageQuery = new PageQuery<>(productReleaseModelPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductReleaseModelPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<ProductReleaseModelVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productReleaseModelConverter.po2VoList(pagination.getList()));
    }

    public void addProductReleaseModel(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        int moreSpec = 2;
        if(dto.getSpecType() != moreSpec) {
            return;
        }
        for(MultipleSpecGoodsDto item:dto.getMultipleSpecGoodsList()){
            ProductReleaseModelPo model = new ProductReleaseModelPo();
            model.setProductReleaseId(dto.getId());
            model.setNum(item.getNum());
            model.setMarketPrice(item.getMarketPrice());
            model.setPrice(item.getPrice());
            if(!StringUtils.ifNull(item.getSpecName1())) {
                ProductPropertyListPo listPo = map.get(item.getSpecName1());
                if(listPo != null) {
                    model.setPropertyId1(listPo.getId());
                    model.setPropertyName1(listPo.getName());
                }
            }
            if(!StringUtils.ifNull(item.getSpecName2())) {
                ProductPropertyListPo listPo = map.get(item.getSpecName2());
                if(listPo != null) {
                    model.setPropertyId2(listPo.getId());
                    model.setPropertyName2(listPo.getName());
                }
            }
            if(!StringUtils.ifNull(item.getSpecName3())) {
                ProductPropertyListPo listPo = map.get(item.getSpecName3());
                if(listPo != null) {
                    model.setPropertyId3(listPo.getId());
                    model.setPropertyName3(listPo.getName());
                }
            }
            model.preInsert();
            repository.insert(model);
        }
    }


    public void updateProductReleaseModel(ProductReleaseEditDto dto, Map<String, ProductPropertyListPo> map){
        ProductReleaseModelPo param = new ProductReleaseModelPo();
        param.setProductReleaseId(dto.getId());
        List<ProductReleaseModelPo> modelPos = repository.findAllMatching(param);
        int moreSpec = 2;
        if(dto.getSpecType() != moreSpec) {
            for(ProductReleaseModelPo item:modelPos){
                repository.delete(item);
            }
            return;
        }
        List<Long> delList = new ArrayList<>();
        for(MultipleSpecGoodsDto item:dto.getMultipleSpecGoodsList()){
            ProductReleaseModelPo model = new ProductReleaseModelPo();
            model.setNum(item.getNum());
            model.setMarketPrice(item.getMarketPrice());
            model.setPrice(item.getPrice());
            if (!StringUtils.ifNull(item.getSpecName1())) {
                ProductPropertyListPo listPo = map.get(item.getSpecName1());
                if (listPo != null) {
                    model.setPropertyId1(listPo.getId());
                    model.setPropertyName1(listPo.getName());
                }
            }
            if (!StringUtils.ifNull(item.getSpecName2())) {
                ProductPropertyListPo listPo = map.get(item.getSpecName2());
                if (listPo != null) {
                    model.setPropertyId2(listPo.getId());
                    model.setPropertyName2(listPo.getName());
                }
            }
            if (!StringUtils.ifNull(item.getSpecName3())) {
                ProductPropertyListPo listPo = map.get(item.getSpecName3());
                if (listPo != null) {
                    model.setPropertyId3(listPo.getId());
                    model.setPropertyName3(listPo.getName());
                }
            }
            if(item.getId() == null) {
                model.setProductReleaseId(dto.getId());
                model.preInsert();
                repository.insert(model);
            }else {
                model.setId(item.getId());
                model.preUpdate();
                repository.update(model);
            }
        }
        for(ProductReleaseModelPo model:modelPos){
            Boolean isDel = true;
            for(MultipleSpecGoodsDto item:dto.getMultipleSpecGoodsList()){
                if(model.getId().equals(item.getId())){
                    isDel = false;
                }
            }
            if(isDel) {
                delList.add(model.getId());
            }
        }
        if(delList.size() > 0){
            repository.deleteBatch(delList);
        }
    }
}

