package com.jhf.youke.product.domain.service;

import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.converter.ProductReleaseCommissionConverter;
import com.jhf.youke.product.domain.converter.ProductReleaseConverter;
import com.jhf.youke.product.domain.gateway.ProductReleaseCommissionRepository;
import com.jhf.youke.product.domain.gateway.ProductReleaseRepository;
import com.jhf.youke.product.domain.model.Do.ProductReleaseDo;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.dto.ProductReleaseEditDto;
import com.jhf.youke.product.domain.model.po.ProductReleaseCommissionPo;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseCommissionVo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 * **/
@Service
public class ProductReleaseService extends AbstractDomainService<ProductReleaseRepository, ProductReleaseDo, ProductReleaseVo> {


    @Resource
    ProductReleaseConverter productReleaseConverter;

    @Resource
    ProductReleaseCommissionRepository productReleaseCommissionRepository;

    @Resource
    ProductReleaseCommissionConverter productReleaseCommissionConverter;

    @Override
    public boolean update(ProductReleaseDo entity) {
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        productReleasePo.preUpdate();
        return repository.update(productReleasePo);
    }

    @Override
    public boolean updateBatch(List<ProductReleaseDo> doList) {
        List<ProductReleasePo> poList = productReleaseConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductReleaseDo entity) {
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        return repository.delete(productReleasePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductReleaseDo entity) {
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        productReleasePo.preInsert();
        return repository.insert(productReleasePo);
    }

    @Override
    public boolean insertBatch(List<ProductReleaseDo> doList) {
        List<ProductReleasePo> poList = productReleaseConverter.do2PoList(doList);
        poList = ProductReleasePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductReleaseVo> findById(Long id) {
        Optional<ProductReleasePo> productReleasePo =  repository.findById(id);
        ProductReleaseVo productReleaseVo = productReleaseConverter.po2Vo(productReleasePo.orElse(new ProductReleasePo()));
        return Optional.ofNullable(productReleaseVo);
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductReleaseVo> findAllMatching(ProductReleaseDo entity) {
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        List<ProductReleasePo>productReleasePoList =  repository.findAllMatching(productReleasePo);
        return productReleaseConverter.po2VoList(productReleasePoList);
    }


    @Override
    public Pagination<ProductReleaseVo> selectPage(ProductReleaseDo entity){
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        PageQuery<ProductReleasePo> pageQuery = new PageQuery<>(productReleasePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductReleasePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productReleaseConverter.po2VoList(pagination.getList()));
    }

    /**
     * 商品发布
     *
     * @param entity 实体
     * @return {@link Boolean}
     */
    public Boolean release(ProductReleaseDo entity){
        entity.release();
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        return repository.update(productReleasePo);
    }

    /**
     * 商品下架
     *
     * @param entity 实体
     * @return {@link Boolean}
     */
    public Boolean soldOut(ProductReleaseDo entity){
        entity.soldOut();
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        return repository.update(productReleasePo);
    }

    /** 获取商品佣金 **/
    public List<ProductReleaseCommissionVo> getProductReleaseCommission(Long id) {
        List<ProductReleaseCommissionPo> productReleaseCommissionPos = productReleaseCommissionRepository.getListByRelease(id);
        List<ProductReleaseCommissionVo> list = productReleaseCommissionConverter.po2VoList(productReleaseCommissionPos);
        return list;
    }

    /** 根据产品二级分类获取发布商品列表 **/
    public Pagination<ProductReleaseVo> getListByStyle(ProductReleaseDo entity){
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(entity);
        Pagination<ProductReleasePo> pagination = repository.getListByProductStyle(productReleasePo);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productReleaseConverter.po2VoList(pagination.getList()));
    }

    public Boolean saveGoods(ProductReleaseEditDto dto) {
        ProductReleaseDo productReleaseDo = productReleaseConverter.editDto2Do(dto);
        productReleaseDo.validateNotNull(productReleaseDo);
        productReleaseDo.validateSpecCommission(dto);
        ProductReleasePo productReleasePo = productReleaseConverter.do2Po(productReleaseDo);
        productReleasePo.preInsert();
        dto.setId(productReleasePo.getId());
        return repository.insert(productReleasePo);
    }

    public Boolean updateGoods(ProductReleaseEditDto dto) {
        ProductReleaseDo productReleaseDo = productReleaseConverter.editDto2Do(dto);
        productReleaseDo.validateNotNull(productReleaseDo);
        productReleaseDo.validateSpecCommission(dto);
        return update(productReleaseDo);
    }

    public Pagination<ProductReleaseVo> getAllInfoList(ProductReleaseDto dto){
        Pagination<ProductReleaseVo> pagination = repository.getAllInfoList(dto);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                pagination.getList());

    }

}

