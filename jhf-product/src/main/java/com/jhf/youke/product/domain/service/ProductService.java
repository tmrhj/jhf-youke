package com.jhf.youke.product.domain.service;

import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.converter.ProductConverter;
import com.jhf.youke.product.domain.gateway.ProductRepository;
import com.jhf.youke.product.domain.model.Do.ProductDo;
import com.jhf.youke.product.domain.model.po.ProductPo;
import com.jhf.youke.product.domain.model.vo.ProductVo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 * **/
@Service
public class ProductService extends AbstractDomainService<ProductRepository, ProductDo, ProductVo> {


    @Resource
    ProductConverter productConverter;

    @Override
    public boolean update(ProductDo entity) {
        ProductPo productPo = productConverter.do2Po(entity);
        productPo.preUpdate();
        return repository.update(productPo);
    }

    @Override
    public boolean updateBatch(List<ProductDo> doList) {
        List<ProductPo> poList = productConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ProductDo entity) {
        ProductPo productPo = productConverter.do2Po(entity);
        return repository.delete(productPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ProductDo entity) {
        ProductPo productPo = productConverter.do2Po(entity);
        productPo.preInsert();
        return repository.insert(productPo);
    }

    @Override
    public boolean insertBatch(List<ProductDo> doList) {
        List<ProductPo> poList = productConverter.do2PoList(doList);
        poList = ProductPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ProductVo> findById(Long id) {
        Optional<ProductPo> productPo =  repository.findById(id);
        ProductVo productVo = productConverter.po2Vo(productPo.orElse(new ProductPo()));
        return Optional.ofNullable(productVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ProductVo> findAllMatching(ProductDo entity) {
        ProductPo productPo = productConverter.do2Po(entity);
        List<ProductPo>productPoList =  repository.findAllMatching(productPo);
        return productConverter.po2VoList(productPoList);
    }


    @Override
    public Pagination<ProductVo> selectPage(ProductDo entity){
        ProductPo productPo = productConverter.do2Po(entity);
        PageQuery<ProductPo> pageQuery = new PageQuery<>(productPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ProductPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<ProductVo>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                productConverter.po2VoList(pagination.getList()));
    }


}

