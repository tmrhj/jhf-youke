package com.jhf.youke.product.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.product.domain.model.po.ProductCollectionPo;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author  makejava
 **/

@Mapper
public interface ProductCollectionMapper  extends BaseMapper<ProductCollectionPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update prod_product_collection set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update prod_product_collection set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 收藏列表分页
     */
    List<ProductCollectionVo> getPageList(ProductCollectionPo productCollectionPo);

}

