package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.product.domain.gateway.ProductCollectionRepository;
import com.jhf.youke.product.domain.model.Do.ProductCollectionDo;
import com.jhf.youke.product.domain.model.po.ProductCollectionPo;
import com.jhf.youke.product.domain.exception.ProductCollectionException;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductCollectionVo;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  makejava
 **/

@Service(value = "ProductCollectionRepository")
public class ProductCollectionRepositoryImpl extends ServiceImpl<ProductCollectionMapper, ProductCollectionPo> implements ProductCollectionRepository {


    @Override
    public boolean update(ProductCollectionPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductCollectionPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductCollectionPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductCollectionPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductCollectionPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductCollectionPo> findById(Long id) {
        ProductCollectionPo productCollectionPo = baseMapper.selectById(id);
        return Optional.ofNullable(productCollectionPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }
        if(StringUtils.isEmpty(ids)){
            throw new ProductCollectionException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductCollectionPo> findAllMatching(ProductCollectionPo entity) {
        QueryWrapper<ProductCollectionPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductCollectionPo> selectPage(PageQuery<ProductCollectionPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductCollectionPo> productCollectionPos = baseMapper.selectList(new QueryWrapper<ProductCollectionPo>(pageQuery.getParam()));
        PageInfo<ProductCollectionPo> pageInfo = new PageInfo<>(productCollectionPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Pagination<ProductCollectionVo> getPageList(PageQuery<ProductCollectionPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductCollectionVo> productCollectionPos = baseMapper.getPageList(pageQuery.getParam());
        PageInfo<ProductCollectionVo> pageInfo = new PageInfo<>(productCollectionPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

