package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.exception.ProductReleaseCommissionException;
import com.jhf.youke.product.domain.gateway.ProductReleaseCommissionRepository;
import com.jhf.youke.product.domain.model.po.ProductReleaseCommissionPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "ProductReleaseCommissionRepository")
public class ProductReleaseCommissionRepositoryImpl extends ServiceImpl<ProductReleaseCommissionMapper, ProductReleaseCommissionPo> implements ProductReleaseCommissionRepository {


    @Override
    public boolean update(ProductReleaseCommissionPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductReleaseCommissionPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductReleaseCommissionPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductReleaseCommissionPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductReleaseCommissionPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductReleaseCommissionPo> findById(Long id) {
        ProductReleaseCommissionPo productReleaseCommissionPo = baseMapper.selectById(id);
        return Optional.ofNullable(productReleaseCommissionPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new ProductReleaseCommissionException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductReleaseCommissionPo> findAllMatching(ProductReleaseCommissionPo entity) {
        QueryWrapper<ProductReleaseCommissionPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductReleaseCommissionPo> selectPage(PageQuery<ProductReleaseCommissionPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductReleaseCommissionPo> productReleaseCommissionPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<ProductReleaseCommissionPo> pageInfo = new PageInfo<>(productReleaseCommissionPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<ProductReleaseCommissionPo> getListByRelease(Long id) {
        QueryWrapper<ProductReleaseCommissionPo> qw = new QueryWrapper<>();
        qw.eq("product_release_id", id);
        return baseMapper.selectList(qw);

    }
}

