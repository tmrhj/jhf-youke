package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.exception.ProductReleaseImageException;
import com.jhf.youke.product.domain.gateway.ProductReleaseImageRepository;
import com.jhf.youke.product.domain.model.po.ProductReleaseImagePo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "ProductReleaseImageRepository")
public class ProductReleaseImageRepositoryImpl extends ServiceImpl<ProductReleaseImageMapper, ProductReleaseImagePo> implements ProductReleaseImageRepository {


    @Override
    public boolean update(ProductReleaseImagePo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductReleaseImagePo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductReleaseImagePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductReleaseImagePo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductReleaseImagePo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductReleaseImagePo> findById(Long id) {
        ProductReleaseImagePo productReleaseImagePo = baseMapper.selectById(id);
        return Optional.ofNullable(productReleaseImagePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";

        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }

        if(StringUtils.isEmpty(ids)){
            throw new ProductReleaseImageException("批量删除ID为空");
        }

        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductReleaseImagePo> findAllMatching(ProductReleaseImagePo entity) {
        QueryWrapper<ProductReleaseImagePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductReleaseImagePo> selectPage(PageQuery<ProductReleaseImagePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductReleaseImagePo> productReleaseImagePos = baseMapper.selectList(new QueryWrapper<ProductReleaseImagePo>(pageQuery.getParam()));
        PageInfo<ProductReleaseImagePo> pageInfo = new PageInfo<>(productReleaseImagePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<ProductReleaseImagePo> getListByRelease(Long id) {
        QueryWrapper<ProductReleaseImagePo> qw = new QueryWrapper<>();
        qw.eq("product_release_id", id);
        return baseMapper.selectList(qw);
    }
}

