package com.jhf.youke.product.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 产品发布映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface ProductReleaseMapper  extends BaseMapper<ProductReleasePo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update prod_product_release set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update prod_product_release set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 被产品样式列表
     *
     * @param companyId      公司标识
     * @param productStyleId 产品风格id
     * @return {@link List}<{@link ProductReleasePo}>
     */
    List<ProductReleasePo> getListByProductStyle(@Param("companyId")Long companyId,
                                                 @Param("productStyleId")Long productStyleId);

    /**
     * 获得页面列表
     *
     * @param productReleasePo 产品发布订单
     * @return {@link List}<{@link ProductReleasePo}>
     */
    List<ProductReleasePo> getPageList(ProductReleasePo productReleasePo);

    /**
     * 得到所有信息列表
     *
     * @param dto dto
     * @return {@link List}<{@link ProductReleaseVo}>
     * @Description: 查询带详情的列表
     * @Param: [dto] 查询条件
     * @return: java.util.List<com.jhf.youke.product.domain.model.vo.ProductReleaseVo> 返回VO
     * @Author: RHJ
     * @Date: 2022/11/3
     */
    List<ProductReleaseVo> getAllInfoList(ProductReleaseDto dto);

}

