package com.jhf.youke.product.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.product.domain.model.po.ProductReleaseModelPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 产品发布模型映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface ProductReleaseModelMapper  extends BaseMapper<ProductReleaseModelPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update prod_product_release_model set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update prod_product_release_model set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

