package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.exception.ProductReleaseModelException;
import com.jhf.youke.product.domain.gateway.ProductReleaseModelRepository;
import com.jhf.youke.product.domain.model.po.ProductReleaseModelPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "ProductReleaseModelRepository")
public class ProductReleaseModelRepositoryImpl extends ServiceImpl<ProductReleaseModelMapper, ProductReleaseModelPo> implements ProductReleaseModelRepository {


    @Override
    public boolean update(ProductReleaseModelPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductReleaseModelPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductReleaseModelPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductReleaseModelPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductReleaseModelPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductReleaseModelPo> findById(Long id) {
        ProductReleaseModelPo productReleaseModelPo = baseMapper.selectById(id);
        return Optional.ofNullable(productReleaseModelPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";
     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }
        
        if(StringUtils.isEmpty(ids)){
            throw new ProductReleaseModelException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductReleaseModelPo> findAllMatching(ProductReleaseModelPo entity) {
        QueryWrapper<ProductReleaseModelPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductReleaseModelPo> selectPage(PageQuery<ProductReleaseModelPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductReleaseModelPo> productReleaseModelPos = baseMapper.selectList(new QueryWrapper<ProductReleaseModelPo>(pageQuery.getParam()));
        PageInfo<ProductReleaseModelPo> pageInfo = new PageInfo<>(productReleaseModelPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<ProductReleaseModelPo> getListByRelease(Long id) {
        QueryWrapper<ProductReleaseModelPo> qw = new QueryWrapper<>();
        qw.eq("product_release_id", id);
        return baseMapper.selectList(qw);
    }
}

