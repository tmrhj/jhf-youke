package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.product.domain.exception.ProductReleaseException;
import com.jhf.youke.product.domain.gateway.ProductReleaseRepository;
import com.jhf.youke.product.domain.model.dto.ProductReleaseDto;
import com.jhf.youke.product.domain.model.po.ProductReleasePo;
import com.jhf.youke.product.domain.model.vo.ProductReleaseVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "ProductReleaseRepository")
public class ProductReleaseRepositoryImpl extends ServiceImpl<ProductReleaseMapper, ProductReleasePo> implements ProductReleaseRepository {


    @Override
    public boolean update(ProductReleasePo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductReleasePo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductReleasePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductReleasePo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductReleasePo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductReleasePo> findById(Long id) {
        ProductReleasePo productReleasePo = baseMapper.selectById(id);
        return Optional.ofNullable(productReleasePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }
        if(StringUtils.isEmpty(ids)){
            throw new ProductReleaseException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductReleasePo> findAllMatching(ProductReleasePo entity) {
        QueryWrapper<ProductReleasePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductReleasePo> selectPage(PageQuery<ProductReleasePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductReleasePo> productReleasePo = baseMapper.getPageList(pageQuery.getParam());
        PageInfo<ProductReleasePo> pageInfo = new PageInfo<>(productReleasePo);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Pagination<ProductReleasePo> getListByProductStyle(ProductReleasePo entity) {
        List<ProductReleasePo> productReleasePos = baseMapper.getListByProductStyle(entity.getCompanyId(),entity.getProductStyleId());
        PageInfo<ProductReleasePo> pageInfo = new PageInfo<>(productReleasePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());

    }

    @Override
    public Pagination<ProductReleaseVo> getAllInfoList(ProductReleaseDto dto) {
        PageHelper.startPage(dto.getCurrentPage(), dto.getPageSize());
        List<ProductReleaseVo> list = baseMapper.getAllInfoList(dto);
        PageInfo<ProductReleaseVo> pageInfo = new PageInfo<>(list);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

