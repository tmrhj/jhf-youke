package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.exception.ProductException;
import com.jhf.youke.product.domain.gateway.ProductRepository;
import com.jhf.youke.product.domain.model.po.ProductPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "ProductRepository")
public class ProductRepositoryImpl extends ServiceImpl<ProductMapper, ProductPo> implements ProductRepository {


    @Override
    public boolean update(ProductPo entity) {
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductPo> list) {
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductPo entity) {
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductPo> list) {
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductPo> findById(Long id) {
        ProductPo productPo = baseMapper.selectById(id);
        return Optional.ofNullable(productPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";

        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }

        if(StringUtils.isEmpty(ids)){
            throw new ProductException("批量删除ID为空");
        }

        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductPo> findAllMatching(ProductPo entity) {
        entity.setDelFlag("0");
        QueryWrapper<ProductPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductPo> selectPage(PageQuery<ProductPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductPo> productPos = baseMapper.selectList(new QueryWrapper<ProductPo>(pageQuery.getParam()));
        PageInfo<ProductPo> pageInfo = new PageInfo<>(productPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ProductPo getByName(String name) {
        QueryWrapper<ProductPo> qw = new QueryWrapper<>();
        qw.eq("name",name);
        ProductPo productPo = baseMapper.selectOne(qw);
        return productPo;
    }
}

