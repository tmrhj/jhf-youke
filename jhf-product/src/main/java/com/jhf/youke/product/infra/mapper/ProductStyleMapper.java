package com.jhf.youke.product.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.product.domain.model.po.ProductStylePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 产品风格映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface ProductStyleMapper  extends BaseMapper<ProductStylePo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update prod_product_style set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update prod_product_style set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 被发布列表
     *
     * @param companyId 公司标识
     * @return {@link List}<{@link ProductStylePo}>
     */
    List<ProductStylePo> getListByRelease(@Param("companyId") Long companyId);

}

