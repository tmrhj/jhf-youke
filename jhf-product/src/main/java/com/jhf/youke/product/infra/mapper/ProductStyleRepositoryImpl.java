package com.jhf.youke.product.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.product.domain.exception.ProductStyleException;
import com.jhf.youke.product.domain.gateway.ProductStyleRepository;
import com.jhf.youke.product.domain.model.po.ProductStylePo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author RHJ
 */
@Service(value = "ProductStyleRepository")
public class ProductStyleRepositoryImpl extends ServiceImpl<ProductStyleMapper, ProductStylePo> implements ProductStyleRepository {


    @Override
    public boolean update(ProductStylePo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<ProductStylePo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(ProductStylePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(ProductStylePo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<ProductStylePo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<ProductStylePo> findById(Long id) {
        ProductStylePo productStylePo = baseMapper.selectById(id);
        return Optional.ofNullable(productStylePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        String ids = "";

        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }

        if(StringUtils.isEmpty(ids)){
            throw new ProductStyleException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<ProductStylePo> findAllMatching(ProductStylePo entity) {
        QueryWrapper<ProductStylePo> wrapper = new QueryWrapper<>();
        entity.setDelFlag("0");
        if(!StringUtils.isEmpty(entity.getName())){
            wrapper.like("name", entity.getName());
            entity.setName(null);
        }
        wrapper.setEntity(entity);
        wrapper.orderByAsc("sort");
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<ProductStylePo> selectPage(PageQuery<ProductStylePo> pageQuery) {
        pageQuery.getParam().setDelFlag("0");
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<ProductStylePo> productStylePos = baseMapper.selectList(new QueryWrapper<ProductStylePo>(pageQuery.getParam()).orderByAsc("sort"));
        PageInfo<ProductStylePo> pageInfo = new PageInfo<>(productStylePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<ProductStylePo> getListByRelease(Long companyId) {
        return baseMapper.getListByRelease(companyId);
    }
}

