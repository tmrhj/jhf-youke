import cn.hutool.json.JSONUtil;
import com.jhf.youke.ProductApplication;
import com.jhf.youke.product.domain.model.vo.ProductStyleVo;
import com.jhf.youke.product.domain.service.ProductStyleService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@SpringBootTest(classes = ProductApplication.class)
class ProductTestApplication {

    @Resource
    ProductStyleService productStyleService;



    @Test
    void test1(){
        List<ProductStyleVo> listByRelease = productStyleService.getListByRelease(1L);
        log.info(JSONUtil.toJsonStr(listByRelease));

    }



}
