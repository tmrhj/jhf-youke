package com.jhf.youke.pulsar.adapter.web;

import com.jhf.youke.pulsar.domain.model.dto.MessageDto;
import com.jhf.youke.pulsar.domain.service.ProducerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @author RHJ
 */
@RestController
public class PulsarController {

    @Resource
    private ProducerService producerService;

    @PostMapping("/send")
    public String selectPage(@RequestBody MessageDto dto){
        String res = "success";
        try{
            producerService.sendMessage(dto.getTopic(),dto.getMessage());
        }catch (Exception e){
            res = "error";
            e.printStackTrace();
        }
        return  res;
    }

}

