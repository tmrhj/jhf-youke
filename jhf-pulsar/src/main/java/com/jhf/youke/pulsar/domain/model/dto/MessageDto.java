package com.jhf.youke.pulsar.domain.model.dto;

import lombok.Data;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/11/17
*/
@Data
public class MessageDto {
    private String topic;
    private String message;
}
