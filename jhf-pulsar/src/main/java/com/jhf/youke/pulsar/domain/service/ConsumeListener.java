package com.jhf.youke.pulsar.domain.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/11/18
*/
@Component
@Log4j2
public class ConsumeListener {

    @Resource
    private ConsumeService consumeService;

    @PostConstruct
    public void listener()throws Exception{
        log.info("listener start ");
        consumeService.consume("1");

    }
}
