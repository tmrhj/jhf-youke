package com.jhf.youke.pulsar.domain.service;


import lombok.Data;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * @author : rhj
 * @version : 1.0
 */
@Component
@Data
public class ProducerClient {

    @Resource
    private  PulsarConfig config;

    private PulsarClient client;

    public ProducerClient(){

    }

    public void init() throws PulsarClientException {
        client = PulsarClient.builder()
                .serviceUrl(config.getServiceUrl())
                .build();
    }

    public void close() throws PulsarClientException {
        client.close();
    }

    public PulsarClient getPulsarClient(){
        return client;
    }

}