package com.jhf.youke.pulsar.domain.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
* @Description:
* @Param:
* @return:
* @Author: RHJ
* @Date: 2022/11/17
*/

@Configuration
@Data
public class PulsarConfig {

        @Value("${pulsar.serviceUrl}")
        private String serviceUrl;

        @Value("${pulsar.serviceHttpUrl}")
        private String serviceHttpUrl;


}
