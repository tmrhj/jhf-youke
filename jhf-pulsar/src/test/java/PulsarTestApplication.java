import com.jhf.youke.PulsarApplication;
import com.jhf.youke.pulsar.domain.service.ProducerService;
import com.jhf.youke.pulsar.domain.service.TenantService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest(classes = PulsarApplication.class)
class PulsarTestApplication {

    @Resource
    private ProducerService producerService;

    @Resource
    private TenantService tenantService;


    @Test
    void test1(){
        producerService.sendOnce("hello world");

    }

    @Test
    public void test2() throws Exception{
        tenantService.createTenant("hczh1","standalone");
        tenantService.getTenants();
    }

    @Test
    public void test3() throws Exception{

        tenantService.createNameSpace("dev","hczh");
    }

    @Test
    public void test4() throws Exception{
        tenantService.getNameSpace("hczh");
        tenantService.createTopic("dev","hczh", "testTopic");
    }



}
