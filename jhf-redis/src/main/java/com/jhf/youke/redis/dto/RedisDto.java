package com.jhf.youke.redis.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RedisDto implements Serializable{

    private String key;
    private String value;
    private long time;

}

