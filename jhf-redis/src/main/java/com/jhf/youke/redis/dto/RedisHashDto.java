package com.jhf.youke.redis.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RedisHashDto implements Serializable{

    private String key;
    private Map<String, Object> value;
    private long time;

}

