package com.jhf.youke.redis.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RedisHashItemDto implements Serializable{

    private String key;
    private String item;
    private Object value;
    private long time;
    private String[] itemList;

}

