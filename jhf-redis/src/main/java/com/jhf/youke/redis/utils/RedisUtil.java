package com.jhf.youke.redis.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Redis 工具类
 * @author Bon
 */
@Component
public class RedisUtil {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private ObjectMapper objectMapper;

    private static final String OK = "OK";

    /**
     * 通过redis获取8位的自增数，需要输入键名
     */
    DefaultRedisScript<Long> eightLengthDigitGenerator = new DefaultRedisScript<>(
        "local key = KEYS[1]\n" +
        "local cnt = redis.call('get', key) or 9999999\n" +
        "cnt = cnt + 1\n" +
        "if (cnt < 10000000 or cnt > 99999999) then\n" +
        "   cnt = 10000000\n" +
        "end\n"+
        "redis.call('set', key, cnt)\n" +
        "return cnt",
        Long.class
    );


    DefaultRedisScript<Long> releaseLockLua = new DefaultRedisScript<>(
            "if redis.call('get', KEYS[1]) == ARGV[1]" +
                    " then" +
                    "     return redis.call('del', KEYS[1])" +
                    " else" +
                    "     return 0" +
                    " end",
            Long.class
    );

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */

    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);

            }

            return true;

        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }

    /**
     * @param key
     * @param uuid
     * @return
     */
    public boolean setnx(String key, String uuid) {
        try {
            if (redisTemplate.opsForValue().setIfAbsent(key, uuid)) {
                return true;
            }
            return false;

        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }


    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */

    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);

    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */

    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);

        } catch (Exception e) {
            e.getMessage();

            return false;

        }

    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete((List<String>) CollectionUtils.arrayToList(key));

            }
        }

    }

    /**
     * 普通缓存获取valueobjects
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        try {
            return key == null ? null : redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            del(key);
        }
        return null;

    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */

    public boolean set(String key, Object value) {
        try {
            if(value instanceof String){
                redisTemplate.opsForValue().set(key, value);
            }else{
                redisTemplate.opsForValue().set(key, objectMapper.writeValueAsString(value));
            }
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }

    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */

    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                if(value instanceof String){
                    redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
                }else {
                    redisTemplate.opsForValue().set(key, objectMapper.writeValueAsString(value), time, TimeUnit.SECONDS);
                }
            } else {
                if(value instanceof String){
                    set(key, value);
                }else{
                    set(key, objectMapper.writeValueAsString(value));
                }
            }
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */

    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");

        }

        return redisTemplate.opsForValue().increment(key, delta);


    }


    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */

    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");

        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);

    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */

    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */

    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }


    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */

    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);

            if (time > 0) {
                expire(key, time);

            }
            return true;

        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */

    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;

        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */

    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);

            }
            return true;

        } catch (Exception e) {

            e.getMessage();
            return false;

        }

    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */

    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);

    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */

    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, long by) {
        return redisTemplate.opsForHash().increment(key, item, by);

    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */

    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);

    }

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */

    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }

    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */

    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);

        } catch (Exception e) {
            e.getMessage();
            return false;

        }

    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */

    public long sSet(String key, Object... values) {

        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.getMessage();
            return 0;

        }

    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */

    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if(time > 0){
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            e.getMessage();
            return 0;
        }

    }


    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);

        } catch (Exception e) {
            e.getMessage();
            return 0;

        }

    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {

        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.getMessage();
            return 0;

        }

    }

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */

    public List<Object> lGet(String key, long start, long end) {

        try {
            return redisTemplate.opsForList().range(key, start, end);

        } catch (Exception e) {
            e.getMessage();
            return null;

        }

    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */

    public long lGetListSize(String key) {

        try {
            return redisTemplate.opsForList().size(key);

        } catch (Exception e) {

            e.getMessage();
            return 0;
        }

    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */

    public Object lGetIndex(String key, long index) {

        try {

            return redisTemplate.opsForList().index(key, index);

        } catch (Exception e) {

            e.getMessage();
            return null;

        }

    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */

    public boolean lSet(String key, Object value) {

        try {

            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        }

    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */

    public boolean lSet(String key, Object value, long time) {

        try {


            redisTemplate.opsForList().rightPush(key, value);


            if (time > 0){
                expire(key, time);
            }

            return true;

        } catch (Exception e) {

            e.getMessage();

            return false;

        }


    }


    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */


    public boolean lSet(String key, List<Object> value) {


        try {


            redisTemplate.opsForList().rightPushAll(key, value);


            return true;


        } catch (Exception e) {

            e.getMessage();

            return false;


        }


    }


    /**
     * 484
     * <p>
     * 将list放入缓存
     * <p>
     * 485
     * <p>
     * <p>
     * <p>
     * 486
     *
     * @param key   键
     *              <p>
     *              487
     * @param value 值
     *              <p>
     *              488
     * @param time  时间(秒)
     *              <p>
     *              489
     * @return 490
     */


    public boolean lSet(String key, List<Object> value, long time) {


        try {

            redisTemplate.opsForList().rightPushAll(key, value);

            if (time > 0){
                expire(key, time);
            }

            return true;


        } catch (Exception e) {

            e.getMessage();

            return false;

        }


    }


    /**
     * 504
     * <p>
     * 根据索引修改list中的某条数据
     * <p>
     * 505
     *
     * @param key   键
     *              <p>
     *              506
     * @param index 索引
     *              <p>
     *              507
     * @param value 值
     *              <p>
     *              508
     * @return 509
     */


    public boolean lUpdateIndex(String key, long index, Object value) {


        try {


            redisTemplate.opsForList().set(key, index, value);


            return true;


        } catch (Exception e) {


            e.getMessage();


            return false;


        }


    }


    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */


    public long lRemove(String key, long count, Object value) {


        try {


            Long remove = redisTemplate.opsForList().remove(key, count, value);


            return remove;


        } catch (Exception e) {

            e.getMessage();
            return 0;

        }

    }

    /**
     * 模糊查询Redis信息
     *
     * @param pattern 键
     * @return
     */
    public Set keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * @MethodName: 清空Redis缓存
     * @Author: Bon
     * @Date: 2021-01-22 16:49
     **/
    public boolean flushRedis() {
        Set<String> keys = redisTemplate.keys("*");
        redisTemplate.delete(keys);
        return true;
    }

    public boolean sismember(String key, String member) {
        return redisTemplate.opsForSet().isMember(key, member);
    }

    public Object spop(String key){
        return redisTemplate.opsForSet().pop(key);
    }

    public void sadd(String key, String...validIds) {
        redisTemplate.opsForSet().add(key, validIds);
    }

    public Long scard(String key) {
        return redisTemplate.opsForSet().size(key);
    }

    public <T> T lua(DefaultRedisScript<T> script, String key, Object...args){
        return redisTemplate.execute(script, Collections.singletonList(key), args);
    }

    public Long get8LenIncrDigit(){
        return lua(eightLengthDigitGenerator, "Gen8Num");
    }


    /**
     * lua 脚本编写
     */
    private static String lockScript = "if redis.call('EXISTS',KEYS[1])==0 then redis.call('set',KEYS[1],ARGV[1]); redis.call('expire',KEYS[1],ARGV[2]); return 1;" +
            " elseif redis.call('get',KEYS[1]) == ARGV[1] then return 2;" +
            " else return 0; end;";

    private static String releaseLockScript = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";



    /**
     * 加锁
     * @param timeout   锁的超时时间
     * @return 锁标识
     */
    public Boolean lockWithTimeout(String key, String requestId, Integer timeout) {

        // 获取锁的超时时间，超过这个时间则放弃获取锁
        long end = System.currentTimeMillis() + timeout;

        while (System.currentTimeMillis() < end) {
            try {
                //不存在则添加 且设置过期时间（单位ms）
                DefaultRedisScript<Long> longDefaultRedisScript = new DefaultRedisScript<>(lockScript, Long.class);
                Long result = redisTemplate.execute(longDefaultRedisScript, Collections.singletonList(key), requestId,String.valueOf(timeout));

                if (result == 1) {
                    System.out.println("lock true");
                    return true;
                }else{
                    Thread.sleep(50);

                }
            } catch (Exception e) {

            }
        }

        return false;
    }

    /**
     * 释放锁
     * @param key 锁的key
     * @param requestId    释放锁的标识
     * @return
     */
    public boolean releaseLock(String key, String requestId) {
        //增加一个脚本
        Long eval;
        try {
            DefaultRedisScript<Long> longDefaultRedisScript = new DefaultRedisScript<>(releaseLockScript, Long.class);
            eval =  redisTemplate.execute(longDefaultRedisScript, Collections.singletonList(key), requestId);
        } catch (Exception e) {
            return false;
        }
        return eval == 1;
    }

}
