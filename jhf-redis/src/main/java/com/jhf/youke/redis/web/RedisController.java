package com.jhf.youke.redis.web;


import com.jhf.youke.redis.dto.RedisHashDto;
import com.jhf.youke.redis.dto.RedisHashItemDto;
import com.jhf.youke.redis.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author RHJ
 * **/
@RestController
@RequestMapping("/redis")
@Log4j2
public class RedisController {

    @Resource
    private RedisUtil redisUtil;

    @RequestMapping(value="/get")
    public String get(@RequestParam("key") String key){
        String value = (String) redisUtil.get(key);
        return value;
    }

    @RequestMapping(value="/set")
    public String set(@RequestParam("key") String key, @RequestParam("value") String value,
                      @RequestParam("time") long time){
        try {
            redisUtil.set(key, value, time);
            System.out.println(redisUtil.get(key));
            return "success";
        }catch (Exception e){
            return e.getMessage();
        }
    }


    @RequestMapping(value="/setByJson")
    public String setByJson(@RequestBody com.jhf.youke.redis.dto.RedisDto dto){
        redisUtil.set(dto.getKey(), dto.getValue(), dto.getTime());
        return "success";
    }

    @RequestMapping(value="/del")
    public String del(@RequestParam("key") String key){
        redisUtil.del(key);
        return "success";
    }

    /**
     * 获取缓存过期时间
     * @param key
     * @return
     */
    @RequestMapping(value="/getTime")
    public String getTime(@RequestParam("key") String key){
        Long time = redisUtil.getExpire(key);
        return  time.toString();
    }

    @RequestMapping(value="/lock")
    public Boolean lockWithTimeout (@RequestParam("key") String key, @RequestParam("requestId") String requestId,@RequestParam("timeout") Integer timeout){
        timeout = timeout == null ? 5000 : timeout;
        return redisUtil.lockWithTimeout(key, requestId, timeout);
    }

    @GetMapping(value="/release")
    public Boolean releaseLock (@RequestParam("key") String key,@RequestParam("retIdentifier") String retIdentifier){
        Boolean  result = false;
        try {
            result = redisUtil.releaseLock(key, retIdentifier);
        }catch (Exception e){

        }
        return result;
    }

    @RequestMapping(value="/getHash")
    public Map<Object, Object> getHash(@RequestParam("key") String key){
        return redisUtil.hmget(key);
    }

    @RequestMapping(value="/setHash")
    public String setHash(@RequestBody RedisHashDto dto){
        redisUtil.hmset(dto.getKey(), dto.getValue(), dto.getTime());
        return "success";
    }

    @RequestMapping(value="/getHashItem")
    public String getHashItem(@RequestParam("key") String key ,@RequestParam("item") String item){
        String object = null;
        try{
            object = (String) redisUtil.hget(key, item);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return object;
    }

    @RequestMapping(value="/setHashItem")
    public String set(@RequestBody RedisHashItemDto dto){
        redisUtil.hset(dto.getKey(), dto.getItem(), dto.getValue(), dto.getTime());
        return "success";
    }

    @RequestMapping(value="/delHashItem")
    public String delHashItem(@RequestBody RedisHashItemDto dto){
        redisUtil.hdel(dto.getKey(), dto.getItemList());
        return "success";
    }


}
