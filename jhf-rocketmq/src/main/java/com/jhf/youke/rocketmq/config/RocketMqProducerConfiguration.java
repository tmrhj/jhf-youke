package com.jhf.youke.rocketmq.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author RHJ
 **/
@Component
@ConfigurationProperties(prefix = "rocketmq.producer")
public class RocketMqProducerConfiguration {

    private String nameServiceAddr;

    private String instanceName;

    private int maxMessageSize ;

    private int sendMsgTimeout ;

    public String getNameServiceAddr() {
        return nameServiceAddr;
    }

    public void setNameServiceAddr(String nameServiceAddr) {
        this.nameServiceAddr = nameServiceAddr;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public int getMaxMessageSize() {
        return maxMessageSize;
    }

    public void setMaxMessageSize(int maxMessageSize) {
        this.maxMessageSize = maxMessageSize;
    }

    public int getSendMsgTimeout() {
        return sendMsgTimeout;
    }

    public void setSendMsgTimeout(int sendMsgTimeout) {
        this.sendMsgTimeout = sendMsgTimeout;
    }


}


