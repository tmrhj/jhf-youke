package com.jhf.youke.rocketmq.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RocketMqDto implements Serializable {

    private static final long serialVersionUID = -1242493306307174690L;

    private String groupName;
    private String topic;
    private String tag;
    private Map<String, Object> messages;
    private Integer count;


}

