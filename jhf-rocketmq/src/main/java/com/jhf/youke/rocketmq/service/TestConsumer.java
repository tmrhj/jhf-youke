//package com.jhf.youke.rocketmq.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Service;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//import reactor.util.function.Tuple2;
//
//import java.util.Map;
//import java.util.function.Consumer;
//import java.util.function.Function;
//
///**
// * description:
// * date: 2022/9/23 14:12
// * author: cyx
// */
//@Slf4j
//@Service
//public class TestConsumer {
//
//    @Bean
//    public Consumer<Message<Map<String, Object>>> demo() {
//        System.out.println("初始化消费者");
//        return message -> {
//            MessageHeaders headers = message.getHeaders();
//            // 可以获取tag等信息
//            System.out.println(message.getPayload());
//        };
//    }
//
//    //@Bean
//    //public Function<Flux<Message<Map<String, Object>>>, Mono<Void>> demo() {
//    //    return flux -> flux.map(message -> {
//    //        System.out.println("测试function");
//    //        System.out.println(message.getPayload());
//    //        return message;
//    //    }).then();
//    //}
//
//    //@Bean
//    //public Function<Tuple2<Flux<String>, Flux<String>>, Flux<String>> demo() {
//    //    return tuple -> {
//    //        System.out.println("测试消费者");
//    //        Flux<String> f1 = tuple.getT1();
//    //        Flux<String> f2 = tuple.getT2();
//    //        return Flux.combineLatest(f1, f2, (str1, str2) -> str1 + ":" + str2);
//    //    };
//    //}
//
//}
