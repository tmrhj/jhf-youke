package com.jhf.youke.rocketmq.web;


import com.jhf.youke.core.entity.Response;
import com.jhf.youke.rocketmq.dto.RocketMqDto;
import com.jhf.youke.rocketmq.service.ProducerService;
import org.apache.rocketmq.client.producer.SendResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author RHJ
 */
@RestController
public class ProducerController {

    @Resource
    private   ProducerService producerService;


    @PostMapping("/send")
    public Response sendCount(@RequestBody RocketMqDto rocketMq){
        try {

            Integer count = rocketMq.getCount() != null ? rocketMq.getCount() : 5 ;
            SendResult send = producerService.send(rocketMq, count);

            return Response.ok(send);
        }catch(Exception e) {
            e.printStackTrace();
            return Response.fail("发送失败");
        }
    }


}
