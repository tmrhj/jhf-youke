//import com.jhf.youke.RocketMqApplication;
//import com.jhf.youke.core.entity.Response;
//import com.jhf.youke.rocketmq.dto.RocketMqDto;
//import com.jhf.youke.rocketmq.web.ProducerController;
//import org.apache.rocketmq.common.message.MessageConst;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.cloud.stream.function.StreamBridge;
//import org.springframework.integration.support.MessageBuilder;
//import org.springframework.messaging.MessageHeaders;
//import org.springframework.util.MimeTypeUtils;
//
//import javax.annotation.Resource;
//import java.util.Map;
//
///**
// * description:
// * date: 2022/9/29 9:53
// * author: cyx
// */
//@SpringBootTest(classes = RocketMqApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class TestController {
//
//    @Resource
//    ProducerController producerController;
//
//    @Autowired
//    private StreamBridge streamBridge;
//
//    @Test
//    public void test() {
//        System.out.println("测试mq");
//        RocketMqDto dto = new RocketMqDto();
//        dto.setGroupName("test-group");
//        dto.setTopic("topicw");
//        dto.setTag("all");
//        dto.setMessages(Map.of("name","Sam Spade"));
//        Response response = producerController.sendCount(dto);
//        System.out.println(response);
//    }
//
//    @Test
//    public void producer() {
//        System.out.println("生产者");
//        Map<String, Object> map = Map.of("id","111","text","消息内容");
//        boolean send = streamBridge.send("demo-out-0",
//                MessageBuilder.withPayload(map)
//                        .setHeader(MessageConst.PROPERTY_TAGS,"tag1")
//                        .build());
//        System.out.println(send);
//    }
//
//}
