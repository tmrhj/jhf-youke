package com.jhf.youke.saga.client.adapter.web;

import com.jhf.youke.saga.client.app.executor.SagaAppService;
import com.jhf.youke.saga.client.domain.model.dto.SagaDto;
import com.jhf.youke.saga.client.domain.model.vo.SagaVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/saga")
public class SagaController {

    @Resource
    private SagaAppService sagaAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody SagaDto dto) {
        return Response.ok(sagaAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody SagaDto dto) {
        return Response.ok(sagaAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody SagaDto dto) {
        return Response.ok(sagaAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<SagaVo>> findById(Long id) {
        return Response.ok(sagaAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(sagaAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<SagaVo>> list(@RequestBody SagaDto dto, @RequestHeader("token") String token) {

        return Response.ok(sagaAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<SagaVo>> selectPage(@RequestBody  SagaDto dto, @RequestHeader("token") String token) {
        return Response.ok(sagaAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(sagaAppService.getPreData());
    }
    

}

