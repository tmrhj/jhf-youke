package com.jhf.youke.saga.client.domain.converter;

import com.jhf.youke.saga.client.domain.model.Do.SagaMessageDo;
import com.jhf.youke.saga.client.domain.model.dto.SagaMessageDto;
import com.jhf.youke.saga.client.domain.model.po.SagaMessagePo;
import com.jhf.youke.saga.client.domain.model.vo.SagaMessageVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 传奇消息转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface SagaMessageConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param sagaMessageDto 传奇消息dto
     * @return {@link SagaMessageDo}
     */
    SagaMessageDo dto2Do(SagaMessageDto sagaMessageDto);

    /**
     * 洗阿宝
     *
     * @param sagaMessageDo 传奇信息做
     * @return {@link SagaMessagePo}
     */
    SagaMessagePo do2Po(SagaMessageDo sagaMessageDo);

    /**
     * 洗签证官
     *
     * @param sagaMessageDo 传奇信息做
     * @return {@link SagaMessageVo}
     */
    SagaMessageVo do2Vo(SagaMessageDo sagaMessageDo);


    /**
     * 洗订单列表
     *
     * @param sagaMessageDoList 传奇消息做列表
     * @return {@link List}<{@link SagaMessagePo}>
     */
    List<SagaMessagePo> do2PoList(List<SagaMessageDo> sagaMessageDoList);

    /**
     * 警察乙做
     *
     * @param sagaMessagePo 传奇消息阿宝
     * @return {@link SagaMessageDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    SagaMessageDo po2Do(SagaMessagePo sagaMessagePo);

    /**
     * 警察乙签证官
     *
     * @param sagaMessagePo 传奇消息阿宝
     * @return {@link SagaMessageVo}
     */
    SagaMessageVo po2Vo(SagaMessagePo sagaMessagePo);

    /**
     * 警察乙做列表
     *
     * @param sagaMessagePoList 传奇消息订单列表
     * @return {@link List}<{@link SagaMessageDo}>
     */
    List<SagaMessageDo> po2DoList(List<SagaMessagePo> sagaMessagePoList);

    /**
     * 警察乙vo列表
     *
     * @param sagaMessagePoList 传奇消息订单列表
     * @return {@link List}<{@link SagaMessageVo}>
     */
    List<SagaMessageVo> po2VoList(List<SagaMessagePo> sagaMessagePoList);


}

