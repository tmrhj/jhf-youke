package com.jhf.youke.saga.client.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.saga.client.domain.model.po.SagaMessagePo;

/**
 * 传奇消息存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface SagaMessageRepository extends Repository<SagaMessagePo> {


    /**
     * 通过商业
     *
     * @param code     代码
     * @param bizId    商业标识
     * @param objectId 对象id
     * @return {@link SagaMessagePo}
     */
    SagaMessagePo getByBiz(String code, Long bizId, Integer objectId);

}

