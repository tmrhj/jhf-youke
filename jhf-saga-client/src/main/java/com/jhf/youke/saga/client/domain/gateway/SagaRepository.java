package com.jhf.youke.saga.client.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.saga.client.domain.model.po.SagaPo;

/**
 * 事件存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface SagaRepository extends Repository<SagaPo> {

      /**
       * 获取业务配置
       * @param code     代码
       * @param bizId    商业标识
       * @param objectId 对象id
       * @param sort     排序
       * @return {@link SagaPo}
       */
      SagaPo getByBiz(String code, Long bizId, Integer objectId, Integer sort);

      /**
       * 更新商业
       *
       * @param sagaPo 传奇阿宝
       * @return int
       */
      int updateByBiz(SagaPo sagaPo);



}

