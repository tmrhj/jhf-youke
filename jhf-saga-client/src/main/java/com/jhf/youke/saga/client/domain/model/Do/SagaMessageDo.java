package com.jhf.youke.saga.client.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.saga.client.domain.exception.SagaMessageException;
import lombok.Data;

import java.util.Map;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class SagaMessageDo extends BaseDoEntity {

  private static final long serialVersionUID = 615330436512787113L;

    /** 主表ID  **/
    private String code;

    /** 业务对象标识  **/
    private Integer objectId;

    /** 业务标识  **/
    private Long bizId;

    /** 消息内容  **/
    private String message;

    public SagaMessageDo(){

    }

    public SagaMessageDo(Map<String, Object> map) {
        this.bizId = StringUtils.toLong(map.get("bizId"));
        this.objectId = StringUtils.toInteger(map.get("objectId"));
        this.code = StringUtils.chgNull(map.get("code"));
        this.message = StringUtils.chgNull(map.get("message"));

    }


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new SagaMessageException(errorMessage);
        }
        return obj;
    }

    private SagaMessageDo validateNull(SagaMessageDo sagaMessageDo){
          //可使用链式法则进行为空检查
          sagaMessageDo.
          requireNonNull(sagaMessageDo, sagaMessageDo.getRemark(),"不能为NULL");
                
        return sagaMessageDo;
    }


}


