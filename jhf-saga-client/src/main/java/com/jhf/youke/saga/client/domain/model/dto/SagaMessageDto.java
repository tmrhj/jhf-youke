package com.jhf.youke.saga.client.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class SagaMessageDto extends BaseDtoEntity {

    private static final long serialVersionUID = 880435647026821599L;


    @ApiModelProperty(value = "主表ID")
    private String code;

    @ApiModelProperty(value = "业务对象标识")
    private Integer objectId;

    @ApiModelProperty(value = "业务标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "消息内容")
    private String message;



}


