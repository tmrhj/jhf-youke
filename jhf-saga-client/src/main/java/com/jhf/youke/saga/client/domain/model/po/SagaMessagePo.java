package com.jhf.youke.saga.client.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sag_saga_message")
public class SagaMessagePo extends BasePoEntity {

    private static final long serialVersionUID = 825924568312634890L;

    /** 主表ID  **/
    private String code;

    /** 业务对象标识  **/
    private Integer objectId;

    /** 业务标识  **/
    private Long bizId;

    /** 消息内容  **/
    private String message;



}


