package com.jhf.youke.saga.client.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sag_saga")
public class SagaPo extends BasePoEntity {

    private static final long serialVersionUID = 700081984615567606L;

    /** 编码  **/
    private String code;

    /** 业务对象  **/
    private Integer objectId;

    /** 业务标识  **/
    private Long bizId;

    /** 主题  **/
    private String topic;

    /** 处理次数  **/
    private Integer count;

    /** 正常顺序  **/
    private Integer sort;

    /** mq消息Id  **/
    private String messageId;

    /** 回滚顺序  **/
    private Integer backSort;

    /** 0 不是 1 是  **/
    private String ifCurrent;

    /** 是否回滚  **/
    private String ifBack;

    /** 是否终止节点  **/
    private String ifEnd;

    /** 0 未开始 1 当前操作 2 发送完成 3 发送失败 4. 消费完成 5 消费失败  **/
    private Integer status;



}


