package com.jhf.youke.saga.client.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class SagaMessageVo extends BaseVoEntity {

    private static final long serialVersionUID = -52702864911832400L;



    @ApiModelProperty(value = "主表ID")
    private String code;

    @ApiModelProperty(value = "业务对象标识")
    private Integer objectId;

    @ApiModelProperty(value = "业务标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "消息内容")
    private String message;



}


