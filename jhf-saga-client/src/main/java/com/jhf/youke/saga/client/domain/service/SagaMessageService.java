package com.jhf.youke.saga.client.domain.service;

import com.jhf.youke.saga.client.domain.converter.SagaMessageConverter;
import com.jhf.youke.saga.client.domain.model.Do.SagaMessageDo;
import com.jhf.youke.saga.client.domain.gateway.SagaMessageRepository;
import com.jhf.youke.saga.client.domain.model.po.SagaMessagePo;
import com.jhf.youke.saga.client.domain.model.vo.SagaMessageVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class SagaMessageService extends AbstractDomainService<SagaMessageRepository, SagaMessageDo, SagaMessageVo> {


    @Resource
    SagaMessageConverter sagaMessageConverter;

    @Override
    public boolean update(SagaMessageDo entity) {
        SagaMessagePo sagaMessagePo = sagaMessageConverter.do2Po(entity);
        sagaMessagePo.preUpdate();
        return repository.update(sagaMessagePo);
    }

    @Override
    public boolean updateBatch(List<SagaMessageDo> doList) {
        List<SagaMessagePo> poList = sagaMessageConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(SagaMessageDo entity) {
        SagaMessagePo sagaMessagePo = sagaMessageConverter.do2Po(entity);
        return repository.delete(sagaMessagePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(SagaMessageDo entity) {
        SagaMessagePo sagaMessagePo = sagaMessageConverter.do2Po(entity);
        sagaMessagePo.preInsert();
        return repository.insert(sagaMessagePo);
    }

    @Override
    public boolean insertBatch(List<SagaMessageDo> doList) {
        List<SagaMessagePo> poList = sagaMessageConverter.do2PoList(doList);
        poList = SagaMessagePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<SagaMessageVo> findById(Long id) {
        Optional<SagaMessagePo> sagaMessagePo =  repository.findById(id);
        SagaMessageVo sagaMessageVo = sagaMessageConverter.po2Vo(sagaMessagePo.orElse(new SagaMessagePo()));
        return Optional.ofNullable(sagaMessageVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<SagaMessageVo> findAllMatching(SagaMessageDo entity) {
        SagaMessagePo sagaMessagePo = sagaMessageConverter.do2Po(entity);
        List<SagaMessagePo>sagaMessagePoList =  repository.findAllMatching(sagaMessagePo);
        return sagaMessageConverter.po2VoList(sagaMessagePoList);
    }


    @Override
    public Pagination<SagaMessageVo> selectPage(SagaMessageDo entity){
        SagaMessagePo sagaMessagePo = sagaMessageConverter.do2Po(entity);
        PageQuery<SagaMessagePo> pageQuery = new PageQuery<>(sagaMessagePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<SagaMessagePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                sagaMessageConverter.po2VoList(pagination.getList()));
    }


}

