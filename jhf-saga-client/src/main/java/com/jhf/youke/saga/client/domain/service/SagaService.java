package com.jhf.youke.saga.client.domain.service;

import com.jhf.youke.saga.client.domain.converter.SagaConverter;
import com.jhf.youke.saga.client.domain.model.Do.SagaDo;
import com.jhf.youke.saga.client.domain.gateway.SagaRepository;
import com.jhf.youke.saga.client.domain.model.po.SagaPo;
import com.jhf.youke.saga.client.domain.model.vo.SagaVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class SagaService extends AbstractDomainService<SagaRepository, SagaDo, SagaVo> {


    @Resource
    SagaConverter sagaConverter;

    @Override
    public boolean update(SagaDo entity) {
        SagaPo sagaPo = sagaConverter.do2Po(entity);
        sagaPo.preUpdate();
        return repository.update(sagaPo);
    }

    @Override
    public boolean updateBatch(List<SagaDo> doList) {
        List<SagaPo> poList = sagaConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(SagaDo entity) {
        SagaPo sagaPo = sagaConverter.do2Po(entity);
        return repository.delete(sagaPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(SagaDo entity) {
        SagaPo sagaPo = sagaConverter.do2Po(entity);
        sagaPo.preInsert();
        return repository.insert(sagaPo);
    }

    @Override
    public boolean insertBatch(List<SagaDo> doList) {
        List<SagaPo> poList = sagaConverter.do2PoList(doList);
        poList = SagaPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<SagaVo> findById(Long id) {
        Optional<SagaPo> sagaPo =  repository.findById(id);
        SagaVo sagaVo = sagaConverter.po2Vo(sagaPo.orElse(new SagaPo()));
        return Optional.ofNullable(sagaVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<SagaVo> findAllMatching(SagaDo entity) {
        SagaPo sagaPo = sagaConverter.do2Po(entity);
        List<SagaPo>sagaPoList =  repository.findAllMatching(sagaPo);
        return sagaConverter.po2VoList(sagaPoList);
    }


    @Override
    public Pagination<SagaVo> selectPage(SagaDo entity){
        SagaPo sagaPo = sagaConverter.do2Po(entity);
        PageQuery<SagaPo> pageQuery = new PageQuery<>(sagaPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<SagaPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                sagaConverter.po2VoList(pagination.getList()));
    }


}

