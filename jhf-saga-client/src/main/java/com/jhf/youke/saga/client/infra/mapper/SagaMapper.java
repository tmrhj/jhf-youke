package com.jhf.youke.saga.client.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.saga.client.domain.model.po.SagaPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 传奇映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface SagaMapper  extends BaseMapper<SagaPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sag_saga set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sag_saga set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 通过商业
     *
     * @param code     代码
     * @param bizId    商业标识
     * @param objectId 对象id
     * @param sort     排序
     * @return {@link SagaPo}
     */
    SagaPo getByBiz(@Param("code")String code, @Param("bizId")Long bizId,
                    @Param("objectId")Integer objectId , @Param("sort")Integer sort);

    /**
     * 更新商业
     *
     * @param sagaPo 传奇阿宝
     * @return int
     */
    int updateByBiz(SagaPo sagaPo);

}

