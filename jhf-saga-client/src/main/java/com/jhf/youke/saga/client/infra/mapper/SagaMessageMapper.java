package com.jhf.youke.saga.client.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.saga.client.domain.model.po.SagaMessagePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 传奇消息映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface SagaMessageMapper  extends BaseMapper<SagaMessagePo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sag_saga_message set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sag_saga_message set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 通过商业
     *
     * @param code     代码
     * @param bizId    商业标识
     * @param objectId 对象id
     * @return {@link SagaMessagePo}
     */
    SagaMessagePo getByBiz(@Param("code")String code, @Param("bizId")Long bizId, @Param("objectId")Integer objectId);

}

