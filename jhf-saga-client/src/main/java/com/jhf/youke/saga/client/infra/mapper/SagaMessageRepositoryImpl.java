package com.jhf.youke.saga.client.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.client.domain.exception.SagaMessageException;
import com.jhf.youke.saga.client.domain.gateway.SagaMessageRepository;
import com.jhf.youke.saga.client.domain.model.po.SagaMessagePo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "SagaMessageRepository")
public class SagaMessageRepositoryImpl extends ServiceImpl<SagaMessageMapper, SagaMessagePo> implements SagaMessageRepository {


    @Override
    public boolean update(SagaMessagePo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<SagaMessagePo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(SagaMessagePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(SagaMessagePo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<SagaMessagePo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<SagaMessagePo> findById(Long id) {
        SagaMessagePo sagaMessagePo = baseMapper.selectById(id);
        return Optional.ofNullable(sagaMessagePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new SagaMessageException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<SagaMessagePo> findAllMatching(SagaMessagePo entity) {
        QueryWrapper<SagaMessagePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<SagaMessagePo> selectPage(PageQuery<SagaMessagePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<SagaMessagePo> sagaMessagePos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<SagaMessagePo> pageInfo = new PageInfo<>(sagaMessagePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public SagaMessagePo getByBiz(String code, Long bizId, Integer objectId) {
        return baseMapper.getByBiz(code,bizId,objectId);
    }
}

