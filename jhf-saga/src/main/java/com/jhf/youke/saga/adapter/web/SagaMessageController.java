package com.jhf.youke.saga.adapter.web;

import com.jhf.youke.saga.app.executor.SagaMessageAppService;
import com.jhf.youke.saga.domain.model.dto.SagaMessageDto;
import com.jhf.youke.saga.domain.model.vo.SagaMessageVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 * **/
@Api(tags = "")
@RestController
@RequestMapping("/sagaMessage")
public class SagaMessageController {

    @Resource
    private SagaMessageAppService sagaMessageAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody SagaMessageDto dto) {
        return Response.ok(sagaMessageAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody SagaMessageDto dto) {
        return Response.ok(sagaMessageAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody SagaMessageDto dto) {
        return Response.ok(sagaMessageAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<SagaMessageVo>> findById(Long id) {
        return Response.ok(sagaMessageAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(sagaMessageAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<SagaMessageVo>> list(@RequestBody SagaMessageDto dto, @RequestHeader("token") String token) {

        return Response.ok(sagaMessageAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<SagaMessageVo>> selectPage(@RequestBody  SagaMessageDto dto, @RequestHeader("token") String token) {
        return Response.ok(sagaMessageAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(sagaMessageAppService.getPreData());
    }
    

}

