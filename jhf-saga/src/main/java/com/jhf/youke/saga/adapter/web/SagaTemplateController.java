package com.jhf.youke.saga.adapter.web;

import com.jhf.youke.saga.app.executor.SagaTemplateAppService;
import com.jhf.youke.saga.domain.model.dto.SagaTemplateDto;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/sagaTemplate")
public class SagaTemplateController {

    @Resource
    private SagaTemplateAppService sagaTemplateAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody SagaTemplateDto dto) {
        return Response.ok(sagaTemplateAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody SagaTemplateDto dto) {
        return Response.ok(sagaTemplateAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody SagaTemplateDto dto) {
        return Response.ok(sagaTemplateAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<SagaTemplateVo>> findById(Long id) {
        return Response.ok(sagaTemplateAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(sagaTemplateAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<SagaTemplateVo>> list(@RequestBody SagaTemplateDto dto, @RequestHeader("token") String token) {

        return Response.ok(sagaTemplateAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<SagaTemplateVo>> selectPage(@RequestBody  SagaTemplateDto dto, @RequestHeader("token") String token) {
        return Response.ok(sagaTemplateAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(sagaTemplateAppService.getPreData());
    }
    

}

