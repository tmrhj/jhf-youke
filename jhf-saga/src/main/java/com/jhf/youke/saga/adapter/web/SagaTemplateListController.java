package com.jhf.youke.saga.adapter.web;

import com.jhf.youke.saga.app.executor.SagaTemplateListAppService;
import com.jhf.youke.saga.domain.model.dto.SagaTemplateListDto;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateListVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/sagaTemplateList")
public class SagaTemplateListController {

    @Resource
    private SagaTemplateListAppService sagaTemplateListAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody SagaTemplateListDto dto) {
        return Response.ok(sagaTemplateListAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody SagaTemplateListDto dto) {
        return Response.ok(sagaTemplateListAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody SagaTemplateListDto dto) {
        return Response.ok(sagaTemplateListAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<SagaTemplateListVo>> findById(Long id) {
        return Response.ok(sagaTemplateListAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(sagaTemplateListAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<SagaTemplateListVo>> list(@RequestBody SagaTemplateListDto dto, @RequestHeader("token") String token) {

        return Response.ok(sagaTemplateListAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<SagaTemplateListVo>> selectPage(@RequestBody  SagaTemplateListDto dto, @RequestHeader("token") String token) {
        return Response.ok(sagaTemplateListAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(sagaTemplateListAppService.getPreData());
    }
    

}

