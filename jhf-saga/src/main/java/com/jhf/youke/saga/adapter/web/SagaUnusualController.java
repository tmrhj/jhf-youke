package com.jhf.youke.saga.adapter.web;

import com.jhf.youke.saga.app.executor.SagaUnusualAppService;
import com.jhf.youke.saga.domain.model.dto.SagaUnusualDto;
import com.jhf.youke.saga.domain.model.vo.SagaUnusualVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "")
@RestController
@RequestMapping("/sagaUnusual")
public class SagaUnusualController {

    @Resource
    private SagaUnusualAppService sagaUnusualAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody SagaUnusualDto dto) {
        return Response.ok(sagaUnusualAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody SagaUnusualDto dto) {
        return Response.ok(sagaUnusualAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody SagaUnusualDto dto) {
        return Response.ok(sagaUnusualAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<SagaUnusualVo>> findById(Long id) {
        return Response.ok(sagaUnusualAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(sagaUnusualAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<SagaUnusualVo>> list(@RequestBody SagaUnusualDto dto, @RequestHeader("token") String token) {

        return Response.ok(sagaUnusualAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<SagaUnusualVo>> selectPage(@RequestBody  SagaUnusualDto dto, @RequestHeader("token") String token) {
        return Response.ok(sagaUnusualAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(sagaUnusualAppService.getPreData());
    }
    

}

