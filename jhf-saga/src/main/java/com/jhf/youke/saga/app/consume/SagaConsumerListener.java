package com.jhf.youke.saga.app.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;
import java.util.function.Consumer;

/**
 * description:
 * date: 2022/9/23 14:12
 * @author: cyx
 */
@Slf4j
@Service
public class SagaConsumerListener {

    @Resource
    private SagaConsumerService sagaConsumerService;

    @Bean
    public Consumer<Message<Map<String, Object>>> sagaReply() {
        log.info("初始化消费者 sagaReplyConsume");
        return message -> {
              log.info("sagaReply 接收信息 {}",message.getPayload());
              sagaConsumerService.reply(message.getPayload());
        };
    }


    @Bean
    public Consumer<Message<Map<String, Object>>> createSaga() {
        log.info("初始化消费者 createSagaConsume");
        return message -> {
            log.info("createSaga info {}", message.getPayload());
            sagaConsumerService.create(message.getPayload());
        };
    }

}
