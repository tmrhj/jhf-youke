package com.jhf.youke.saga.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.converter.SagaConverter;
import com.jhf.youke.saga.domain.model.Do.SagaDo;
import com.jhf.youke.saga.domain.model.dto.SagaDto;
import com.jhf.youke.saga.domain.model.vo.SagaVo;
import com.jhf.youke.saga.domain.service.SagaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class SagaAppService extends BaseAppService {

    @Resource
    private SagaService sagaService;

    @Resource
    private SagaConverter sagaConverter;


    public boolean update(SagaDto dto) {
        SagaDo sagaDo =  sagaConverter.dto2Do(dto);
        return sagaService.update(sagaDo);
    }

    public boolean delete(SagaDto dto) {
        SagaDo sagaDo =  sagaConverter.dto2Do(dto);
        return sagaService.delete(sagaDo);
    }


    public boolean insert(SagaDto dto) {
        SagaDo sagaDo =  sagaConverter.dto2Do(dto);
        return sagaService.insert(sagaDo);
    }

    public Optional<SagaVo> findById(Long id) {
        return sagaService.findById(id);
    }


    public boolean remove(Long id) {
        return sagaService.remove(id);
    }


    public List<SagaVo> findAllMatching(SagaDto dto) {
        SagaDo sagaDo =  sagaConverter.dto2Do(dto);
        return sagaService.findAllMatching(sagaDo);
    }


    public Pagination<SagaVo> selectPage(SagaDto dto) {
        SagaDo sagaDo =  sagaConverter.dto2Do(dto);
        return sagaService.selectPage(sagaDo);
    }
    

        

}

