package com.jhf.youke.saga.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.converter.SagaMessageConverter;
import com.jhf.youke.saga.domain.model.Do.SagaMessageDo;
import com.jhf.youke.saga.domain.model.dto.SagaMessageDto;
import com.jhf.youke.saga.domain.model.vo.SagaMessageVo;
import com.jhf.youke.saga.domain.service.SagaMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class SagaMessageAppService extends BaseAppService {

    @Resource
    private SagaMessageService sagaMessageService;

    @Resource
    private SagaMessageConverter sagaMessageConverter;


    public boolean update(SagaMessageDto dto) {
        SagaMessageDo sagaMessageDo =  sagaMessageConverter.dto2Do(dto);
        return sagaMessageService.update(sagaMessageDo);
    }

    public boolean delete(SagaMessageDto dto) {
        SagaMessageDo sagaMessageDo =  sagaMessageConverter.dto2Do(dto);
        return sagaMessageService.delete(sagaMessageDo);
    }


    public boolean insert(SagaMessageDto dto) {
        SagaMessageDo sagaMessageDo =  sagaMessageConverter.dto2Do(dto);
        return sagaMessageService.insert(sagaMessageDo);
    }

    public Optional<SagaMessageVo> findById(Long id) {
        return sagaMessageService.findById(id);
    }


    public boolean remove(Long id) {
        return sagaMessageService.remove(id);
    }


    public List<SagaMessageVo> findAllMatching(SagaMessageDto dto) {
        SagaMessageDo sagaMessageDo =  sagaMessageConverter.dto2Do(dto);
        return sagaMessageService.findAllMatching(sagaMessageDo);
    }


    public Pagination<SagaMessageVo> selectPage(SagaMessageDto dto) {
        SagaMessageDo sagaMessageDo =  sagaMessageConverter.dto2Do(dto);
        return sagaMessageService.selectPage(sagaMessageDo);
    }
    

}

