package com.jhf.youke.saga.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.converter.SagaTemplateConverter;
import com.jhf.youke.saga.domain.model.Do.SagaTemplateDo;
import com.jhf.youke.saga.domain.model.dto.SagaTemplateDto;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateVo;
import com.jhf.youke.saga.domain.service.SagaTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class SagaTemplateAppService extends BaseAppService {

    @Resource
    private SagaTemplateService sagaTemplateService;

    @Resource
    private SagaTemplateConverter sagaTemplateConverter;


    public boolean update(SagaTemplateDto dto) {
        SagaTemplateDo sagaTemplateDo =  sagaTemplateConverter.dto2Do(dto);
        return sagaTemplateService.update(sagaTemplateDo);
    }

    public boolean delete(SagaTemplateDto dto) {
        SagaTemplateDo sagaTemplateDo =  sagaTemplateConverter.dto2Do(dto);
        return sagaTemplateService.delete(sagaTemplateDo);
    }


    public boolean insert(SagaTemplateDto dto) {
        SagaTemplateDo sagaTemplateDo =  sagaTemplateConverter.dto2Do(dto);
        return sagaTemplateService.insert(sagaTemplateDo);
    }

    public Optional<SagaTemplateVo> findById(Long id) {
        return sagaTemplateService.findById(id);
    }


    public boolean remove(Long id) {
        return sagaTemplateService.remove(id);
    }


    public List<SagaTemplateVo> findAllMatching(SagaTemplateDto dto) {
        SagaTemplateDo sagaTemplateDo =  sagaTemplateConverter.dto2Do(dto);
        return sagaTemplateService.findAllMatching(sagaTemplateDo);
    }


    public Pagination<SagaTemplateVo> selectPage(SagaTemplateDto dto) {
        SagaTemplateDo sagaTemplateDo =  sagaTemplateConverter.dto2Do(dto);
        return sagaTemplateService.selectPage(sagaTemplateDo);
    }


}

