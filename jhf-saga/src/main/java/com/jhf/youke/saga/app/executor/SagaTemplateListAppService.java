package com.jhf.youke.saga.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.converter.SagaTemplateListConverter;
import com.jhf.youke.saga.domain.model.Do.SagaTemplateListDo;
import com.jhf.youke.saga.domain.model.dto.SagaTemplateListDto;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateListVo;
import com.jhf.youke.saga.domain.service.SagaTemplateListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class SagaTemplateListAppService extends BaseAppService {

    @Resource
    private SagaTemplateListService sagaTemplateListService;

    @Resource
    private SagaTemplateListConverter sagaTemplateListConverter;


    public boolean update(SagaTemplateListDto dto) {
        SagaTemplateListDo sagaTemplateListDo =  sagaTemplateListConverter.dto2Do(dto);
        return sagaTemplateListService.update(sagaTemplateListDo);
    }

    public boolean delete(SagaTemplateListDto dto) {
        SagaTemplateListDo sagaTemplateListDo =  sagaTemplateListConverter.dto2Do(dto);
        return sagaTemplateListService.delete(sagaTemplateListDo);
    }


    public boolean insert(SagaTemplateListDto dto) {
        SagaTemplateListDo sagaTemplateListDo =  sagaTemplateListConverter.dto2Do(dto);
        return sagaTemplateListService.insert(sagaTemplateListDo);
    }

    public Optional<SagaTemplateListVo> findById(Long id) {
        return sagaTemplateListService.findById(id);
    }


    public boolean remove(Long id) {
        return sagaTemplateListService.remove(id);
    }


    public List<SagaTemplateListVo> findAllMatching(SagaTemplateListDto dto) {
        SagaTemplateListDo sagaTemplateListDo =  sagaTemplateListConverter.dto2Do(dto);
        return sagaTemplateListService.findAllMatching(sagaTemplateListDo);
    }


    public Pagination<SagaTemplateListVo> selectPage(SagaTemplateListDto dto) {
        SagaTemplateListDo sagaTemplateListDo =  sagaTemplateListConverter.dto2Do(dto);
        return sagaTemplateListService.selectPage(sagaTemplateListDo);
    }


}

