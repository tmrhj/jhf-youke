package com.jhf.youke.saga.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.converter.SagaUnusualConverter;
import com.jhf.youke.saga.domain.model.Do.SagaUnusualDo;
import com.jhf.youke.saga.domain.model.dto.SagaUnusualDto;
import com.jhf.youke.saga.domain.model.vo.SagaUnusualVo;
import com.jhf.youke.saga.domain.service.SagaUnusualService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class SagaUnusualAppService extends BaseAppService {

    @Resource
    private SagaUnusualService sagaUnusualService;

    @Resource
    private SagaUnusualConverter sagaUnusualConverter;


    public boolean update(SagaUnusualDto dto) {
        SagaUnusualDo sagaUnusualDo =  sagaUnusualConverter.dto2Do(dto);
        return sagaUnusualService.update(sagaUnusualDo);
    }

    public boolean delete(SagaUnusualDto dto) {
        SagaUnusualDo sagaUnusualDo =  sagaUnusualConverter.dto2Do(dto);
        return sagaUnusualService.delete(sagaUnusualDo);
    }


    public boolean insert(SagaUnusualDto dto) {
        SagaUnusualDo sagaUnusualDo =  sagaUnusualConverter.dto2Do(dto);
        return sagaUnusualService.insert(sagaUnusualDo);
    }

    public Optional<SagaUnusualVo> findById(Long id) {
        return sagaUnusualService.findById(id);
    }


    public boolean remove(Long id) {
        return sagaUnusualService.remove(id);
    }


    public List<SagaUnusualVo> findAllMatching(SagaUnusualDto dto) {
        SagaUnusualDo sagaUnusualDo =  sagaUnusualConverter.dto2Do(dto);
        return sagaUnusualService.findAllMatching(sagaUnusualDo);
    }


    public Pagination<SagaUnusualVo> selectPage(SagaUnusualDto dto) {
        SagaUnusualDo sagaUnusualDo =  sagaUnusualConverter.dto2Do(dto);
        return sagaUnusualService.selectPage(sagaUnusualDo);
    }


}

