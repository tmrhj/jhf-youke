package com.jhf.youke.saga.domain.converter;

import com.jhf.youke.saga.domain.model.Do.SagaDo;
import com.jhf.youke.saga.domain.model.dto.SagaDto;
import com.jhf.youke.saga.domain.model.po.SagaPo;
import com.jhf.youke.saga.domain.model.po.SagaUnusualPo;
import com.jhf.youke.saga.domain.model.vo.SagaVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.Collection;
import java.util.List;


/**
 * 传奇转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface SagaConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param sagaDto 传奇dto
     * @return {@link SagaDo}
     */
    SagaDo dto2Do(SagaDto sagaDto);

    /**
     * 洗阿宝
     *
     * @param sagaDo 传奇做
     * @return {@link SagaPo}
     */
    SagaPo do2Po(SagaDo sagaDo);

    /**
     * 洗签证官
     *
     * @param sagaDo 传奇做
     * @return {@link SagaVo}
     */
    SagaVo do2Vo(SagaDo sagaDo);

    /**
     * 洗不寻常阿宝
     *
     * @param sagaDo 传奇做
     * @return {@link SagaUnusualPo}
     */
    SagaUnusualPo do2UnusualPo(SagaDo sagaDo);

    /**
     * 洗订单列表
     *
     * @param sagaDoList 传奇做列表
     * @return {@link List}<{@link SagaPo}>
     */
    List<SagaPo> do2PoList(List<SagaDo> sagaDoList);

    /**
     * 警察乙做
     *
     * @param sagaPo 传奇阿宝
     * @return {@link SagaDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    SagaDo po2Do(SagaPo sagaPo);

    /**
     * 警察乙签证官
     *
     * @param sagaPo 传奇阿宝
     * @return {@link SagaVo}
     */
    SagaVo po2Vo(SagaPo sagaPo);

    /**
     * 警察乙做列表
     *
     * @param sagaPoList 传奇订单列表
     * @return {@link List}<{@link SagaDo}>
     */
    List<SagaDo> po2DoList(List<SagaPo> sagaPoList);

    /**
     * 警察乙vo列表
     *
     * @param sagaPoList 传奇订单列表
     * @return {@link List}<{@link SagaVo}>
     */
    List<SagaVo> po2VoList(List<SagaPo> sagaPoList);


}

