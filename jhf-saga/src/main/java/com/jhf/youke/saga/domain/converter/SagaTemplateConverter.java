package com.jhf.youke.saga.domain.converter;

import com.jhf.youke.saga.domain.model.Do.SagaTemplateDo;
import com.jhf.youke.saga.domain.model.dto.SagaTemplateDto;
import com.jhf.youke.saga.domain.model.po.SagaTemplatePo;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 传奇模板转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface SagaTemplateConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param sagaTemplateDto 传奇模板dto
     * @return {@link SagaTemplateDo}
     */
    SagaTemplateDo dto2Do(SagaTemplateDto sagaTemplateDto);

    /**
     * 洗阿宝
     *
     * @param sagaTemplateDo 传奇模板做
     * @return {@link SagaTemplatePo}
     */
    SagaTemplatePo do2Po(SagaTemplateDo sagaTemplateDo);

    /**
     * 洗签证官
     *
     * @param sagaTemplateDo 传奇模板做
     * @return {@link SagaTemplateVo}
     */
    SagaTemplateVo do2Vo(SagaTemplateDo sagaTemplateDo);


    /**
     * 洗订单列表
     *
     * @param sagaTemplateDoList 传奇模板做列表
     * @return {@link List}<{@link SagaTemplatePo}>
     */
    List<SagaTemplatePo> do2PoList(List<SagaTemplateDo> sagaTemplateDoList);

    /**
     * 警察乙做
     *
     * @param sagaTemplatePo 传奇模板阿宝
     * @return {@link SagaTemplateDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    SagaTemplateDo po2Do(SagaTemplatePo sagaTemplatePo);

    /**
     * 警察乙签证官
     *
     * @param sagaTemplatePo 传奇模板阿宝
     * @return {@link SagaTemplateVo}
     */
    SagaTemplateVo po2Vo(SagaTemplatePo sagaTemplatePo);

    /**
     * 警察乙做列表
     *
     * @param sagaTemplatePoList 阿宝传奇模板列表
     * @return {@link List}<{@link SagaTemplateDo}>
     */
    List<SagaTemplateDo> po2DoList(List<SagaTemplatePo> sagaTemplatePoList);

    /**
     * 警察乙vo列表
     *
     * @param sagaTemplatePoList 阿宝传奇模板列表
     * @return {@link List}<{@link SagaTemplateVo}>
     */
    List<SagaTemplateVo> po2VoList(List<SagaTemplatePo> sagaTemplatePoList);


}

