package com.jhf.youke.saga.domain.converter;

import com.jhf.youke.saga.domain.model.Do.SagaTemplateListDo;
import com.jhf.youke.saga.domain.model.dto.SagaTemplateListDto;
import com.jhf.youke.saga.domain.model.po.SagaTemplateListPo;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateListVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 传奇模板列表转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface SagaTemplateListConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param sagaTemplateListDto 传奇dto模板列表
     * @return {@link SagaTemplateListDo}
     */
    SagaTemplateListDo dto2Do(SagaTemplateListDto sagaTemplateListDto);

    /**
     * 洗阿宝
     *
     * @param sagaTemplateListDo 传奇模板列表做
     * @return {@link SagaTemplateListPo}
     */
    SagaTemplateListPo do2Po(SagaTemplateListDo sagaTemplateListDo);

    /**
     * 洗签证官
     *
     * @param sagaTemplateListDo 传奇模板列表做
     * @return {@link SagaTemplateListVo}
     */
    SagaTemplateListVo do2Vo(SagaTemplateListDo sagaTemplateListDo);


    /**
     * 洗订单列表
     *
     * @param sagaTemplateListDoList 传奇模板列表做列表
     * @return {@link List}<{@link SagaTemplateListPo}>
     */
    List<SagaTemplateListPo> do2PoList(List<SagaTemplateListDo> sagaTemplateListDoList);

    /**
     * 警察乙做
     *
     * @param sagaTemplateListPo 阿宝传奇模板列表
     * @return {@link SagaTemplateListDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    SagaTemplateListDo po2Do(SagaTemplateListPo sagaTemplateListPo);

    /**
     * 警察乙签证官
     *
     * @param sagaTemplateListPo 阿宝传奇模板列表
     * @return {@link SagaTemplateListVo}
     */
    SagaTemplateListVo po2Vo(SagaTemplateListPo sagaTemplateListPo);

    /**
     * 警察乙做列表
     *
     * @param sagaTemplateListPoList 传奇模板列表订单列表
     * @return {@link List}<{@link SagaTemplateListDo}>
     */
    List<SagaTemplateListDo> po2DoList(List<SagaTemplateListPo> sagaTemplateListPoList);

    /**
     * 警察乙vo列表
     *
     * @param sagaTemplateListPoList 传奇模板列表订单列表
     * @return {@link List}<{@link SagaTemplateListVo}>
     */
    List<SagaTemplateListVo> po2VoList(List<SagaTemplateListPo> sagaTemplateListPoList);


}

