package com.jhf.youke.saga.domain.converter;

import com.jhf.youke.saga.domain.model.Do.SagaUnusualDo;
import com.jhf.youke.saga.domain.model.dto.SagaUnusualDto;
import com.jhf.youke.saga.domain.model.po.SagaUnusualPo;
import com.jhf.youke.saga.domain.model.vo.SagaUnusualVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 传奇不寻常转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface SagaUnusualConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param sagaUnusualDto 传奇不寻常dto
     * @return {@link SagaUnusualDo}
     */
    SagaUnusualDo dto2Do(SagaUnusualDto sagaUnusualDto);

    /**
     * 洗阿宝
     *
     * @param sagaUnusualDo 传奇不寻常做
     * @return {@link SagaUnusualPo}
     */
    SagaUnusualPo do2Po(SagaUnusualDo sagaUnusualDo);

    /**
     * 洗签证官
     *
     * @param sagaUnusualDo 传奇不寻常做
     * @return {@link SagaUnusualVo}
     */
    SagaUnusualVo do2Vo(SagaUnusualDo sagaUnusualDo);


    /**
     * 洗订单列表
     *
     * @param sagaUnusualDoList 传奇不寻常任务清单
     * @return {@link List}<{@link SagaUnusualPo}>
     */
    List<SagaUnusualPo> do2PoList(List<SagaUnusualDo> sagaUnusualDoList);

    /**
     * 警察乙做
     *
     * @param sagaUnusualPo 传奇不寻常阿宝
     * @return {@link SagaUnusualDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    SagaUnusualDo po2Do(SagaUnusualPo sagaUnusualPo);

    /**
     * 警察乙签证官
     *
     * @param sagaUnusualPo 传奇不寻常阿宝
     * @return {@link SagaUnusualVo}
     */
    SagaUnusualVo po2Vo(SagaUnusualPo sagaUnusualPo);

    /**
     * 警察乙做列表
     *
     * @param sagaUnusualPoList 传奇不寻常订单列表
     * @return {@link List}<{@link SagaUnusualDo}>
     */
    List<SagaUnusualDo> po2DoList(List<SagaUnusualPo> sagaUnusualPoList);

    /**
     * 警察乙vo列表
     *
     * @param sagaUnusualPoList 传奇不寻常订单列表
     * @return {@link List}<{@link SagaUnusualVo}>
     */
    List<SagaUnusualVo> po2VoList(List<SagaUnusualPo> sagaUnusualPoList);


}

