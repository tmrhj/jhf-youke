package com.jhf.youke.saga.domain.exception;


import com.jhf.youke.core.exception.DomainException;


/**
 * @author RHJ
 */
public class SagaException extends DomainException {

    public SagaException(String message) { super(message); }
}

