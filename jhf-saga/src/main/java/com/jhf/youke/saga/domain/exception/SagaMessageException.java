package com.jhf.youke.saga.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class SagaMessageException extends DomainException {

    public SagaMessageException(String message) { super(message); }
}

