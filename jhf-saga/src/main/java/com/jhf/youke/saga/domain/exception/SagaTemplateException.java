package com.jhf.youke.saga.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class SagaTemplateException extends DomainException {

    public SagaTemplateException(String message) { super(message); }
}

