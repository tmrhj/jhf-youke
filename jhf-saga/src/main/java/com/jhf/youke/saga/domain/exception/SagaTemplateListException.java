package com.jhf.youke.saga.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class SagaTemplateListException extends DomainException {

    public SagaTemplateListException(String message) { super(message); }
}

