package com.jhf.youke.saga.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class SagaUnusualException extends DomainException {

    public SagaUnusualException(String message) { super(message); }
}

