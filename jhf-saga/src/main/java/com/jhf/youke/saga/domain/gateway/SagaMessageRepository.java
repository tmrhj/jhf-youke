package com.jhf.youke.saga.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.saga.domain.model.po.SagaMessagePo;

/**
 * 传奇消息存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface SagaMessageRepository extends Repository<SagaMessagePo> {


    /**
     * 得到消息
     *
     * @param code     代码
     * @param bizId    商业标识
     * @param objectId 对象id
     * @return {@link SagaMessagePo}
     */
    SagaMessagePo getMessage(String code, Long bizId, Integer objectId);

}

