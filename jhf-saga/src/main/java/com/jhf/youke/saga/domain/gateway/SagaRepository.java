package com.jhf.youke.saga.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.saga.domain.model.po.SagaPo;

import java.util.List;
import java.util.Map;

/**
 * 事件存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface SagaRepository extends Repository<SagaPo> {

    /**
     * 得到下一个列表
     *
     * @param map 地图
     * @return {@link List}<{@link SagaPo}>
     */
    List<SagaPo> getNextList(Map<String, Object> map);

    /**
     * 被商业列表
     *
     * @param code     代码
     * @param bizId    商业标识
     * @param objectId 对象id
     * @return {@link List}<{@link SagaPo}>
     */
    List<SagaPo> getListByBiz(String code, Long bizId, Integer objectId);

    /**
     * 更新商业
     *
     * @param sagaPo 传奇阿宝
     * @return {@link Boolean}
     */
    Boolean updateByBiz(SagaPo sagaPo);

}

