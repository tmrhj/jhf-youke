package com.jhf.youke.saga.domain.gateway;


import com.jhf.youke.saga.domain.model.po.SagaTemplateListPo;
import com.jhf.youke.core.ddd.Repository;

import java.util.List;

/**
 * 传奇模板库列表
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface SagaTemplateListRepository extends Repository<SagaTemplateListPo> {


    /**
     * 得到模板列表
     *
     * @param code 代码
     * @return {@link List}<{@link SagaTemplateListPo}>
     */
    List<SagaTemplateListPo> getTemplateList(String code);

}

