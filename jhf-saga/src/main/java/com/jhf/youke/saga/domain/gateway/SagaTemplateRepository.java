package com.jhf.youke.saga.domain.gateway;


import com.jhf.youke.saga.domain.model.po.SagaTemplatePo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface SagaTemplateRepository extends Repository<SagaTemplatePo> {


}

