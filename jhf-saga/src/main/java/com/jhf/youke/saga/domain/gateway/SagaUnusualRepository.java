package com.jhf.youke.saga.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.saga.domain.model.po.SagaUnusualPo;

/**
 * @author RHJ
 */
public interface SagaUnusualRepository extends Repository<SagaUnusualPo> {



}

