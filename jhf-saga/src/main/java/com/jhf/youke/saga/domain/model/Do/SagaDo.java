package com.jhf.youke.saga.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.entity.SagaMsg;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.saga.domain.exception.SagaException;
import lombok.Data;

import java.util.Date;
import java.util.Map;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class SagaDo extends BaseDoEntity {

    private static final long serialVersionUID = -96988114650170017L;

    public static final String IS_CURRENT = "1";

    public static final String IS_END = "1";

    public static final String IS_NOT_CURRENT ="0";
    /** saga创建 **/
    public static final Integer SAGA_CREATE = 0;
    /** 发送MQ成功 **/
    public static final Integer SEND_OK = 1;
    /** 发送MQ失败 **/
    public static final Integer SEND_FAIL = 2;
    /** 业务方接收成功 **/
    public static final Integer RECEIVE_OK = 3;
    /** 消费成功 **/
    public static final Integer CONSUME_OK = 4;
    /** 消费失败 **/
    public static final Integer CONSUME_FAIL = 5;
    /** 消费成功后，发送Mq成功 **/
    public static final Integer CONSUME_SEND_OK = 6;
    /** 消费成功后，发送Mq失败 **/
    public static final Integer CONSUME_SEND_FAIL = 7;
    /** 应答OK **/
    public static final Integer REPLY_OK = 8;
    /** 应答失败 **/
    public static final Integer REPLY_FAIL = 9;

    /** 编码 **/
    private String code;

    /** 业务对象 **/
    private Integer objectId;

    /** 业务标识 **/
    private Long bizId;

    /** 主题 **/
    private String topic;

    /** 处理次数 **/
    private Integer count;

    /** 正常顺序 **/
    private Integer sort;

    /** mq消息Id **/
    private String messageId;

    /** 回滚顺序 **/
    private Integer backSort;

    /** 0 不是 1 是 **/
    private String ifCurrent;

    /** 是否回滚 **/
    private String ifBack;

    /** 是否终止节点 **/
    private String ifEnd;

    /** 0 未开始 1 当前操作 2 发送完成 3 发送失败 4. 消费完成 5 消费失败 **/
    private Integer status;

    public SagaDo(){

    }

    public SagaDo(Map<String, Object> map) {
      this.sort =  StringUtils.toInteger(map.get("sort"));
      this.topic = StringUtils.chgNull(map.get("topic"));
      this.code = StringUtils.chgNull(map.get("code"));
      this.bizId = StringUtils.toLong(map.get("bizId"));
      this.objectId = StringUtils.toInteger(map.get("objectId"));
      this.status =  StringUtils.toInteger(map.get("status"));

    }


  public SagaDo(SagaMsg msg, SagaTemplateListDo template) {
    if(template.getSort() == 1){
      this.ifCurrent = IS_CURRENT;
    }else{
      this.ifCurrent = IS_NOT_CURRENT;
    }
    this.code = msg.getCode();
    this.objectId = msg.getObjectId();
    this.bizId = msg.getBizId();
    this.topic = template.getTopic();
    this.sort = template.getSort();
    this.count = 0;
    this.status = 0;
    this.ifBack = "0";
    this.ifEnd = template.getIfEnd();
    this.backSort = template.getBackSort();
    this.createTime = new Date();
    this.updateTime = new Date();
  }



  public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new SagaException(errorMessage);
        }
        return obj;
    }

    private SagaDo validateNull(SagaDo sagaDo){
          // 可使用链式法则进行为空检查
          sagaDo.
          requireNonNull(sagaDo, sagaDo.getRemark(),"不能为NULL");
                
        return sagaDo;
    }


}


