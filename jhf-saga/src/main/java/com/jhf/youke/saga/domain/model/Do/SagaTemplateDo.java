package com.jhf.youke.saga.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.saga.domain.exception.SagaTemplateException;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class SagaTemplateDo extends BaseDoEntity {

  private static final long serialVersionUID = -58486058390830812L;

    /** 名字  **/
    private String name;

    /** 编码  **/
    private String code;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new SagaTemplateException(errorMessage);
        }
        return obj;
    }

    private SagaTemplateDo validateNull(SagaTemplateDo sagaTemplateDo){
          // 可使用链式法则进行为空检查
          sagaTemplateDo.
          requireNonNull(sagaTemplateDo, sagaTemplateDo.getRemark(),"不能为NULL");
                
        return sagaTemplateDo;
    }


}


