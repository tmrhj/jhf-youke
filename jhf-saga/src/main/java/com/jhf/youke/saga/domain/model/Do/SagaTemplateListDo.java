package com.jhf.youke.saga.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.saga.domain.exception.SagaTemplateListException;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class SagaTemplateListDo extends BaseDoEntity {

  private static final long serialVersionUID = 728654753820295361L;

    /** 编码  **/
    private String code;

    /** 模板ID  **/
    private Long sagaTemplateId;

    /** 服务名  **/
    private String serviceName;

    /** 主题  **/
    private String topic;

    /** 回滚主题  **/
    private String backTopic;

    /** 顺序  **/
    private Integer sort;

    /** 回滚顺序  **/
    private Integer backSort;

    /** 是否终止节点  **/
    private String ifEnd;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new SagaTemplateListException(errorMessage);
        }
        return obj;
    }

    private SagaTemplateListDo validateNull(SagaTemplateListDo sagaTemplateListDo){
          // 可使用链式法则进行为空检查
          sagaTemplateListDo.
          requireNonNull(sagaTemplateListDo, sagaTemplateListDo.getRemark(),"不能为NULL");
                
        return sagaTemplateListDo;
    }


}


