package com.jhf.youke.saga.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.saga.domain.exception.SagaUnusualException;
import lombok.Data;

import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class SagaUnusualDo extends BaseDoEntity {

  private static final long serialVersionUID = -27135253340375302L;

    /** 业务对象  **/
    private Integer objectId;

    /** 业务标识  **/
    private Long bizId;

    /** 编码  **/
    private String code;

    /** 主题  **/
    private String topic;

    /** 顺序  **/
    private Integer sort;

    /** 操作次数  **/
    private Integer count;

    /** 异常状态  **/
    private Integer status;

    public SagaUnusualDo(){

    }

    public SagaUnusualDo(SagaDo sagaDo){
        this.objectId = sagaDo.getObjectId();
        this.bizId = sagaDo.getBizId();
        this.sort = sagaDo.getSort();
        this.status = sagaDo.getStatus();
        this.code = sagaDo.getCode();
        this.topic = sagaDo.getTopic();
        this.count = sagaDo.getCount();
    }

    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new SagaUnusualException(errorMessage);
        }
        return obj;
    }

    private SagaUnusualDo validateNull(SagaUnusualDo sagaUnusualDo){
          // 可使用链式法则进行为空检查
          sagaUnusualDo.
          requireNonNull(sagaUnusualDo, sagaUnusualDo.getRemark(),"不能为NULL");
                
        return sagaUnusualDo;
    }


}


