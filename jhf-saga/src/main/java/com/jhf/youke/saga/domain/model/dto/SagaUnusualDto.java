package com.jhf.youke.saga.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class SagaUnusualDto extends BaseDtoEntity {

    private static final long serialVersionUID = 231342829401595933L;


    @ApiModelProperty(value = "业务对象")
    private Integer objectId;

    @ApiModelProperty(value = "业务标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "主题")
    private String topic;

    @ApiModelProperty(value = "顺序")
    private Integer sort;

    @ApiModelProperty(value = "操作次数")
    private Integer count;

    @ApiModelProperty(value = "异常状态")
    private Integer status;



}


