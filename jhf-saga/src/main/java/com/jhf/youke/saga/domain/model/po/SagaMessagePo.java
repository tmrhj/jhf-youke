package com.jhf.youke.saga.domain.model.po;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import com.jhf.youke.core.entity.SagaMsg;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * 传奇消息阿宝
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sag_saga_message")
public class SagaMessagePo extends BasePoEntity {

    /**
     * 串行版本uid
     */
    private static final long serialVersionUID = -10570313443659366L;

    /**
     * 主表ID
     */
    private String code;

    /**
     * 业务对象标识
     */
    private Integer objectId;

    /**
     * 业务标识
     */
    private Long bizId;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 传奇消息阿宝
     */
    public SagaMessagePo() {

    }

    /**
     * 传奇消息阿宝
     *
     * @param msg 味精
     */
    public SagaMessagePo(SagaMsg msg) {
        this.code = msg.getCode();
        this.objectId = msg.getObjectId();
        this.bizId = msg.getBizId();
        this.message = JSONUtil.toJsonStr(msg.getMessage());

    }
}


