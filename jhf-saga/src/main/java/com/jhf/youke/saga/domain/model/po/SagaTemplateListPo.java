package com.jhf.youke.saga.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sag_saga_template_list")
public class SagaTemplateListPo extends BasePoEntity {

    private static final long serialVersionUID = 700862397085057415L;

    /** 编码  **/
    private String code;

    /** 模板ID  **/
    private Long sagaTemplateId;

    /** 服务名  **/
    private String serviceName;

    /** 主题  **/
    private String topic;

    /** 回滚主题  **/
    private String backTopic;

    /** 顺序  **/
    private Integer sort;

    /** 回滚顺序  **/
    private Integer backSort;

    /** 是否终止节点  **/
    private String ifEnd;



}


