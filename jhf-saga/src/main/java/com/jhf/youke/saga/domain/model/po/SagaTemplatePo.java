package com.jhf.youke.saga.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sag_saga_template")
public class SagaTemplatePo extends BasePoEntity {

    private static final long serialVersionUID = 758841695859803979L;

    /** 名字 **/
    private String name;

    /** 编码 **/
    private String code;



}


