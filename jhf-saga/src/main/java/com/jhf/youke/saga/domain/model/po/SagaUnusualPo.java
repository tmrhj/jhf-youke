package com.jhf.youke.saga.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sag_saga_unusual")
public class SagaUnusualPo extends BasePoEntity {

    private static final long serialVersionUID = -45042435512745968L;

    /** 业务对象  **/
    private Integer objectId;

    /** 业务标识  **/
    private Long bizId;

    /** 编码  **/
    private String code;

    /** 主题  **/
    private String topic;

    /** 顺序  **/
    private Integer sort;

    /** 操作次数  **/
    private Integer count;

    /** 异常状态  **/
    private Integer status;



}


