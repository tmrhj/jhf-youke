package com.jhf.youke.saga.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class SagaTemplateListVo extends BaseVoEntity {

    private static final long serialVersionUID = -14383141195955173L;


    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "模板ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long sagaTemplateId;

    @ApiModelProperty(value = "服务名")
    private String serviceName;

    @ApiModelProperty(value = "主题")
    private String topic;

    @ApiModelProperty(value = "回滚主题")
    private String backTopic;

    @ApiModelProperty(value = "顺序")
    private Integer sort;

    @ApiModelProperty(value = "回滚顺序")
    private Integer backSort;

    @ApiModelProperty(value = "是否终止节点")
    private String ifEnd;


}


