package com.jhf.youke.saga.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class SagaVo extends BaseVoEntity {

    private static final long serialVersionUID = 114987289473773605L;


    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "业务对象")
    private Integer objectId;

    @ApiModelProperty(value = "业务标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "主题")
    private String topic;

    @ApiModelProperty(value = "处理次数")
    private Integer count;

    @ApiModelProperty(value = "正常顺序")
    private Integer sort;

    @ApiModelProperty(value = "mq消息Id")
    private String messageId;

    @ApiModelProperty(value = "回滚顺序")
    private Integer backSort;

    @ApiModelProperty(value = "0 不是 1 是")
    private String ifCurrent;

    @ApiModelProperty(value = "是否回滚")
    private String ifBack;

    @ApiModelProperty(value = "是否终止节点")
    private String ifEnd;

    @ApiModelProperty(value = "0 未开始 1 当前操作 2 发送完成 3 发送失败 4. 消费完成 5 消费失败")
    private Integer status;


}


