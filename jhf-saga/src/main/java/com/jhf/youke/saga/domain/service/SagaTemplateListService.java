package com.jhf.youke.saga.domain.service;

import com.jhf.youke.saga.domain.converter.SagaTemplateListConverter;
import com.jhf.youke.saga.domain.model.Do.SagaTemplateListDo;
import com.jhf.youke.saga.domain.gateway.SagaTemplateListRepository;
import com.jhf.youke.saga.domain.model.po.SagaTemplateListPo;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateListVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class SagaTemplateListService extends AbstractDomainService<SagaTemplateListRepository, SagaTemplateListDo, SagaTemplateListVo> {


    @Resource
    SagaTemplateListConverter sagaTemplateListConverter;

    @Override
    public boolean update(SagaTemplateListDo entity) {
        SagaTemplateListPo sagaTemplateListPo = sagaTemplateListConverter.do2Po(entity);
        sagaTemplateListPo.preUpdate();
        return repository.update(sagaTemplateListPo);
    }

    @Override
    public boolean updateBatch(List<SagaTemplateListDo> doList) {
        List<SagaTemplateListPo> poList = sagaTemplateListConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(SagaTemplateListDo entity) {
        SagaTemplateListPo sagaTemplateListPo = sagaTemplateListConverter.do2Po(entity);
        return repository.delete(sagaTemplateListPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(SagaTemplateListDo entity) {
        SagaTemplateListPo sagaTemplateListPo = sagaTemplateListConverter.do2Po(entity);
        sagaTemplateListPo.preInsert();
        return repository.insert(sagaTemplateListPo);
    }

    @Override
    public boolean insertBatch(List<SagaTemplateListDo> doList) {
        List<SagaTemplateListPo> poList = sagaTemplateListConverter.do2PoList(doList);
        poList = SagaTemplateListPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<SagaTemplateListVo> findById(Long id) {
        Optional<SagaTemplateListPo> sagaTemplateListPo =  repository.findById(id);
        SagaTemplateListVo sagaTemplateListVo = sagaTemplateListConverter.po2Vo(sagaTemplateListPo.orElse(new SagaTemplateListPo()));
        return Optional.ofNullable(sagaTemplateListVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<SagaTemplateListVo> findAllMatching(SagaTemplateListDo entity) {
        SagaTemplateListPo sagaTemplateListPo = sagaTemplateListConverter.do2Po(entity);
        List<SagaTemplateListPo>sagaTemplateListPoList =  repository.findAllMatching(sagaTemplateListPo);
        return sagaTemplateListConverter.po2VoList(sagaTemplateListPoList);
    }


    @Override
    public Pagination<SagaTemplateListVo> selectPage(SagaTemplateListDo entity){
        SagaTemplateListPo sagaTemplateListPo = sagaTemplateListConverter.do2Po(entity);
        PageQuery<SagaTemplateListPo> pageQuery = new PageQuery<>(sagaTemplateListPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<SagaTemplateListPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                sagaTemplateListConverter.po2VoList(pagination.getList()));
    }


}

