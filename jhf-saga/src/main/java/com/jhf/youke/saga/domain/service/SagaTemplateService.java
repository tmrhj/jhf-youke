package com.jhf.youke.saga.domain.service;

import com.jhf.youke.saga.domain.converter.SagaTemplateConverter;
import com.jhf.youke.saga.domain.model.Do.SagaTemplateDo;
import com.jhf.youke.saga.domain.gateway.SagaTemplateRepository;
import com.jhf.youke.saga.domain.model.po.SagaTemplatePo;
import com.jhf.youke.saga.domain.model.vo.SagaTemplateVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;



/**
 * @author RHJ
 */
@Service
public class SagaTemplateService extends AbstractDomainService<SagaTemplateRepository, SagaTemplateDo, SagaTemplateVo> {


    @Resource
    SagaTemplateConverter sagaTemplateConverter;

    @Override
    public boolean update(SagaTemplateDo entity) {
        SagaTemplatePo sagaTemplatePo = sagaTemplateConverter.do2Po(entity);
        sagaTemplatePo.preUpdate();
        return repository.update(sagaTemplatePo);
    }

    @Override
    public boolean updateBatch(List<SagaTemplateDo> doList) {
        List<SagaTemplatePo> poList = sagaTemplateConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(SagaTemplateDo entity) {
        SagaTemplatePo sagaTemplatePo = sagaTemplateConverter.do2Po(entity);
        return repository.delete(sagaTemplatePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(SagaTemplateDo entity) {
        SagaTemplatePo sagaTemplatePo = sagaTemplateConverter.do2Po(entity);
        sagaTemplatePo.preInsert();
        return repository.insert(sagaTemplatePo);
    }

    @Override
    public boolean insertBatch(List<SagaTemplateDo> doList) {
        List<SagaTemplatePo> poList = sagaTemplateConverter.do2PoList(doList);
        poList = SagaTemplatePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<SagaTemplateVo> findById(Long id) {
        Optional<SagaTemplatePo> sagaTemplatePo =  repository.findById(id);
        SagaTemplateVo sagaTemplateVo = sagaTemplateConverter.po2Vo(sagaTemplatePo.orElse(new SagaTemplatePo()));
        return Optional.ofNullable(sagaTemplateVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<SagaTemplateVo> findAllMatching(SagaTemplateDo entity) {
        SagaTemplatePo sagaTemplatePo = sagaTemplateConverter.do2Po(entity);
        List<SagaTemplatePo>sagaTemplatePoList =  repository.findAllMatching(sagaTemplatePo);
        return sagaTemplateConverter.po2VoList(sagaTemplatePoList);
    }


    @Override
    public Pagination<SagaTemplateVo> selectPage(SagaTemplateDo entity){
        SagaTemplatePo sagaTemplatePo = sagaTemplateConverter.do2Po(entity);
        PageQuery<SagaTemplatePo> pageQuery = new PageQuery<>(sagaTemplatePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<SagaTemplatePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                sagaTemplateConverter.po2VoList(pagination.getList()));
    }


}

