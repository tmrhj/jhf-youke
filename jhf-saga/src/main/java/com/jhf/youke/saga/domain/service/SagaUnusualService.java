package com.jhf.youke.saga.domain.service;

import com.jhf.youke.saga.domain.converter.SagaUnusualConverter;
import com.jhf.youke.saga.domain.model.Do.SagaDo;
import com.jhf.youke.saga.domain.model.Do.SagaUnusualDo;
import com.jhf.youke.saga.domain.gateway.SagaUnusualRepository;
import com.jhf.youke.saga.domain.model.po.SagaUnusualPo;
import com.jhf.youke.saga.domain.model.vo.SagaUnusualVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class SagaUnusualService extends AbstractDomainService<SagaUnusualRepository, SagaUnusualDo, SagaUnusualVo> {


    @Resource
    SagaUnusualConverter sagaUnusualConverter;

    @Override
    public boolean update(SagaUnusualDo entity) {
        SagaUnusualPo sagaUnusualPo = sagaUnusualConverter.do2Po(entity);
        sagaUnusualPo.preUpdate();
        return repository.update(sagaUnusualPo);
    }

    @Override
    public boolean updateBatch(List<SagaUnusualDo> doList) {
        List<SagaUnusualPo> poList = sagaUnusualConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(SagaUnusualDo entity) {
        SagaUnusualPo sagaUnusualPo = sagaUnusualConverter.do2Po(entity);
        return repository.delete(sagaUnusualPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(SagaUnusualDo entity) {
        SagaUnusualPo sagaUnusualPo = sagaUnusualConverter.do2Po(entity);
        sagaUnusualPo.preInsert();
        return repository.insert(sagaUnusualPo);
    }

    @Override
    public boolean insertBatch(List<SagaUnusualDo> doList) {
        List<SagaUnusualPo> poList = sagaUnusualConverter.do2PoList(doList);
        poList = SagaUnusualPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<SagaUnusualVo> findById(Long id) {
        Optional<SagaUnusualPo> sagaUnusualPo =  repository.findById(id);
        SagaUnusualVo sagaUnusualVo = sagaUnusualConverter.po2Vo(sagaUnusualPo.orElse(new SagaUnusualPo()));
        return Optional.ofNullable(sagaUnusualVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<SagaUnusualVo> findAllMatching(SagaUnusualDo entity) {
        SagaUnusualPo sagaUnusualPo = sagaUnusualConverter.do2Po(entity);
        List<SagaUnusualPo>sagaUnusualPoList =  repository.findAllMatching(sagaUnusualPo);
        return sagaUnusualConverter.po2VoList(sagaUnusualPoList);
    }


    @Override
    public Pagination<SagaUnusualVo> selectPage(SagaUnusualDo entity){
        SagaUnusualPo sagaUnusualPo = sagaUnusualConverter.do2Po(entity);
        PageQuery<SagaUnusualPo> pageQuery = new PageQuery<>(sagaUnusualPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<SagaUnusualPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                sagaUnusualConverter.po2VoList(pagination.getList()));
    }


    public void saveBySaga(SagaDo saga) {
        SagaUnusualDo sagaUnusualDo = new SagaUnusualDo(saga);
        SagaUnusualPo sagaUnusualPo = sagaUnusualConverter.do2Po(sagaUnusualDo);
        sagaUnusualPo.preInsert();
        repository.insert(sagaUnusualPo);
    }
}

