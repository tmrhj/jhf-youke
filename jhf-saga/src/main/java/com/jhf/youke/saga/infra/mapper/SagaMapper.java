package com.jhf.youke.saga.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.saga.domain.model.po.SagaPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;


/**
 * 传奇映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface SagaMapper  extends BaseMapper<SagaPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sag_saga set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sag_saga set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 得到下一个列表
     *
     * @param map 地图
     * @return {@link List}<{@link SagaPo}>
     */
    List<SagaPo> getNextList(Map<String, Object> map);

    /**
     * 被商业列表
     *
     * @param code     代码
     * @param bizId    商业标识
     * @param objectId 对象id
     * @return {@link List}<{@link SagaPo}>
     */
    List<SagaPo> getListByBiz(@Param("code")String code,@Param("bizId") Long bizId, @Param("objectId")Integer objectId);

    /**
     * 更新商业
     *
     * @param sagaPo 传奇阿宝
     * @return int
     */
    int updateByBiz(SagaPo sagaPo);

}

