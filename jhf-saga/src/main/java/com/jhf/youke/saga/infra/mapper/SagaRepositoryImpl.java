package com.jhf.youke.saga.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.exception.SagaException;
import com.jhf.youke.saga.domain.gateway.SagaRepository;
import com.jhf.youke.saga.domain.model.po.SagaPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "SagaRepository")
public class SagaRepositoryImpl extends ServiceImpl<SagaMapper, SagaPo> implements SagaRepository {


    @Override
    public boolean update(SagaPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<SagaPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(SagaPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(SagaPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<SagaPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<SagaPo> findById(Long id) {
        SagaPo sagaPo = baseMapper.selectById(id);
        return Optional.ofNullable(sagaPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new SagaException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<SagaPo> findAllMatching(SagaPo entity) {
        QueryWrapper<SagaPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<SagaPo> selectPage(PageQuery<SagaPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<SagaPo> sagaPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<SagaPo> pageInfo = new PageInfo<>(sagaPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<SagaPo> getNextList(Map<String, Object> map) {
        return baseMapper.getNextList(map);
    }

    @Override
    public List<SagaPo> getListByBiz(String code, Long bizId, Integer objectId) {
        return baseMapper.getListByBiz(code,bizId,objectId);
    }

    @Override
    public Boolean updateByBiz(SagaPo sagaPo) {
        return baseMapper.updateByBiz(sagaPo) > 0;
    }
}

