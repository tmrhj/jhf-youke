package com.jhf.youke.saga.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.saga.domain.model.po.SagaTemplateListPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 传奇模板列表映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface SagaTemplateListMapper  extends BaseMapper<SagaTemplateListPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sag_saga_template_list set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sag_saga_template_list set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 得到模板列表
     *
     * @param code 代码
     * @return {@link List}<{@link SagaTemplateListPo}>
     */
    List<SagaTemplateListPo> getTemplateList(@Param("code")String code);
}

