package com.jhf.youke.saga.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.exception.SagaTemplateListException;
import com.jhf.youke.saga.domain.gateway.SagaTemplateListRepository;
import com.jhf.youke.saga.domain.model.po.SagaTemplateListPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "SagaTemplateListRepository")
public class SagaTemplateListRepositoryImpl extends ServiceImpl<SagaTemplateListMapper, SagaTemplateListPo> implements SagaTemplateListRepository {


    @Override
    public boolean update(SagaTemplateListPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<SagaTemplateListPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(SagaTemplateListPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(SagaTemplateListPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<SagaTemplateListPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<SagaTemplateListPo> findById(Long id) {
        SagaTemplateListPo sagaTemplateListPo = baseMapper.selectById(id);
        return Optional.ofNullable(sagaTemplateListPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new SagaTemplateListException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<SagaTemplateListPo> findAllMatching(SagaTemplateListPo entity) {
        QueryWrapper<SagaTemplateListPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<SagaTemplateListPo> selectPage(PageQuery<SagaTemplateListPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<SagaTemplateListPo> sagaTemplateListPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<SagaTemplateListPo> pageInfo = new PageInfo<>(sagaTemplateListPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<SagaTemplateListPo> getTemplateList(String code) {
        return baseMapper.getTemplateList(code);
    }
}

