package com.jhf.youke.saga.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.saga.domain.model.po.SagaTemplatePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 传奇模板映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface SagaTemplateMapper  extends BaseMapper<SagaTemplatePo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sag_saga_template set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sag_saga_template set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

