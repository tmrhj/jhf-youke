package com.jhf.youke.saga.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.saga.domain.gateway.SagaTemplateRepository;
import com.jhf.youke.saga.domain.model.Do.SagaTemplateDo;
import com.jhf.youke.saga.domain.model.po.SagaTemplatePo;
import com.jhf.youke.saga.domain.exception.SagaTemplateException;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "SagaTemplateRepository")
public class SagaTemplateRepositoryImpl extends ServiceImpl<SagaTemplateMapper, SagaTemplatePo> implements SagaTemplateRepository {


    @Override
    public boolean update(SagaTemplatePo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<SagaTemplatePo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(SagaTemplatePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(SagaTemplatePo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<SagaTemplatePo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<SagaTemplatePo> findById(Long id) {
        SagaTemplatePo sagaTemplatePo = baseMapper.selectById(id);
        return Optional.ofNullable(sagaTemplatePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new SagaTemplateException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<SagaTemplatePo> findAllMatching(SagaTemplatePo entity) {
        QueryWrapper<SagaTemplatePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<SagaTemplatePo> selectPage(PageQuery<SagaTemplatePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<SagaTemplatePo> sagaTemplatePos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<SagaTemplatePo> pageInfo = new PageInfo<>(sagaTemplatePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

