package com.jhf.youke.saga.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.saga.domain.exception.SagaUnusualException;
import com.jhf.youke.saga.domain.gateway.SagaUnusualRepository;
import com.jhf.youke.saga.domain.model.Do.SagaDo;
import com.jhf.youke.saga.domain.model.Do.SagaUnusualDo;
import com.jhf.youke.saga.domain.model.po.SagaUnusualPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "SagaUnusualRepository")
public class SagaUnusualRepositoryImpl extends ServiceImpl<SagaUnusualMapper, SagaUnusualPo> implements SagaUnusualRepository {


    @Override
    public boolean update(SagaUnusualPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<SagaUnusualPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(SagaUnusualPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(SagaUnusualPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<SagaUnusualPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<SagaUnusualPo> findById(Long id) {
        SagaUnusualPo sagaUnusualPo = baseMapper.selectById(id);
        return Optional.ofNullable(sagaUnusualPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new SagaUnusualException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<SagaUnusualPo> findAllMatching(SagaUnusualPo entity) {
        QueryWrapper<SagaUnusualPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<SagaUnusualPo> selectPage(PageQuery<SagaUnusualPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<SagaUnusualPo> sagaUnusualPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<SagaUnusualPo> pageInfo = new PageInfo<>(sagaUnusualPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }


}

