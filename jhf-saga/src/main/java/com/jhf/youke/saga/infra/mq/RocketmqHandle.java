package com.jhf.youke.saga.infra.mq;

import ch.qos.logback.classic.Logger;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.jhf.youke.core.ddd.DomainMq;
import com.jhf.youke.core.entity.SendResult;
import lombok.extern.log4j.Log4j2;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author RHJ
 */
@Component
@Log4j2
public class RocketmqHandle implements DomainMq {
    private final static Logger logger = (Logger) LoggerFactory.getLogger(RocketmqHandle.class);

    @Value("${feign.mqUrl}")
    public String mqUrl;

    @Override
    public SendResult send(String msg) {

        HttpResponse response = HttpRequest.post(mqUrl + "/rocketmq/send").body(JSONUtil.toJsonStr(msg)).execute();
        SendResult result = JSONUtil.toBean(response.body(), SendResult.class);
        log.info(" send rocketmq success {}", result.toString());
        return result;
    }


}
