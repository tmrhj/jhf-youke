import cn.hutool.json.JSONUtil;
import com.jhf.youke.SagaApplication;
import com.jhf.youke.core.ddd.DomainMq;
import com.jhf.youke.core.entity.Message;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootTest(classes = SagaApplication.class)
class SagaTestApplication {


    @Resource
    DomainMq domainMQ;

    @Resource
    ApplicationContext applicationContext;

    @Test
    void test1()throws Exception{
        Message message = new Message();
        message.setTopic("createSagaConsume");
        message.setGroupName("sagaGroup");
        message.setTag("all");
        Map<String, Object> map = new HashMap<>();
        map.put("companyId", 1000);
        map.put("userId", 1);
        map.put("userName", "张三");
        message.setMessages(map);
        domainMQ.send(JSONUtil.toJsonStr(message));
    }






}
