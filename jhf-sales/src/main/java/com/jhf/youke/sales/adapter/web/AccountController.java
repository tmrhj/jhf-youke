package com.jhf.youke.sales.adapter.web;

import com.jhf.youke.sales.app.executor.AccountAppService;
import com.jhf.youke.sales.domain.model.dto.AccountDto;
import com.jhf.youke.sales.domain.model.vo.AccountVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "账户表")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Resource
    private AccountAppService accountAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody AccountDto dto) {
        return Response.ok(accountAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody AccountDto dto) {
        return Response.ok(accountAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody AccountDto dto) {
        return Response.ok(accountAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<AccountVo>> findById(Long id) {
        return Response.ok(accountAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(accountAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<AccountVo>> list(@RequestBody AccountDto dto) {

        return Response.ok(accountAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<AccountVo>> selectPage(@RequestBody  AccountDto dto) {
        return Response.ok(accountAppService.selectPage(dto));
    }

}

