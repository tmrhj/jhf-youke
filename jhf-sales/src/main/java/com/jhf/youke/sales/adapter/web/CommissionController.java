package com.jhf.youke.sales.adapter.web;

import com.jhf.youke.sales.app.executor.CommissionAppService;
import com.jhf.youke.sales.domain.model.dto.CommissionDto;
import com.jhf.youke.sales.domain.model.vo.CommissionVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 *
 * @author RHJ
 **/

@Api(tags = "")
@RestController
@RequestMapping("/commission")
public class CommissionController {

    @Resource
    private CommissionAppService commissionAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody CommissionDto dto) {
        return Response.ok(commissionAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody CommissionDto dto) {
        return Response.ok(commissionAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody CommissionDto dto) {
        return Response.ok(commissionAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<CommissionVo>> findById(Long id) {
        return Response.ok(commissionAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(commissionAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<CommissionVo>> list(@RequestBody CommissionDto dto) {
        return Response.ok(commissionAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<CommissionVo>> selectPage(@RequestBody  CommissionDto dto) {
        return Response.ok(commissionAppService.selectPage(dto));
    }


    @PostMapping("/updatePaid")
    @ApiOperation("根据ID更新已支付状态")
    public Response<Boolean> updatePaid(@RequestBody  CommissionDto dto) {
        return Response.ok(commissionAppService.updatePaid(dto.getId()));
    }


}

