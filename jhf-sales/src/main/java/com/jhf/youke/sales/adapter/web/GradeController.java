package com.jhf.youke.sales.adapter.web;

import com.jhf.youke.core.entity.User;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.sales.app.executor.GradeAppService;
import com.jhf.youke.sales.domain.model.dto.GradeDto;
import com.jhf.youke.sales.domain.model.vo.GradeVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "级别参数设置")
@RestController
@RequestMapping("/grade")
public class GradeController {

    @Resource
    private GradeAppService gradeAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody GradeDto dto) {
        return Response.ok(gradeAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody GradeDto dto) {
        return Response.ok(gradeAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody GradeDto dto) {
        return Response.ok(gradeAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<GradeVo>> findById(Long id) {
        return Response.ok(gradeAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(gradeAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<GradeVo>> list(@RequestHeader("token") String token, @RequestBody GradeDto dto) {
        User user = CacheUtils.getUser(token);
        dto.setCompanyId(user.getRootId());
        return Response.ok(gradeAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<GradeVo>> selectPage(@RequestBody  GradeDto dto) {
        return Response.ok(gradeAppService.selectPage(dto));
    }

}

