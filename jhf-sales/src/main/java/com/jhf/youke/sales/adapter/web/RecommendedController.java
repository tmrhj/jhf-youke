package com.jhf.youke.sales.adapter.web;

import com.jhf.youke.core.entity.DeleteDto;
import com.jhf.youke.sales.app.executor.RecommendedAppService;
import com.jhf.youke.sales.domain.model.dto.RecommendedDto;
import com.jhf.youke.sales.domain.model.vo.RecommendedVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "用户推荐关系设置")
@RestController
@RequestMapping("/recommended")
public class RecommendedController {

    @Resource
    private RecommendedAppService recommendedAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody RecommendedDto dto) {
        return Response.ok(recommendedAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody RecommendedDto dto) {
        return Response.ok(recommendedAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody RecommendedDto dto) {
        return Response.ok(recommendedAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<RecommendedVo>> findById(Long id) {
        return Response.ok(recommendedAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(recommendedAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<RecommendedVo>> list(@RequestBody RecommendedDto dto) {

        return Response.ok(recommendedAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<RecommendedVo>> selectPage(@RequestBody  RecommendedDto dto) {
        return Response.ok(recommendedAppService.selectPage(dto));
    }

    @PostMapping("/removeBatch")
    @ApiOperation("批量标记删除")
    public Response<Boolean> removeBatch(@RequestBody DeleteDto dto) {
        return  Response.ok(recommendedAppService.removeBatch(dto.getIdList()));
    }

}

