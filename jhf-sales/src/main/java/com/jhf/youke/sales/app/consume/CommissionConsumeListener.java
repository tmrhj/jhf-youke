package com.jhf.youke.sales.app.consume;

import cn.hutool.json.JSONUtil;
import com.jhf.youke.sales.domain.model.dto.OrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.function.Consumer;

/**
 * description:
 * date: 2022/9/23 14:12
 * @author: cyx
 */
@Slf4j
@Service
public class CommissionConsumeListener {

    @Resource
    private CommissionConsumeService commissionConsumeService;


    @Bean
    public Consumer<Message<String>> createCommissionConsume() {
        return message -> {
            OrderDto dto = JSONUtil.toBean(message.getPayload(), OrderDto.class);
            commissionConsumeService.createCommission(dto);
        };
    }

}
