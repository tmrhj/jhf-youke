package com.jhf.youke.sales.app.consume;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.jhf.youke.core.common.localChace.LocalCache;
import com.jhf.youke.sales.app.feign.ProductFeign;
import com.jhf.youke.sales.domain.model.dto.OrderDto;
import com.jhf.youke.sales.domain.model.dto.OrderItemDto;
import com.jhf.youke.sales.domain.model.dto.ProductCommissionDto;
import com.jhf.youke.sales.domain.model.po.CommissionPo;
import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import com.jhf.youke.sales.domain.service.CommissionService;
import com.jhf.youke.sales.domain.service.RecommendedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * description:
 * date: 2022/9/23 14:12
 * @author: cyx
 */
@Slf4j
@Service
public class CommissionConsumeService {

     @Resource
     private ProductFeign productFeign;

     @Resource
     private RecommendedService recommendedService;

     @Resource
     private CommissionService commissionService;

     public List<CommissionPo> createCommission(OrderDto orderDto){
         Map<String,Object> map = new HashMap<>(8);

         List<CommissionPo> commissionList = new ArrayList<>();

         // 1 判断当前操作是否幂等

         // 2 检查参数完整性

         // 3 取到销售员团长的推荐关系
         List<RecommendedPo> recommendedPoList = recommendedService.getParentList(orderDto.getCompanyId(), orderDto.getSalesman());
         // 4 根据订单明细计算佣金
         for(OrderItemDto item: orderDto.getOrderItems()){
             List<ProductCommissionDto> commissionDtoList = getCommissionListByReleaseId(item.getProductReleaseId());
             List<CommissionPo> commissionPoList = recommendedService.computeCommission(orderDto,item,
                     recommendedPoList, commissionDtoList);
             commissionList.addAll(commissionPoList);
         }

         // 5 保存佣金
         commissionService.insertBatchPo(commissionList);

         return commissionList;
     }



    public  List<ProductCommissionDto> getCommissionListByReleaseId(Long releaseId){
        String key = "commission_list_" + releaseId;
        List<ProductCommissionDto> data = ( List<ProductCommissionDto>) LocalCache.get(key);
        if(data == null){
            data = new ArrayList<>();
            Object object =  productFeign.getProductReleaseCommission(releaseId);
            Map<String,Object> response = BeanUtil.beanToMap(object);
            List<Map<String,Object>> mapList = (List<Map<String,Object>>) response.get("data");
            List<ProductCommissionDto> list = new ArrayList<>();
            mapList.forEach( map ->{
                        ProductCommissionDto product = BeanUtil.mapToBean(map, ProductCommissionDto.class, false, new CopyOptions());
                        list.add(product);
                    }

            );
            LocalCache.put(key, list);
            return list;
        }

        return data;
     }

}
