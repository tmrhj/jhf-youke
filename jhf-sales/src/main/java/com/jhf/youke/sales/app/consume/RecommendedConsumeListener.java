package com.jhf.youke.sales.app.consume;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;
import java.util.function.Consumer;

/**
 * description:
 * date: 2022/9/23 14:12
 * author: cyx
 * @author RHJ
 */
@Slf4j
@Service
public class RecommendedConsumeListener {

    @Resource
    private RecommendedConsumeService recommendedConsumeService;


    @Bean
    public Consumer<Message<String>> createRecommendConsume() {
        return message -> {
            log.info("createRecommendConsume {}", message);
            Map<String,Object> map = JSONUtil.toBean(message.getPayload(), Map.class);
            recommendedConsumeService.createRecommended(map);
        };
    }

}
