package com.jhf.youke.sales.app.consume;

import com.jhf.youke.saga.client.annotation.SagaReply;
import com.jhf.youke.sales.domain.model.Do.RecommendedDo;
import com.jhf.youke.sales.domain.service.RecommendedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * description:
 * date: 2022/9/23 14:12
 * @author: cyx
 */
@Slf4j
@Service
public class RecommendedConsumeService {

     @Resource
     private RecommendedService recommendedService;


     @SagaReply(abnormal = "create_recommend_abnormal")
     public void createRecommended(Map<String,Object> map){
         Map<String,Object> msg = (Map<String,Object>) map.get("message");
         RecommendedDo recommendedDo = new RecommendedDo(msg);
         recommendedService.recommend(recommendedDo);
     }


}
