package com.jhf.youke.sales.app.executor;

import com.jhf.youke.sales.domain.converter.AccountConverter;
import com.jhf.youke.sales.domain.model.Do.AccountDo;
import com.jhf.youke.sales.domain.model.dto.AccountDto;
import com.jhf.youke.sales.domain.model.vo.AccountVo;
import com.jhf.youke.sales.domain.service.AccountService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class AccountAppService {

    @Resource
    private AccountService accountService;

    @Resource
    private AccountConverter accountConverter;


    public boolean update(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.update(accountDo);
    }

    public boolean delete(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.delete(accountDo);
    }


    public boolean insert(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.insert(accountDo);
    }

    public Optional<AccountVo> findById(Long id) {
        return accountService.findById(id);
    }


    public boolean remove(Long id) {
        return accountService.remove(id);
    }


    public List<AccountVo> findAllMatching(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.findAllMatching(accountDo);
    }


    public Pagination<AccountVo> selectPage(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.selectPage(accountDo);
    }

}

