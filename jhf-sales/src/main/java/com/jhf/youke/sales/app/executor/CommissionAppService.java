package com.jhf.youke.sales.app.executor;

import com.jhf.youke.sales.domain.converter.CommissionConverter;
import com.jhf.youke.sales.domain.model.Do.CommissionDo;
import com.jhf.youke.sales.domain.model.dto.CommissionDto;
import com.jhf.youke.sales.domain.model.vo.CommissionVo;
import com.jhf.youke.sales.domain.service.CommissionService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class CommissionAppService {

    @Resource
    private CommissionService commissionService;

    @Resource
    private CommissionConverter commissionConverter;


    public boolean update(CommissionDto dto) {
        CommissionDo commissionDo =  commissionConverter.dto2Do(dto);
        return commissionService.update(commissionDo);
    }

    public boolean delete(CommissionDto dto) {
        CommissionDo commissionDo =  commissionConverter.dto2Do(dto);
        return commissionService.delete(commissionDo);
    }


    public boolean insert(CommissionDto dto) {
        CommissionDo commissionDo =  commissionConverter.dto2Do(dto);
        return commissionService.insert(commissionDo);
    }

    public Optional<CommissionVo> findById(Long id) {
        return commissionService.findById(id);
    }


    public boolean remove(Long id) {
        return commissionService.remove(id);
    }


    public List<CommissionVo> findAllMatching(CommissionDto dto) {
        CommissionDo commissionDo =  commissionConverter.dto2Do(dto);
        return commissionService.findAllMatching(commissionDo);
    }


    public Pagination<CommissionVo> selectPage(CommissionDto dto) {
        CommissionDo commissionDo =  commissionConverter.dto2Do(dto);
        return commissionService.selectPage(commissionDo);
    }

    public Boolean updatePaid(Long id){
        return commissionService.updatePaid(id);
    }

}

