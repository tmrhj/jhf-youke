package com.jhf.youke.sales.app.executor;

import com.jhf.youke.sales.domain.converter.GradeConverter;
import com.jhf.youke.sales.domain.model.Do.GradeDo;
import com.jhf.youke.sales.domain.model.dto.GradeDto;
import com.jhf.youke.sales.domain.model.vo.GradeVo;
import com.jhf.youke.sales.domain.service.GradeService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class GradeAppService {

    @Resource
    private GradeService gradeService;

    @Resource
    private GradeConverter gradeConverter;


    public boolean update(GradeDto dto) {
        GradeDo gradeDo =  gradeConverter.dto2Do(dto);
        return gradeService.update(gradeDo);
    }

    public boolean delete(GradeDto dto) {
        GradeDo gradeDo =  gradeConverter.dto2Do(dto);
        return gradeService.delete(gradeDo);
    }


    public boolean insert(GradeDto dto) {
        GradeDo gradeDo =  gradeConverter.dto2Do(dto);
        return gradeService.insert(gradeDo);
    }

    public Optional<GradeVo> findById(Long id) {
        return gradeService.findById(id);
    }


    public boolean remove(Long id) {
        return gradeService.remove(id);
    }


    public List<GradeVo> findAllMatching(GradeDto dto) {
        GradeDo gradeDo =  gradeConverter.dto2Do(dto);
        return gradeService.findAllMatching(gradeDo);
    }


    public Pagination<GradeVo> selectPage(GradeDto dto) {
        GradeDo gradeDo =  gradeConverter.dto2Do(dto);
        return gradeService.selectPage(gradeDo);
    }

}

