package com.jhf.youke.sales.app.executor;

import com.jhf.youke.sales.domain.converter.RecommendedConverter;
import com.jhf.youke.sales.domain.model.Do.RecommendedDo;
import com.jhf.youke.sales.domain.model.dto.RecommendedDto;
import com.jhf.youke.sales.domain.model.vo.RecommendedVo;
import com.jhf.youke.sales.domain.service.RecommendedService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class RecommendedAppService {

    @Resource
    private RecommendedService recommendedService;

    @Resource
    private RecommendedConverter recommendedConverter;


    public boolean update(RecommendedDto dto) {
        RecommendedDo recommendedDo =  recommendedConverter.dto2Do(dto);
        return recommendedService.update(recommendedDo);
    }

    public boolean delete(RecommendedDto dto) {
        RecommendedDo recommendedDo =  recommendedConverter.dto2Do(dto);
        return recommendedService.delete(recommendedDo);
    }


    public boolean insert(RecommendedDto dto) {
        RecommendedDo recommendedDo =  recommendedConverter.dto2Do(dto);
        return recommendedService.insert(recommendedDo);
    }

    public Optional<RecommendedVo> findById(Long id) {
        return recommendedService.findById(id);
    }


    public boolean remove(Long id) {
        return recommendedService.remove(id);
    }


    public List<RecommendedVo> findAllMatching(RecommendedDto dto) {
        RecommendedDo recommendedDo =  recommendedConverter.dto2Do(dto);
        return recommendedService.findAllMatching(recommendedDo);
    }


    public Pagination<RecommendedVo> selectPage(RecommendedDto dto) {
        RecommendedDo recommendedDo =  recommendedConverter.dto2Do(dto);
        return recommendedService.selectPage(recommendedDo);
    }

    public boolean removeBatch(List<Long> idList) {
        return recommendedService.removeBatch(idList);
    }

}

