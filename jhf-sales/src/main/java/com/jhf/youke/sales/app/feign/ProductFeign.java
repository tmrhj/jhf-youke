package com.jhf.youke.sales.app.feign;


import com.jhf.youke.sales.app.feign.hystrix.ProductFeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * 产品装
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Component
@FeignClient(name="jhf-product-service", url = "${feign.productUrl}" ,fallback = ProductFeignHystrix.class)
public interface ProductFeign{

    /**
     * 得到产品发布佣金
     *
     * @param id id
     * @return {@link Object}
     */
    @RequestMapping(value="/product/productRelease/getProductReleaseCommission")
    Object getProductReleaseCommission(@RequestParam("id") Long id);



}
