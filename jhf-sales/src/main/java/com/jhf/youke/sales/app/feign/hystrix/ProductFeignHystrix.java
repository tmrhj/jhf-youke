package com.jhf.youke.sales.app.feign.hystrix;


import com.jhf.youke.core.entity.Response;
import com.jhf.youke.sales.app.feign.ProductFeign;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author RHJ
 */
@Component
public class ProductFeignHystrix implements FallbackFactory<ProductFeign> {


    @Override
    public ProductFeign create(Throwable arg0) {

        return new ProductFeign(){
          @Override
          public  Object getProductReleaseCommission(@RequestParam("id") Long id){
              return  Response.fail(arg0.getMessage());
          }

        };

    }

}
