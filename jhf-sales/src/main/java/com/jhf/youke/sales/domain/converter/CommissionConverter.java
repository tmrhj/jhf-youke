package com.jhf.youke.sales.domain.converter;

import com.jhf.youke.sales.domain.model.Do.CommissionDo;
import com.jhf.youke.sales.domain.model.dto.CommissionDto;
import com.jhf.youke.sales.domain.model.po.CommissionPo;
import com.jhf.youke.sales.domain.model.vo.CommissionVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.Collection;

import java.util.List;


/**
 * 委员会转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommissionConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param commissionDto 委员会dto
     * @return {@link CommissionDo}
     */
    @Mapping(target = "delFlag", source = "delFlag", defaultValue = "0")
    CommissionDo dto2Do(CommissionDto commissionDto);

    /**
     * 洗阿宝
     *
     * @param commissionDo 委员会所做
     * @return {@link CommissionPo}
     */
    CommissionPo do2Po(CommissionDo commissionDo);

    /**
     * 洗签证官
     *
     * @param commissionDo 委员会所做
     * @return {@link CommissionVo}
     */
    CommissionVo do2Vo(CommissionDo commissionDo);


    /**
     * 洗订单列表
     *
     * @param commissionDoList 委员会做列表
     * @return {@link List}<{@link CommissionPo}>
     */
    List<CommissionPo> do2PoList(List<CommissionDo> commissionDoList);

    /**
     * 警察乙做
     *
     * @param commissionPo 委员会阿宝
     * @return {@link CommissionDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    CommissionDo po2Do(CommissionPo commissionPo);

    /**
     * 警察乙签证官
     *
     * @param commissionPo 委员会阿宝
     * @return {@link CommissionVo}
     */
    CommissionVo po2Vo(CommissionPo commissionPo);

    /**
     * 警察乙做列表
     *
     * @param commissionPoList 委员会订单列表
     * @return {@link List}<{@link CommissionDo}>
     */
    List<CommissionDo> po2DoList(List<CommissionPo> commissionPoList);

    /**
     * 警察乙vo列表
     *
     * @param commissionPoList 委员会订单列表
     * @return {@link List}<{@link CommissionVo}>
     */
    List<CommissionVo> po2VoList(List<CommissionPo> commissionPoList);


}

