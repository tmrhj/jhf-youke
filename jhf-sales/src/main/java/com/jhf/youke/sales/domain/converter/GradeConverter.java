package com.jhf.youke.sales.domain.converter;

import com.jhf.youke.sales.domain.model.Do.GradeDo;
import com.jhf.youke.sales.domain.model.dto.GradeDto;
import com.jhf.youke.sales.domain.model.po.GradePo;
import com.jhf.youke.sales.domain.model.vo.GradeVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.Collection;

import java.util.List;


/**
 * 年级转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GradeConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param gradeDto 年级dto
     * @return {@link GradeDo}
     */
    @Mapping(target = "delFlag", source = "delFlag", defaultValue = "0")
    GradeDo dto2Do(GradeDto gradeDto);

    /**
     * 洗阿宝
     *
     * @param gradeDo 年级做
     * @return {@link GradePo}
     */
    GradePo do2Po(GradeDo gradeDo);

    /**
     * 洗签证官
     *
     * @param gradeDo 年级做
     * @return {@link GradeVo}
     */
    GradeVo do2Vo(GradeDo gradeDo);


    /**
     * 洗订单列表
     *
     * @param gradeDoList 年级做列表
     * @return {@link List}<{@link GradePo}>
     */
    List<GradePo> do2PoList(List<GradeDo> gradeDoList);

    /**
     * 警察乙做
     *
     * @param gradePo 年级阿宝
     * @return {@link GradeDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    GradeDo po2Do(GradePo gradePo);

    /**
     * 警察乙签证官
     *
     * @param gradePo 年级阿宝
     * @return {@link GradeVo}
     */
    GradeVo po2Vo(GradePo gradePo);

    /**
     * 警察乙做列表
     *
     * @param gradePoList 年级订单列表
     * @return {@link List}<{@link GradeDo}>
     */
    List<GradeDo> po2DoList(List<GradePo> gradePoList);

    /**
     * 警察乙vo列表
     *
     * @param gradePoList 年级订单列表
     * @return {@link List}<{@link GradeVo}>
     */
    List<GradeVo> po2VoList(List<GradePo> gradePoList);


}

