package com.jhf.youke.sales.domain.converter;

import com.jhf.youke.sales.domain.model.Do.RecommendedDo;
import com.jhf.youke.sales.domain.model.dto.RecommendedDto;
import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import com.jhf.youke.sales.domain.model.vo.RecommendedVo;
import org.mapstruct.*;

import java.util.Collection;

import java.util.List;


/**
 * 推荐转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RecommendedConverter {

    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param recommendedDto 推荐dto
     * @return {@link RecommendedDo}
     */
    @Mapping(target = "delFlag", source = "delFlag", defaultValue = "0")
    RecommendedDo dto2Do(RecommendedDto recommendedDto);

    /**
     * 洗阿宝
     *
     * @param recommendedDo 建议做
     * @return {@link RecommendedPo}
     */
    RecommendedPo do2Po(RecommendedDo recommendedDo);

    /**
     * 洗签证官
     *
     * @param recommendedDo 建议做
     * @return {@link RecommendedVo}
     */
    RecommendedVo do2Vo(RecommendedDo recommendedDo);


    /**
     * 洗订单列表
     *
     * @param recommendedDoList 建议做列表
     * @return {@link List}<{@link RecommendedPo}>
     */
    List<RecommendedPo> do2PoList(List<RecommendedDo> recommendedDoList);

    /**
     * 警察乙做
     *
     * @param recommendedPo 推荐阿宝
     * @return {@link RecommendedDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    RecommendedDo po2Do(RecommendedPo recommendedPo);

    /**
     * 警察乙签证官
     *
     * @param recommendedPo 推荐阿宝
     * @return {@link RecommendedVo}
     */
    RecommendedVo po2Vo(RecommendedPo recommendedPo);

    /**
     * 警察乙做列表
     *
     * @param recommendedPoList 建议订单列表
     * @return {@link List}<{@link RecommendedDo}>
     */
    List<RecommendedDo> po2DoList(List<RecommendedPo> recommendedPoList);

    /**
     * 警察乙vo列表
     *
     * @param recommendedPoList 建议订单列表
     * @return {@link List}<{@link RecommendedVo}>
     */
    List<RecommendedVo> po2VoList(List<RecommendedPo> recommendedPoList);


}

