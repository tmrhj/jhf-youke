package com.jhf.youke.sales.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class CommissionException extends DomainException {

    public CommissionException(String message) { super(message); }
}

