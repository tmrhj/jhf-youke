package com.jhf.youke.sales.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class GradeException extends DomainException {

    public GradeException(String message) { super(message); }
}

