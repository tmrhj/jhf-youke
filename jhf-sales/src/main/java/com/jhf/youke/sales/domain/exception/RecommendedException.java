package com.jhf.youke.sales.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class RecommendedException extends DomainException {

    public RecommendedException(String message) { super(message); }
}

