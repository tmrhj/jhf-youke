package com.jhf.youke.sales.domain.gateway;


import com.jhf.youke.sales.domain.model.po.AccountPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface AccountRepository extends Repository<AccountPo> {


}

