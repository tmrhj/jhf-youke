package com.jhf.youke.sales.domain.gateway;


import com.jhf.youke.sales.domain.model.po.CommissionPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * 委员会库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface CommissionRepository extends Repository<CommissionPo> {

     /**
      * 更新了
      *
      * @param id id
      * @return boolean
      */
     boolean updatePaid(Long id);
}

