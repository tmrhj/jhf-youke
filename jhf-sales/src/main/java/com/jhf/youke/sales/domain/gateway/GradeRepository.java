package com.jhf.youke.sales.domain.gateway;


import com.jhf.youke.sales.domain.model.po.GradePo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface GradeRepository extends Repository<GradePo> {


}

