package com.jhf.youke.sales.domain.gateway;


import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import com.jhf.youke.core.ddd.Repository;

import java.util.List;


/**
 * 推荐存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface RecommendedRepository extends Repository<RecommendedPo> {


    /**
     * 让儿子列表
     *
     * @param companyId 公司标识
     * @param salesman  推销员
     * @return {@link List}<{@link RecommendedPo}>
     */
    List<RecommendedPo> getSonList(Long companyId, Long salesman);

    /**
     * 获得父列表
     *
     * @param companyId 公司标识
     * @param salesman  推销员
     * @return {@link List}<{@link RecommendedPo}>
     */
    List<RecommendedPo> getParentList(Long companyId, Long salesman);

    /**
     * 通过用户
     *
     * @param companyId 公司标识
     * @param userId    用户id
     * @return {@link RecommendedPo}
     */
    RecommendedPo getByUser(Long companyId, Long userId);

}

