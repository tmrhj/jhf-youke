package com.jhf.youke.sales.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.sales.domain.exception.AccountException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author RHJ
 * **/
@Data
public class AccountDo extends BaseDoEntity {

  private static final long serialVersionUID = -80930487125883075L;

    /** 用户标识 **/
    private Long userId;

    /** 累计销售 **/
    private BigDecimal sumFee;

    /** 余额 **/
    private BigDecimal fee;

    /** 支出金额 **/
    private String payFee;

    /** 银行账号 **/
    private String bankAccount;

    /** 银行名称 **/
    private String bankName;

    /** 开户行地址 **/
    private String bankAddress;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new AccountException(errorMessage);
        }
        return obj;
    }

    private AccountDo validateNull(AccountDo accountDo){
          //可使用链式法则进行为空检查
          accountDo.
          requireNonNull(accountDo, accountDo.getRemark(),"不能为NULL");
                
        return accountDo;
    }


}


