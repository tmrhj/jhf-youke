package com.jhf.youke.sales.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.sales.domain.exception.CommissionException;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * 该注解注释在service方法上，标注为链接slaves库 
 * @author RHJ
 **/
@Data
public class CommissionDo extends BaseDoEntity {

  private static final long serialVersionUID = 330384829533814445L;

    /** 业务ID **/
    private Long bizId;

    /** 业务对象 **/
    private Long objectId;

    /** 金额 **/
    private BigDecimal fee;

    /** 用户ID **/
    private Long userId;

    /** 用户名 **/
    private String userName;

    private String productName;

    /** 状态 0 待支付 1已支付 **/
    private Integer status;

    /** 类型 **/
    private Integer type;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new CommissionException(errorMessage);
        }
        return obj;
    }

    private CommissionDo validateNull(CommissionDo commissionDo){
          //可使用链式法则进行为空检查
          commissionDo.
          requireNonNull(commissionDo, commissionDo.getRemark(),"不能为NULL");
                
        return commissionDo;
    }


}


