package com.jhf.youke.sales.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.sales.domain.exception.GradeException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class GradeDo extends BaseDoEntity {

  private static final long serialVersionUID = 705887719809920732L;

    /**等级 **/
    private Integer level;

    /**等级名称 **/
    private String name;

    /**升级需要订单数 **/
    private Integer orderNum;

    /**升级订单金额 **/
    private Integer orderFee;

    /**平级提成比例 **/
    private BigDecimal commission;

    private Long companyId;

    private String companyName;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new GradeException(errorMessage);
        }
        return obj;
    }

    private GradeDo validateNull(GradeDo gradeDo){
          //可使用链式法则进行为空检查
          gradeDo.
          requireNonNull(gradeDo, gradeDo.getRemark(),"不能为NULL");
                
        return gradeDo;
    }


}


