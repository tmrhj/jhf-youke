package com.jhf.youke.sales.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.sales.domain.exception.RecommendedException;
import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import lombok.Data;

import java.util.Map;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class RecommendedDo extends BaseDoEntity {

  private static final long serialVersionUID = -23972547740406686L;

    /** 供应链运营商ID **/
    private Long companyId;

    /** 当前用户 **/
    private Long userId;

    /** 当前用户 **/
    private String userName;

    /** 推荐人 **/
    private Long referrer;

    /** 等级 **/
    private Integer level;

    private String parentIds;

    public RecommendedDo(){

    }

    public RecommendedDo(Map<String, Object> msg) {
        this.companyId = StringUtils.toLong(StringUtils.chgZero(msg.get("companyId")));
        this.userId = StringUtils.toLong(StringUtils.chgZero(msg.get("userId")));
        this.userName = StringUtils.chgNull(msg.get("userName"));
        this.referrer = StringUtils.toLong(StringUtils.chgZero(msg.get("referrer")));
    }

    public RecommendedDo setParent(RecommendedPo parent) {
        if(parent != null){
            this.parentIds = parent.getParentIds() +  this.userId + ",";
            this.level = parent.getLevel() + 1;
        }else{
            this.parentIds = this.userId + ",";
            this.level = 1;
        }

        return this;
    }


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new RecommendedException(errorMessage);
        }
        return obj;
    }

    private RecommendedDo validateNull(RecommendedDo recommendedDo){
          // 可使用链式法则进行为空检查
          recommendedDo.
          requireNonNull(recommendedDo, recommendedDo.getRemark(),"不能为NULL");
                
        return recommendedDo;
    }


}


