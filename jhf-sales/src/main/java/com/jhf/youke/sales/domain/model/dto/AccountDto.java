package com.jhf.youke.sales.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AccountDto extends BaseDtoEntity {

    private static final long serialVersionUID = -91213406894607371L;


    @ApiModelProperty(value = "用户标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "累计销售")
    private BigDecimal sumFee;

    @ApiModelProperty(value = "余额")
    private BigDecimal fee;

    @ApiModelProperty(value = "支出金额")
    private String payFee;

    @ApiModelProperty(value = "银行账号")
    private String bankAccount;

    @ApiModelProperty(value = "银行名称")
    private String bankName;

    @ApiModelProperty(value = "开户行地址")
    private String bankAddress;



}


