package com.jhf.youke.sales.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class GradeDto extends BaseDtoEntity {

    private static final long serialVersionUID = -17015633672482097L;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "等级名称")
    private String name;

    @ApiModelProperty(value = "升级需要订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "升级订单金额")
    private Integer orderFee;

    @ApiModelProperty(value = "平级提成比例")
    private BigDecimal commission;

    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "单位名称")
    private String companyName;






}


