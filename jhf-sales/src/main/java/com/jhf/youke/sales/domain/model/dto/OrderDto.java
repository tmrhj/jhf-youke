package com.jhf.youke.sales.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class OrderDto extends BaseDtoEntity {

    private static final long serialVersionUID = -17015633672482097L;

    @ApiModelProperty(value = "订单Id")
    private Long orderId;

    @ApiModelProperty(value = "单位id")
    private Long companyId;

    @ApiModelProperty(value = "团长Id")
    private Long salesman;

    @ApiModelProperty(value = "订单明细")
    private List<OrderItemDto> orderItems;




}


