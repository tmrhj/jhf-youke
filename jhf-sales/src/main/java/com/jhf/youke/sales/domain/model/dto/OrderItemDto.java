package com.jhf.youke.sales.domain.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
public class OrderItemDto {

    @ApiModelProperty(value = "产品名")
    private String productName;

    @ApiModelProperty(value = "计量单位")
    private String unitName;

    @ApiModelProperty(value = "商品发布Id")
    private Long productReleaseId;

    @ApiModelProperty(value = "规格1")
    private Long propertyId1;

    @ApiModelProperty(value = "规格2")
    private Long propertyId2;

    @ApiModelProperty(value = "规格3")
    private Long propertyId3;

    @ApiModelProperty(value = "数量")
    private BigDecimal num;

}
