package com.jhf.youke.sales.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ProductCommissionDto extends BaseDtoEntity {

    private static final long serialVersionUID = -99181048673155190L;

    @ApiModelProperty(value = "商品发布标识")
    private Long productReleaseId;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "佣金")
    private BigDecimal commission;


    @ApiModelProperty(value = "平级佣金")
    private BigDecimal peersCommission;


    @ApiModelProperty(value = "多规格1")
    private Long propertyId1;

    @ApiModelProperty(value = "多规格2")
    private Long propertyId2;

    @ApiModelProperty(value = "多规格3")
    private Long propertyId3;

    @ApiModelProperty(value = "1 按金额 2 按比率")
    private Integer type;


}


