package com.jhf.youke.sales.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RecommendedDto extends BaseDtoEntity {

    private static final long serialVersionUID = 297404959523741342L;

    @ApiModelProperty(value = "供应链运营商ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "当前用户")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "推荐人")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long referrer;

    @ApiModelProperty(value = "等级")
    private Integer level;



}


