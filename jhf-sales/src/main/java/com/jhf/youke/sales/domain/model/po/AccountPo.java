package com.jhf.youke.sales.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sale_account")
public class AccountPo extends BasePoEntity {

    private static final long serialVersionUID = 235440835902670224L;

    /**用户标识 **/
    private Long userId;

    /**累计销售 **/
    private BigDecimal sumFee;

    /**余额 **/
    private BigDecimal fee;

    /**支出金额 **/
    private String payFee;

    /**银行账号 **/
    private String bankAccount;

    /**银行名称 **/
    private String bankName;

    /**开户行地址 **/
    private String bankAddress;



}


