package com.jhf.youke.sales.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sale_commission")
public class CommissionPo extends BasePoEntity {

    private static final long serialVersionUID = -93888960351278175L;

    /**业务ID **/
    private Long bizId;

    /**业务对象 **/
    private Long objectId;

    /**金额 **/
    private BigDecimal fee;

    private Long productReleaseId;

    private Long productId;

    private String productName;

    /**用户ID **/
    private Long userId;

    /**用户名 **/
    private String userName;

    /**状态 0 待支付 1已支付 **/
    private Integer status;

    /**类型 **/
    private Integer type;



}


