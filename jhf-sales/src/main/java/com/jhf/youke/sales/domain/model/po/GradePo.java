package com.jhf.youke.sales.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.math.BigDecimal;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sale_grade")
public class GradePo extends BasePoEntity {

    private static final long serialVersionUID = 986967379769259722L;

    /**等级 **/
    private Integer level;

    /**等级名称 **/
    private String name;

    /**升级需要订单数 **/
    private Integer orderNum;

    /**升级订单金额 **/
    private Integer orderFee;

    /**平级提成比例 **/
    private BigDecimal commission;

    private Long companyId;

    private String companyName;



}


