package com.jhf.youke.sales.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author RHJ
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sale_recommended")
public class RecommendedPo extends BasePoEntity {

    private static final long serialVersionUID = 432432273797559484L;

    /** 供应链运营商ID **/
    private Long companyId;

    /** 当前用户 **/
    private Long userId;

    /** 当前用户 **/
    private String userName;

    /** 推荐人 **/
    private Long referrer;

    /** 等级 **/
    private Integer level;

    private String parentIds;



}


