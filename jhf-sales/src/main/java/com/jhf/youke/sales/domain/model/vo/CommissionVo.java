package com.jhf.youke.sales.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author Rhj
 * **/
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class CommissionVo extends BaseVoEntity {

    private static final long serialVersionUID = -22076659235472107L;


    @ApiModelProperty(value = "业务ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "业务对象")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long objectId;

    @ApiModelProperty(value = "金额")
    private BigDecimal fee;

    @ApiModelProperty(value = "用户ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "状态 0 待支付 1已支付")
    private Integer status;

    @ApiModelProperty(value = "类型")
    private Integer type;

    @ApiModelProperty(value = "产品名")
    private String productName;



}


