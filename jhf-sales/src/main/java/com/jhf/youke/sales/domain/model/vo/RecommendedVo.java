package com.jhf.youke.sales.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RecommendedVo extends BaseVoEntity {

    private static final long serialVersionUID = 541122913179695431L;


    @ApiModelProperty(value = "供应链运营商ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "当前用户")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "推荐人")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long referrer;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "ids")
    private String parentIds;

}


