package com.jhf.youke.sales.domain.service;

import com.jhf.youke.sales.domain.converter.AccountConverter;
import com.jhf.youke.sales.domain.model.Do.AccountDo;
import com.jhf.youke.sales.domain.gateway.AccountRepository;
import com.jhf.youke.sales.domain.model.po.AccountPo;
import com.jhf.youke.sales.domain.model.vo.AccountVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class AccountService extends AbstractDomainService<AccountRepository, AccountDo, AccountVo> {


    @Resource
    AccountConverter accountConverter;

    @Override
    public boolean update(AccountDo entity) {
        AccountPo accountPo = accountConverter.do2Po(entity);
        accountPo.preUpdate();
        return repository.update(accountPo);
    }

    @Override
    public boolean updateBatch(List<AccountDo> doList) {
        List<AccountPo> poList = accountConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(AccountDo entity) {
        AccountPo accountPo = accountConverter.do2Po(entity);
        return repository.delete(accountPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(AccountDo entity) {
        AccountPo accountPo = accountConverter.do2Po(entity);
        accountPo.preInsert();
        return repository.insert(accountPo);
    }

    @Override
    public boolean insertBatch(List<AccountDo> doList) {
        List<AccountPo> poList = accountConverter.do2PoList(doList);
        poList = AccountPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<AccountVo> findById(Long id) {
        Optional<AccountPo> accountPo =  repository.findById(id);
        AccountVo accountVo = accountConverter.po2Vo(accountPo.orElse(new AccountPo()));
        return Optional.ofNullable(accountVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<AccountVo> findAllMatching(AccountDo entity) {
        AccountPo accountPo = accountConverter.do2Po(entity);
        List<AccountPo>accountPoList =  repository.findAllMatching(accountPo);
        return accountConverter.po2VoList(accountPoList);
    }


    @Override
    public Pagination<AccountVo> selectPage(AccountDo entity){
        AccountPo accountPo = accountConverter.do2Po(entity);
        PageQuery<AccountPo> pageQuery = new PageQuery<>(accountPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<AccountPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                accountConverter.po2VoList(pagination.getList()));
    }


}

