package com.jhf.youke.sales.domain.service;

import com.jhf.youke.sales.domain.converter.CommissionConverter;
import com.jhf.youke.sales.domain.model.Do.CommissionDo;
import com.jhf.youke.sales.domain.gateway.CommissionRepository;
import com.jhf.youke.sales.domain.model.po.CommissionPo;
import com.jhf.youke.sales.domain.model.vo.CommissionVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class CommissionService extends AbstractDomainService<CommissionRepository, CommissionDo, CommissionVo> {


    @Resource
    CommissionConverter commissionConverter;

    @Override
    public boolean update(CommissionDo entity) {
        CommissionPo commissionPo = commissionConverter.do2Po(entity);
        commissionPo.preUpdate();
        return repository.update(commissionPo);
    }

    @Override
    public boolean updateBatch(List<CommissionDo> doList) {
        List<CommissionPo> poList = commissionConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(CommissionDo entity) {
        CommissionPo commissionPo = commissionConverter.do2Po(entity);
        return repository.delete(commissionPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(CommissionDo entity) {
        CommissionPo commissionPo = commissionConverter.do2Po(entity);
        commissionPo.preInsert();
        return repository.insert(commissionPo);
    }

    @Override
    public boolean insertBatch(List<CommissionDo> doList) {
        List<CommissionPo> poList = commissionConverter.do2PoList(doList);
        poList = CommissionPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    public boolean insertBatchPo(List<CommissionPo> poList) {
        poList = CommissionPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<CommissionVo> findById(Long id) {
        Optional<CommissionPo> commissionPo =  repository.findById(id);
        CommissionVo commissionVo = commissionConverter.po2Vo(commissionPo.orElse(new CommissionPo()));
        return Optional.ofNullable(commissionVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<CommissionVo> findAllMatching(CommissionDo entity) {
        CommissionPo commissionPo = commissionConverter.do2Po(entity);
        List<CommissionPo>commissionPoList =  repository.findAllMatching(commissionPo);
        return commissionConverter.po2VoList(commissionPoList);
    }


    @Override
    public Pagination<CommissionVo> selectPage(CommissionDo entity){
        CommissionPo commissionPo = commissionConverter.do2Po(entity);
        PageQuery<CommissionPo> pageQuery = new PageQuery<>(commissionPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<CommissionPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                commissionConverter.po2VoList(pagination.getList()));
    }

    public Boolean updatePaid(Long id){
        return repository.updatePaid(id);
    }


}

