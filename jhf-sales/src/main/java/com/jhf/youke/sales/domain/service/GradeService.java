package com.jhf.youke.sales.domain.service;

import com.jhf.youke.sales.domain.converter.GradeConverter;
import com.jhf.youke.sales.domain.model.Do.GradeDo;
import com.jhf.youke.sales.domain.gateway.GradeRepository;
import com.jhf.youke.sales.domain.model.po.GradePo;
import com.jhf.youke.sales.domain.model.vo.GradeVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class GradeService extends AbstractDomainService<GradeRepository, GradeDo, GradeVo> {


    @Resource
    GradeConverter gradeConverter;

    @Override
    public boolean update(GradeDo entity) {
        GradePo gradePo = gradeConverter.do2Po(entity);
        gradePo.preUpdate();
        return repository.update(gradePo);
    }

    @Override
    public boolean updateBatch(List<GradeDo> doList) {
        List<GradePo> poList = gradeConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(GradeDo entity) {
        GradePo gradePo = gradeConverter.do2Po(entity);
        return repository.delete(gradePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(GradeDo entity) {
        GradePo gradePo = gradeConverter.do2Po(entity);
        gradePo.preInsert();
        return repository.insert(gradePo);
    }

    @Override
    public boolean insertBatch(List<GradeDo> doList) {
        List<GradePo> poList = gradeConverter.do2PoList(doList);
        poList = GradePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<GradeVo> findById(Long id) {
        Optional<GradePo> gradePo =  repository.findById(id);
        GradeVo gradeVo = gradeConverter.po2Vo(gradePo.orElse(new GradePo()));
        return Optional.ofNullable(gradeVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<GradeVo> findAllMatching(GradeDo entity) {
        GradePo gradePo = gradeConverter.do2Po(entity);
        List<GradePo>gradePoList =  repository.findAllMatching(gradePo);
        return gradeConverter.po2VoList(gradePoList);
    }


    @Override
    public Pagination<GradeVo> selectPage(GradeDo entity){
        GradePo gradePo = gradeConverter.do2Po(entity);
        PageQuery<GradePo> pageQuery = new PageQuery<>(gradePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<GradePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                gradeConverter.po2VoList(pagination.getList()));
    }


}

