package com.jhf.youke.sales.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.sales.domain.model.po.CommissionPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 委员会映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface CommissionMapper  extends BaseMapper<CommissionPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sale_commission set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sale_commission set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);


    /**
     * 更新状态通过id
     *
     * @param id     id
     * @param status 状态
     * @return int
     */
    int updateStatusById(@Param("id") Long id, @Param("status") Integer status);

}

