package com.jhf.youke.sales.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.sales.domain.exception.CommissionException;
import com.jhf.youke.sales.domain.gateway.CommissionRepository;
import com.jhf.youke.sales.domain.model.po.CommissionPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * @author RHJ
 * **/
@Service(value = "CommissionRepository")
public class CommissionRepositoryImpl extends ServiceImpl<CommissionMapper, CommissionPo> implements CommissionRepository {


    @Override
    public boolean update(CommissionPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<CommissionPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(CommissionPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(CommissionPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<CommissionPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<CommissionPo> findById(Long id) {
        CommissionPo commissionPo = baseMapper.selectById(id);
        return Optional.ofNullable(commissionPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new CommissionException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<CommissionPo> findAllMatching(CommissionPo entity) {
        QueryWrapper<CommissionPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<CommissionPo> selectPage(PageQuery<CommissionPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<CommissionPo> commissionPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<CommissionPo> pageInfo = new PageInfo<>(commissionPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public boolean updatePaid(Long id) {
        return baseMapper.updateStatusById(id,1) > 0;
    }
}

