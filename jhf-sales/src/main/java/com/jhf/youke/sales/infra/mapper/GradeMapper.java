package com.jhf.youke.sales.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.sales.domain.model.po.GradePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 级映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface GradeMapper  extends BaseMapper<GradePo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sale_grade set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sale_grade set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

}

