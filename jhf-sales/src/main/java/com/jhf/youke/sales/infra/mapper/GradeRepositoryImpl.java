package com.jhf.youke.sales.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.sales.domain.exception.GradeException;
import com.jhf.youke.sales.domain.gateway.GradeRepository;
import com.jhf.youke.sales.domain.model.po.GradePo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * @author RHJ
 * **/
@Service(value = "GradeRepository")
public class GradeRepositoryImpl extends ServiceImpl<GradeMapper, GradePo> implements GradeRepository {


    @Override
    public boolean update(GradePo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<GradePo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(GradePo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(GradePo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<GradePo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<GradePo> findById(Long id) {
        GradePo gradePo = baseMapper.selectById(id);
        return Optional.ofNullable(gradePo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new GradeException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<GradePo> findAllMatching(GradePo entity) {
        QueryWrapper<GradePo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<GradePo> selectPage(PageQuery<GradePo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<GradePo> gradePos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<GradePo> pageInfo = new PageInfo<>(gradePos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

