package com.jhf.youke.sales.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * 建议映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface RecommendedMapper  extends BaseMapper<RecommendedPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sale_recommended set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sale_recommended set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 获得父列表
     *
     * @param companyId 公司标识
     * @param userId    用户id
     * @return {@link List}<{@link RecommendedPo}>
     */
    List<RecommendedPo> getParentList(@Param("companyId")Long companyId, @Param("userId") Long userId);

    /**
     * 通过用户
     *
     * @param companyId 公司标识
     * @param userId    用户id
     * @return {@link RecommendedPo}
     */
    RecommendedPo getByUser(@Param("companyId")Long companyId, @Param("userId") Long userId);

    /**
     * 通过id
     *
     * @param ids id
     * @return {@link List}<{@link RecommendedPo}>
     */
    List<RecommendedPo>  getByIds (@Param("ids")String ids);

}

