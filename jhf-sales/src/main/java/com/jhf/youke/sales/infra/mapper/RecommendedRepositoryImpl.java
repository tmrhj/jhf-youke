package com.jhf.youke.sales.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.sales.domain.exception.RecommendedException;
import com.jhf.youke.sales.domain.gateway.RecommendedRepository;
import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author RHJ
 */
@Service(value = "RecommendedRepository")
public class RecommendedRepositoryImpl extends ServiceImpl<RecommendedMapper, RecommendedPo> implements RecommendedRepository {


    @Override
    public boolean update(RecommendedPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<RecommendedPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(RecommendedPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(RecommendedPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) > 0 ;
    }

    @Override
    public boolean insertBatch(List<RecommendedPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<RecommendedPo> findById(Long id) {
        RecommendedPo recommendedPo = baseMapper.selectById(id);
        return Optional.ofNullable(recommendedPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new RecommendedException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<RecommendedPo> findAllMatching(RecommendedPo entity) {
        QueryWrapper<RecommendedPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<RecommendedPo> selectPage(PageQuery<RecommendedPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<RecommendedPo> recommendedPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<RecommendedPo> pageInfo = new PageInfo<>(recommendedPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<RecommendedPo> getSonList(Long companyId, Long salesman) {
        QueryWrapper<RecommendedPo> qw = new QueryWrapper<>();
        qw.eq("company_id",companyId);
        qw.likeRight("parent_ids", salesman+",");
        return baseMapper.selectList(qw);
    }

    @Override
    public List<RecommendedPo> getParentList(Long companyId, Long salesman) {
        List<RecommendedPo> list = new ArrayList<>();
        RecommendedPo recommendedPo = baseMapper.getByUser(companyId, salesman);
        list.add(recommendedPo);
        String parentIds = recommendedPo.getParentIds();
        parentIds =StringUtils.substringBefore( parentIds,","+salesman+",");
        List<RecommendedPo> poList = baseMapper.getByIds(parentIds);
        list.addAll(poList);
        return list;
    }

    @Override
    public RecommendedPo getByUser(Long companyId, Long userId) {
        return baseMapper.getByUser(companyId,userId);
    }

}

