import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.jhf.youke.SalesApplication;
import com.jhf.youke.sales.app.consume.CommissionConsumeService;
import com.jhf.youke.sales.app.feign.ProductFeign;
import com.jhf.youke.sales.domain.model.dto.OrderDto;
import com.jhf.youke.sales.domain.model.dto.OrderItemDto;
import com.jhf.youke.sales.domain.model.dto.ProductCommissionDto;
import com.jhf.youke.sales.domain.model.po.RecommendedPo;
import com.jhf.youke.sales.domain.service.RecommendedService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootTest(classes = SalesApplication.class)
class SalesTestApplication {

    @Resource
    ProductFeign productFeign;

    @Resource
    RecommendedService recommendedService;

    @Resource
    CommissionConsumeService commissionConsumeService;

    @Test
    void test1(){
       Map<String,Object> o =  (Map<String,Object>) productFeign.getProductReleaseCommission(1L);
       List<Map<String,Object>> data = (List<Map<String,Object>>) o.get("data");
       List<ProductCommissionDto> list = new ArrayList<>();
       data.forEach( map ->{
               ProductCommissionDto product = BeanUtil.mapToBean(map, ProductCommissionDto.class, false, new CopyOptions());
               list.add(product);
           }

       );
       log.info(o.toString());
    }

    @Test
    void test2(){
        List<RecommendedPo> list1 = recommendedService.getParentList(1L, 10101L);
        log.info(list1.toString());

        List<RecommendedPo> list2 = recommendedService.getParentList(1L, 10101L);
        log.info(list2.toString());
    }

    @Test
    void test3(){
        OrderDto orderDto = new OrderDto();
        orderDto.setOrderId(1L);
        orderDto.setCompanyId(1L);
        orderDto.setSalesman(10101L);
        List<OrderItemDto> list = new ArrayList<>();
        OrderItemDto item1 = new OrderItemDto();
        item1.setNum(new BigDecimal("2"));
        item1.setProductName("苹果");
        item1.setProductReleaseId(1L);
        item1.setPropertyId1(0L);
        item1.setPropertyId2(0L);
        item1.setPropertyId3(0L);
        list.add(item1);
        orderDto.setOrderItems(list);
        commissionConsumeService.createCommission(orderDto);
    }



}
