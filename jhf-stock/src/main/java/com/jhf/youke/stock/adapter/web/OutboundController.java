package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import com.jhf.youke.stock.app.executor.OutboundAppService;
import com.jhf.youke.stock.domain.model.dto.OutboundDto;
import com.jhf.youke.stock.domain.model.vo.OutboundVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "")
@RestController
@RequestMapping("/outbound")
public class OutboundController {

    @Resource
    private OutboundAppService outboundAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody OutboundDto dto) {
        return Response.ok(outboundAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody OutboundDto dto) {
        return Response.ok(outboundAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody OutboundDto dto) {
        return Response.ok(outboundAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<OutboundVo>> findById(Long id) {
        return Response.ok(outboundAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(outboundAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<OutboundVo>> list(@RequestBody OutboundDto dto, @RequestHeader("token") String token) {

        return Response.ok(outboundAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<OutboundVo>> selectPage(@RequestBody  OutboundDto dto, @RequestHeader("token") String token) {
        return Response.ok(outboundAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(outboundAppService.getPreData());
    }


    @PostMapping("/accountConfirm")
    @ApiOperation("料账确认")
    public Response<Boolean> accountConfirm(@RequestBody OutboundDto dto) {
        return Response.ok(outboundAppService.accountConfirm(dto));
    }

    @PostMapping("/whConfirm")
    @ApiOperation("仓库确认")
    public Response<Boolean> whConfirm(@RequestBody  OutboundDto dto) {
        return Response.ok(outboundAppService.whConfirm(dto));
    }
    

}

