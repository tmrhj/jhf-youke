package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.OutboundListAppService;
import com.jhf.youke.stock.domain.model.dto.OutboundListDto;
import com.jhf.youke.stock.domain.model.vo.OutboundListVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "")
@RestController
@RequestMapping("/outboundList")
public class OutboundListController {

    @Resource
    private OutboundListAppService outboundListAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody OutboundListDto dto) {
        return Response.ok(outboundListAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody OutboundListDto dto) {
        return Response.ok(outboundListAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody OutboundListDto dto) {
        return Response.ok(outboundListAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<OutboundListVo>> findById(Long id) {
        return Response.ok(outboundListAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(outboundListAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<OutboundListVo>> list(@RequestBody OutboundListDto dto, @RequestHeader("token") String token) {

        return Response.ok(outboundListAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<OutboundListVo>> selectPage(@RequestBody  OutboundListDto dto, @RequestHeader("token") String token) {
        return Response.ok(outboundListAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(outboundListAppService.getPreData());
    }
    

}

