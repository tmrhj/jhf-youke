package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.ReceiveAppService;
import com.jhf.youke.stock.domain.model.dto.ReceiveDto;
import com.jhf.youke.stock.domain.model.vo.ReceiveVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "入库单")
@RestController
@RequestMapping("/receive")
public class ReceiveController {

    @Resource
    private ReceiveAppService receiveAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ReceiveDto dto) {
        return Response.ok(receiveAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ReceiveDto dto) {
        return Response.ok(receiveAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ReceiveDto dto) {
        return Response.ok(receiveAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ReceiveVo>> findById(Long id) {
        return Response.ok(receiveAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(receiveAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ReceiveVo>> list(@RequestBody ReceiveDto dto, @RequestHeader("token") String token) {

        return Response.ok(receiveAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ReceiveVo>> selectPage(@RequestBody  ReceiveDto dto, @RequestHeader("token") String token) {
        return Response.ok(receiveAppService.selectPage(dto));
    }

    @PostMapping("/accountConfirm")
    @ApiOperation("料账确认")
    public Response<Boolean> accountConfirm(@RequestBody  ReceiveDto dto, @RequestHeader("token") String token) {
        return Response.ok(receiveAppService.accountConfirm(dto));
    }

    @PostMapping("/whConfirm")
    @ApiOperation("仓库确认")
    public Response<Boolean> whConfirm(@RequestBody  ReceiveDto dto, @RequestHeader("token") String token) {
        return Response.ok(receiveAppService.whConfirm(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(receiveAppService.getPreData());
    }
    

}

