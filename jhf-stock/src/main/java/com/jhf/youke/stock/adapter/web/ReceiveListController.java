package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.ReceiveListAppService;
import com.jhf.youke.stock.domain.model.dto.ReceiveListDto;
import com.jhf.youke.stock.domain.model.vo.ReceiveListVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "入库明细表")
@RestController
@RequestMapping("/receiveList")
public class ReceiveListController {

    @Resource
    private ReceiveListAppService receiveListAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody ReceiveListDto dto) {
        return Response.ok(receiveListAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody ReceiveListDto dto) {
        return Response.ok(receiveListAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody ReceiveListDto dto) {
        return Response.ok(receiveListAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<ReceiveListVo>> findById(Long id) {
        return Response.ok(receiveListAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(receiveListAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<ReceiveListVo>> list(@RequestBody ReceiveListDto dto, @RequestHeader("token") String token) {

        return Response.ok(receiveListAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<ReceiveListVo>> selectPage(@RequestBody  ReceiveListDto dto, @RequestHeader("token") String token) {
        return Response.ok(receiveListAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(receiveListAppService.getPreData());
    }
    

}

