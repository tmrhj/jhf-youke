package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import com.jhf.youke.stock.app.executor.StockAppService;
import com.jhf.youke.stock.domain.model.dto.StockDto;
import com.jhf.youke.stock.domain.model.po.StockPo;
import com.jhf.youke.stock.domain.model.vo.StockVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "库存表")
@RestController
@RequestMapping("/stock")
public class StockController {

    @Resource
    private StockAppService stockAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody StockDto dto) {
        return Response.ok(stockAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody StockDto dto) {
        return Response.ok(stockAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody StockDto dto) {
        return Response.ok(stockAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<StockVo>> findById(Long id) {
        return Response.ok(stockAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(stockAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<StockVo>> list(@RequestBody StockDto dto, @RequestHeader("token") String token) {

        return Response.ok(stockAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<StockVo>> selectPage(@RequestBody  StockDto dto, @RequestHeader("token") String token) {
        return Response.ok(stockAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(stockAppService.getPreData());
    }


    @PostMapping("/getListByProductId")
    @ApiOperation("根据产品信息查询库存")
    public Response<List<StockPo>> getListByProductId(@RequestBody StockDto stockDto){
        return Response.ok(stockAppService.getListByProductId(stockDto));
    }

    @PostMapping("/test")
    @ApiOperation("出库明细汇总")
    public Response<Map<String, Map<String, BigDecimal>>> sumStock(@RequestBody StockDto stockDto){
        return Response.ok(stockAppService.sumStock(stockDto));
    }
    

}

