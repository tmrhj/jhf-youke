package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.StockFrostAppService;
import com.jhf.youke.stock.domain.model.dto.StockFrostDto;
import com.jhf.youke.stock.domain.model.vo.StockFrostVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "库存冻结表")
@RestController
@RequestMapping("/stockFrost")
public class StockFrostController {

    @Resource
    private StockFrostAppService stockFrostAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody StockFrostDto dto) {
        return Response.ok(stockFrostAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody StockFrostDto dto) {
        return Response.ok(stockFrostAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody StockFrostDto dto) {
        return Response.ok(stockFrostAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<StockFrostVo>> findById(Long id) {
        return Response.ok(stockFrostAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(stockFrostAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<StockFrostVo>> list(@RequestBody StockFrostDto dto, @RequestHeader("token") String token) {

        return Response.ok(stockFrostAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<StockFrostVo>> selectPage(@RequestBody  StockFrostDto dto, @RequestHeader("token") String token) {
        return Response.ok(stockFrostAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(stockFrostAppService.getPreData());
    }
    

}

