package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.VoucherAppService;
import com.jhf.youke.stock.domain.model.dto.VoucherDto;
import com.jhf.youke.stock.domain.model.vo.VoucherVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "出入库凭证表")
@RestController
@RequestMapping("/voucher")
public class VoucherController {

    @Resource
    private VoucherAppService voucherAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody VoucherDto dto) {
        return Response.ok(voucherAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody VoucherDto dto) {
        return Response.ok(voucherAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody VoucherDto dto) {
        return Response.ok(voucherAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<VoucherVo>> findById(Long id) {
        return Response.ok(voucherAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(voucherAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<VoucherVo>> list(@RequestBody VoucherDto dto, @RequestHeader("token") String token) {

        return Response.ok(voucherAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<VoucherVo>> selectPage(@RequestBody  VoucherDto dto, @RequestHeader("token") String token) {
        return Response.ok(voucherAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(voucherAppService.getPreData());
    }
    

}

