package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.VoucherListAppService;
import com.jhf.youke.stock.domain.model.dto.VoucherListDto;
import com.jhf.youke.stock.domain.model.vo.VoucherListVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "")
@RestController
@RequestMapping("/voucherList")
public class VoucherListController {

    @Resource
    private VoucherListAppService voucherListAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody VoucherListDto dto) {
        return Response.ok(voucherListAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody VoucherListDto dto) {
        return Response.ok(voucherListAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody VoucherListDto dto) {
        return Response.ok(voucherListAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<VoucherListVo>> findById(Long id) {
        return Response.ok(voucherListAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(voucherListAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<VoucherListVo>> list(@RequestBody VoucherListDto dto, @RequestHeader("token") String token) {

        return Response.ok(voucherListAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<VoucherListVo>> selectPage(@RequestBody  VoucherListDto dto, @RequestHeader("token") String token) {
        return Response.ok(voucherListAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(voucherListAppService.getPreData());
    }
    

}

