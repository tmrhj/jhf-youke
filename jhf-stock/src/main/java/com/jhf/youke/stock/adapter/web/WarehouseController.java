package com.jhf.youke.stock.adapter.web;

import com.jhf.youke.stock.app.executor.WarehouseAppService;
import com.jhf.youke.stock.domain.model.dto.WarehouseDto;
import com.jhf.youke.stock.domain.model.vo.WarehouseVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Api(tags = "仓库表")
@RestController
@RequestMapping("/warehouse")
public class WarehouseController {

    @Resource
    private WarehouseAppService warehouseAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody WarehouseDto dto) {
        return Response.ok(warehouseAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody WarehouseDto dto) {
        return Response.ok(warehouseAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody WarehouseDto dto) {
        return Response.ok(warehouseAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<WarehouseVo>> findById(Long id) {
        return Response.ok(warehouseAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(warehouseAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<WarehouseVo>> list(@RequestBody WarehouseDto dto, @RequestHeader("token") String token) {

        return Response.ok(warehouseAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<WarehouseVo>> selectPage(@RequestBody  WarehouseDto dto, @RequestHeader("token") String token) {
        return Response.ok(warehouseAppService.selectPage(dto));
    }
    
    @GetMapping("/getPreData")
    @ApiOperation("获取前置数据")
    public Response<Long> getPreData() {
        return Response.ok(warehouseAppService.getPreData());
    }
    

}

