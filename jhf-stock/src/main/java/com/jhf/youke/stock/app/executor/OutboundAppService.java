package com.jhf.youke.stock.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.converter.OutboundConverter;
import com.jhf.youke.stock.domain.model.Do.OutboundDo;
import com.jhf.youke.stock.domain.model.Do.StockDo;
import com.jhf.youke.stock.domain.model.dto.OutboundDto;
import com.jhf.youke.stock.domain.model.vo.OutboundVo;
import com.jhf.youke.stock.domain.service.OutboundService;
import com.jhf.youke.stock.domain.service.StockFrostService;
import com.jhf.youke.stock.domain.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class OutboundAppService extends BaseAppService {

    @Resource
    private OutboundService outboundService;

    @Resource
    private OutboundConverter outboundConverter;

    @Resource
    private StockService stockService;

    @Resource
    private StockFrostService stockFrostService;


    public boolean update(OutboundDto dto) {
        OutboundDo outboundDo =  outboundConverter.dto2Do(dto);
        return outboundService.update(outboundDo);
    }

    public boolean delete(OutboundDto dto) {
        OutboundDo outboundDo =  outboundConverter.dto2Do(dto);
        return outboundService.delete(outboundDo);
    }


    public boolean insert(OutboundDto dto) {
        OutboundDo outboundDo =  outboundConverter.dto2Do(dto);
        return outboundService.insert(outboundDo);
    }

    public Optional<OutboundVo> findById(Long id) {
        return outboundService.findById(id);
    }


    public boolean remove(Long id) {
        return outboundService.remove(id);
    }


    public List<OutboundVo> findAllMatching(OutboundDto dto) {
        OutboundDo outboundDo =  outboundConverter.dto2Do(dto);
        return outboundService.findAllMatching(outboundDo);
    }


    public Pagination<OutboundVo> selectPage(OutboundDto dto) {
        OutboundDo outboundDo =  outboundConverter.dto2Do(dto);
        return outboundService.selectPage(outboundDo);
    }


    @Transactional
    public Boolean accountConfirm(OutboundDto outboundDto) {
        OutboundDo outboundDo = outboundConverter.dto2Do(outboundDto);
        outboundService.confirmAccount(outboundDo);
        Boolean stockFrozen = freezeStock(outboundDto);
        return stockFrozen;
    }

    private Boolean freezeStock(OutboundDto outboundDto) {
        StockDo stockDo = new StockDo(outboundDto);
        List<StockDo> frozenStock = stockService.freezeStock(stockDo);
        Boolean result = stockFrostService.create(frozenStock);
        return result;
    }


    public Boolean whConfirm(OutboundDto dto) {
        OutboundDo outboundDo =  outboundConverter.dto2Do(dto);
        outboundService.whConfirm(outboundDo);
        return false;
    }


}

