package com.jhf.youke.stock.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.converter.OutboundListConverter;
import com.jhf.youke.stock.domain.model.Do.OutboundListDo;
import com.jhf.youke.stock.domain.model.dto.OutboundListDto;
import com.jhf.youke.stock.domain.model.vo.OutboundListVo;
import com.jhf.youke.stock.domain.service.OutboundListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class OutboundListAppService extends BaseAppService {

    @Resource
    private OutboundListService outboundListService;

    @Resource
    private OutboundListConverter outboundListConverter;


    public boolean update(OutboundListDto dto) {
        OutboundListDo outboundListDo =  outboundListConverter.dto2Do(dto);
        return outboundListService.update(outboundListDo);
    }

    public boolean delete(OutboundListDto dto) {
        OutboundListDo outboundListDo =  outboundListConverter.dto2Do(dto);
        return outboundListService.delete(outboundListDo);
    }


    public boolean insert(OutboundListDto dto) {
        OutboundListDo outboundListDo =  outboundListConverter.dto2Do(dto);
        return outboundListService.insert(outboundListDo);
    }

    public Optional<OutboundListVo> findById(Long id) {
        return outboundListService.findById(id);
    }


    public boolean remove(Long id) {
        return outboundListService.remove(id);
    }


    public List<OutboundListVo> findAllMatching(OutboundListDto dto) {
        OutboundListDo outboundListDo =  outboundListConverter.dto2Do(dto);
        return outboundListService.findAllMatching(outboundListDo);
    }


    public Pagination<OutboundListVo> selectPage(OutboundListDto dto) {
        OutboundListDo outboundListDo =  outboundListConverter.dto2Do(dto);
        return outboundListService.selectPage(outboundListDo);
    }
    
  

}

