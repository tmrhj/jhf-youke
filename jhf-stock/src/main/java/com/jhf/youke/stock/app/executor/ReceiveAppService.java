package com.jhf.youke.stock.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.converter.ReceiveConverter;
import com.jhf.youke.stock.domain.model.Do.ReceiveDo;
import com.jhf.youke.stock.domain.model.dto.ReceiveDto;
import com.jhf.youke.stock.domain.model.vo.ReceiveVo;
import com.jhf.youke.stock.domain.service.ReceiveService;
import com.jhf.youke.stock.domain.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class ReceiveAppService extends BaseAppService {

    @Resource
    private ReceiveService receiveService;

    @Resource
    private ReceiveConverter receiveConverter;

    @Resource
    private StockService stockService;


    public boolean update(ReceiveDto dto) {
        ReceiveDo receiveDo =  receiveConverter.dto2Do(dto);
        return receiveService.update(receiveDo);
    }

    public boolean delete(ReceiveDto dto) {
        ReceiveDo receiveDo =  receiveConverter.dto2Do(dto);
        return receiveService.delete(receiveDo);
    }


    public boolean insert(ReceiveDto dto) {
        ReceiveDo receiveDo =  receiveConverter.dto2Do(dto);
        return receiveService.insert(receiveDo);
    }

    public Optional<ReceiveVo> findById(Long id) {
        return receiveService.findById(id);
    }


    public boolean remove(Long id) {
        return receiveService.remove(id);
    }


    public List<ReceiveVo> findAllMatching(ReceiveDto dto) {
        ReceiveDo receiveDo =  receiveConverter.dto2Do(dto);
        return receiveService.findAllMatching(receiveDo);
    }


    public Pagination<ReceiveVo> selectPage(ReceiveDto dto) {
        ReceiveDo receiveDo =  receiveConverter.dto2Do(dto);
        return receiveService.selectPage(receiveDo);
    }


    public Boolean accountConfirm(ReceiveDto dto) {

        return false;
    }

    public Boolean whConfirm(ReceiveDto dto) {

        return false;
    }
}

