package com.jhf.youke.stock.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.stock.domain.converter.ReceiveListConverter;
import com.jhf.youke.stock.domain.model.Do.ReceiveListDo;
import com.jhf.youke.stock.domain.model.dto.ReceiveListDto;
import com.jhf.youke.stock.domain.model.vo.ReceiveListVo;
import com.jhf.youke.stock.domain.service.ReceiveListService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class ReceiveListAppService extends BaseAppService {

    @Resource
    private ReceiveListService receiveListService;

    @Resource
    private ReceiveListConverter receiveListConverter;


    public boolean update(ReceiveListDto dto) {
        ReceiveListDo receiveListDo =  receiveListConverter.dto2Do(dto);
        return receiveListService.update(receiveListDo);
    }

    public boolean delete(ReceiveListDto dto) {
        ReceiveListDo receiveListDo =  receiveListConverter.dto2Do(dto);
        return receiveListService.delete(receiveListDo);
    }


    public boolean insert(ReceiveListDto dto) {
        ReceiveListDo receiveListDo =  receiveListConverter.dto2Do(dto);
        return receiveListService.insert(receiveListDo);
    }

    public Optional<ReceiveListVo> findById(Long id) {
        return receiveListService.findById(id);
    }


    public boolean remove(Long id) {
        return receiveListService.remove(id);
    }


    public List<ReceiveListVo> findAllMatching(ReceiveListDto dto) {
        ReceiveListDo receiveListDo =  receiveListConverter.dto2Do(dto);
        return receiveListService.findAllMatching(receiveListDo);
    }


    public Pagination<ReceiveListVo> selectPage(ReceiveListDto dto) {
        ReceiveListDo receiveListDo =  receiveListConverter.dto2Do(dto);
        return receiveListService.selectPage(receiveListDo);
    }
    
  

}

