package com.jhf.youke.stock.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.converter.StockConverter;
import com.jhf.youke.stock.domain.model.Do.StockDo;
import com.jhf.youke.stock.domain.model.dto.StockDto;
import com.jhf.youke.stock.domain.model.po.StockPo;
import com.jhf.youke.stock.domain.model.vo.StockVo;
import com.jhf.youke.stock.domain.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class StockAppService extends BaseAppService {

    @Resource
    private StockService stockService;

    @Resource
    private StockConverter stockConverter;


    public boolean update(StockDto dto) {
        StockDo stockDo =  stockConverter.dto2Do(dto);
        return stockService.update(stockDo);
    }

    public boolean delete(StockDto dto) {
        StockDo stockDo =  stockConverter.dto2Do(dto);
        return stockService.delete(stockDo);
    }


    public boolean insert(StockDto dto) {
        StockDo stockDo =  stockConverter.dto2Do(dto);
        return stockService.insert(stockDo);
    }

    public Optional<StockVo> findById(Long id) {
        return stockService.findById(id);
    }


    public boolean remove(Long id) {
        return stockService.remove(id);
    }


    public List<StockVo> findAllMatching(StockDto dto) {
        StockDo stockDo =  stockConverter.dto2Do(dto);
        return stockService.findAllMatching(stockDo);
    }


    public Pagination<StockVo> selectPage(StockDto dto) {
        StockDo stockDo =  stockConverter.dto2Do(dto);
        return stockService.selectPage(stockDo);
    }

    public List<StockPo> getListByProductId(StockDto dto){

        return stockService.getListByProductId(dto);
    }

    public Map<String, Map<String, BigDecimal>> sumStock(StockDto dto){
        StockDo stockDo =  stockConverter.dto2Do(dto);
        Map<String, Map<String, BigDecimal>> map = stockDo.sumStock();
        Map<String, BigDecimal> a = map.get("A");
        for (Map.Entry<String, BigDecimal> entry : a.entrySet()){
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        return stockDo.sumStock();
    }
    
  

}

