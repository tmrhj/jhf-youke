package com.jhf.youke.stock.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.stock.domain.converter.StockFrostConverter;
import com.jhf.youke.stock.domain.model.Do.StockFrostDo;
import com.jhf.youke.stock.domain.model.dto.StockFrostDto;
import com.jhf.youke.stock.domain.model.vo.StockFrostVo;
import com.jhf.youke.stock.domain.service.StockFrostService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class StockFrostAppService extends BaseAppService {

    @Resource
    private StockFrostService stockFrostService;

    @Resource
    private StockFrostConverter stockFrostConverter;


    public boolean update(StockFrostDto dto) {
        StockFrostDo stockFrostDo =  stockFrostConverter.dto2Do(dto);
        return stockFrostService.update(stockFrostDo);
    }

    public boolean delete(StockFrostDto dto) {
        StockFrostDo stockFrostDo =  stockFrostConverter.dto2Do(dto);
        return stockFrostService.delete(stockFrostDo);
    }


    public boolean insert(StockFrostDto dto) {
        StockFrostDo stockFrostDo =  stockFrostConverter.dto2Do(dto);
        return stockFrostService.insert(stockFrostDo);
    }

    public Optional<StockFrostVo> findById(Long id) {
        return stockFrostService.findById(id);
    }


    public boolean remove(Long id) {
        return stockFrostService.remove(id);
    }


    public List<StockFrostVo> findAllMatching(StockFrostDto dto) {
        StockFrostDo stockFrostDo =  stockFrostConverter.dto2Do(dto);
        return stockFrostService.findAllMatching(stockFrostDo);
    }


    public Pagination<StockFrostVo> selectPage(StockFrostDto dto) {
        StockFrostDo stockFrostDo =  stockFrostConverter.dto2Do(dto);
        return stockFrostService.selectPage(stockFrostDo);
    }
    
  

}

