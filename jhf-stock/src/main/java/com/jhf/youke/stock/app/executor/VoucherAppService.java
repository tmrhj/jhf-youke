package com.jhf.youke.stock.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.stock.domain.converter.VoucherConverter;
import com.jhf.youke.stock.domain.model.Do.VoucherDo;
import com.jhf.youke.stock.domain.model.dto.VoucherDto;
import com.jhf.youke.stock.domain.model.vo.VoucherVo;
import com.jhf.youke.stock.domain.service.VoucherService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class VoucherAppService extends BaseAppService {

    @Resource
    private VoucherService voucherService;

    @Resource
    private VoucherConverter voucherConverter;


    public boolean update(VoucherDto dto) {
        VoucherDo voucherDo =  voucherConverter.dto2Do(dto);
        return voucherService.update(voucherDo);
    }

    public boolean delete(VoucherDto dto) {
        VoucherDo voucherDo =  voucherConverter.dto2Do(dto);
        return voucherService.delete(voucherDo);
    }


    public boolean insert(VoucherDto dto) {
        VoucherDo voucherDo =  voucherConverter.dto2Do(dto);
        return voucherService.insert(voucherDo);
    }

    public Optional<VoucherVo> findById(Long id) {
        return voucherService.findById(id);
    }


    public boolean remove(Long id) {
        return voucherService.remove(id);
    }


    public List<VoucherVo> findAllMatching(VoucherDto dto) {
        VoucherDo voucherDo =  voucherConverter.dto2Do(dto);
        return voucherService.findAllMatching(voucherDo);
    }


    public Pagination<VoucherVo> selectPage(VoucherDto dto) {
        VoucherDo voucherDo =  voucherConverter.dto2Do(dto);
        return voucherService.selectPage(voucherDo);
    }
    
  

}

