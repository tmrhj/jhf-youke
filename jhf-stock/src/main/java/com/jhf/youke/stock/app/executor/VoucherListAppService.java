package com.jhf.youke.stock.app.executor;
import com.jhf.youke.core.utils.IdGen;
import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.stock.domain.converter.VoucherListConverter;
import com.jhf.youke.stock.domain.model.Do.VoucherListDo;
import com.jhf.youke.stock.domain.model.dto.VoucherListDto;
import com.jhf.youke.stock.domain.model.vo.VoucherListVo;
import com.jhf.youke.stock.domain.service.VoucherListService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class VoucherListAppService extends BaseAppService {

    @Resource
    private VoucherListService voucherListService;

    @Resource
    private VoucherListConverter voucherListConverter;


    public boolean update(VoucherListDto dto) {
        VoucherListDo voucherListDo =  voucherListConverter.dto2Do(dto);
        return voucherListService.update(voucherListDo);
    }

    public boolean delete(VoucherListDto dto) {
        VoucherListDo voucherListDo =  voucherListConverter.dto2Do(dto);
        return voucherListService.delete(voucherListDo);
    }


    public boolean insert(VoucherListDto dto) {
        VoucherListDo voucherListDo =  voucherListConverter.dto2Do(dto);
        return voucherListService.insert(voucherListDo);
    }

    public Optional<VoucherListVo> findById(Long id) {
        return voucherListService.findById(id);
    }


    public boolean remove(Long id) {
        return voucherListService.remove(id);
    }


    public List<VoucherListVo> findAllMatching(VoucherListDto dto) {
        VoucherListDo voucherListDo =  voucherListConverter.dto2Do(dto);
        return voucherListService.findAllMatching(voucherListDo);
    }


    public Pagination<VoucherListVo> selectPage(VoucherListDto dto) {
        VoucherListDo voucherListDo =  voucherListConverter.dto2Do(dto);
        return voucherListService.selectPage(voucherListDo);
    }
    
  

}

