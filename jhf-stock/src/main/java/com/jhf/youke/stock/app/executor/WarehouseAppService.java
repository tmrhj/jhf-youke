package com.jhf.youke.stock.app.executor;

import com.jhf.youke.core.ddd.BaseAppService;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.converter.WarehouseConverter;
import com.jhf.youke.stock.domain.model.Do.WarehouseDo;
import com.jhf.youke.stock.domain.model.dto.WarehouseDto;
import com.jhf.youke.stock.domain.model.vo.WarehouseVo;
import com.jhf.youke.stock.domain.service.WarehouseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Slf4j
@Service
public class WarehouseAppService extends BaseAppService {

    @Resource
    private WarehouseService warehouseService;

    @Resource
    private WarehouseConverter warehouseConverter;


    public boolean update(WarehouseDto dto) {
        WarehouseDo warehouseDo =  warehouseConverter.dto2Do(dto);
        return warehouseService.update(warehouseDo);
    }

    public boolean delete(WarehouseDto dto) {
        WarehouseDo warehouseDo =  warehouseConverter.dto2Do(dto);
        return warehouseService.delete(warehouseDo);
    }


    public boolean insert(WarehouseDto dto) {
        WarehouseDo warehouseDo =  warehouseConverter.dto2Do(dto);
        return warehouseService.insert(warehouseDo);
    }

    public Optional<WarehouseVo> findById(Long id) {
        return warehouseService.findById(id);
    }


    public boolean remove(Long id) {
        return warehouseService.remove(id);
    }


    public List<WarehouseVo> findAllMatching(WarehouseDto dto) {
        WarehouseDo warehouseDo =  warehouseConverter.dto2Do(dto);
        return warehouseService.findAllMatching(warehouseDo);
    }


    public Pagination<WarehouseVo> selectPage(WarehouseDto dto) {
        WarehouseDo warehouseDo =  warehouseConverter.dto2Do(dto);
        return warehouseService.selectPage(warehouseDo);
    }


    
  

}

