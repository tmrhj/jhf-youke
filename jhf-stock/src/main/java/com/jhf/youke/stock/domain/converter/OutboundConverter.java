package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.OutboundDo;
import com.jhf.youke.stock.domain.model.dto.OutboundDto;
import com.jhf.youke.stock.domain.model.po.OutboundPo;
import com.jhf.youke.stock.domain.model.vo.OutboundVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface OutboundConverter extends BaseConverter<OutboundDo,OutboundPo,OutboundDto,OutboundVo> {


}

