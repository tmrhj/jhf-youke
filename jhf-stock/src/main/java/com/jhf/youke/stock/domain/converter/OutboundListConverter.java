package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.OutboundListDo;
import com.jhf.youke.stock.domain.model.dto.OutboundListDto;
import com.jhf.youke.stock.domain.model.po.OutboundListPo;
import com.jhf.youke.stock.domain.model.vo.OutboundListVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface OutboundListConverter extends BaseConverter<OutboundListDo,OutboundListPo,OutboundListDto,OutboundListVo> {


}

