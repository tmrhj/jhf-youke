package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.ReceiveDo;
import com.jhf.youke.stock.domain.model.dto.ReceiveDto;
import com.jhf.youke.stock.domain.model.po.ReceivePo;
import com.jhf.youke.stock.domain.model.vo.ReceiveVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface ReceiveConverter extends BaseConverter<ReceiveDo,ReceivePo,ReceiveDto,ReceiveVo> {


}

