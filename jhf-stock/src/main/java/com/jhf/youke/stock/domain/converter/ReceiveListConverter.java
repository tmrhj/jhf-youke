package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.ReceiveListDo;
import com.jhf.youke.stock.domain.model.dto.ReceiveListDto;
import com.jhf.youke.stock.domain.model.po.ReceiveListPo;
import com.jhf.youke.stock.domain.model.vo.ReceiveListVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface ReceiveListConverter extends BaseConverter<ReceiveListDo,ReceiveListPo,ReceiveListDto,ReceiveListVo> {


}

