package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.core.ddd.BaseConverter;
import com.jhf.youke.stock.domain.model.Do.StockDo;
import com.jhf.youke.stock.domain.model.dto.StockDto;
import com.jhf.youke.stock.domain.model.po.StockPo;
import com.jhf.youke.stock.domain.model.vo.StockVo;
import org.mapstruct.Mapper;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface StockConverter extends BaseConverter<StockDo,StockPo,StockDto,StockVo> {


}

