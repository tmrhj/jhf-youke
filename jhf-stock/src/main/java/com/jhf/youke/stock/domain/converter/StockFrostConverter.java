package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.StockFrostDo;
import com.jhf.youke.stock.domain.model.dto.StockFrostDto;
import com.jhf.youke.stock.domain.model.po.StockFrostPo;
import com.jhf.youke.stock.domain.model.vo.StockFrostVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface StockFrostConverter extends BaseConverter<StockFrostDo,StockFrostPo,StockFrostDto,StockFrostVo> {


}

