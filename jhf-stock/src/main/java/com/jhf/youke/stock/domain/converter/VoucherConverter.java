package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.VoucherDo;
import com.jhf.youke.stock.domain.model.dto.VoucherDto;
import com.jhf.youke.stock.domain.model.po.VoucherPo;
import com.jhf.youke.stock.domain.model.vo.VoucherVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface VoucherConverter extends BaseConverter<VoucherDo,VoucherPo,VoucherDto,VoucherVo> {


}

