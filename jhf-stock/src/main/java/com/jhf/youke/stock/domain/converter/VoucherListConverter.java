package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.VoucherListDo;
import com.jhf.youke.stock.domain.model.dto.VoucherListDto;
import com.jhf.youke.stock.domain.model.po.VoucherListPo;
import com.jhf.youke.stock.domain.model.vo.VoucherListVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface VoucherListConverter extends BaseConverter<VoucherListDo,VoucherListPo,VoucherListDto,VoucherListVo> {


}

