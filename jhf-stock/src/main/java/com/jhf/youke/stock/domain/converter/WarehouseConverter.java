package com.jhf.youke.stock.domain.converter;

import com.jhf.youke.stock.domain.model.Do.WarehouseDo;
import com.jhf.youke.stock.domain.model.dto.WarehouseDto;
import com.jhf.youke.stock.domain.model.po.WarehousePo;
import com.jhf.youke.stock.domain.model.vo.WarehouseVo;
import org.mapstruct.InheritInverseConfiguration;
import com.jhf.youke.core.ddd.BaseConverter;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper(componentModel = "spring")
public interface WarehouseConverter extends BaseConverter<WarehouseDo,WarehousePo,WarehouseDto,WarehouseVo> {


}

