package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class OutboundException extends DomainException {

    public OutboundException(String message) { super(message); }
}

