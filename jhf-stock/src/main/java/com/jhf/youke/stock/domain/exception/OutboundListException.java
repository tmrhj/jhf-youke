package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class OutboundListException extends DomainException {

    public OutboundListException(String message) { super(message); }
}

