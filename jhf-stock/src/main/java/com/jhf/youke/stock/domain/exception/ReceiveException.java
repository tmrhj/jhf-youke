package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class ReceiveException extends DomainException {

    public ReceiveException(String message) { super(message); }
}

