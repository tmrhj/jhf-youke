package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class ReceiveListException extends DomainException {

    public ReceiveListException(String message) { super(message); }
}

