package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class StockException extends DomainException {

    public StockException(String message) { super(message); }
}

