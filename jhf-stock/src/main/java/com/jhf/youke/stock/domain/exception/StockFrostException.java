package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class StockFrostException extends DomainException {

    public StockFrostException(String message) { super(message); }
}

