package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class VoucherException extends DomainException {

    public VoucherException(String message) { super(message); }
}

