package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class VoucherListException extends DomainException {

    public VoucherListException(String message) { super(message); }
}

