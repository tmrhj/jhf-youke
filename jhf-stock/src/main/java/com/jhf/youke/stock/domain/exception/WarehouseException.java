package com.jhf.youke.stock.domain.exception;


import com.jhf.youke.core.exception.DomainException;
/**
 * @author  RHJ
 **/

public class WarehouseException extends DomainException {

    public WarehouseException(String message) { super(message); }
}

