package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.OutboundListPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface OutboundListRepository extends Repository<OutboundListPo> {


}

