package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.Do.OutboundDo;
import com.jhf.youke.stock.domain.model.po.OutboundPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface OutboundRepository extends Repository<OutboundPo> {

    /**
    * @Description:  出库料账确认
    * @Param: [outboundDo] 出库单
    * @return: void
    * @Author: RHJ
    * @Date: 2023/6/28
    */
    void confirmAccount(OutboundDo outboundDo);


    /**
     * @Description:  出库仓管员确认
     * @Param: outboundDo 出库单
     * @return: void
     * @Author: RHJ
     * @Date: 2023/6/28
     */
    void confirm(OutboundDo outboundDo);

}

