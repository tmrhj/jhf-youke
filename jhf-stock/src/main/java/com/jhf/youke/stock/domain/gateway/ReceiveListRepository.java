package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.ReceiveListPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface ReceiveListRepository extends Repository<ReceiveListPo> {


}

