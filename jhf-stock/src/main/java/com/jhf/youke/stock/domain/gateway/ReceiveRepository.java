package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.ReceivePo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface ReceiveRepository extends Repository<ReceivePo> {


}

