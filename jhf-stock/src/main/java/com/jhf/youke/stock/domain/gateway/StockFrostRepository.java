package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.StockFrostPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface StockFrostRepository extends Repository<StockFrostPo> {


}

