package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.core.ddd.Repository;
import com.jhf.youke.stock.domain.model.po.StockPo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author  RHJ
 **/
public interface StockRepository extends Repository<StockPo> {


      /**
      * @Description:  根据仓库ID与产品ID查询出库存
      * @Param: [warehouseId, productIds]
      * @return: java.util.List<com.jhf.youke.stock.domain.model.po.StockPo>
      * @Author: RHJ
      * @Date: 2023/6/23
      */
      List<StockPo> getListByProductId(@Param("warehouseId") Long warehouseId,
                                       @Param("productIds") List<Long> productIds,
                                       @Param("specs") String specs);


}

