package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.VoucherListPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface VoucherListRepository extends Repository<VoucherListPo> {


}

