package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.VoucherPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface VoucherRepository extends Repository<VoucherPo> {


}

