package com.jhf.youke.stock.domain.gateway;


import com.jhf.youke.stock.domain.model.po.WarehousePo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author  RHJ
 **/
public interface WarehouseRepository extends Repository<WarehousePo> {


}

