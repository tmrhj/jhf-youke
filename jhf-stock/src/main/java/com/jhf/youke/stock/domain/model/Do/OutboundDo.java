package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.stock.domain.exception.OutboundException;
import lombok.Data;

import java.util.List;
import java.util.Objects;

/**
 * @author  RHJ
 **/

@Data
public class OutboundDo extends BaseDoEntity {

  private static final long serialVersionUID = -17561083224172768L;

    /**
     * 出库单名称
     */
    private String name;

    /**
     * 单位ID
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库标识
     */
    private Long warehouseId;

    /**
     * 状态
     */
    private Integer status;

    private List<OutboundListDo> outboundListDoList;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new OutboundException(errorMessage);
        }
        return obj;
    }

    private OutboundDo validateNull(OutboundDo outboundDo){
          //可使用链式法则进行为空检查
          outboundDo.
          requireNonNull(outboundDo, outboundDo.getRemark(),"不能为NULL");
                
        return outboundDo;
    }


}


