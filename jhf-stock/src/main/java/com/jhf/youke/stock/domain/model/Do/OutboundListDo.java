package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.stock.domain.exception.OutboundListException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
public class OutboundListDo extends BaseDoEntity {

  private static final long serialVersionUID = -80584454865700490L;

    /**
     * 主表ID
     */
    private Long outboundId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 规格型号
     */
    private String specs;

    /**
     * 计量单位
     */
    private String unitName;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 金额
     */
    private BigDecimal fee;

    /**
     * 库存ID
     */
    private Long stockId;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new OutboundListException(errorMessage);
        }
        return obj;
    }

    private OutboundListDo validateNull(OutboundListDo outboundListDo){
          //可使用链式法则进行为空检查
          outboundListDo.
          requireNonNull(outboundListDo, outboundListDo.getRemark(),"不能为NULL");
                
        return outboundListDo;
    }


}


