package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.stock.domain.exception.ReceiveException;
import lombok.Data;

import java.util.List;
import java.util.Objects;

/**
 * @author  RHJ
 **/

@Data
public class ReceiveDo extends BaseDoEntity {

  private static final long serialVersionUID = 534112257406389248L;

    /**
     * 入库单名
     */
    private String name;

    /**
     * 单位标识
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库标识
     */
    private Long warehouseId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 收货单明细
     */
    private List<ReceiveListDo> receiveListDoList;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ReceiveException(errorMessage);
        }
        return obj;
    }

    private ReceiveDo validateNull(ReceiveDo receiveDo){
          //可使用链式法则进行为空检查
          receiveDo.
          requireNonNull(receiveDo, receiveDo.getRemark(),"不能为NULL");
                
        return receiveDo;
    }


}


