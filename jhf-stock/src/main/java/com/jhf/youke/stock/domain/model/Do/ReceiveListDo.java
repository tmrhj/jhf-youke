package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.stock.domain.exception.ReceiveListException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author  RHJ
 **/

@Data
public class ReceiveListDo extends BaseDoEntity {

  private static final long serialVersionUID = 233597465520507763L;

    /**
     * 主表ID
     */
    private Long receiveId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 规格
     */
    private String specs;

    /**
     * 计量单位
     */
    private String unitName;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 金额
     */
    private BigDecimal fee;

    /**
     * 库存ID
     */
    private Long stockId;


    private List<OutboundListDo> outboundListDoList;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new ReceiveListException(errorMessage);
        }
        return obj;
    }

    private ReceiveListDo validateNull(ReceiveListDo receiveListDo){
          //可使用链式法则进行为空检查
          receiveListDo.
          requireNonNull(receiveListDo, receiveListDo.getRemark(),"不能为NULL");
                
        return receiveListDo;
    }


}


