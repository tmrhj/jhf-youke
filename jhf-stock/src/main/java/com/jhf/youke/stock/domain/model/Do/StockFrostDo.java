package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.stock.domain.exception.StockFrostException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author  RHJ
 **/

@Data
public class StockFrostDo extends BaseDoEntity {

  private static final long serialVersionUID = 595041000576121621L;

    /**
     * 库存ID
     */
    private Long stockId;

    /**
     * 业务ID
     */
    private Long bizId;

    /**
     * 业务明细ID
     */
    private Long bizListId;

    /**
     * 业务对象ID
     */
    private Integer objectId;

    /**
     * 数量
     */
    private BigDecimal num;


    private List<StockFrostDo> stockFrostDoList;

    public StockFrostDo(){

    }

    public StockFrostDo(List<StockDo> stockDoList){
        List<StockFrostDo> stockFrostDos = new ArrayList<>();
        for(StockDo stockDo : stockDoList){
            StockFrostDo stockFrost = new StockFrostDo();
            stockFrost.setStockId(stockDo.getId());
            stockFrost.setNum(stockDo.getActNum());
            stockFrost.setBizId(stockDo.getBizId());
            stockFrostDos.add(stockFrost);
        }
        this.stockFrostDoList = stockFrostDos;
    }


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new StockFrostException(errorMessage);
        }
        return obj;
    }

    private StockFrostDo validateNull(StockFrostDo stockFrostDo){
          //可使用链式法则进行为空检查
          stockFrostDo.
          requireNonNull(stockFrostDo, stockFrostDo.getRemark(),"不能为NULL");
                
        return stockFrostDo;
    }


}


