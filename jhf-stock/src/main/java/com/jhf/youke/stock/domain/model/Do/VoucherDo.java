package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.stock.domain.exception.VoucherException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
public class VoucherDo extends BaseDoEntity {

  private static final long serialVersionUID = -26583951951740510L;

    /**
     * 出入库名
     */
    private String name;

    /**
     * 单位ID
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库ID
     */
    private Long warehouseId;

    /**
     * 类型 1 入库 2 出库
     */
    private Integer type;

    /**
     * 金额
     */
    private BigDecimal fee;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new VoucherException(errorMessage);
        }
        return obj;
    }

    private VoucherDo validateNull(VoucherDo voucherDo){
          //可使用链式法则进行为空检查
          voucherDo.
          requireNonNull(voucherDo, voucherDo.getRemark(),"不能为NULL");
                
        return voucherDo;
    }


}


