package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.stock.domain.exception.VoucherListException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
public class VoucherListDo extends BaseDoEntity {

  private static final long serialVersionUID = 988279071392226619L;

    /**
     * 主表ID
     */
    private Long voucherId;

    /**
     * 库存ID
     */
    private Long stockId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 规格
     */
    private String specs;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 金额
     */
    private BigDecimal fee;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new VoucherListException(errorMessage);
        }
        return obj;
    }

    private VoucherListDo validateNull(VoucherListDo voucherListDo){
          //可使用链式法则进行为空检查
          voucherListDo.
          requireNonNull(voucherListDo, voucherListDo.getRemark(),"不能为NULL");
                
        return voucherListDo;
    }


}


