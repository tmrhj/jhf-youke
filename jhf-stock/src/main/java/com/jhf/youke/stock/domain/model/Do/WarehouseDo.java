package com.jhf.youke.stock.domain.model.Do;

import com.jhf.youke.stock.domain.exception.WarehouseException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
public class WarehouseDo extends BaseDoEntity {

  private static final long serialVersionUID = 177012786887934460L;

    /**
     * 仓库名
     */
    private String name;

    /**
     * 单位ID
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库类型
     */
    private Integer type;

    /**
     * 状态
     */
    private Integer status;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new WarehouseException(errorMessage);
        }
        return obj;
    }

    private WarehouseDo validateNull(WarehouseDo warehouseDo){
          //可使用链式法则进行为空检查
          warehouseDo.
          requireNonNull(warehouseDo, warehouseDo.getRemark(),"不能为NULL");
                
        return warehouseDo;
    }


}


