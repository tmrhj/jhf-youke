package com.jhf.youke.stock.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class OutboundDto extends BaseDtoEntity{

    private static final long serialVersionUID = -35507860052264903L;

    @ApiModelProperty(value = "出库单名称")
    private String name;
    @ApiModelProperty(value = "单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;
    @ApiModelProperty(value = "单位名")
    private String companyName;
    @ApiModelProperty(value = "仓库标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long warehouseId;

    @ApiModelProperty(value = "出库类型")
    private Integer outType;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "出库明细信息")
    private List<OutboundListDto> outboundListDoList;



}


