package com.jhf.youke.stock.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ReceiveDto extends BaseDtoEntity{

    private static final long serialVersionUID = -59496528693182728L;

    @ApiModelProperty(value = "入库单名")
    private String name;
    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;
    @ApiModelProperty(value = "单位名")
    private String companyName;
    @ApiModelProperty(value = "仓库标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long warehouseId;
    @ApiModelProperty(value = "状态")
    private Integer status;

}


