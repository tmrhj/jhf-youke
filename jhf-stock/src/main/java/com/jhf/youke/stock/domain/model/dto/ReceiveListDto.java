package com.jhf.youke.stock.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ReceiveListDto extends BaseDtoEntity{

    private static final long serialVersionUID = -46173785148742916L;

    @ApiModelProperty(value = "主表ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long receiveId;
    @ApiModelProperty(value = "产品ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;
    @ApiModelProperty(value = "产品名")
    private String productName;
    @ApiModelProperty(value = "规格")
    private String specs;
    @ApiModelProperty(value = "计量单位")
    private String unitName;
    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;
    @ApiModelProperty(value = "数量")
    private BigDecimal num;
    @ApiModelProperty(value = "金额")
    private BigDecimal fee;
    @ApiModelProperty(value = "库存ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long stockId;

}


