package com.jhf.youke.stock.domain.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.stock.domain.model.Do.StockDo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class StockDto extends BaseDtoEntity{

    private static final long serialVersionUID = 612041855412167914L;

    @ApiModelProperty(value = "产品ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;
    @ApiModelProperty(value = "产品名")
    private String productName;
    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;
    @ApiModelProperty(value = "单位名")
    private String companyName;
    @ApiModelProperty(value = "仓库标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long warehouseId;
    @ApiModelProperty(value = "入库数量")
    private BigDecimal inNum;
    @ApiModelProperty(value = "出库数量")
    private BigDecimal outNum;
    @ApiModelProperty(value = "库存数量")
    private BigDecimal whNum;
    @ApiModelProperty(value = "预占数量")
    private BigDecimal preNum;
    @ApiModelProperty(value = "可用数量")
    private BigDecimal num;
    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;
    @ApiModelProperty(value = "销售价")
    private BigDecimal price;
    @ApiModelProperty(value = "入库金额")
    private BigDecimal inFee;
    @ApiModelProperty(value = "出库金额")
    private BigDecimal outFee;
    @ApiModelProperty(value = "规格型号")
    private String specs;
    @ApiModelProperty(value = "计量单位")
    private String unitName;

    @ApiModelProperty(value = "库存金额")
    private BigDecimal fee;

    @ApiModelProperty(value = "产品IDS")
    private List<Long> productIds;

    @ApiModelProperty(value = "库存信息集合")
    private List<StockDo> stockDoList;

}


