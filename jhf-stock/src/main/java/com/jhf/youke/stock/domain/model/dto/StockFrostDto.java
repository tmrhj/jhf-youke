package com.jhf.youke.stock.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseDtoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class StockFrostDto extends BaseDtoEntity{

    private static final long serialVersionUID = 846602487301141069L;

    @ApiModelProperty(value = "库存ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long stockId;
    @ApiModelProperty(value = "业务ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;
    @ApiModelProperty(value = "业务明细ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizListId;
    @ApiModelProperty(value = "业务对象ID")
    private Integer objectId;
    @ApiModelProperty(value = "数量")
    private BigDecimal num;

}


