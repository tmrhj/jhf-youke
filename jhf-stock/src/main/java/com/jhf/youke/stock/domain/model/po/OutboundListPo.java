package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_outbound_list")
public class OutboundListPo extends BasePoEntity {

    private static final long serialVersionUID = 753437093352698691L;

    /**
     * 主表ID
     */
    private Long outboundId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 规格型号
     */
    private String specs;

    /**
     * 计量单位
     */
    private String unitName;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 金额
     */
    private BigDecimal fee;

    /**
     * 库存ID
     */
    private Long stockId;



}


