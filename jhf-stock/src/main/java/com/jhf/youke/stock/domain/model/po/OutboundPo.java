package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_outbound")
public class OutboundPo extends BasePoEntity {

    private static final long serialVersionUID = 308991278051588476L;

    /**
     * 出库单名称
     */
    private String name;

    /**
     * 单位ID
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库标识
     */
    private Long warehouseId;

    /**
     * 状态
     */
    private Integer status;



}


