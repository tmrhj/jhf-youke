package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_receive_list")
public class ReceiveListPo extends BasePoEntity {

    private static final long serialVersionUID = -17690522752081322L;

    /**
     * 主表ID
     */
    private Long receiveId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 规格
     */
    private String specs;

    /**
     * 计量单位
     */
    private String unitName;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 金额
     */
    private BigDecimal fee;

    /**
     * 库存ID
     */
    private Long stockId;



}


