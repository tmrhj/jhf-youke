package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_receive")
public class ReceivePo extends BasePoEntity {

    private static final long serialVersionUID = -49623450060299088L;

    /**
     * 入库单名
     */
    private String name;

    /**
     * 单位标识
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库标识
     */
    private Long warehouseId;

    /**
     * 状态
     */
    private Integer status;



}


