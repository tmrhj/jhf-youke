package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_stock_frost")
public class StockFrostPo extends BasePoEntity {

    private static final long serialVersionUID = -76695694634750575L;

    /**
     * 库存ID
     */
    private Long stockId;

    /**
     * 业务ID
     */
    private Long bizId;

    /**
     * 业务明细ID
     */
    private Long bizListId;

    /**
     * 业务对象ID
     */
    private Integer objectId;

    /**
     * 数量
     */
    private BigDecimal num;



}


