package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_stock")
public class StockPo extends BasePoEntity {

    private static final long serialVersionUID = -28255072081964974L;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 单位标识
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库标识
     */
    private Long warehouseId;

    /**
     * 入库数量
     */
    private BigDecimal inNum;

    /**
     * 出库数量
     */
    private BigDecimal outNum;

    /**
     * 库存数量
     */
    private BigDecimal whNum;

    /**
     * 预占数量
     */
    private BigDecimal preNum;

    /**
     * 可用数量
     */
    private BigDecimal num;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 销售价
     */
    private BigDecimal price;

    /**
     * 入库金额
     */
    private BigDecimal inFee;

    /**
     * 出库金额
     */
    private BigDecimal outFee;

    /**
     * 规格型号
     */
    private String specs;

    /**
     * 计量单位
     */
    private String unitName;

    /**
     * 库存金额
     */
    private BigDecimal fee;



}


