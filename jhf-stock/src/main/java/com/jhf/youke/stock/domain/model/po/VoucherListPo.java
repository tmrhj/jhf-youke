package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_voucher_list")
public class VoucherListPo extends BasePoEntity {

    private static final long serialVersionUID = -69785620341257340L;

    /**
     * 主表ID
     */
    private Long voucherId;

    /**
     * 库存ID
     */
    private Long stockId;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 规格
     */
    private String specs;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 金额
     */
    private BigDecimal fee;



}


