package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_voucher")
public class VoucherPo extends BasePoEntity {

    private static final long serialVersionUID = -17789952661168069L;

    /**
     * 出入库名
     */
    private String name;

    /**
     * 单位ID
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库ID
     */
    private Long warehouseId;

    /**
     * 类型 1 入库 2 出库
     */
    private Integer type;

    /**
     * 金额
     */
    private BigDecimal fee;



}


