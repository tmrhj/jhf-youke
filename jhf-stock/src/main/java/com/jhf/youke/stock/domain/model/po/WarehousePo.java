package com.jhf.youke.stock.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;
import java.math.BigDecimal;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "sto_warehouse")
public class WarehousePo extends BasePoEntity {

    private static final long serialVersionUID = -85601959109610927L;

    /**
     * 仓库名
     */
    private String name;

    /**
     * 单位ID
     */
    private Long companyId;

    /**
     * 单位名
     */
    private String companyName;

    /**
     * 仓库类型
     */
    private Integer type;

    /**
     * 状态
     */
    private Integer status;



}


