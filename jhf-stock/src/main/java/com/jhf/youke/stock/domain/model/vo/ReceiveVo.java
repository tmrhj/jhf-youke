package com.jhf.youke.stock.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class ReceiveVo extends BaseVoEntity {

    private static final long serialVersionUID = -23705046952734760L;


    @ApiModelProperty(value = "入库单名")
    private String name;

    @ApiModelProperty(value = "单位标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "单位名")
    private String companyName;

    @ApiModelProperty(value = "仓库标识")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long warehouseId;

    @ApiModelProperty(value = "状态")
    private Integer status;

}


