package com.jhf.youke.stock.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class StockFrostVo extends BaseVoEntity {

    private static final long serialVersionUID = -29812708762891933L;


    @ApiModelProperty(value = "库存ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long stockId;

    @ApiModelProperty(value = "业务ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizId;

    @ApiModelProperty(value = "业务明细ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long bizListId;

    @ApiModelProperty(value = "业务对象ID")
    private Integer objectId;

    @ApiModelProperty(value = "数量")
    private BigDecimal num;

}


