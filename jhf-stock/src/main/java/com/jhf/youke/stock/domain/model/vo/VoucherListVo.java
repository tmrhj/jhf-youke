package com.jhf.youke.stock.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class VoucherListVo extends BaseVoEntity {

    private static final long serialVersionUID = 756033440301137692L;


    @ApiModelProperty(value = "主表ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long voucherId;

    @ApiModelProperty(value = "库存ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long stockId;

    @ApiModelProperty(value = "产品ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long productId;

    @ApiModelProperty(value = "产品名")
    private String productName;

    @ApiModelProperty(value = "规格")
    private String specs;

    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;

    @ApiModelProperty(value = "数量")
    private BigDecimal num;

    @ApiModelProperty(value = "金额")
    private BigDecimal fee;

}


