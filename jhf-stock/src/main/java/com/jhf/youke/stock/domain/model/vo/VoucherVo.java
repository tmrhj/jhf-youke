package com.jhf.youke.stock.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import com.jhf.youke.core.ddd.BaseVoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author  RHJ
 **/

@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class VoucherVo extends BaseVoEntity {

    private static final long serialVersionUID = -17807737003026730L;


    @ApiModelProperty(value = "出入库名")
    private String name;

    @ApiModelProperty(value = "单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "单位名")
    private String companyName;

    @ApiModelProperty(value = "仓库ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long warehouseId;

    @ApiModelProperty(value = "类型 1 入库 2 出库")
    private Integer type;

    @ApiModelProperty(value = "金额")
    private BigDecimal fee;

}


