package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.OutboundListConverter;
import com.jhf.youke.stock.domain.model.Do.OutboundListDo;
import com.jhf.youke.stock.domain.gateway.OutboundListRepository;
import com.jhf.youke.stock.domain.model.po.OutboundListPo;
import com.jhf.youke.stock.domain.model.vo.OutboundListVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class OutboundListService extends AbstractDomainService<OutboundListRepository, OutboundListDo, OutboundListVo> {


    @Resource
    OutboundListConverter outboundListConverter;

    @Override
    public boolean update(OutboundListDo entity) {
        OutboundListPo outboundListPo = outboundListConverter.do2Po(entity);
        outboundListPo.preUpdate();
        return repository.update(outboundListPo);
    }

    @Override
    public boolean updateBatch(List<OutboundListDo> doList) {
        List<OutboundListPo> poList = outboundListConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(OutboundListDo entity) {
        OutboundListPo outboundListPo = outboundListConverter.do2Po(entity);
        return repository.delete(outboundListPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(OutboundListDo entity) {
        OutboundListPo outboundListPo = outboundListConverter.do2Po(entity);
        outboundListPo.preInsert();
        return repository.insert(outboundListPo);
    }

    @Override
    public boolean insertBatch(List<OutboundListDo> doList) {
        List<OutboundListPo> poList = outboundListConverter.do2PoList(doList);
        OutboundListPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<OutboundListVo> findById(Long id) {
        Optional<OutboundListPo> outboundListPo =  repository.findById(id);
        OutboundListVo outboundListVo = outboundListConverter.po2Vo(outboundListPo.orElse(new OutboundListPo()));
        return Optional.ofNullable(outboundListVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<OutboundListVo> findAllMatching(OutboundListDo entity) {
        OutboundListPo outboundListPo = outboundListConverter.do2Po(entity);
        List<OutboundListPo>outboundListPoList =  repository.findAllMatching(outboundListPo);
        return outboundListConverter.po2VoList(outboundListPoList);
    }


    @Override
    public Pagination<OutboundListVo> selectPage(OutboundListDo entity){
        OutboundListPo outboundListPo = outboundListConverter.do2Po(entity);
        PageQuery<OutboundListPo> pageQuery = new PageQuery<>(outboundListPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<OutboundListPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                outboundListConverter.po2VoList(pagination.getList()));
    }


}

