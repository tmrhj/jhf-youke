package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.OutboundConverter;
import com.jhf.youke.stock.domain.model.Do.OutboundDo;
import com.jhf.youke.stock.domain.gateway.OutboundRepository;
import com.jhf.youke.stock.domain.model.po.OutboundPo;
import com.jhf.youke.stock.domain.model.vo.OutboundVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class OutboundService extends AbstractDomainService<OutboundRepository, OutboundDo, OutboundVo> {


    @Resource
    OutboundConverter outboundConverter;

    @Override
    public boolean update(OutboundDo entity) {
        OutboundPo outboundPo = outboundConverter.do2Po(entity);
        outboundPo.preUpdate();
        return repository.update(outboundPo);
    }

    @Override
    public boolean updateBatch(List<OutboundDo> doList) {
        List<OutboundPo> poList = outboundConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(OutboundDo entity) {
        OutboundPo outboundPo = outboundConverter.do2Po(entity);
        return repository.delete(outboundPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(OutboundDo entity) {
        OutboundPo outboundPo = outboundConverter.do2Po(entity);
        outboundPo.preInsert();
        return repository.insert(outboundPo);
    }

    @Override
    public boolean insertBatch(List<OutboundDo> doList) {
        List<OutboundPo> poList = outboundConverter.do2PoList(doList);
        OutboundPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<OutboundVo> findById(Long id) {
        Optional<OutboundPo> outboundPo =  repository.findById(id);
        OutboundVo outboundVo = outboundConverter.po2Vo(outboundPo.orElse(new OutboundPo()));
        return Optional.ofNullable(outboundVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<OutboundVo> findAllMatching(OutboundDo entity) {
        OutboundPo outboundPo = outboundConverter.do2Po(entity);
        List<OutboundPo>outboundPoList =  repository.findAllMatching(outboundPo);
        return outboundConverter.po2VoList(outboundPoList);
    }


    @Override
    public Pagination<OutboundVo> selectPage(OutboundDo entity){
        OutboundPo outboundPo = outboundConverter.do2Po(entity);
        PageQuery<OutboundPo> pageQuery = new PageQuery<>(outboundPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<OutboundPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                outboundConverter.po2VoList(pagination.getList()));
    }


    public void confirmAccount(OutboundDo outboundDo) {
        repository.confirmAccount(outboundDo);
    }


    public void whConfirm(OutboundDo outboundDo) {
        repository.confirm(outboundDo);
    }

}

