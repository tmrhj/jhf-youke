package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.ReceiveListConverter;
import com.jhf.youke.stock.domain.model.Do.ReceiveListDo;
import com.jhf.youke.stock.domain.gateway.ReceiveListRepository;
import com.jhf.youke.stock.domain.model.po.ReceiveListPo;
import com.jhf.youke.stock.domain.model.vo.ReceiveListVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class ReceiveListService extends AbstractDomainService<ReceiveListRepository, ReceiveListDo, ReceiveListVo> {


    @Resource
    ReceiveListConverter receiveListConverter;

    @Override
    public boolean update(ReceiveListDo entity) {
        ReceiveListPo receiveListPo = receiveListConverter.do2Po(entity);
        receiveListPo.preUpdate();
        return repository.update(receiveListPo);
    }

    @Override
    public boolean updateBatch(List<ReceiveListDo> doList) {
        List<ReceiveListPo> poList = receiveListConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ReceiveListDo entity) {
        ReceiveListPo receiveListPo = receiveListConverter.do2Po(entity);
        return repository.delete(receiveListPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ReceiveListDo entity) {
        ReceiveListPo receiveListPo = receiveListConverter.do2Po(entity);
        receiveListPo.preInsert();
        return repository.insert(receiveListPo);
    }

    @Override
    public boolean insertBatch(List<ReceiveListDo> doList) {
        List<ReceiveListPo> poList = receiveListConverter.do2PoList(doList);
        ReceiveListPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ReceiveListVo> findById(Long id) {
        Optional<ReceiveListPo> receiveListPo =  repository.findById(id);
        ReceiveListVo receiveListVo = receiveListConverter.po2Vo(receiveListPo.orElse(new ReceiveListPo()));
        return Optional.ofNullable(receiveListVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ReceiveListVo> findAllMatching(ReceiveListDo entity) {
        ReceiveListPo receiveListPo = receiveListConverter.do2Po(entity);
        List<ReceiveListPo>receiveListPoList =  repository.findAllMatching(receiveListPo);
        return receiveListConverter.po2VoList(receiveListPoList);
    }


    @Override
    public Pagination<ReceiveListVo> selectPage(ReceiveListDo entity){
        ReceiveListPo receiveListPo = receiveListConverter.do2Po(entity);
        PageQuery<ReceiveListPo> pageQuery = new PageQuery<>(receiveListPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ReceiveListPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                receiveListConverter.po2VoList(pagination.getList()));
    }


}

