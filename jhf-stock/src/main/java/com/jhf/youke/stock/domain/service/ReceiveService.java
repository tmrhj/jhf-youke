package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.ReceiveConverter;
import com.jhf.youke.stock.domain.model.Do.ReceiveDo;
import com.jhf.youke.stock.domain.gateway.ReceiveRepository;
import com.jhf.youke.stock.domain.model.po.ReceivePo;
import com.jhf.youke.stock.domain.model.vo.ReceiveVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class ReceiveService extends AbstractDomainService<ReceiveRepository, ReceiveDo, ReceiveVo> {


    @Resource
    ReceiveConverter receiveConverter;

    @Override
    public boolean update(ReceiveDo entity) {
        ReceivePo receivePo = receiveConverter.do2Po(entity);
        receivePo.preUpdate();
        return repository.update(receivePo);
    }

    @Override
    public boolean updateBatch(List<ReceiveDo> doList) {
        List<ReceivePo> poList = receiveConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(ReceiveDo entity) {
        ReceivePo receivePo = receiveConverter.do2Po(entity);
        return repository.delete(receivePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(ReceiveDo entity) {
        ReceivePo receivePo = receiveConverter.do2Po(entity);
        receivePo.preInsert();
        return repository.insert(receivePo);
    }

    @Override
    public boolean insertBatch(List<ReceiveDo> doList) {
        List<ReceivePo> poList = receiveConverter.do2PoList(doList);
        ReceivePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<ReceiveVo> findById(Long id) {
        Optional<ReceivePo> receivePo =  repository.findById(id);
        ReceiveVo receiveVo = receiveConverter.po2Vo(receivePo.orElse(new ReceivePo()));
        return Optional.ofNullable(receiveVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<ReceiveVo> findAllMatching(ReceiveDo entity) {
        ReceivePo receivePo = receiveConverter.do2Po(entity);
        List<ReceivePo>receivePoList =  repository.findAllMatching(receivePo);
        return receiveConverter.po2VoList(receivePoList);
    }


    @Override
    public Pagination<ReceiveVo> selectPage(ReceiveDo entity){
        ReceivePo receivePo = receiveConverter.do2Po(entity);
        PageQuery<ReceivePo> pageQuery = new PageQuery<>(receivePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<ReceivePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                receiveConverter.po2VoList(pagination.getList()));
    }


}

