package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.StockFrostConverter;
import com.jhf.youke.stock.domain.model.Do.StockDo;
import com.jhf.youke.stock.domain.model.Do.StockFrostDo;
import com.jhf.youke.stock.domain.gateway.StockFrostRepository;
import com.jhf.youke.stock.domain.model.po.StockFrostPo;
import com.jhf.youke.stock.domain.model.vo.StockFrostVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class StockFrostService extends AbstractDomainService<StockFrostRepository, StockFrostDo, StockFrostVo> {


    @Resource
    StockFrostConverter stockFrostConverter;

    @Override
    public boolean update(StockFrostDo entity) {
        StockFrostPo stockFrostPo = stockFrostConverter.do2Po(entity);
        stockFrostPo.preUpdate();
        return repository.update(stockFrostPo);
    }

    @Override
    public boolean updateBatch(List<StockFrostDo> doList) {
        List<StockFrostPo> poList = stockFrostConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(StockFrostDo entity) {
        StockFrostPo stockFrostPo = stockFrostConverter.do2Po(entity);
        return repository.delete(stockFrostPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(StockFrostDo entity) {
        StockFrostPo stockFrostPo = stockFrostConverter.do2Po(entity);
        stockFrostPo.preInsert();
        return repository.insert(stockFrostPo);
    }

    @Override
    public boolean insertBatch(List<StockFrostDo> doList) {
        List<StockFrostPo> poList = stockFrostConverter.do2PoList(doList);
        StockFrostPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<StockFrostVo> findById(Long id) {
        Optional<StockFrostPo> stockFrostPo =  repository.findById(id);
        StockFrostVo stockFrostVo = stockFrostConverter.po2Vo(stockFrostPo.orElse(new StockFrostPo()));
        return Optional.ofNullable(stockFrostVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<StockFrostVo> findAllMatching(StockFrostDo entity) {
        StockFrostPo stockFrostPo = stockFrostConverter.do2Po(entity);
        List<StockFrostPo>stockFrostPoList =  repository.findAllMatching(stockFrostPo);
        return stockFrostConverter.po2VoList(stockFrostPoList);
    }


    @Override
    public Pagination<StockFrostVo> selectPage(StockFrostDo entity){
        StockFrostPo stockFrostPo = stockFrostConverter.do2Po(entity);
        PageQuery<StockFrostPo> pageQuery = new PageQuery<>(stockFrostPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<StockFrostPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                stockFrostConverter.po2VoList(pagination.getList()));
    }


    public Boolean create(List<StockDo> stockDos) {
        StockFrostDo stockFrostDo = new StockFrostDo(stockDos);
        Boolean result = insertBatch(stockFrostDo.getStockFrostDoList());
        return result;
    }

}

