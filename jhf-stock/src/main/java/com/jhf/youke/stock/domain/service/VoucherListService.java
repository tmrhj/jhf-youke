package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.VoucherListConverter;
import com.jhf.youke.stock.domain.model.Do.VoucherListDo;
import com.jhf.youke.stock.domain.gateway.VoucherListRepository;
import com.jhf.youke.stock.domain.model.po.VoucherListPo;
import com.jhf.youke.stock.domain.model.vo.VoucherListVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class VoucherListService extends AbstractDomainService<VoucherListRepository, VoucherListDo, VoucherListVo> {


    @Resource
    VoucherListConverter voucherListConverter;

    @Override
    public boolean update(VoucherListDo entity) {
        VoucherListPo voucherListPo = voucherListConverter.do2Po(entity);
        voucherListPo.preUpdate();
        return repository.update(voucherListPo);
    }

    @Override
    public boolean updateBatch(List<VoucherListDo> doList) {
        List<VoucherListPo> poList = voucherListConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(VoucherListDo entity) {
        VoucherListPo voucherListPo = voucherListConverter.do2Po(entity);
        return repository.delete(voucherListPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(VoucherListDo entity) {
        VoucherListPo voucherListPo = voucherListConverter.do2Po(entity);
        voucherListPo.preInsert();
        return repository.insert(voucherListPo);
    }

    @Override
    public boolean insertBatch(List<VoucherListDo> doList) {
        List<VoucherListPo> poList = voucherListConverter.do2PoList(doList);
        VoucherListPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<VoucherListVo> findById(Long id) {
        Optional<VoucherListPo> voucherListPo =  repository.findById(id);
        VoucherListVo voucherListVo = voucherListConverter.po2Vo(voucherListPo.orElse(new VoucherListPo()));
        return Optional.ofNullable(voucherListVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<VoucherListVo> findAllMatching(VoucherListDo entity) {
        VoucherListPo voucherListPo = voucherListConverter.do2Po(entity);
        List<VoucherListPo>voucherListPoList =  repository.findAllMatching(voucherListPo);
        return voucherListConverter.po2VoList(voucherListPoList);
    }


    @Override
    public Pagination<VoucherListVo> selectPage(VoucherListDo entity){
        VoucherListPo voucherListPo = voucherListConverter.do2Po(entity);
        PageQuery<VoucherListPo> pageQuery = new PageQuery<>(voucherListPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<VoucherListPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                voucherListConverter.po2VoList(pagination.getList()));
    }


}

