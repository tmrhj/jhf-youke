package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.VoucherConverter;
import com.jhf.youke.stock.domain.model.Do.VoucherDo;
import com.jhf.youke.stock.domain.gateway.VoucherRepository;
import com.jhf.youke.stock.domain.model.po.VoucherPo;
import com.jhf.youke.stock.domain.model.vo.VoucherVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class VoucherService extends AbstractDomainService<VoucherRepository, VoucherDo, VoucherVo> {


    @Resource
    VoucherConverter voucherConverter;

    @Override
    public boolean update(VoucherDo entity) {
        VoucherPo voucherPo = voucherConverter.do2Po(entity);
        voucherPo.preUpdate();
        return repository.update(voucherPo);
    }

    @Override
    public boolean updateBatch(List<VoucherDo> doList) {
        List<VoucherPo> poList = voucherConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(VoucherDo entity) {
        VoucherPo voucherPo = voucherConverter.do2Po(entity);
        return repository.delete(voucherPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(VoucherDo entity) {
        VoucherPo voucherPo = voucherConverter.do2Po(entity);
        voucherPo.preInsert();
        return repository.insert(voucherPo);
    }

    @Override
    public boolean insertBatch(List<VoucherDo> doList) {
        List<VoucherPo> poList = voucherConverter.do2PoList(doList);
        VoucherPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<VoucherVo> findById(Long id) {
        Optional<VoucherPo> voucherPo =  repository.findById(id);
        VoucherVo voucherVo = voucherConverter.po2Vo(voucherPo.orElse(new VoucherPo()));
        return Optional.ofNullable(voucherVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<VoucherVo> findAllMatching(VoucherDo entity) {
        VoucherPo voucherPo = voucherConverter.do2Po(entity);
        List<VoucherPo>voucherPoList =  repository.findAllMatching(voucherPo);
        return voucherConverter.po2VoList(voucherPoList);
    }


    @Override
    public Pagination<VoucherVo> selectPage(VoucherDo entity){
        VoucherPo voucherPo = voucherConverter.do2Po(entity);
        PageQuery<VoucherPo> pageQuery = new PageQuery<>(voucherPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<VoucherPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                voucherConverter.po2VoList(pagination.getList()));
    }


}

