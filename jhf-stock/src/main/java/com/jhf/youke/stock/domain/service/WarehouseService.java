package com.jhf.youke.stock.domain.service;

import com.jhf.youke.stock.domain.converter.WarehouseConverter;
import com.jhf.youke.stock.domain.model.Do.WarehouseDo;
import com.jhf.youke.stock.domain.gateway.WarehouseRepository;
import com.jhf.youke.stock.domain.model.po.WarehousePo;
import com.jhf.youke.stock.domain.model.vo.WarehouseVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author  RHJ
 **/

@Service
public class WarehouseService extends AbstractDomainService<WarehouseRepository, WarehouseDo, WarehouseVo> {


    @Resource
    WarehouseConverter warehouseConverter;

    @Override
    public boolean update(WarehouseDo entity) {
        WarehousePo warehousePo = warehouseConverter.do2Po(entity);
        warehousePo.preUpdate();
        return repository.update(warehousePo);
    }

    @Override
    public boolean updateBatch(List<WarehouseDo> doList) {
        List<WarehousePo> poList = warehouseConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(WarehouseDo entity) {
        WarehousePo warehousePo = warehouseConverter.do2Po(entity);
        return repository.delete(warehousePo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(WarehouseDo entity) {
        WarehousePo warehousePo = warehouseConverter.do2Po(entity);
        warehousePo.preInsert();
        return repository.insert(warehousePo);
    }

    @Override
    public boolean insertBatch(List<WarehouseDo> doList) {
        List<WarehousePo> poList = warehouseConverter.do2PoList(doList);
        WarehousePo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<WarehouseVo> findById(Long id) {
        Optional<WarehousePo> warehousePo =  repository.findById(id);
        WarehouseVo warehouseVo = warehouseConverter.po2Vo(warehousePo.orElse(new WarehousePo()));
        return Optional.ofNullable(warehouseVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<WarehouseVo> findAllMatching(WarehouseDo entity) {
        WarehousePo warehousePo = warehouseConverter.do2Po(entity);
        List<WarehousePo>warehousePoList =  repository.findAllMatching(warehousePo);
        return warehouseConverter.po2VoList(warehousePoList);
    }


    @Override
    public Pagination<WarehouseVo> selectPage(WarehouseDo entity){
        WarehousePo warehousePo = warehouseConverter.do2Po(entity);
        PageQuery<WarehousePo> pageQuery = new PageQuery<>(warehousePo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<WarehousePo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                warehouseConverter.po2VoList(pagination.getList()));
    }


}

