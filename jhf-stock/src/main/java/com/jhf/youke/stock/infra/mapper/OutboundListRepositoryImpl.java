package com.jhf.youke.stock.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.stock.domain.gateway.OutboundListRepository;
import com.jhf.youke.stock.domain.model.Do.OutboundListDo;
import com.jhf.youke.stock.domain.model.po.OutboundListPo;
import com.jhf.youke.stock.domain.exception.OutboundListException;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  RHJ
 **/

@Service(value = "OutboundListRepository")
public class OutboundListRepositoryImpl extends ServiceImpl<OutboundListMapper, OutboundListPo> implements OutboundListRepository {


    @Override
    public boolean update(OutboundListPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<OutboundListPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(OutboundListPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(OutboundListPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<OutboundListPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<OutboundListPo> findById(Long id) {
        OutboundListPo outboundListPo = baseMapper.selectById(id);
        return Optional.ofNullable(outboundListPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new OutboundListException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<OutboundListPo> findAllMatching(OutboundListPo entity) {
        QueryWrapper<OutboundListPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<OutboundListPo> selectPage(PageQuery<OutboundListPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<OutboundListPo> outboundListPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<OutboundListPo> pageInfo = new PageInfo<>(outboundListPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

