package com.jhf.youke.stock.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.stock.domain.model.po.OutboundPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @author  RHJ
 **/

@Mapper
public interface OutboundMapper  extends BaseMapper<OutboundPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sto_outbound set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sto_outbound set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 删除批处理
     * @param id id
     * @return int
     */
    @Update("update sto_outbound set status =502 where id =#{id}")
    void confirmAccount(@Param("id") Long id);

    /**
     * 删除批处理
     * @param id id
     * @return int
     */
    @Update("update sto_outbound set status =503 where id =#{id}")
    void confirm(@Param("id") Long id);

}

