package com.jhf.youke.stock.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.exception.OutboundException;
import com.jhf.youke.stock.domain.gateway.OutboundRepository;
import com.jhf.youke.stock.domain.model.Do.OutboundDo;
import com.jhf.youke.stock.domain.model.po.OutboundPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  RHJ
 **/

@Service(value = "OutboundRepository")
public class OutboundRepositoryImpl extends ServiceImpl<OutboundMapper, OutboundPo> implements OutboundRepository {


    @Override
    public boolean update(OutboundPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<OutboundPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(OutboundPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(OutboundPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<OutboundPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<OutboundPo> findById(Long id) {
        OutboundPo outboundPo = baseMapper.selectById(id);
        return Optional.ofNullable(outboundPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new OutboundException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<OutboundPo> findAllMatching(OutboundPo entity) {
        QueryWrapper<OutboundPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<OutboundPo> selectPage(PageQuery<OutboundPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<OutboundPo> outboundPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<OutboundPo> pageInfo = new PageInfo<>(outboundPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public void confirmAccount(OutboundDo outboundDo) {
        baseMapper.confirmAccount(outboundDo.getId());
    }

    @Override
    public void confirm(OutboundDo outboundDo) {
        baseMapper.confirm(outboundDo.getId());
    }

}

