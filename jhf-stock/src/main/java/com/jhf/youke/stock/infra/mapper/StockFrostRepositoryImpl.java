package com.jhf.youke.stock.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.stock.domain.gateway.StockFrostRepository;
import com.jhf.youke.stock.domain.model.Do.StockFrostDo;
import com.jhf.youke.stock.domain.model.po.StockFrostPo;
import com.jhf.youke.stock.domain.exception.StockFrostException;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  RHJ
 **/

@Service(value = "StockFrostRepository")
public class StockFrostRepositoryImpl extends ServiceImpl<StockFrostMapper, StockFrostPo> implements StockFrostRepository {


    @Override
    public boolean update(StockFrostPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<StockFrostPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(StockFrostPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(StockFrostPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<StockFrostPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<StockFrostPo> findById(Long id) {
        StockFrostPo stockFrostPo = baseMapper.selectById(id);
        return Optional.ofNullable(stockFrostPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new StockFrostException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<StockFrostPo> findAllMatching(StockFrostPo entity) {
        QueryWrapper<StockFrostPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<StockFrostPo> selectPage(PageQuery<StockFrostPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<StockFrostPo> stockFrostPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<StockFrostPo> pageInfo = new PageInfo<>(stockFrostPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

