package com.jhf.youke.stock.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.stock.domain.model.po.StockPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author  RHJ
 **/

@Mapper
public interface StockMapper  extends BaseMapper<StockPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update sto_stock set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update sto_stock set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);


    List<StockPo> getListByProductId(@Param("warehouseId") Long warehouseId,
                                     @Param("productIds") List<Long> productIds,
                                     @Param("specs") String specs);

}

