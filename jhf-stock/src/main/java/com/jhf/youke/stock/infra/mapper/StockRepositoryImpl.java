package com.jhf.youke.stock.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.stock.domain.exception.StockException;
import com.jhf.youke.stock.domain.gateway.StockRepository;
import com.jhf.youke.stock.domain.model.po.StockPo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  RHJ
 **/

@Service(value = "StockRepository")
public class StockRepositoryImpl extends ServiceImpl<StockMapper, StockPo> implements StockRepository {


    @Override
    public boolean update(StockPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<StockPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(StockPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(StockPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<StockPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<StockPo> findById(Long id) {
        StockPo stockPo = baseMapper.selectById(id);
        return Optional.ofNullable(stockPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new StockException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<StockPo> findAllMatching(StockPo entity) {
        QueryWrapper<StockPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<StockPo> selectPage(PageQuery<StockPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<StockPo> stockPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<StockPo> pageInfo = new PageInfo<>(stockPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }



    @Override
    public List<StockPo> getListByProductId(Long warehouseId, List<Long> productIds, String specs) {
        return baseMapper.getListByProductId(warehouseId,productIds,specs);
    }

}

