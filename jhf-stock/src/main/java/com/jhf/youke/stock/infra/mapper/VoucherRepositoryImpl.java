package com.jhf.youke.stock.infra.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jhf.youke.stock.domain.gateway.VoucherRepository;
import com.jhf.youke.stock.domain.model.Do.VoucherDo;
import com.jhf.youke.stock.domain.model.po.VoucherPo;
import com.jhf.youke.stock.domain.exception.VoucherException;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author  RHJ
 **/

@Service(value = "VoucherRepository")
public class VoucherRepositoryImpl extends ServiceImpl<VoucherMapper, VoucherPo> implements VoucherRepository {


    @Override
    public boolean update(VoucherPo entity) {
        entity.preUpdate();
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public boolean updateBatch(List<VoucherPo> list) {
        list.forEach( entity -> entity.preUpdate());
        return super.updateBatchById(list);
    }

    @Override
    public boolean delete(VoucherPo entity) {
        return baseMapper.deleteById(entity) >0;
    }

    @Override
    public boolean deleteBatch(List<Long> list) {
        return baseMapper.deleteBatchIds(list) > 0;
    }

    @Override
    public boolean insert(VoucherPo entity) {
        entity.preInsert();
        return baseMapper.insert(entity) >0 ;
    }

    @Override
    public boolean insertBatch(List<VoucherPo> list) {
        list.forEach( entity -> entity.preInsert());
        return super.saveBatch(list);
    }

    @Override
    public Optional<VoucherPo> findById(Long id) {
        VoucherPo voucherPo = baseMapper.selectById(id);
        return Optional.ofNullable(voucherPo);
    }


    @Override
    public boolean remove(Long id) {
        return baseMapper.remove(id) > 0;
    }

    @Override
    public boolean removeBatch(List<Long> idList) {       
        String ids = "";     
        if(idList != null){
            ids = idList.stream().map(id -> String.valueOf(id)).collect(Collectors.joining(","));
        }        
        if(StringUtils.isEmpty(ids)){
            throw new VoucherException("批量删除ID为空");
        }
        return baseMapper.removeBatch(ids) > 0;
    }

    @Override
    public List<VoucherPo> findAllMatching(VoucherPo entity) {
        QueryWrapper<VoucherPo> wrapper = new QueryWrapper<>();
        wrapper.setEntity(entity);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Pagination<VoucherPo> selectPage(PageQuery<VoucherPo> pageQuery) {
        PageHelper.startPage(pageQuery.getCurrentPage(), pageQuery.getPageSize());
        List<VoucherPo> voucherPos = baseMapper.selectList(new QueryWrapper<>(pageQuery.getParam()));
        PageInfo<VoucherPo> pageInfo = new PageInfo<>(voucherPos);
        return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getList());
    }

}

