package com.jhf.youke.wechat.adapter.web;

import com.jhf.youke.wechat.app.executor.UserAppService;
import com.jhf.youke.wechat.domain.model.dto.UserDto;
import com.jhf.youke.wechat.domain.model.vo.UserVo;
import com.jhf.youke.core.entity.Pagination;
import com.jhf.youke.core.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Api(tags = "微信用户表")
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserAppService userAppService;


    @PostMapping("/update")
    @ApiOperation("更新")
    public Response<Boolean> update(@RequestBody UserDto dto) {
        return Response.ok(userAppService.update(dto));
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public Response<Boolean> delete(@RequestBody UserDto dto) {
        return Response.ok(userAppService.delete(dto));
    }

    @PostMapping("/insert")
    @ApiOperation("插入")
    public Response<Boolean> insert(@RequestBody UserDto dto) {
        return Response.ok(userAppService.insert(dto));
    }

    @GetMapping("/findById")
    @ApiOperation("根据ID查询")
    public Response<Optional<UserVo>> findById(Long id) {
        return Response.ok(userAppService.findById(id));
    }


    @GetMapping("/remove")
    @ApiOperation("标记删除")
    public Response<Boolean> remove(Long id) {
        return  Response.ok(userAppService.remove(id));
    }

    @PostMapping("/list")
    @ApiOperation("列表查询")
    public Response<List<UserVo>> list(@RequestBody UserDto dto) {

        return Response.ok(userAppService.findAllMatching(dto));
    }

    @PostMapping("/selectPage")
    @ApiOperation("分页查询")
    public Response<Pagination<UserVo>> selectPage(@RequestBody  UserDto dto) {
        return Response.ok(userAppService.selectPage(dto));
    }

}

