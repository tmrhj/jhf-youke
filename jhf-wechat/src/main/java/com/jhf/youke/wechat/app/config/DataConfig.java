package com.jhf.youke.wechat.app.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * description:
 * date: 2022/7/8 14:26
 * @author: cyx
 */
@Data
@Configuration
public class DataConfig {

    @Value("${wx.appletConfigId}")
    private Long appletConfigId;

}
