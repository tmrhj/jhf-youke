package com.jhf.youke.wechat.app.constant;

/**
 * description:
 * date: 2022/10/12 8:48
 * @author: cyx
 */
public class WxConstant {
    /** 微信小程序配置缓存key **/
    public final static String APPLET_CONFIG_KEY = "wxAppletConfig";
    /** 请求成功标识 **/
    public final static int OK = 200;
    /** 小程序获取openid **/
    public final static String GET_OAUTH_OPEN_ID = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    /**
     * 小程序获取token 接口
     */
    public static final String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    /**
     * 网页授权OAuth2.0获取用户信息
     */
    public static final String GET_OAUTH_USERINFO = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";
}
