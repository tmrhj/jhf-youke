package com.jhf.youke.wechat.app.consume;


import com.jhf.youke.saga.client.annotation.SagaReply;
import com.jhf.youke.wechat.domain.model.Do.UserDo;
import com.jhf.youke.wechat.domain.service.UserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author RHJ
 */
@Component
public class WxUserConsumeService {

    @Resource
    UserService userService;

    @SagaReply(abnormal = "create_wx_user_abnormal")
    public void create(Map<String, Object> map) {
        Map<String,Object> msg = (Map<String,Object>) map.get("message");
        UserDo customerDo = new UserDo(msg);
        userService.create(customerDo);
    }

}
