package com.jhf.youke.wechat.app.executor;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.jhf.youke.core.utils.CacheUtils;
import com.jhf.youke.wechat.app.config.DataConfig;
import com.jhf.youke.wechat.app.constant.WxConstant;
import com.jhf.youke.wechat.domain.converter.AccountConverter;
import com.jhf.youke.wechat.domain.exception.AccountException;
import com.jhf.youke.wechat.domain.model.Do.AccountDo;
import com.jhf.youke.wechat.domain.model.dto.AccountDto;
import com.jhf.youke.wechat.domain.model.vo.AccountVo;
import com.jhf.youke.wechat.domain.service.AccountService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author RHJ
 */
@Slf4j
@Service
public class AccountAppService {

    @Resource
    private AccountService accountService;

    @Resource
    private AccountConverter accountConverter;

    @Resource
    private DataConfig dataConfig;


    public boolean update(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.update(accountDo);
    }

    public boolean delete(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.delete(accountDo);
    }


    public boolean insert(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.insert(accountDo);
    }

    public Optional<AccountVo> findById(Long id) {
        return accountService.findById(id);
    }


    public boolean remove(Long id) {
        return accountService.remove(id);
    }


    public List<AccountVo> findAllMatching(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.findAllMatching(accountDo);
    }


    public Pagination<AccountVo> selectPage(AccountDto dto) {
        AccountDo accountDo =  accountConverter.dto2Do(dto);
        return accountService.selectPage(accountDo);
    }

    public AccountVo getAppletConfig() {
        String data = CacheUtils.get(WxConstant.APPLET_CONFIG_KEY);
        AccountVo accountVo;
        if (StrUtil.isBlank(data)) {
            Optional<AccountVo> accountOptional = findById(dataConfig.getAppletConfigId());
            accountOptional.map(AccountVo::getId).orElseThrow(() -> new AccountException("配置数据为空"));
            accountVo = accountOptional.get();
            CacheUtils.set(WxConstant.APPLET_CONFIG_KEY, JSONUtil.toJsonStr(accountVo), 5 * 60);
        } else {
            accountVo = JSONUtil.toBean(data, AccountVo.class);
        }
        return accountVo;
    }

}

