package com.jhf.youke.wechat.app.executor;

import com.jhf.youke.wechat.domain.converter.UserConverter;
import com.jhf.youke.wechat.domain.model.Do.UserDo;
import com.jhf.youke.wechat.domain.model.dto.UserDto;
import com.jhf.youke.wechat.domain.model.vo.UserVo;
import com.jhf.youke.wechat.domain.service.UserService;
import com.jhf.youke.core.entity.Pagination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Slf4j
@Service
public class UserAppService {

    @Resource
    private UserService userService;

    @Resource
    private UserConverter userConverter;


    public boolean update(UserDto dto) {
        UserDo userDo =  userConverter.dto2Do(dto);
        return userService.update(userDo);
    }

    public boolean delete(UserDto dto) {
        UserDo userDo =  userConverter.dto2Do(dto);
        return userService.delete(userDo);
    }


    public boolean insert(UserDto dto) {
        UserDo userDo =  userConverter.dto2Do(dto);
        return userService.insert(userDo);
    }

    public Optional<UserVo> findById(Long id) {
        return userService.findById(id);
    }


    public boolean remove(Long id) {
        return userService.remove(id);
    }


    public List<UserVo> findAllMatching(UserDto dto) {
        UserDo userDo =  userConverter.dto2Do(dto);
        return userService.findAllMatching(userDo);
    }


    public Pagination<UserVo> selectPage(UserDto dto) {
        UserDo userDo =  userConverter.dto2Do(dto);
        return userService.selectPage(userDo);
    }

}

