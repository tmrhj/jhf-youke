package com.jhf.youke.wechat.domain.converter;

import com.jhf.youke.wechat.domain.model.Do.AccountDo;
import com.jhf.youke.wechat.domain.model.dto.AccountDto;
import com.jhf.youke.wechat.domain.model.po.AccountPo;
import com.jhf.youke.wechat.domain.model.vo.AccountVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 账户转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface AccountConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param accountDto 账户dto
     * @return {@link AccountDo}
     */
    AccountDo dto2Do(AccountDto accountDto);

    /**
     * 洗阿宝
     *
     * @param accountDo 账户做
     * @return {@link AccountPo}
     */
    AccountPo do2Po(AccountDo accountDo);

    /**
     * 洗签证官
     *
     * @param accountDo 账户做
     * @return {@link AccountVo}
     */
    AccountVo do2Vo(AccountDo accountDo);


    /**
     * 洗订单列表
     *
     * @param accountDoList 账户做列表
     * @return {@link List}<{@link AccountPo}>
     */
    List<AccountPo> do2PoList(List<AccountDo> accountDoList);

    /**
     * 警察乙做
     *
     * @param accountPo 账户阿宝
     * @return {@link AccountDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    AccountDo po2Do(AccountPo accountPo);

    /**
     * 警察乙签证官
     *
     * @param accountPo 账户阿宝
     * @return {@link AccountVo}
     */
    AccountVo po2Vo(AccountPo accountPo);

    /**
     * 警察乙做列表
     *
     * @param accountPoList 账户订单列表
     * @return {@link List}<{@link AccountDo}>
     */
    List<AccountDo> po2DoList(List<AccountPo> accountPoList);

    /**
     * 警察乙vo列表
     *
     * @param accountPoList 账户订单列表
     * @return {@link List}<{@link AccountVo}>
     */
    List<AccountVo> po2VoList(List<AccountPo> accountPoList);


}

