package com.jhf.youke.wechat.domain.converter;

import com.jhf.youke.wechat.domain.model.Do.UserDo;
import com.jhf.youke.wechat.domain.model.dto.UserDto;
import com.jhf.youke.wechat.domain.model.po.UserPo;
import com.jhf.youke.wechat.domain.model.vo.UserVo;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import java.util.Collection;

import java.util.List;


/**
 * 用户转换器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper(componentModel = "spring")
public interface UserConverter {


    /**
     * 转换为集合
     *
     * @param collection 集合
     * @return {@link Collection}<{@link Object}>
     */
    Collection<Object> convertToCollection(Collection<Object> collection);

    /**
     * dto2做
     *
     * @param userDto 用户dto
     * @return {@link UserDo}
     */
    UserDo dto2Do(UserDto userDto);

    /**
     * 洗阿宝
     *
     * @param userDo 用户做
     * @return {@link UserPo}
     */
    UserPo do2Po(UserDo userDo);

    /**
     * 洗签证官
     *
     * @param userDo 用户做
     * @return {@link UserVo}
     */
    UserVo do2Vo(UserDo userDo);


    /**
     * 洗订单列表
     *
     * @param userDoList 用户做列表
     * @return {@link List}<{@link UserPo}>
     */
    List<UserPo> do2PoList(List<UserDo> userDoList);

    /**
     * 警察乙做
     *
     * @param userPo 用户订单
     * @return {@link UserDo}
     */
    @InheritInverseConfiguration(name = "do2Po")
    UserDo po2Do(UserPo userPo);

    /**
     * 警察乙签证官
     *
     * @param userPo 用户订单
     * @return {@link UserVo}
     */
    UserVo po2Vo(UserPo userPo);

    /**
     * 警察乙做列表
     *
     * @param userPoList 用户订单列表
     * @return {@link List}<{@link UserDo}>
     */
    List<UserDo> po2DoList(List<UserPo> userPoList);

    /**
     * 警察乙vo列表
     *
     * @param userPoList 用户订单列表
     * @return {@link List}<{@link UserVo}>
     */
    List<UserVo> po2VoList(List<UserPo> userPoList);


}

