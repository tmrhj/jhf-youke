package com.jhf.youke.wechat.domain.event;

import com.jhf.youke.core.ddd.BaseEvent;
import lombok.Getter;


/**
 * @author RHJ
 */
@Getter
public class WxPayEvent extends BaseEvent {

    public WxPayEvent(Object source, String type, String topic, String message) {
        super(source);
        this.topic = topic;
        this.type = type;
        this.message = message;
    }


}



