package com.jhf.youke.wechat.domain.event;

import com.jhf.youke.core.ddd.BaseEventListener;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;


/**
 * @author RHJ
 */
@Component
@Log4j2
public class WxPayEventListener extends BaseEventListener<WxPayEvent> {


    @Override
    public void handleEventDomain(WxPayEvent event) {
        String topic = event.getTopic();
        switch (topic) {
            case "spring":
                log.info("domain event topic {}", topic);
                break;
            case "update":
                log.info("domain event topic is {}", topic);
                break;
            default:
                log.info("not equals topic");
        }
    }




}
