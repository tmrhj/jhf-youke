package com.jhf.youke.wechat.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class AccountException extends DomainException {

    public AccountException(String message) { super(message); }
}

