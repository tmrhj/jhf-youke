package com.jhf.youke.wechat.domain.exception;


import com.jhf.youke.core.exception.DomainException;

/**
 * @author RHJ
 */
public class UserException extends DomainException {

    public UserException(String message) { super(message); }
}

