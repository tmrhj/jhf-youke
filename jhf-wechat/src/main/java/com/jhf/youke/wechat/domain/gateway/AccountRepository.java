package com.jhf.youke.wechat.domain.gateway;


import com.jhf.youke.wechat.domain.model.po.AccountPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * @author RHJ
 */
public interface AccountRepository extends Repository<AccountPo> {


}

