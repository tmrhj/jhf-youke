package com.jhf.youke.wechat.domain.gateway;


import com.jhf.youke.wechat.domain.model.po.UserPo;
import com.jhf.youke.core.ddd.Repository;

/**
 * 用户存储库
 *
 * @author RHJ
 * @date 2022/11/17
 */
public interface UserRepository extends Repository<UserPo> {


    /**
     * 通过开放id
     *
     * @param openId 开放id
     * @return {@link UserPo}
     */
    UserPo getByOpenId(String openId);
}

