package com.jhf.youke.wechat.domain.model.Do;

import com.jhf.youke.wechat.domain.exception.AccountException;

import com.jhf.youke.core.ddd.BaseDoEntity;
import lombok.Data;
import java.util.Objects;

/**
 * @author RHJ
 */
@Data
public class AccountDo extends BaseDoEntity {

  private static final long serialVersionUID = 453707376080262967L;

    /** 单位ID **/
    private Long companyId;

    /** 公众号名 **/
    private String name;

    /** 公众号编号 **/
    private String code;

    /** 公众号类型 **/
    private Integer type;

    /** Token **/
    private String token;

    /** 公众号APPID **/
    private String appId;

    /** 公众帐号APPSECRET **/
    private String appSecret;

    /** 公众帐号原始ID **/
    private String original;

    /** 证书地址 **/
    private String certPath;

    /** 支付通知URL **/
    private String notifyUrl;

    /** 授权回调URL **/
    private String callbackUrl;

    /** 商户号ID **/
    private String mchId;

    /** 支付KEY **/
    private String paternerKey;

    /** 商户证书序列号 **/
    private String serialNumber;

    /** 私钥证书路径 **/
    private String paternerKeyPath;

    /** 微信支付V3密钥 **/
    private String apiV3Key;

    /** 公众号描述 **/
    private String description;

    /** 根据v3秘钥生成的平台证书，方便验签 **/
    private String platformCertPath;


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new AccountException(errorMessage);
        }
        return obj;
    }

    private AccountDo validateNull(AccountDo accountDo){
          /** 可使用链式法则进行为空检查 **/
          accountDo.
          requireNonNull(accountDo, accountDo.getRemark(),"不能为NULL");
                
        return accountDo;
    }


}


