package com.jhf.youke.wechat.domain.model.Do;

import com.jhf.youke.core.ddd.BaseDoEntity;
import com.jhf.youke.core.utils.StringUtils;
import com.jhf.youke.wechat.domain.exception.UserException;
import lombok.Data;

import java.util.Map;
import java.util.Objects;


/**
 * @author RHJ
 */
@Data
public class UserDo extends BaseDoEntity {

  private static final long serialVersionUID = -96354157307606248L;

    /** 用户所属账号id  **/
    private Long accountId;

    /** 用户唯一标识  **/
    private String openId;

    /** 手机号码  **/
    private String phone;

    /** 用户昵称  **/
    private String nickName;

    private Long userId;

    private Long companyId;

    /** 头像  **/
    private String avatarUrl;

    /** 性别 0未知 1男性 2女性  **/
    private Integer gender;

    /** 用户所在国家  **/
    private String country;

    /** 用户所在省份  **/
    private String province;

    /** 用户所在城市  **/
    private String city;

    public UserDo(){

    }

    public UserDo(Map<String, Object> map) {
      this.nickName = StringUtils.chgNull(map.get("nickName"));
      this.userId = StringUtils.toLong(StringUtils.chgZero(map.get("userId")));
      this.companyId = StringUtils.toLong(StringUtils.chgZero(map.get("companyId")));
      this.openId = StringUtils.chgNull(map.get("openId"));
      this.phone = StringUtils.chgNull(map.get("phone"));
      this.gender = StringUtils.toInteger(StringUtils.chgZero(map.get("sex")));
      this.country = StringUtils.chgNull(map.get("country"));
      this.province = StringUtils.chgNull(map.get("province"));
      this.city = StringUtils.chgNull(map.get("city"));
      this.phone = StringUtils.chgNull(map.get("phone"));

    }


    public <T> T requireNonNull(T obj, Object column, String errorMessage) {
        if (Objects.isNull(column)) {
            throw new UserException(errorMessage);
        }
        return obj;
    }

    private UserDo validateNull(UserDo userDo){
          // 可使用链式法则进行为空检查
          userDo.
          requireNonNull(userDo, userDo.getRemark(),"不能为NULL");
                
        return userDo;
    }


}


