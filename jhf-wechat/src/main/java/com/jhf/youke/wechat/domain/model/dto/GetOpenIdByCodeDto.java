package com.jhf.youke.wechat.domain.model.dto;


import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @author RHJ
 */
@Data
public class GetOpenIdByCodeDto extends BaseDtoEntity {

    private static final long serialVersionUID = 851338877485453821L;

    @NotBlank(message = "不能为空")
    @ApiModelProperty(value = "前端授权获取的code")
    private String code;

}

