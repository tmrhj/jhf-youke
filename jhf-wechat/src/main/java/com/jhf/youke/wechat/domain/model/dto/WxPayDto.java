package com.jhf.youke.wechat.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * description:
 * date: 2022/10/12 10:32
 * @author: cyx
 */
@Data
public class WxPayDto extends BaseDtoEntity {
    private static final long serialVersionUID = 2052700993229393436L;

    @NotBlank(message = "订单id不能为空")
    @ApiModelProperty(value = "订单id")
    private String outTradeNo;

    @NotNull(message = "支付金额不能为空")
    @ApiModelProperty(value = "支付金额")
    private BigDecimal fee;

    @NotBlank(message = "openid不能为空")
    @ApiModelProperty(value = "用户标识")
    private String openId;

    @NotBlank(message = "商品描述不能为空")
    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "附加数据")
    private String attach;


}
