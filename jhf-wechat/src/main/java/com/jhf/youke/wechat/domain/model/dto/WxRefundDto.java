package com.jhf.youke.wechat.domain.model.dto;

import com.jhf.youke.core.ddd.BaseDtoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * description:
 * date: 2022/10/12 11:19
 * @author: cyx
 */
@Data
public class WxRefundDto extends BaseDtoEntity {
    private static final long serialVersionUID = -7462675220186694739L;

    @ApiModelProperty(value = "订单id")
    private String outTradeNo;

    @ApiModelProperty(value = "微信支付订单号")
    private String transactionId;

    @NotNull(message = "支付金额不能为空")
    @ApiModelProperty(value = "支付金额")
    private BigDecimal fee;

    @NotBlank(message = "退款原因不能不空")
    @ApiModelProperty(value = "退款原因")
    private String reason;

    @ApiModelProperty(value = "商品编码")
    private String merchantGoodsId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;
}
