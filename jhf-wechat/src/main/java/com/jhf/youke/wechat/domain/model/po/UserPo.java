package com.jhf.youke.wechat.domain.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jhf.youke.core.ddd.BasePoEntity;
import lombok.Data;
import lombok.experimental.Accessors;



/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
@TableName(value = "wx_user")
public class UserPo extends BasePoEntity {

    private static final long serialVersionUID = -46292953123710436L;

    /** 用户所属账号id  **/
    private Long accountId;

    /** 用户唯一标识  **/
    private String openId;

    /** 手机号码  **/
    private String phone;

    /** 用户昵称  **/
    private String nickName;

    /** 头像  **/
    private String avatarUrl;

    private  Long  userId;

    private Long companyId;

    /** 性别 0未知 1男性 2女性  **/
    private Integer gender;

    /** 用户所在国家  **/
    private String country;

    /** 用户所在省份  **/
    private String province;

    /** 用户所在城市  **/
    private String city;



}


