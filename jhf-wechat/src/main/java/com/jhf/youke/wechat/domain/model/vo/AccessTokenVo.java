package com.jhf.youke.wechat.domain.model.vo;


import lombok.Data;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class AccessTokenVo implements Serializable {

	/**
	 * 接口访问凭证
 	 */
	private String accessToken;

	/**
	 * 凭证有效期，单位：秒
 	 */
	private long expiresIn;

	/**
	 * 创建时间，单位：秒 ，用于判断是否过期
	 */
	private long createTime;

	/**
	 * 错误编码
	 */
	private Integer errcode;

	/**
	 * 错误消息
	 */
	private String errmsg;

}
