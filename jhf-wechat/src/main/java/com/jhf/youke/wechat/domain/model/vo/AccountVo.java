package com.jhf.youke.wechat.domain.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhf.youke.core.ddd.BaseVoEntity;
import com.jhf.youke.core.utils.JacksonStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author RHJ
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("serial")
public class AccountVo extends BaseVoEntity {

    private static final long serialVersionUID = 927899292652402695L;

    @ApiModelProperty(value = "单位ID")
    @JsonSerialize(using = JacksonStringSerializer.class)
    private Long companyId;

    @ApiModelProperty(value = "公众号名")
    private String name;

    @ApiModelProperty(value = "公众号编号")
    private String code;

    @ApiModelProperty(value = "公众号类型")
    private Integer type;

    @ApiModelProperty(value = "Token")
    private String token;

    @ApiModelProperty(value = "公众号APPID")
    private String appId;

    @ApiModelProperty(value = "公众帐号APPSECRET")
    private String appSecret;

    @ApiModelProperty(value = "公众帐号原始ID")
    private String original;

    @ApiModelProperty(value = "证书地址")
    private String certPath;

    @ApiModelProperty(value = "支付通知URL")
    private String notifyUrl;

    @ApiModelProperty(value = "授权回调URL")
    private String callbackUrl;

    @ApiModelProperty(value = "商户号ID")
    private String mchId;

    @ApiModelProperty(value = "支付KEY")
    private String paternerKey;

    @ApiModelProperty(value = "商户证书序列号")
    private String serialNumber;

    @ApiModelProperty(value = "私钥证书路径")
    private String paternerKeyPath;

    @ApiModelProperty(value = "微信支付V3密钥")
    private String apiV3Key;

    @ApiModelProperty(value = "公众号描述")
    private String description;

    @ApiModelProperty(value = "根据v3秘钥生成的平台证书，方便验签")
    private String platformCertPath;

}


