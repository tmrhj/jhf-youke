package com.jhf.youke.wechat.domain.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class UserInfoVo implements Serializable {

    private static final long serialVersionUID = 851338877485453821L;

    private String openid;

    private String nickname;

    private Integer sex;

    private String headimgurl;

    private String accessToken;
}
