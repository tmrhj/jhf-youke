package com.jhf.youke.wechat.domain.service;

import com.jhf.youke.wechat.domain.converter.UserConverter;
import com.jhf.youke.wechat.domain.model.Do.UserDo;
import com.jhf.youke.wechat.domain.gateway.UserRepository;
import com.jhf.youke.wechat.domain.model.po.UserPo;
import com.jhf.youke.wechat.domain.model.vo.UserVo;
import com.jhf.youke.core.ddd.AbstractDomainService;
import com.jhf.youke.core.entity.PageQuery;
import com.jhf.youke.core.entity.Pagination;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


/**
 * @author RHJ
 */
@Service
public class UserService extends AbstractDomainService<UserRepository, UserDo, UserVo> {


    @Resource
    UserConverter userConverter;

    @Override
    public boolean update(UserDo entity) {
        UserPo userPo = userConverter.do2Po(entity);
        userPo.preUpdate();
        return repository.update(userPo);
    }

    @Override
    public boolean updateBatch(List<UserDo> doList) {
        List<UserPo> poList = userConverter.do2PoList(doList);
        return repository.updateBatch(poList);
    }

    @Override
    public boolean delete(UserDo entity) {
        UserPo userPo = userConverter.do2Po(entity);
        return repository.delete(userPo);
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        return repository.deleteBatch(idList);
    }

    @Override
    public boolean insert(UserDo entity) {
        UserPo userPo = userConverter.do2Po(entity);
        userPo.preInsert();
        return repository.insert(userPo);
    }

    @Override
    public boolean insertBatch(List<UserDo> doList) {
        List<UserPo> poList = userConverter.do2PoList(doList);
        poList = UserPo.getInsertListId(poList);
        return repository.insertBatch(poList);
    }

    @Override
    public Optional<UserVo> findById(Long id) {
        Optional<UserPo> userPo =  repository.findById(id);
        UserVo userVo = userConverter.po2Vo(userPo.orElse(new UserPo()));
        return Optional.ofNullable(userVo);
        
    }

    @Override
    public boolean remove(Long id) {
        return repository.remove(id);
    }

    @Override
    public boolean removeBatch(List<Long> idList) {
        return repository.removeBatch(idList);
    }

    @Override
    public List<UserVo> findAllMatching(UserDo entity) {
        UserPo userPo = userConverter.do2Po(entity);
        List<UserPo>userPoList =  repository.findAllMatching(userPo);
        return userConverter.po2VoList(userPoList);
    }


    @Override
    public Pagination<UserVo> selectPage(UserDo entity){
        UserPo userPo = userConverter.do2Po(entity);
        PageQuery<UserPo> pageQuery = new PageQuery<>(userPo,entity.getCurrentPage(), entity.getPageSize(), entity.getQuerySort());
        Pagination<UserPo> pagination = repository.selectPage(pageQuery);
        return new Pagination<>(pagination.getPageNum(),pagination.getPageSize(),pagination.getTotalSize(),
                userConverter.po2VoList(pagination.getList()));
    }


    public void create(UserDo userDo) {
        UserPo userPo = repository.getByOpenId(userDo.getOpenId());
        if(userPo != null){
            return;
        }else {
            insert(userDo);
        }
    }

}

