package com.jhf.youke.wechat.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhf.youke.wechat.domain.model.po.UserPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 用户映射器
 *
 * @author RHJ
 * @date 2022/11/17
 */
@Mapper
public interface UserMapper  extends BaseMapper<UserPo> {

    /**
     * 删除
     *
     * @param id id
     * @return int
     */
    @Update("update wx_user set del_flag ='1' where id =#{id}")
    int remove(@Param("id") Long id);

    /**
     * 删除批处理
     *
     * @param ids id
     * @return int
     */
    @Update("update wx_user set del_flag = '1' where id in (${ids}) ")
    int removeBatch(String ids);

    /**
     * 通过开放id
     *
     * @param openId 开放id
     * @return {@link UserPo}
     */
    UserPo getByOpenId(@Param("openId")String openId);
}

